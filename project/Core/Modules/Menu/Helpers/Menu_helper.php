<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Menu_helper
 *
 *
 *
 * @version 1.0.0
 */
class Menu_helper
{
    /**
     * @method	Get_admin_mail_sent_status_change_url
     * @access	public
     * @desc    Create and returns the admin sent status change url for selected mail
     * @author	Cousin Béla
     *
     * @param   object                      $mail                      - the mail object to create URL
     *
     * @version	1.0.0
     * @return  string
     */
    /*public static function Get_admin_mail_sent_status_change_url( & $mail )
    {
        return MY_Url_helper::Get_admin_base_url()."mails/ajax/change_sent_status/"."?id=".$mail->id;
    }*/
}

/* End of file Menu_helper.php */
/* Location: ./Core/Modules/Menus/Helpers/ */