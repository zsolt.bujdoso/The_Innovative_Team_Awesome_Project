<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * Class Admin_controller
 *
 * @version     1.0.0
 */
class Admin_controller extends MY_Controller
{
    public function Action_index()
    {
        $this->Load_quick_lists();

        $this->Load_view( "//menu/admin/index" );
    }

    private function Load_quick_lists()
    {
        foreach( $this->data["modules"]->Get_result() as $module )
        {
            if( ! $this->Has_to_show_on_quick_listing( $module ) )
            {
                continue;
            }

            $this->Set_list_data_for_module( $module );
        }
    }

    public function Has_to_show_on_quick_listing( & $module )
    {
        if( ! $module->show_in_menu )
        {
            return FALSE;
        }

        if( ! $module->can_be_shown_in_quick_access )
        {
            return FALSE;
        }

        return FALSE;
    }

    private function Set_list_data_for_module( & $module )
    {
        $class_name = ucfirst( rtrim( $module->link, "s" ) ) . "_helper";
        if( ! App()->Has_class( $class_name ) )
        {
            Logger_helper::Error( "Class missing for quick listing: ".$class_name );
            return;
        }
        if( ! method_exists( $class_name, "Get_quick_list_for_admin" ) )
        {
            Logger_helper::Error( "- Get_quick_list_for_admin function missing from ".$class_name );
            return;
        }
        
        // TODO: Cousin, az alább beírt számot (10) dinamikussá kell tenni
        $this->data["last_".$module->link] = $class_name::Get_quick_list_for_admin( 10 );
    }
}

/* End of file Admin_controller.php */
/* Location: ./Core/Modules/Menu/Controllers/ */