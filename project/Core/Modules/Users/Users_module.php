<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Users_module
 *
 * @version     1.0.0
 */
class Users_module extends MModule
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Set_import()
    {
        return array();
    }

    public function Init()
    {
        parent::Init();
    }
}

/* End of file Users_module.php */
/* Location: ./Core/Modules/Users/ */