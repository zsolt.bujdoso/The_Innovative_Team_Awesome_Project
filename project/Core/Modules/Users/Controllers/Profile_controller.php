<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * Class Profile_controller
 *
 * @version     1.0.0
 */
class Profile_controller extends MY_Controller
{
    public function Action_index()
    {
        $this->Load_view( "//users/profile/index" );
    }

    public function Action_edit()
    {
        $this->Load_view( "//users/profile/edit" );
    }
}

/* End of file Profile_controller.php */
/* Location: ./Core/Modules/Users/Controllers/ */