<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MY_Url_helper
 *
 *
 * @version     1.0.0
 */
class MY_Url_helper extends Url_helper
{
    public static $admin_category_url       = "category/";
    public static $admin_delete_url         = "delete/";
    public static $admin_edit_url           = "item/";
    public static $admin_view_url           = "view/";
    public static $admin_status_change_url  = "ajax/change_enabled_status/";

    public static function Get_link_to_url_for_type( $item_type, $item_id, $language_code )
    {
        $model_name     = ucfirst( strtolower( $item_type ) );
        $model_name     .= "_model";
        $item_details   = $model_name::Get_model()->Get_by_item_id_and_language( $item_id, $language_code );

        return self::Get_link_by_type( $item_type, $item_details );
    }

    public static function Get_link_by_type( $item_type, & $item_details )
    {
        /* todo: figyelembe kell venni a domaint is */

        if( ! $item_details )
        {
            if( strpos( $item_type, "_categories" ) === FALSE )
            {
                return $item_type;
            }

            return "";
        }

        return Item_helper::Get_url_by_item_type( $item_type, $item_details );
    }

    /**
     * @method  Active_admin_module_url
     * @access  public static
     * @desc    This method return the active admin module url
     *          ex: http://tizbani.no-ip.org/admin/products/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Active_admin_module_url()
    {
        return self::Site_url()."admin/".App()->router->Get_module()."/";
    }

    /**
     * @method  Get_admin_base_url
     * @access  public static
     * @desc    This method return the admin url
     *          ex: http://tizbani.no-ip.org/admin/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_admin_base_url()
    {
        return self::Base_url().App()->lang->Get_language_code()."/admin/";
    }

    /**
     * @method  Site_url
     * @access  public static
     * @desc    This method return the site url with language code
     *          ex: http://tizbani.no-ip.org/hu/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Site_url()
    {
        return self::Base_url().App()->lang->Get_language_code()."/";
    }

    /**
     * @method  Active_module_edit_url
     * @access  public static
     * @desc    This method return the active module edit url
     *          ex: http://tizbani.no-ip.org/admin/products/item/
     * @author  Cousin Bela
     *
     * @param   object                      $object                     - the object to create URL
     *
     * @version 1.0
     * @return  string
     */
    /*public static function Active_module_edit_url( & $object )
    {
        $link = self::Active_admin_module_url().self::$admin_edit_url;

        return self::Add_ids_to_url( $link, $object );
    }*/

    /**
     * @method  Active_module_delete_url
     * @access  public static
     * @desc    This method return the active module delete url
     *          ex: http://tizbani.no-ip.org/admin/products/delete/
     * @author  Cousin Bela
     *
     * @param   object                      $object                     - the object to create URL
     *
     * @version 1.0
     * @return  string
     */
    /*public static function Active_module_delete_url( & $object )
    {
        $link = self::Active_admin_module_url().self::$admin_delete_url;

        return self::Add_ids_to_url( $link, $object );
    }*/

    /**
     * @method  Active_module_category_delete_url
     * @access  public static
     * @desc    This method return the active module delete url for categories
     *          ex: http://tizbani.no-ip.org/admin/products/category/delete/
     * @author  Cousin Bela
     *
     * @param   object                      $object                     - the object to create URL
     *
     * @version 1.0
     * @return  string
     */
    /*public static function Active_module_category_delete_url( & $object )
    {
        $link = self::Active_admin_module_url().self::$admin_category_url.self::$admin_delete_url;

        return self::Add_ids_to_url( $link, $object );
    }*/

    /**
     * @method  Active_module_category_edit_url
     * @access  public static
     * @desc    This method return the active module edit url for categories
     *          ex: http://tizbani.no-ip.org/admin/products/category/item/
     * @author  Cousin Bela
     *
     * @param   object                      $object                     - the object to create URL
     *
     * @version 1.0
     * @return  string
     */
    /*public static function Active_module_category_edit_url( & $object )
    {
        $link = self::Active_admin_module_url().self::$admin_category_url.self::$admin_edit_url;

        return self::Add_ids_to_url( $link, $object );
    }*/

    /**
     * @method  Active_module_status_change_url
     * @access  public static
     * @desc    This method return the active module status change url
     *          ex: http://tizbani.no-ip.org/products/admin_ajax/change_enabled_status/
     * @author  Cousin Bela
     *
     * @param   object                      $object                     - the object to create URL
     *
     * @version 1.0
     * @return  string
     */
    /*public static function Active_module_status_change_url( & $object )
    {
        $link = self::Active_admin_module_url().self::$admin_status_change_url;

        return self::Add_ids_to_url( $link, $object );
    }*/

    /**
     * @method  Get_module_admin_view_url
     * @access  public static
     * @desc    This method return the module view url for admin side, by module
     *          ex: http://tizbani.no-ip.org/admin/products/view/
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $item                       - the item object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_module_admin_view_url( $module, & $item )
    {
        $link = self::Get_admin_base_url().$module."/".self::$admin_view_url;

        return self::Add_ids_to_url( $link, $item );
    }

    public static function Add_ids_to_url( $link, & $object )
    {
        $link .= "?id=".Variable_helper::Has_value( $object, "id" );

        if( Variable_helper::Has_value( $object, "details_id" ) )
        {
            $link .= "&details_id=".Variable_helper::Has_value( $object, "details_id" );
        }

        return $link;
    }

    /**
     * @method  Get_module_edit_url
     * @access  public static
     * @desc    This method return the module edit url by module
     *          ex: http://tizbani.no-ip.org/admin/products/item/
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $item                       - the item object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_module_edit_url( $module, & $item )
    {
        $link = self::Site_url().$module."/".self::$admin_edit_url;

        return self::Add_ids_to_edit_url( $link, $item );
    }

    /**
     * @method  Get_module_admin_edit_url
     * @access  public static
     * @desc    This method return the module edit url by module
     *          ex: http://tizbani.no-ip.org/admin/products/item/
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $item                       - the item object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_module_admin_edit_url( $module, & $item )
    {
        $link = self::Get_admin_base_url().$module."/".self::$admin_edit_url;

        return self::Add_ids_to_edit_url( $link, $item );
    }

    public static function Add_ids_to_edit_url( $link, & $object )
    {
        $link .= "?id=".$object->id;

        if( Variable_helper::Has_value( $object, "language_code" ) )
        {
            $link .= "#details_lang_".Variable_helper::Has_value( $object, "language_code" );
        }

        return $link;
    }

    /**
     * @method  Get_module_delete_url
     * @access  public static
     * @desc    This method return the module delete url by module
     *          ex: http://tizbani.no-ip.org/admin/products/delete/
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $item                       - the item object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_module_delete_url( $module, $item )
    {
        $link = self::Get_admin_base_url().$module."/".self::$admin_delete_url;

        return self::Add_ids_to_url( $link, $item );
    }

    /**
     * @method  Get_module_status_change_url
     * @access  public static
     * @desc    This method return the module status change url by module
     *          ex: http://tizbani.no-ip.org/admin/products/ajax/change_enabled_status/
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $item                       - the item object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_module_status_change_url( $module, & $item )
    {
        $link = self::Get_admin_base_url().$module."/".self::$admin_status_change_url;

        return self::Add_ids_to_url( $link, $item );
    }

    /**
     * @method  Get_module_category_edit_url
     * @access  public static
     * @desc    This method return the module edit url for category by module
     *          ex: http://tizbani.no-ip.org/admin/products/category/item/
     * @author  Attila
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $category                   - the category object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_module_category_edit_url( $module, & $category )
    {
        $link = self::Get_admin_category_url( $module ).self::$admin_edit_url;
        $link = self::Add_ids_to_url( $link, $category );

        if( Variable_helper::Has_value( $category, "language_code" ) )
        {
            $link .= "#details_lang_".Variable_helper::Has_value( $category, "language_code" );
        }

        return $link;
    }

    /**
     * @method  Get_admin_category_url
     * @access  public static
     * @desc    This method return the active module category url
     *          ex: http://tizbani.no-ip.org/admin/products/category
     * @author  Attila
     *
     * @param   string                      $module                     - the module name to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_admin_category_url( $module )
    {
        $link = self::Get_admin_base_url().$module."/".self::$admin_category_url;

        return $link;
    }

    /**
     * @method  Get_module_category_delete_url
     * @access  public static
     * @desc    This method return the module delete url for category by module
     *          ex: http://tizbani.no-ip.org/admin/products/category/delete/
     * @author  Attila
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $category                   - the category object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_module_category_delete_url( $module, & $category )
    {
        $link = self::Get_admin_category_url( $module ).self::$admin_delete_url;

        return self::Add_ids_to_url( $link, $category );
    }

    /**
     * @method  Get_active_url_without_language
     * @access  public static
     * @desc    This method returns an URL part with parameters from actual URL without language code
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_active_url_without_language()
    {
        $link           = Server_helper::Get( "QUERY_STRING" );
        // replace actual language code in link, ex: hu/admin/menu will be admin/menu
        $link           = preg_replace( "/^(".App()->lang->Get_language_code()."\/)(.*)/", "$2", $link );
        $query_string   = explode( "&", $link, 2 );

        // add the query string parameters
        $link           = trim( $query_string[0], "/" )."/".( isset( $query_string[1] ) ? "?".$query_string[1] : "" );

        return $link;
    }

    /**
     * @method  Get_active_url_without_language_and_parameters
     * @access  public static
     * @desc    This method returns an URL part with parameters from actual URL without language code
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_active_url_without_language_and_parameters()
    {
        $link           = Server_helper::Get( "QUERY_STRING" );
        // replace actual language code in link, ex: hu/admin/menu will be admin/menu
        $link           = preg_replace( "/^(".App()->lang->Get_language_code()."\/)(.*)/", "$2", $link );
        $query_string   = explode( "&", $link, 2 );

        // add the query string parameters
        $link           = trim( $query_string[0], "/" )."/";

        return $link;
    }

    /**
     * @method  Get_last_listing_url
     * @access  public static
     * @desc    This method returns an URL for last listing items on admin side, saved in Admin_Side_Controller
     * @author  Cousin Bela
     *
     * @param   string                      $default_url                - the default URL, if link wasn't set
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_last_listing_url( $default_url )
    {
        $url = MY_Url_helper::Site_url();

        if( ! App()->session->Has( "admin_last_listing_url" ) )
        {
            return $url.$default_url;
        }

        $admin_last_listing_url = App()->session->Get( "admin_last_listing_url" );
        $selected_module        = App()->controller->selected_module;

        if( ! empty( $admin_last_listing_url[$selected_module] ) )
        {
            return $url.$admin_last_listing_url[$selected_module];
        }

        return $url.$default_url;
    }

    /**
     * @method  Get_url
     * @access  public static
     * @desc    This method returns the view url for selected item on front side
     *          ex: http://tizbani.no-ip.org/product/view/objectname/objectcode
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $item                       - the item object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_url( $module, & $item )
    {
        /* todo: figyelembe kell venni a domaint is azoknál az oldalaknál ami a frontra visz */

        $url = MY_Url_helper::Base_url();

        if( ! empty( $item->language_code ) )
        {
            $url .= $item->language_code."/";
        }
        else
        {
            $url .= App()->lang->Get_language_code()."/";
        }

        // replace api parameters in link
        if( defined( "IS_API" ) && IS_API == TRUE )
        {
            $url = str_replace( "api/v".App()->config->api_version."/", "", $url );
        }

        if( empty( $item ) || empty( $item->name ) )
        {
            return $url;
        }

        // must delete the last character from module name, ex: products = product
        $module = substr_replace( $module, "", -1 );
        $url    .= $module."/view/".String_helper::Encode_url( $item->name )."/".$item->code;

        return $url;
    }

    /**
     * @method  Get_category_url
     * @access  public static
     * @desc    This method returns the view url for selected category item on front side
     *          ex: http://tizbani.no-ip.org/product/category/objectcode
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $category                   - the category object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_category_url( $module, & $category )
    {
        $url = MY_Url_helper::Base_url();

        if( ! empty( $category->language_code ) )
        {
            $url .= $category->language_code."/";
        }

        // replace api parameters in link
        if( defined( "IS_API" ) && IS_API == TRUE )
        {
            $url = str_replace( "api/v".App()->config->api_version."/", "", $url );
        }

        if( empty( $category ) )
        {
            return $url;
        }

        // must delete the last character from module name, ex: products = product
        $module = substr_replace( $module, "", -1 );

        $url .= $module."/category/".$category->code;

        return $url;
    }

    /**
     * @method  Get_admin_preview_url
     * @access  public static
     * @desc    This method return the module view url
     *          ex: http://tizbani.no-ip.org/admin/products/preview/?id=1
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $item                       - the item object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_admin_preview_url( $module, & $item )
    {
        $url = self::Get_admin_base_url().$module."/preview/";

        return self::Add_ids_to_url( $url, $item );
    }

    /**
     * @method  Get_admin_category_listing_url
     * @access  public static
     * @desc    This method return the view url for selected category on admin side
     *          ex: http://tizbani.no-ip.org/admin/products/category/index/?id=1
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - the module name to create URL
     * @param   object                      $category                   - the category object to create URL
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_admin_category_listing_url( $module, & $category )
    {
        $url = self::Get_admin_category_url( $module )."index/";

        return self::Add_ids_to_url( $url, $category );
    }

    /**
     * @method  Get_active_url
     * @access  public static
     * @desc    This method return the active url where are we now
     *          ex: http://tizbani.no-ip.org/admin/settings/index/?page=2
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_active_url()
    {
        return App()->lang->Get_language_code()."/".self::Get_active_url_without_language();
    }
}

/* End of file MY_Url_helper.php */
/* Location: ./Core/Helpers/ */