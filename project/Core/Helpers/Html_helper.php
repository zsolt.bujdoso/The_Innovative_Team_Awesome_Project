<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Html_helper
 *
 * This class helps us to get html elements printed in templates, exemple: javascript, css
 *
 * @version 1.0.0
 */
class Html_helper
{
    /**
     * @method	Js
     * @access	public
     * @desc    Get a javascript html element with name specified from themes or module
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - the name of the file with or without path to get from javascripts
     * @param   bool                        $defer                      - defer the javascript on load or not, load async or normally
     * @param   string                      $module                     - name of the module to get from
     * @param   bool                        $static                     - add the static folder to path or not
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Js( $file_name, $defer = TRUE, $module = "", $static = TRUE )
    {
        $url = self::Calculate_path( $file_name, "js", $module, $static );
        $url = str_replace( "\\", "/", $url );

        return '<script type="text/javascript" src="'.$url.'" '.( $defer ? "defer" : "" ).'></script>';
    }

    /**
     * @method	Css
     * @access	public
     * @desc    Get a stylesheet html element with name specified from themes or module
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - the name of the file with or without path to get from javascripts
     * @param   string                      $module                     - name of the module to get from
     * @param   bool                        $static                     - add the static folder to path or not
     * @param   string                      $media                      - set media info to know when the browser load this css
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Css( $file_name, $module = "", $static = TRUE, $media = "none" )
    {
        $url = self::Calculate_path( $file_name, "css", $module, $static );
        $url = str_replace( "\\", "/", $url );

        return '
            <link rel="stylesheet" href="'.$url.'" media="'.$media.'" '.( $media == "none" ? 'onload="if(media!=\'all\')media=\'all\'"' : '' ).'/>
        ';
    }

    /**
     * @method	Calculate_path
     * @access	public
     * @desc    Calculate the path where to include from the item specified by name
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - the name of the file with or without path to get from javascripts
     * @param   string                      $type                       - type of item to calculate
     * @param   string                      $module                     - name of the module to get from
     * @param   bool                        $static                     - add the static folder to path or not
     *
     * @version	1.0.0
     * @return  string
     */
    private static function Calculate_path( $file_name, $type = "", $module = "", $static = TRUE )
    {
        $file_location = "assets/".$type."/".( $static ? "static/" : "" );

        // replace the first slashes with nothing
        $file_name = preg_replace( "/^\/{1,}(.*)/", "$1", $file_name );
        // replace the extension with nothing, if it is set
        $file_name = preg_replace( "/(.*)\.".$type."$/", "$1", $file_name );

        // If we are in a module
        if( App()->router->Get_module() )
        {
            $path = MODULEPATH.ucfirst( App()->router->Get_module() ).DIRECTORY_SEPARATOR;
            $url  = Url_helper::Base_url().str_replace( BASEPATH, "", MODULEPATH ).
                        ucfirst( App()->router->Get_module() )."/";

            if( file_exists( $path.$file_location.$file_name.".".$type ) )
            {
                return $url.$file_location.$file_name.".".$type;
            }
        }

        if( ! empty( $module ) )
        {
            $module = ucfirst( $module );
            $path   = MODULEPATH.$module.DIRECTORY_SEPARATOR;
            $url    = Url_helper::Base_url().str_replace( BASEPATH, "", MODULEPATH ).$module."/";

            if( file_exists( $path.$file_location.$file_name.".".$type ) )
            {
                return $url.$file_location.$file_name.".".$type;
            }
        }

        $path = App()->theme->Get_path();
        $url  = MY_Url_helper::Theme_url();

        if( file_exists( $path.$file_location.$file_name.".".$type ) )
        {
            return self::Create_minified_and_return_path(
                $type,
                $url,
                $path,
                $file_location.$file_name.".".$type
            );
        }

        $url  = MY_Url_helper::Base_url();

        // if file exists, return the url
        return self::Create_minified_and_return_path(
            $type,
            $url,
            BASEPATH,
            $file_location.$file_name.".".$type
        );
    }

    private static function Create_minified_and_return_path( $type, $url, $path, $file_name_with_location )
    {
        $content        = "";
        if( file_exists( $path.$file_name_with_location ) )
        {
            $content    = file_get_contents( $path . $file_name_with_location );
        }

        $theme_path     = str_replace( BASEPATH, "", App()->theme->Get_path() );
        $theme_path     = str_replace( "Themes".DIRECTORY_SEPARATOR, "", $theme_path );

        $file_location  = dirname( $file_name_with_location );
        $cache_path     = "Runtime/".$theme_path;
        $cache_file_path= BASEPATH.$cache_path.$file_location;

        if( ! file_exists( $cache_file_path ) || ! is_dir( $cache_file_path ) )
        {
            mkdir( $cache_file_path, 0755, TRUE );
            //chmod( $cache_file_path, 0777 );
        }

        if( $path == BASEPATH )
        {
            return $url.$file_name_with_location;
        }

        if( self::Cache_file_exists( $type, BASEPATH.$cache_path.$file_name_with_location ) )
        {
            return MY_Url_helper::Base_url().$cache_path.$file_name_with_location;
        }

        if( self::Has_to_minify( $file_name_with_location ) )
        {
            if( file_exists( $cache_path.$file_name_with_location ) )
            {
                try
                {
                    @unlink( $cache_path . $file_name_with_location );
                }
                catch( MPHP_exception $e )
                {
                }
            }

            try
            {
                $content = MMinifier::Minify( $content, $type );
                file_put_contents( $cache_path.$file_name_with_location, $content, FILE_APPEND );

                return MY_Url_helper::Base_url().$cache_path.$file_name_with_location;
            }
            catch( MPHP_exception $e) {}
        }

        return $url.$file_name_with_location;
    }

    private static function Cache_file_exists( $type, $file_name_with_path )
    {
        if( ! file_exists( $file_name_with_path ) )
        {
            return FALSE;
        }

        $config = App()->config;

        if( ( $type == "css" && Config_helper::Get( "regenerate_css_cache" ) ) )
        {
            return FALSE;
        }

        if( ( $type == "js" && Config_helper::Get( "regenerate_js_cache" ) ) )
        {
            return FALSE;
        }

        $is_jquery = strpos( $file_name_with_path, "jquery" );
        // If this is a jquery file, check if it is older than 180 day
        if( $is_jquery !== FALSE && filemtime( $file_name_with_path ) < ( time() - 1555200 ) )
        {
            return FALSE;
        }

        $is_ckeditor = strpos( $file_name_with_path, "ckeditor" );
        // If this is a file for ckeditor, check if it is older than 180 day
        if( $is_jquery === FALSE && $is_ckeditor !== FALSE && filemtime( $file_name_with_path ) < ( time() - 1555200 ) )
        {
            return FALSE;
        }

        // Check if file is older than 1 day
        if( $is_jquery === FALSE && $is_ckeditor === FALSE && filemtime( $file_name_with_path ) < ( time() - 86400 ) )
        {
            return FALSE;
        }

        return FALSE;
    }

    private static function Has_to_minify( $file_name_with_location )
    {
        if( strpos( $file_name_with_location, ".min." ) !== FALSE )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Get_css_loader_script
     * @access	public
     * @desc    Get a link to a stylesheet from themes or module
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - the name of the file with or without path to get from javascripts
     * @param   string                      $module                     - name of the module to get from
     * @param   bool                        $static                     - add the static folder to path or not
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_css_loader_script( $file_name, $module = "", $static = TRUE )
    {
        $url = self::Calculate_path( $file_name, "css", $module, $static );
        $url = str_replace( "\\", "/", $url );

        $random_number  = String_helper::Random_string( "alnum", 10 )."_".time();
        $raf            = "raf_".$random_number;
        $callback       = "callback_".$random_number;

        $script = "
            <script>
                var ".$callback." = function() {
                    var l = document.createElement('link'); l.rel = 'stylesheet';
                    l.href = '".$url."';
                    var h = document.getElementsByTagName('title')[0];
                    h.parentNode.insertBefore(l, h);
                };
                var ".$raf." = ( requestAnimationFrame != null && requestAnimationFrame ) 
                                || ( mozRequestAnimationFrame != null && mozRequestAnimationFrame )
                                || ( webkitRequestAnimationFrame != null && webkitRequestAnimationFrame )
                                || ( msRequestAnimationFrame != null && msRequestAnimationFrame );
                if( ".$raf." )
                    ".$raf."( ".$callback." );
                else
                window.addEventListener( 'load', ".$callback." );
            </script>
        ";

        return $script;
    }

    public static function Get_options_from_result( & $items, $selected_item_id = null, $translate = TRUE )
    {
        if( empty( $items ) )
        {
            return FALSE;
        }

        $html = "";
        foreach( $items->Get_result() as $item )
        {
            $item_name  = Variable_helper::Has_value( $item, "name" );
            if( $translate && ! empty( $item_name ) )
            {
                $item_name = App()->lang->Get( $item_name );
            }

            $item_id = Variable_helper::Has_value( $item, "id" );
            //echo $item_id." ".$selected_item_id;die;
            $html .= '<option value="'.$item_id.'" '
                        .( $item_id == $selected_item_id ? 'selected="selected"' : "" ). '>'
                        .$item_name
                    .'</option>';
        }

        return $html;
    }

    public static function Get_options_from_result_by_code( & $items, $selected_item_code = null, $translate = TRUE, $name_equal_code = FALSE )
    {
        if( empty( $items ) )
        {
            return FALSE;
        }

        $html = "";
        foreach( $items->Get_result() as $item )
        {
            $item_name = Variable_helper::Has_value( $item, "name" );
            $item_code = Variable_helper::Has_value( $item, "code" );

            if( $name_equal_code )
            {
                $item_name = $item_code;
            }

            if( $translate && ! empty( $item_name ) )
            {
                $item_name = App()->lang->Get( $item_name );
            }

            $html .= '<option value="'.$item_code.'" '
                        .( $item_code == $selected_item_code ? 'selected="selected"' : "" ). '>'
                        .$item_name
                    .'</option>';
        }

        return $html;
    }

    public static function Get_options_from_result_by_value_and_text( & $items, $value_name, $text_names, $selected_item_id = null, $translate = TRUE )
    {
        if( empty( $items ) )
        {
            return FALSE;
        }

        // create an array with text names if isn't already
        if( ! is_array( $text_names ) )
        {
            $text_names = array( $text_names );
        }

        $html = "";
        foreach( $items->Get_result() as $item )
        {
            $item_name = "";

            foreach( $text_names as $text_name )
            {
                $new_name = Variable_helper::Has_value( $item, $text_name );

                if( $translate && ! empty( $item_name ) )
                {
                    $new_name = App()->lang->Get( $item_name );
                }

                if( ! empty( $item_name ) )
                {
                    $item_name .= " - ";
                }

                $item_name .= $new_name;
            }

            $item_value = Variable_helper::Has_value( $item, $value_name );
            $html .= '<option value="'.$item_value.'" '
                        .( $item_value == $selected_item_id ? 'selected="selected"' : "" ). '>'
                        .$item_name
                    .'</option>';
        }

        return $html;
    }

    public static function Get_options_from_array( & $data, $selected = null )
    {
        if( empty( $data ) || ! is_array( $data ) )
        {
            return FALSE;
        }

        $html = "";
        foreach( $data as $value => $name )
        {
            $is_selected    = FALSE;

            // needed when value = 0 or selected value = 0
            if( ( is_numeric( $value ) && is_numeric( $selected ) ) && ( float ) $value == ( float ) $selected )
            {
                $is_selected = TRUE;
            }
            elseif( ( is_numeric( $value ) || is_numeric( $selected ) ) && $value === $selected )
            {
                $is_selected = TRUE;
            }
            elseif( $value === $selected )
            {
                $is_selected = TRUE;
            }
            elseif( ! is_numeric( $value ) && ! is_numeric( $selected ) && $value == $selected )
            {
                $is_selected = TRUE;
            }

            $html .= '<option value="'.$value.'" '.( $is_selected ? 'selected="selected"' : "" ). '>'
                        .$name
                    .'</option>';
        }

        return $html;
    }
}

/* End of file Html_helper.php */
/* Location: ./Core/Helpers/ */