<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Main_languages_model
 *
 * @property    int         $id
 * @property    int         $modified_user_id
 * @property    string      $code
 * @property    string      $name
 * @property    string      $image
 * @property    boolean     $is_enabled_on_front
 * @property    datetime    $insert_date
 * @property    datetime    $modify_date
 * @property    boolean     $is_enabled
 * @property    boolean     $is_deleted
 *
 * @version     1.0.0
 */
class Main_languages_model extends Common_model
{
    public function __construct()
    {
        parent::__construct();

        $this->table_name   = "languages";
        $this->primary_key  = "id";
        $this->alias        = "l.";
    }

    /**
     * @return Main_languages_model
     */
    public static function Get_model()
    {
        return parent::Get_model();
    }

    /*******************************************************************************************************************
     * ADMIN FUNCTIONS
     *******************************************************************************************************************/
    //<editor-fold desc="ADMIN FUNCTIONS">

    /**
     * @method  Get_filters
     * @access  protected
     * @desc    This method create filters for selecting items with details
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  MDB_Filter
     */
    protected function Get_filters( $language_code = "" )
    {
        $filters = $this->Get_filters_for_count( $language_code );

        $filters->Select( "ld.*, ".$this->alias."*" );
        $filters->Select( $this->alias.$this->primary_key, "id" );
        $filters->Select( "ld.id", "details_id" );

        return $filters;
    }

    /**
     * @method  Get_filters_for_count
     * @access  protected
     * @desc    This method create filters for counting items with details
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  MDB_Filter
     */
    protected function Get_filters_for_count( $language_code = "" )
    {
        $filters    = new MDB_Filter();
        $alias      = trim( $this->alias, "." );

        $join_where = "";
        if( ! empty( $language_code ) )
        {
            $join_where = " AND ( ".$alias . "d.language_code = '". $this->Escape( $language_code )."' OR ".$alias . "d.language_code IS NULL )";
        }

        $filters->Alias( trim( $this->alias, "." ) );
        $filters->Join_left(
            "language_details",
            "ld.language_id = ".$this->alias.$this->primary_key." AND ld.is_deleted = 0"
                .$join_where,
            "ld",
            FALSE
        );
        $filters->Where_equal( $this->alias."is_deleted", "0" );

        return $filters;
    }
    //</editor-fold>


    /*******************************************************************************************************************
     * FRONT FUNCTIONS
     *******************************************************************************************************************/
    //<editor-fold desc="FRONT FUNCTIONS">

    /**
     * @method  Get_list_by_language
     * @access  public
     * @desc    Get list of active items by languages, limited with param limit
     * @author  Cousin Bela
     *
     * @param   string                      $language_code              - language code
     * @param   int                         $limit                      - how many course categories to return
     * @param   int                         $limit_from                 - start the list from
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  MDB_Result
     */
    public function Get_list_by_language( $language_code, $limit = 50, $limit_from = 0, $search_filters = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters( $language_code );

        $filters->Where_equal( $alias."d.is_enabled", "1" );
        $this->Process_search( $filters, $search_filters );

        $filters->Order_by( $this->alias."sort_order" );
        $filters->Limit( $limit, $limit_from );

        $this->db->Set_cache_to_file();
        $result = $this->Get_list_by_filters( $filters );
        $this->db->Set_cache_back_to_last_state();

        return $result;
    }

    /**
     * @return MDB_Result
     */
    public function Get_list_where_is_enabled( $limit = 50, $limit_from = 0, $search_filters = null )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( "ld.is_enabled", "1" );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Get_list_by_language_where_is_enabled_on_front
     * @access  public
     * @desc    Get list of active course categories by languages, enabled on front page
     * @author  Cousin Bela
     *
     * @param   string                      $language_code              - language code
     *
     * @version 1.0.0
     * @return  MDB_Result
     */
    public function Get_list_by_language_where_is_enabled_on_front( $language_code )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters( $language_code );

        $filters->Where_equal( $this->alias."is_enabled_on_front", "1" );
        $filters->Where_equal( $alias."d.is_enabled", "1" );
        $filters->Order_by( $this->alias."sort_order" );

        return $this->Get_list_by_filters( $filters );
    }

    public function Get_list_where_is_enabled_on_front()
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( "ld.is_enabled", "1" );
        $filters->Where_equal( $this->alias."is_enabled_on_front", "1" );

        return $this->Get_list_by_filters( $filters );
    }
    //</editor-fold>
}

/* End of file Main_languages_model.php */
/* Location: ./Core/Models/ */