<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Main_users_model
 *
 * @property    int         $id
 * @property    int         $user_id
 * @property    int         $user_type_id
 * @property    int         $invited_user_id
 * @property    int         $modified_user_id
 * @property    int         $domain_id
 * @property    string      $email
 * @property    string      $password
 * @property    string      $first_name
 * @property    string      $last_name
 * @property    string      $gender
 * @property    datetime    $birth_date
 * @property    string      $image
 * @property    string      $language_code
 * @property    string      $last_login_ip
 * @property    boolean     $is_receiving_newsletter
 * @property    datetime    $insert_date
 * @property    datetime    $modify_date
 * @property    datetime    $last_login_date
 * @property    boolean     $is_enabled
 * @property    boolean     $is_deleted
 *
 * @version     1.0.0
 */
class Main_users_model extends Common_model
{
    public function __construct()
    {
        parent::__construct();

        $this->table_name   = "users";
        $this->primary_key  = "id";
        $this->alias        = "u.";
    }

    /**
     * @return Main_users_model
     */
    public static function Get_model()
    {
        return parent::Get_model();
    }
}

/* End of file Main_users_model.php */
/* Location: ./Core/Models/ */