<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );


/**
 * Class Main_themes_model
 *
 * @property    int         $id
 * @property    int         $domain_id
 * @property    int         $modified_user_id
 * @property    string      $name
 * @property    string      $code
 * @property    boolean     $is_for_admin
 * @property    boolean     $is_for_front
 * @property    boolean     $can_be_deleted
 * @property    datetime    $insert_date
 * @property    datetime    $modify_date
 * @property    boolean     $is_default
 * @property    boolean     $is_enabled_on_admin
 * @property    boolean     $is_enabled_on_front
 * @property    boolean     $is_deleted
 *
 */
class Main_themes_model extends Common_model
{
    public function __construct()
    {
        parent::__construct();

        $this->table_name   = "themes";
        $this->primary_key  = "id";
        $this->alias        = "t.";
    }

    /**
     * @return Main_themes_model
     */
    public static function Get_model()
    {
        return parent::Get_model();
    }

    /*******************************************************************************************************************
     * ADMIN FUNCTIONS
     *******************************************************************************************************************/
    //<editor-fold desc="ADMIN FUNCTIONS">

    /**
     * @method  Get_filters
     * @access  protected
     * @desc    This method create filters for selecting items with details
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  MDB_Filter
     */
    protected function Get_filters( $language_code = "" )
    {
        $filters = $this->Get_filters_for_count( $language_code );

        $filters->Select( $this->alias."*" );

        return $filters;
    }

    /**
     * @method  Get_filters_for_count
     * @access  protected
     * @desc    This method create filters for counting items with details
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  MDB_Filter
     */
    protected function Get_filters_for_count( $language_code = "" )
    {
        $filters = new MDB_Filter();

        $filters->Alias( trim( $this->alias, "." ) );
        $filters->Where_equal( $this->alias."is_deleted", "0" );

        return $filters;
    }

    /**
     * @method  Process_search_string
     * @access  protected
     * @desc    This method filters the item(s) by given keyword(s)
     * @author  Cousin Bela
     *
     * @param   MDB_Filter                  $filters                    - the filter for item
     * @param   string                      $search_filters             - string with search filters
     *
     * @version 1.0.0
     */
    protected function Process_search_string( & $filters, $search_filters = null )
    {
        if( ! is_string( $search_filters ) )
        {
            return;
        }

        $filters->Or_like( $this->alias."code", $search_filters );
        $filters->Or_like( $this->alias."name", $search_filters );
    }

    /**
     * @method  Process_search_array
     * @access  protected
     * @desc    This method filters the item(s) by given array
     * @author  Cousin Bela
     *
     * @param   MDB_Filter                  $filters                    - the filter for item
     * @param   array                       $search_filters             - array with search filters
     *
     * @version 1.0.0
     */
    protected function Process_search_array( & $filters, $search_filters = array() )
    {
        if( ! is_array( $search_filters ) )
        {
            return;
        }

        if( ! empty( $search_filters["item_id"] ) )
        {
            $filters->Like( $this->alias.$this->primary_key, $search_filters["item_id"] );
        }

        if( ! empty( $search_filters["code"] ) )
        {
            $filters->Like( $this->alias."code", $search_filters["code"] );
        }

        if( ! empty( $search_filters["name"] ) )
        {
            $filters->Like( $this->alias."name", $search_filters["name"] );
        }

        if( isset( $search_filters["is_enabled_on_front"] ) )
        {
            $filters->Where_equal( $this->alias."is_enabled", $search_filters["is_enabled"] );
        }

        if( isset( $search_filters["is_enabled_on_admin"] ) )
        {
            $filters->Where_equal( $this->alias."is_enabled", $search_filters["is_enabled"] );
        }
    }
    //</editor-fold>


    /*******************************************************************************************************************
     * FRONT FUNCTIONS
     *******************************************************************************************************************/
    //<editor-fold desc="FRONT FUNCTIONS">

    /**
     * @method  Get_by_type
     * @access  public
     * @desc    Get theme details by type
     * @author  Cousin Bela
     *
     * @param   string                      $type                       - type of theme to return, admin or front
     *
     * @version 1.0.0
     * @return  Themes_model
     */
    public function Get_by_type( $type )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal(
            ( $type == "admin" ? $this->alias."is_for_admin" : $this->alias."is_for_front" ),
            "1"
        );

        $filters->Where_equal(
            ( $type == "admin" ? $this->alias."is_enabled_on_admin" : $this->alias."is_enabled_on_front" ),
            "1"
        );

        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_by_primary_key_and_type
     * @access  public
     * @desc    Get theme details by type
     * @author  Cousin Bela
     *
     * @param   int                         $theme_id                   - id of the theme
     * @param   string                      $type                       - type of theme to return, admin or front
     *
     * @version 1.0.0
     * @return  Themes_model
     */
    public function Get_by_primary_key_and_type( $theme_id, $type )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( $this->primary_key, $theme_id );
        $filters->Where_equal(
            ( $type == "admin" ? $this->alias."is_for_admin" : $this->alias."is_for_front" ),
            "1"
        );

        $filters->Where_equal(
            ( $type == "admin" ? $this->alias."is_enabled_on_admin" : $this->alias."is_enabled_on_front" ),
            "1"
        );

        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_by_type_where_is_default
     * @access  public
     * @desc    Get theme data by type where is default
     * @author  Cousin Bela
     *
     * @param   string                      $type                       - type of theme to return, admin or front
     *
     * @version 1.0.0
     * @return  Themes_model
     */
    public function Get_by_type_where_is_default( $type )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal(
            ( $type == "admin" ? $this->alias."is_for_admin" : $this->alias."is_for_front" ),
            "1"
        );

        $filters->Order_by( $this->alias."is_default", "DESC" );
        $filters->Order_by(
            ( $type == "admin" ? $this->alias."is_enabled_on_admin" : $this->alias."is_enabled_on_front" ),
            "DESC"
        );

        return $this->Get_by_filters( $filters );
    }
    //</editor-fold>
}

/* End of file Main_themes_model.php */
/* Location: ./Core/Models/ */