<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Main_language_details_model
 *
 * @property    int         $id
 * @property    int         $modified_user_id
 * @property    string      $name
 * @property    datetime    $insert_date
 * @property    datetime    $modify_date
 * @property    boolean     $is_enabled
 * @property    boolean     $is_deleted
 *
 * @version     1.0.0
 */
class Main_language_details_model extends Common_model
{
    public function __construct()
    {
        parent::__construct();

        $this->table_name   = "language_details";
        $this->primary_key  = "id";
        $this->alias        = "ld.";
    }

    /**
     * @return Main_language_details_model
     */
    public static function Get_model()
    {
        return parent::Get_model();
    }

    /*******************************************************************************************************************
     * ADMIN FUNCTIONS
     *******************************************************************************************************************/
    //<editor-fold desc="ADMIN FUNCTIONS">

    /**
     * @method  Get_list_by_language_id
     * @access  public
     * @desc    Getting the list of items by product id
     * @author  Cousin Bela
     *
     * @param   int                         $language_id                    - the id of product
     *
     * @version 1.0.0
     * @return  Language_details_model
     */
    public function Get_list_by_language_id( $language_id )
    {
        $filters = new MDB_Filter();
        $filters->Where_equal( "language_id", $language_id );
        $filters->Where_equal( "is_deleted", "0" );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Count_by_language_id
     * @access  public
     * @desc    Getting the number of items by product id
     * @author  Cousin Bela
     *
     * @param   int                         $language_id                    - the id of product
     *
     * @version 1.0.0
     * @return  int
     */
    public function Count_by_language_id( $language_id )
    {
        $filters = new MDB_Filter();
        $filters->Where_equal( "language_id", $language_id );
        $filters->Where_equal( "is_deleted", "0" );

        return $this->Count_by_filters( $filters );
    }
    //</editor-fold>
}

/* End of file Main_language_details_model.php */
/* Location: ./Core/Models/ */