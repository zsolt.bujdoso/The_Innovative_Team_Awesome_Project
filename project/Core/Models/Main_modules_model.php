<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Main_modules_model
 *
 * @property    int         $id
 * @property    int         $modified_user_id
 * @property    string      $name
 * @property    string      $link
 * @property    string      $helper
 * @property    enum        $type
 * @property    string      $image
 * @property    boolean     $show_in_menu
 * @property    boolean     $can_be_shown_in_quick_access
 * @property    string      $tables
 * @property    string      $version
 * @property    string      $update
 * @property    datetime    $insert_date
 * @property    datetime    $modify_date
 * @property    datetime    $update_date
 * @property    boolean     $has_items_for_home
 * @property    boolean     $is_installed
 * @property    boolean     $is_updated
 * @property    boolean     $is_enabled
 * @property    boolean     $is_deleted
 *
 * @version     1.0.0
 */
class Main_modules_model extends Common_model
{
    public function __construct()
    {
        parent::__construct();

        $this->table_name   = "modules";
        $this->primary_key  = "id";
        $this->alias        = "m.";
    }

    /**
     * @return Main_modules_model
     */
    public static function Get_model()
    {
        return parent::Get_model();
    }

    /*******************************************************************************************************************
     * ADMIN FUNCTIONS
     *******************************************************************************************************************/
    //<editor-fold desc="ADMIN FUNCTIONS">

    /**
     * @method  Get_list_by_user_type_id
     * @access  public
     * @desc    Get the list of modules those user can view
     * @author  Cousin Bela
     *
     * @param   int                         $user_type_id               - the id of user type logged in
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_by_user_type_id( $user_type_id )
    {
        $filters = $this->Get_filters();

        $filters->Select( "utmp.*" );
        $filters->Select( "m.id" );
        $filters->Join_left(
            "user_type_module_permissions",
            "utmp.module_id = m.id
                AND utmp.can_view = 1
                AND utmp.user_type_id = '".$user_type_id."'",
            "utmp"
        );
        $filters->Join_left(
            "user_type_module_orders",
            "utmo.module_id = m.id
                AND utmo.user_type_id = '".$user_type_id."'",
            "utmo"
        );
        $filters->Where_equal( "m.is_enabled", "1" );
        $filters->Order_by( "utmo.order,".$this->alias."sort_order" );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Get_filters
     * @access  protected
     * @desc    This method create filters for selecting items with details
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  MDB_Filter
     */
    protected function Get_filters( $language_code = "" )
    {
        $filters = $this->Get_filters_for_count( $language_code );

        $filters->Select( $this->alias."*" );

        return $filters;
    }

    /**
     * @method  Get_filters_for_count
     * @access  protected
     * @desc    This method create filters for counting items with details
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  MDB_Filter
     */
    protected function Get_filters_for_count( $language_code = "" )
    {
        $filters = new MDB_Filter();

        $filters->Alias( trim( $this->alias, "." ) );
        $filters->Where_equal( $this->alias."is_deleted", "0" );

        return $filters;
    }

    /**
     * @method  Get_list_for_modification_by_user_type_id
     * @access  public
     * @desc    Get the list of modules those user can view
     * @author  Cousin Bela
     *
     * @param   int                         $user_type_id               - the id of user type logged in
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_for_modification_by_user_type_id( $user_type_id )
    {
        $filters = $this->Get_filters();

        $filters->Distinct( TRUE );
        $filters->Select( "utmp.*" );
        $filters->Select( $this->alias.$this->primary_key );
        $filters->Join_left(
            "user_type_module_permissions",
            "utmp.module_id = ".$this->alias.$this->primary_key."
                AND utmp.user_type_id = '".$user_type_id."'",
            "utmp"
        );
        $filters->Join_left( "user_type_module_orders", "utmo.module_id = ".$this->alias.$this->primary_key, "utmo" );
        $filters->Where_equal( $this->alias."is_enabled", "1" );
        $filters->Where( "( ".$this->alias."show_in_menu = 1 OR ".$this->alias."type = 'system' )" );
        $filters->Order_by( "utmo.order" );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Get_list_by_module_type
     * @access  public
     * @desc    Get the list of modules by type
     * @author  Cousin Bela
     *
     * @param   int                         $type                       - the id of user type logged in
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_by_module_type( $type )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( $this->alias."type", $type );
        $filters->Where_equal( $this->alias."is_enabled", "1" );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Get_list_by_has_api_communication
     * @access  public
     * @desc    Get the list of modules by type
     * @author  Cousin Bela
     *
     * @param   int                         $has_communication          - has communication or not
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_by_has_api_communication( $has_communication )
    {
        $filters = $this->Get_filters();

        $filters->Where(
            "( ".$this->alias."has_api_communication = ".$filters->Protect_value( $has_communication )." OR "
            .$this->alias."type = 'system' )"
        );
        $filters->Where_equal( $this->alias."is_enabled", "1" );

        return $this->Get_list_by_filters( $filters );
    }
    //</editor-fold>


    /*******************************************************************************************************************
     * FRONT FUNCTIONS
     *******************************************************************************************************************/
    //<editor-fold desc="FRONT FUNCTIONS">

    //</editor-fold>
}

/* End of file Main_modules_model.php */
/* Location: ./Core/Models/ */