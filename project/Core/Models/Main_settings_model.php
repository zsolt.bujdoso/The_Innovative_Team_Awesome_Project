<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Main_settings_model
 *
 * @property    int         $id
 * @property    int         $modified_user_id
 * @property    int         $domain_id
 * @property    string      $name
 * @property    string      $code
 * @property    string      $domain
 * @property    string      $value
 * @property    string      $value_text
 * @property    string      $comment
 * @property    string      $type
 * @property    datetime    $insert_date
 * @property    datetime    $modify_date
 * @property    boolean     $can_be_modified
 * @property    boolean     $is_visible
 * @property    boolean     $is_deleted
 *
 * @version     1.0.0
 */
class Main_settings_model extends Common_model
{
    public function __construct()
    {
        parent::__construct();

        $this->table_name   = "settings";
        $this->primary_key  = "id";
        $this->alias        = "s.";
    }

    /**
     * @return Main_settings_model
     */
    public static function Get_model()
    {
        return parent::Get_model();
    }

    /**
     * @method  Get_by_item_code_and_domain
     * @access  public
     * @desc    Get one setting by code and domain
     * @author  Cousin Bela
     *
     * @param   string                  $code                   - domain name
     * @param   string                  $domain_id              - domain name
     *
     * @version 1.0.0
     * @return  Settings_model
     */
    public function Get_by_item_code_and_domain( $code, $domain_id = "" )
    {
        $filters = new MDB_Filter();
        $filters->Where_equal( "is_deleted", "0" );
        $filters->Where_equal( "code", $code );
        if( ! empty( $domain_id ) )
        {
            $filters->Where_equal( "domain_id", $domain_id );
        }

        return $this->Get_by_filters( $filters );
    }
    //</editor-fold>
}

/* End of file Main_settings_model.php */
/* Location: ./Core/Models/ */