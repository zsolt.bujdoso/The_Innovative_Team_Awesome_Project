<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "prefix"                => "admin_view_",
    "width"                 => "250",
    "height"                => "170",
    "crop"                  => TRUE,
    "crop_original"         => FALSE,
    "watermark"             => FALSE,
);


/* End of file admin_view.php */
/* Location: ./Core/Config/image/ */