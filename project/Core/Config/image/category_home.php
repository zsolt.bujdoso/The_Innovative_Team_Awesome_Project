<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "prefix"                => "category_home_",
    "width"                 => "258",
    "height"                => "0",
    "crop"                  => FALSE,
    "crop_original"         => FALSE,
    "watermark"             => FALSE,
    "watermark_file"        => "sakersoft-logo.png", // original, auto, fit, 50, 60, ...
    "watermark_position"    => "center", // top-left, top-center, top-right, left-center, right-center, bottom-left, bottom-center, bottom-right, center
);


/* End of file category_home.php */
/* Location: ./Core/Config/image/ */