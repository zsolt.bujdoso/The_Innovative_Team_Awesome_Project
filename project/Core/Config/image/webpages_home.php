<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "prefix"                => "webpage_",
    "width"                 => "100",
    "height"                => "60",
    "crop"                  => FALSE,
    "crop_original"         => FALSE,
    "watermark"             => FALSE,
);


/* End of file webpages_home.php */
/* Location: ./Core/Config/image/ */