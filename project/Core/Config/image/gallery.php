<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "prefix"                => "gallery_",
    "width"                 => 60,
    "height"                => 60,
    "crop"                  => FALSE,
    "crop_original"         => FALSE,
    "watermark"             => FALSE,
    "watermark_file"        => "a.png",
    "watermark_percent"     => "50",  // original, auto, fit, 50, 60, ...
    "watermark_position"    => "center", // top-left, top-center, top-right, left-center, right-center, bottom-left, bottom-center, bottom-right, center
);


/* End of file gallery.php */
/* Location: ./Core/Config/image/ */