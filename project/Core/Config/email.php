<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    // Set the mailer type: mail, smtp or sendmail
    "mailer"      => "",
    
    // if mailer is smtp, set the config
    //"smtp_host"   => "ssl://smtp.gmail.com",
    "smtp_port"   => "465",
    "smtp_user"   => "",
    "smtp_pass"   => "",
    "smtp_timeout"=> 30,

    // if mailer is sendmail, set the path
    "sendmail"    => "/usr/sbin/sendmail",
);


/* End of file email.php */
/* Location: ./Core/Config/ */