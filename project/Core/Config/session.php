<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    // Set the session type: SESSION or DB
    //"type"              => "MSession",
    // If type is DB
    "table_name"        => "session",
    
    // Common
    "encryption_key"    => "",
    "match_ip"          => TRUE,
    "match_agent"       => TRUE,
    "encrypt_cookie"    => FALSE,
    "expiration"        => 7200,
);


/* End of file session.php */
/* Location: ./Core/Config/ */