<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "default"       => "local",
    "is_cache_on"   => FALSE,
    "cache_path"    => "cache/database/",
    "cache_class"   => "MDB_Mem_cache",
    "cache_time"    => 300,
);


/* End of file database.php */
/* Location: ./Core/Config/ */