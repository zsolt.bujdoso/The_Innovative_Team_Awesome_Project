<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "google"    => array(
        "client_id"     => "",
        "secret_key"    => "",
        "api_key"       => "",
    ),
    "yahoo"     => array(
        "client_id"     => "--",
        "secret_key"    => "",
    ),
    "facebook"  => array(
        "client_id"     => "",
        "secret_key"    => "",
    ),
);


/* End of file third_party_logins.php */
/* Location: ./Core/Config/ */