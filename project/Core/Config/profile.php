<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "auto_show"                 => FALSE,
    "show_memory"               => FALSE,
    "show_time"                 => FALSE,
    "show_system_time"          => FALSE,
    "show_queries"              => FALSE,
    "show_number_of_queries"    => FALSE,
    "show_duplicated_queries"   => FALSE,
    "show_pre_controller_time"  => FALSE,
    "show_controller_time"      => FALSE,
    "template"                  => "//profile/result",
);


/* End of file profile.php */
/* Location: ./Core/Config/ */

