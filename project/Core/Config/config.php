<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "site_name"         => "Innovative",
    "user_session_id"   => "user_id",
    "admin_session_id"  => "admin_id",

    // Set the Log types, which of types will be logged: 
    // 0 - turned off
    // 1 - ALL
    // 2 - DEBUG
    // 3 - ERROR
    // 4 - INFO
    // 5 - INFO
    "log_types"         => 4,
    "log_add_date"      => TRUE,
    "log_date_format"   => "Y-m-d H:i:s",
    // Set this to TRUE or 1, if you want to stop when error occurs
    "debug_mode"        => TRUE,
    // Set the theme
    "theme"             => "test",
    "admin_theme"       => "test",
    // encryption key for encrypting codes
    "encryption_key"    => "ih9b8ey98gy39buő237työbteughsd7isd",
    "admin_last_listing_excludes"    => array(
                            "admin_view",
                            "admin_add",
                            "admin_clone",
                            "admin_link_view",
                            "admin_search_view",
                            "admin_user_login_view",
                            "admin_visitor_view",
                            "admin_login",
                        ),
);


/* End of file config.php */
/* Location: ./Core/Config/ */