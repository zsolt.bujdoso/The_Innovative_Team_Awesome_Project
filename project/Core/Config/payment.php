<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "payment_mode"                  => "test", // TEST or LIVE
    "express_checkout_success_url"  => "orders/pay/success_checkout/",
    "express_checkout_cancel_url"   => "orders/pay/cancel_checkout/",
    "payment_confirm_url"           => "orders/pay/confirm_checkout/",
);


/* End of file payment.php */
/* Location: ./Core/Config/ */