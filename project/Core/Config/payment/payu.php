<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "live"      => array(
        "merchant"                      => "",
        "secret_key"                    => "",
        "pay_method"                    => "CCVISAMC",
        "checkout_url"                  => "https://secure.payu.ro/order/lu.php",
    ),
    "test"      => array(
        "merchant"                      => "",
        "secret_key"                    => "",
        "pay_method"                    => "",
        "checkout_url"                  => "https://secure.payu.ro/order/lu.php",
    )
);


/* End of file payu.php */
/* Location: ./Core/Config/payment/ */