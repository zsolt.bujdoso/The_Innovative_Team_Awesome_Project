<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "live"      => array(
        "card_public_key_file"          => "",
        "card_private_key_file"         => "",
        "sms_public_key_file"           => "",
        "sms_private_key_file"          => "",
        "card_signature"                => "",
        "sms_signature"                 => "",
        "card_service"                  => "",
        "sms_service"                   => "",
        "checkout_url"                  => 'https://secure.mobilpay.ro',
    ),
    "test"      => array(
        "card_public_key_file"          => "",
        "card_private_key_file"         => "",
        "sms_public_key_file"           => "",
        "sms_private_key_file"          => "",
        "card_signature"                => "",
        "sms_signature"                 => "",
        "card_service"                  => "",
        "sms_service"                   => "",
        "checkout_url"                  => 'http://sandboxsecure.mobilpay.ro',
    )
);


/* End of file mobilpay.php */
/* Location: ./Core/Config/payment/ */