<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "version"   => "93.0",
    "live"      => array(
        "paypal_api_username"           => "",
        "paypal_api_password"           => "",
        "paypal_api_signature"          => "",
        "endpoint"                      => "https://api-3t.paypal.com/nvp",
        "use_proxy"                     => FALSE,
        "proxy_host"                    => "",
        "proxy_port"                    => "",
        "checkout_url"                  => "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=",
    ),
    "test"      => array(
        "paypal_api_username"           => "",
        "paypal_api_password"           => "",
        "paypal_api_signature"          => "",
        "endpoint"                      => "https://api-3t.sandbox.paypal.com/nvp",
        "use_proxy"                     => FALSE,
        "proxy_host"                    => "",
        "proxy_port"                    => "",
        "checkout_url"                  => "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=",
    )
);


/* End of file paypal.php */
/* Location: ./Core/Config/payment/ */