<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "live"      => array(
        "merchant"                      => "",
        "secret_key"                    => "",
        "checkout_url"                  => "https://secure.euplatesc.ro/tdsprocess/tranzactd.php",
    ),
    "test"      => array(
        "merchant"                      => "",
        "secret_key"                    => "",
        "checkout_url"                  => "https://secure.euplatesc.ro/tdsprocess/tranzactd.php",
    )
);


/* End of file euplatesc.php */
/* Location: ./Core/Config/payment/ */