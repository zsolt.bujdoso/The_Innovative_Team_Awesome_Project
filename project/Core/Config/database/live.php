<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "hostname"  => "localhost",
    "port"      => "3306",
    "username"  => "",
    "password"  => "",
    "database"  => "wannacode_com",
    "driver"    => "mysqli",
    "prefix"    => "m_",
    "charset"   => "utf8",
    "collate"   => "utf8_unicode_ci",
    "caching"   => FALSE,
    "expire"    => "",
);


/* End of file live.php */
/* Location: ./Core/Config/payment/ */