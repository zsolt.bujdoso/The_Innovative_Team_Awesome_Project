<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

define( "PRODUCTS_CATEGORY_ID_MAIN",            1 );
define( "ARTICLES_CATEGORY_ID_MOTIVATIONS",     2 );
define( "ARTICLES_CATEGORY_ID_WEBPAGES_CREATED",3 );
define( "ARTICLES_CATEGORY_ID_LINKS",           4 );

// For Login security, send email to user if he didn't received in last hours
define( "USER_LOGIN_ERROR_MAIL_MAX_OLD",        "-6 hours" );


define( "MAIL_TYPE_LOGIN_ERRORS_ID",            4 );


/* End of file constant.php */
/* Location: ./Core/Config/ */