<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



return array(
    "type"                  => "path", // this can be PATH or GET
    "default_module"        => "", // default controller
    "default_controller"    => "default", // default controller
    "default_function"      => "index", // default function in controller
    "routes"                => array(
        "^([a-zA-Z]{2}\/)?admin\/reports\/add\/(.*)"            => "reports/admin/add/$2",
        "^([a-zA-Z]{2}\/)?admin\/([a-zA-Z\_0-9]+)\/([a-zA-Z\_0-9]+)\/(.*)\/(.*)"    => "$2/admin_$3/$4/$5",
        "^([a-zA-Z]{2}\/)?admin\/([a-zA-Z\_0-9]+)\/([a-zA-Z\_0-9]+)\/(.*)"          => "$2/admin_$3/$4",
        "^([a-zA-Z]{2}\/)?admin\/([a-zA-Z\_0-9]+)\/index\/?"    => "$2/admin/index",
        "^([a-zA-Z]{2}\/)?admin\/([a-zA-Z\_0-9]+)\/item\/?"     => "$2/admin/item",
        "^([a-zA-Z]{2}\/)?admin\/([a-zA-Z\_0-9]+)\/preview\/?"  => "$2/admin/preview",
        "^([a-zA-Z]{2}\/)?admin\/ajax\/(.*)?"                   => "ajax/admin/$2",
        "^([a-zA-Z]{2}\/)?admin\/([a-zA-Z\_0-9]+)\/delete\/?"   => "$2/admin/delete",
        "^([a-zA-Z]{2}\/)?admin\/([a-zA-Z\_0-9]+)\/(.*)\/?"     => "$2/admin_$3/index",
        "^([a-zA-Z]{2}\/)?admin\/(.*)\/?"                       => "$2/admin/index",
        "^([a-zA-Z]{2}\/)?admin\/?"                             => "menu/admin/index",
        "^([a-zA-Z0-9\-\_]+)\/admin_ajax\/(.*)\/?"              => "$1/admin_ajax/$2",
        "^([a-zA-Z]{2}\/)?download\/([a-zA-Z\_]+)\/(.*)\/?"     => "download/index/$2/$3",
    ),
    "default_routes"        => array(
        "^([a-zA-Z]{2}\/)?download\/([a-zA-Z\_]+)\/(.*)\/?"     => "download/index/$2",
        "^([a-zA-Z]{2}\/)?index\/?"                             => "default/index",
        "^([a-zA-Z]{2})\/?"                                     => "default/index",
        "^$"                                                    => "default/index",
        // Everything else
        "([a-zA-Z]{2})\/(.*)"                                   => "$2",
    ),
);


/* End of file url.php */
/* Location: ./Core/Config/ */