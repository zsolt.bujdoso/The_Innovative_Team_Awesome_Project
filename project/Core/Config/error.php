<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "debug"     => "//errors/debug",
    "301"       => null,
    "302"       => null,
    "404"       => "//errors/404",
    "500"       => "//errors/500",
);


/* End of file error.php */
/* Location: ./Core/Config/ */