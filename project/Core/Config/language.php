<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "default"   => "hungarian",
    "code"      => "hu",
    "languages" => array(
                    "hu" => array( "folder" => "hungarian", "name" => "Hungarian", "date_format" => "Y-m-d" ),
                    "en" => array( "folder" => "english",   "name" => "English", "date_format" => "d-m-Y" ),
                    "ro" => array( "folder" => "romanian",  "name" => "Romanian", "date_format" => "d-m-Y" )
                ),
);


/* End of file language.php */
/* Location: ./Core/Config/ */