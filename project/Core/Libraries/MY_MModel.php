<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MY_MModel
 *
 * @version 1.0.0
 */
class MY_MModel extends BModel
{
    protected   $table_name = "";
    protected   $primary_key= "";
    protected   $alias      = "";

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @method  Close_connection
     * @desc    This method close the database connection
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    public function Close_connection()
    {
        $this->db->Close_connection();
    }
    //</editor-fold>

    /******************************************
     * TABLE MODIFICATIONS
     ******************************************/
    //<editor-fold desc="Table modifiers">
    /**
     * @method  Create_table
     * @access  public
     * @desc    Create new table
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $columns                    - an array with columns where index is name of the column
     *                                                                and value is type, length, and other options
     * @param   string                  $primary_key                - the primary key column name
     * @param   bool                    $auto_increment             - add auto increment for primary key
     * @param   string                  $engine                     - engine use for this table
     *
     * @version 1.0.0
     * @throws  MException
     * @return  bool
     */
    public function Create_table( $table_name, array $columns, $primary_key = "", $auto_increment = TRUE, $engine = "InnoDB" )
    {
        return $this->db->Create_table( $table_name, $columns, $primary_key, $auto_increment, $engine );
    }

    /**
     * @method  Check_table_name
     * @access  private
     * @desc    This method checks and add the prefix for selected table if it is not added
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table
     *
     * @version 1.0.0
     * @return string
     */
    public function Check_table_name( $table_name )
    {
        return $this->db->Check_table_name( $table_name );
    }

    /**
     * @method  Delete_table
     * @desc    This method try to delete a table by table name
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     *
     * @version 1.0
     * @return  bool
     */
    public function Delete_table( $table_name )
    {
        return $this->db->Delete_table( $table_name );
    }

    /**
     * @method  Truncate
     * @desc    Try to truncate a table by name
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Truncate()
    {
        return $this->db->Truncate( $this->table_name );
    }

    /**
     * @method  Alter_table
     * @desc    Try to alter table by name and modifications string
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $query                      - modifications string
     *
     * @version 1.0
     * @return  bool
     */
    public function Alter_table( $table_name, $query )
    {
        return $this->db->Alter_table( $table_name, $query );
    }

    /**
     * @method  Add_column
     * @desc    Try to add a column into specified table after a column
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $column                     - the column name
     * @param   string                  $type                       - type of the column, length, and other modifiers
     * @param   string                  $after                      - column name to insert after
     *
     * @version 1.0
     * @return  bool
     */
    public function Add_column( $table_name, $column, $type, $after )
    {
        return $this->db->Add_column( $table_name, $column, $type, $after );
    }

    /**
     * @method  Delete_column
     * @desc    Try to delete a column from specified table
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $column                     - the column name
     *
     * @version 1.0
     * @return  bool
     */
    public function Delete_column( $table_name, $column )
    {
        return $this->db->Delete_column( $table_name, $column );
    }

    /**
     * @method  Rename
     * @desc    Try to rename a table
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $new_table_name             - new name for table
     *
     * @version 1.0
     * @return  bool
     */
    public function Rename( $table_name, $new_table_name )
    {
        return $this->db->Rename( $table_name, $new_table_name );
    }
    //</editor-fold>

    /******************************************
     * TRANSACTIONS
     ******************************************/
    //<editor-fold desc="Transactions">
    /**
     * @method  Transaction_start
     * @desc    Try to start a transaction
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_start()
    {
        return $this->db->Transaction_start();
    }

    /**
     * @method  Transaction_end
     * @desc    Try to end transaction started before
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_end()
    {
        return $this->db->Transaction_end();
    }

    /**
     * @method  Transaction_status
     * @desc    Returns the transaction status
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_status()
    {
        return $this->db->Transaction_status();
    }

    /**
     * @method  Transaction_rollback
     * @desc    Try to rollback started transaction before
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_rollback()
    {
        return $this->db->Transaction_rollback();
    }
    //</editor-fold>

    /******************************************
     * GETTERS
     ******************************************/
    //<editor-fold desc="Getters">
    /**
     * @method  Get_list
     * @access  public
     * @desc    Returns an array with list of items from specified table or FALSE
     * @author  Cousin Béla
     *
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list( $forced_index = null, $force_from_db = FALSE )
    {
        return $this->db->Get_list( $this->table_name, $forced_index, $force_from_db );
    }

    /**
     * @method  Get_by_primary_key
     * @access  public
     * @desc    Returns one item from specified table by primary key
     * @author  Cousin Béla
     *
     * @param   string                  $value                      - primary key value
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_primary_key( $value, $force_from_db = FALSE )
    {
        return $this->db->Get_by_primary_key( $this->table_name, $this->primary_key, $value, $force_from_db );
    }

    /**
     * @method  Get_by_column
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - name of the column
     * @param   string                  $value                      - value of column
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_column( $column, $value, $force_from_db = FALSE )
    {
        return $this->db->Get_by_column( $this->table_name, $column, $value, $force_from_db );
    }

    /**
     * @method  Get_by_attributes
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   array                   $attributes                 - an array with attributes where index will be
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_attributes( array $attributes = array(), $force_from_db = FALSE )
    {
        return $this->db->Get_by_attributes( $this->table_name, $attributes, $force_from_db );
    }

    /**
     * @method  Get_by_filters
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   mixed                   $filters                    - an object with specified filters
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_filters( $filters = null, $force_from_db = FALSE )
    {
        return $this->db->Get_by_filters( $this->table_name, $filters, $force_from_db );
    }

    /**
     * @method  Get_list_by_column
     * @access  public
     * @desc    Returns an array with list of items from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - name of the column
     * @param   string                  $value                      - value of column
     * @param   int                     $limit                      - number of items to return
     * @param   int                     $limit_from                 - number where to start from returning
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list_by_column( $column, $value, $limit = null, $limit_from = null, $force_from_db = FALSE )
    {
        return $this->db->Get_list_by_column( $this->table_name, $column, $value, $limit, $limit_from, $force_from_db );
    }

    /**
     * @method  Get_list_by_attributes
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   int                     $limit                      - number of items to return
     * @param   int                     $limit_from                 - number where to start from returning
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list_by_attributes( array $attributes = array(), $limit = null, $limit_from = null, $force_from_db = FALSE )
    {
        return $this->db->Get_list_by_attributes( $this->table_name, $attributes, $limit, $limit_from, $force_from_db );
    }

    /**
     * @method  Get_list_by_filters
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   mixed                   $filters                    - an object with specified filters
     * @param   int                     $limit                      - number of items to return
     * @param   int                     $limit_from                 - number where to start from returning
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  MDB_Result
     */
    public function Get_list_by_filters( $filters = null, $limit = null, $limit_from = null, $force_from_db = FALSE )
    {
        return $this->db->Get_list_by_filters( $this->table_name, $filters, $limit, $limit_from, $force_from_db );
    }
    //</editor-fold>

    /******************************************
     * CRUD FUNCTIONS
     ******************************************/
    //<editor-fold desc="Crud functions">
    /**
     * @method  Insert
     * @access  public
     * @desc    Insert a new row into specified table
     * @author  Cousin Béla
     *
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     *
     * @version 1.0.0
     * @return  int
     */
    public function Insert( array $data_set = array() )
    {
        return $this->db->Insert( $this->table_name, $data_set );
    }

    /**
     * @method  Insert_by_sql
     * @access  public
     * @desc    Insert some new rows by an SQL into specified table
     * @author  Cousin Béla
     *
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $sql                        - an sql command for selecting rows from other table
     *
     * @version 1.0.0
     * @return  int
     */
    public function Insert_by_sql( array $data_set = array(), $sql = "" )
    {
        return $this->db->Insert_by_sql( $this->table_name, $data_set, $sql );
    }

    /**
     * @method  Update
     * @access  public
     * @desc    Update one or more row in specified table by column
     * @author  Cousin Béla
     *
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $column                     - name of specified column
     * @param   string                  $value                      - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_column( array $data_set = array(), $column, $value )
    {
        return $this->db->Update_by_column( $this->table_name, $data_set, $column, $value );
    }

    /**
     * @method  Update
     * @access  public
     * @desc    Update one or more row in specified table by primary key
     * @author  Cousin Béla
     *
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $primary_key_value          - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_primary_key( array $data_set = array(), $primary_key_value )
    {
        return $this->db->Update_by_primary_key( $this->table_name, $data_set, $this->primary_key, $primary_key_value );
    }

    /**
     * @method  Update
     * @access  public
     * @desc    Update one or more row in specified table by attributes
     * @author  Cousin Béla
     *
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_attributes( array $data_set = array(), array $attributes )
    {
        // Update by columns, here the attributes has more column, and when will be checked by filters class
        // will be put into where equal one by one
        return $this->db->Update_by_attributes( $this->table_name, $data_set, $attributes, "" );
    }

    /**
     * @method  Update_by_filters
     * @access  public
     * @desc    Update one or more row in specified table by filters
     * @author  Cousin Béla
     *
     * @param   mixed                   $filters                    - an object with specified filters
     * @param   int                     $limit                      - number of items to return
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_filters( $filters = null, $limit = null )
    {
        return $this->db->Update_by_filters( $this->table_name, $filters, $limit );
    }

    /**
     * @method  Delete_by_primary_key
     * @access  public
     * @desc    Update one row from specified table by primary key
     * @author  Cousin Béla
     *
     * @param   string                  $primary_key_value          - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_primary_key( $primary_key_values )
    {
        $filters = new MDB_Filter( $this->db );
        $filters->Set( "is_deleted", TRUE );

        if( is_array( $primary_key_values ) )
        {
            $filters->Where_in( $this->primary_key, $primary_key_values );
        }
        else
        {
            $filters->Where_equal( $this->primary_key, $primary_key_values );
        }

        return $this->Update_by_filters( $filters );
    }

    /**
     * @method  Delete_by_column
     * @access  public
     * @desc    Update one row from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - name of column
     * @param   string                  $value                      - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_column( $column, $value )
    {
        $filters = new MDB_Filter( $this->db );
        $filters->Set( "is_deleted", TRUE );
        $filters->Where_equal( $column, $value );

        return $this->Update_by_filters( $filters );
    }

    /**
     * @method  Delete_by_attributes
     * @access  public
     * @desc    Delete one or more rows from specified table by attributes
     * @author  Cousin Béla
     *
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_attributes( array $attributes )
    {
        $set = array(
            "is_deleted" => "1"
        );

        return $this->Update_by_attributes( $set, $attributes );
    }

    /**
     * @method  Delete_by_filters
     * @access  public
     * @desc    Delete one or more row from specified table by filters
     * @author  Cousin Béla
     *
     * @param   object                  $filters                    - object with filters
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_filters( $filters = null )
    {
        if( $filters instanceof MDB_Filter )
        {
            $filters->Set( "is_deleted", TRUE );
        }

        return $this->Update_by_filters( $filters );
    }

    /**
     * @method  Count_by_column
     * @access  public
     * @desc    Count rows from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - name of column
     * @param   string                  $value                      - value of primary key
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Count_by_column( $column, $value, $force_from_db = FALSE )
    {
        return $this->db->Count_by_column( $this->table_name, $column, $value, $force_from_db );
    }

    /**
     * @method  Count_by_attributes
     * @access  public
     * @desc    Count rows from specified table by attributes
     * @author  Cousin Béla
     *
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Count_by_attributes( array $attributes, $force_from_db = FALSE )
    {
        return $this->db->Count_by_attributes( $this->table_name, $attributes, $force_from_db );
    }

    /**
     * @method  Count_by_filters
     * @access  public
     * @desc    Count rows from specified table by filters
     * @author  Cousin Béla
     *
     * @param   object                  $filters                    - object with filters
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Count_by_filters( $filters = null, $force_from_db = FALSE )
    {
        return $this->db->Count_by_filters( $this->table_name, $filters, $force_from_db );
    }
    //</editor-fold>

    /******************************************
     * INFORMATIONS
     ******************************************/
    //<editor-fold desc="Information getters">
    /**
     * @method  Get_primary_key
     * @access  public
     * @desc    Returns the primary key for this model
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string|bool
     */
    public function Get_primary_key()
    {
        return $this->db->Get_primary_key( $this->table_name );
    }

    public function Get_table_name()
    {
        return $this->table_name;
    }

    public function Get_database_name()
    {
        return $this->db->Get_database_name();
    }
    //</editor-fold>

    /**
     * @method	Save
     * @access	public
     * @desc    Save a new item or update an existing by primary key
     * @author	Cousin Bela
     *
     * @param   array                       $data                       - an array with data to save
     * @param   mixed                       $primary_key_value          - primary key value
     *
     * @version	1.0.0
     * @return  bool
     */
    public function Save( $data, $primary_key_value = null )
    {
        if( empty( $primary_key_value ) )
        {
            return $this->Insert( $data );
        }

        return $this->Update_by_primary_key( $data, $primary_key_value );
    }

    /**
     * @method	Increment_viewed_by_primary_key
     * @access	public
     * @desc    Increment viewed value by primary key
     * @author	Cousin Bela
     *
     * @param   mixed                       $primary_key_value          - primary key value
     *
     * @version	1.0.0
     * @return  bool
     */
    public function Increment_viewed_by_primary_key( $primary_key_value )
    {
        $filters = new MDB_Filter( $this->db );
        $filters->Set( "viewed", "( viewed + 1 )", FALSE );
        $filters->Where_equal( $this->primary_key, $primary_key_value );

        return $this->Update_by_filters( $filters );
    }

    public function Is_enabled_or_is_admin_logged( & $item )
    {
        if( empty( $item ) )
        {
            return FALSE;
        }

        if( ! $item->is_enabled && ! User_helper::Is_admin_logged_in() )
        {
            return FALSE;
        }

        return TRUE;
    }

    public function Has_to_try_from_all_domain( & $item, $domain_id )
    {
        if( ! empty( $item ) )
        {
            return FALSE;
        }

        if( empty( $domain_id ) )
        {
            return FALSE;
        }

        return TRUE;
    }
}