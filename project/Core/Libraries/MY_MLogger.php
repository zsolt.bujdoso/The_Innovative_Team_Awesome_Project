<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MY_MLogger
 *
 * @version     1.0.0
 */
class MY_MLogger extends MLogger
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @method	GetLogs
     * @access	public
     * @desc    Return all logs
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    public function Write_logs()
    {
        $log_data = $this->Get_log_data();

        if( empty( $log_data ) )
        {
            return;
        }

        $log_data .= "<br /><br />Link: ".$_SERVER["REQUEST_URI"];

        $data = array(
            "user_id"           => User_helper::Get_user_id(),
            "modified_user_id"  => User_helper::Get_user_id(),
            "session_id"        => App()->session->Get_id(),
            "domain_id"         => $this->Get_domain_id(),
            "type"              => "system",
            "message"           => $log_data,
            "insert_date"       => Date_helper::Get_date_time(),
        );

        Logs_model::Get_model()->Insert( $data );
    }

    private function Get_domain_id()
    {
        $controller = App()->Get_instantiated_object( "controller" );
        if( empty( $controller->domain_id ) )
        {
            return 0;
        }

        return $controller->domain_id;
    }
}

/* End of file MY_MLogger.php */
/* Location: ./core/Libraries/ */