<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MY_Controller
 *
 * @property    MSession            $session
 * @property    MY_MLang            $lang
 * @property    MY_MLogger          $logger
 * @property    MY_MRedirect        $redirect
 * @property    MY_MRequest         $request
 * @property    MY_MRouter          $router
 * @property    MRenderer           $renderer
 * @property    MProfiler           $profiler
 * @property    DB_Mysql            $db
 *
 * @property    int                 $domain_id
 *
 *
 * @version     1.0.0
 */
class MY_Controller extends MController
{
    public $data;
    public $module_permissions;
    public $domain_id           = 0;
    public $domain_details      = "";

    protected $selected_module  = "";
    protected $module;
    protected $limit_per_page   = 50;
    protected $last_listing_url = "";
    protected $active_url       = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function Init()
    {
        parent::Init();

        if( $this->router->Get_module() )
        {
            $this->module = App()->module;
        }

        $this->Load_settings();
        $this->Load_default_variables();

        // Set the post data into session, for we can return them into fields
        if( ! empty( $_POST ) )
        {
            $this->session->Set_flash( "post_data", $_POST );
        }

        $this->Try_to_get_return_url();
        $this->Set_active_url();
        $this->Load_modules();
        $this->Load_languages();
        $this->Load_flash_messages();
        $this->Set_watermark_for_files();
        $this->Create_default_extension_variables();
    }

    /**
     * @method	Load_module_permissions
     * @access	public
     * @desc    Calculate the module permission for admin side
     * @author	Cousin Béla
     *
     * @param   string                      $module_link                - module link to check access
     * @param   bool                        $return                     - return the module permission or set it
     *
     * @version	1.0.0
     */
    public function Load_module_permissions( $module_link = null, $return = FALSE )
    {
        if( ! $module_link )
        {
            $module_link = $this->router->Get_module();
        }

        if( empty( App()->data["modules"] ) || ! $module_link )
        {
            return;
        }

        $modules = App()->data["modules"];

        foreach( $modules->Get_result() as $module )
        {
            if( $module->link == $module_link )
            {
                if( ! $return )
                {
                    $this->module_permissions = $module;
                    return;
                }

                return $module;
            }
        }
    }

    /**
     * @method	Load_settings
     * @access	public
     * @desc    Load settings from database and set it into config
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    private function Load_settings()
    {
        $settings = Main_settings_model::Get_model()->Get_list_where_not_deleted();

        if( $settings )
        {
            foreach( $settings->Get_result() as $setting )
            {
                $config_name                = $setting->code;
                $this->config->$config_name = ( !empty( $setting->value_text ) ? $setting->value_text : $setting->value );
            }
        }

        $timezone = Config_helper::Get( "default_timezone" );
        if( ! empty( $timezone ) )
        {
            date_default_timezone_set( $this->config->default_timezone );
        }
    }

    /**
     * @method	Load_default_variables
     * @access	public
     * @desc    Load default variables from framework
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    private function Load_default_variables()
    {
        $this->selected_module                  = $this->router->Get_module();
        $this->data                             = array();
        $this->data["base_url"]                 = Url_helper::Base_url();
        $this->data["theme_url"]                = Url_helper::Theme_url();
        $this->data["selected_module"]          = $this->selected_module;
        $this->data["selected_controller"]      = $this->router->Get_controller();
        $this->data["selected_function"]        = $this->router->Get_function();
    }

    /**
     * @method	Try_to_get_return_url
     * @access	private
     * @desc    Save into session the return to url if it is set
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    private function Try_to_get_return_url()
    {
        $return_url = Get_helper::Get( "return" );
        $type       = "return_url";

        if( $this->router->Is_admin() )
        {
            $type = "admin_".$type;
        }

        if( ! empty( $return_url ) )
        {
            $this->session->Set( $type, $return_url );
        }
    }

    /**
     * @method	Set_active_url
     * @access	private
     * @desc    Set last listing url and last active url
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    private function Set_active_url()
    {
        $this->active_url = MY_Url_helper::Base_url().MY_Url_helper::Get_active_url();
    }

    protected function Load_modules()
    {
        $this->data["modules"]  = Main_modules_model::Get_model()->Get_list_where_not_deleted();
    }

    protected function Load_languages()
    {
        $this->data["languages"]  = App()->data["languages"];
    }

    protected function Load_flash_messages()
    {
        if( ! $this->session->Has_flash( "messages" ) )
        {
            return;
        }

        $this->data["flash_messages"] = $this->session->Get_flash( "messages" );
    }

    protected function Set_watermark_for_files()
    {
        $image_config = $this->config->image;

        if( empty( $image_config["list"] ) || ! is_array( $image_config["list"] ) )
        {
            return;
        }

        if( empty( $this->config->watermark ) )
        {
            return;
        }

        foreach( $image_config["list"] as $template_name => & $image_template )
        {
            $image_template["watermark_file"]   = $this->config->watermark;
            $image_template["watermark"]        = TRUE;
        }
    }

    private function Create_default_extension_variables()
    {
        $this->data["extension_tabs"]           = "";
        $this->data["extension_contents"]       = "";
        $this->data["extension_banners"]        = "";
        $this->data["extension_styles"]         = "";
        $this->data["extension_scripts"]        = "";
        $this->data["extension_sidebar"]        = "";
    }

    protected function Get_module_helper_name( & $module )
    {
        $class_name = ucfirst( strtolower( $module->helper ) );

        if( empty( $class_name ) )
        {
            $class_name = ucfirst( strtolower( $module->link ) );

            // if the module name ends with an s, that means is plural
            if( substr( $class_name, -1, 1 ) == "s" )
            {
                $class_name = substr( $class_name, 0, -1 );
            }
        }

        $class_name .= "_helper";

        return $class_name;
    }

    /**
     * @method	Try_to_redirect_to_return_url
     * @access	protected
     * @desc    Check and return to url that was previously set from url
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    protected function Try_to_redirect_to_return_url()
    {
        $return_url = "";
        $type       = "return_url";

        if( $this->router->Is_admin() )
        {
            $type = "admin_".$type;
        }

        if( $this->session->Has( $type ) )
        {
            $return_url = urldecode( $this->session->Get( $type ) );
        }

        if( empty( $return_url ) )
        {
            return;
        }

        $this->session->Remove( $type );
        $this->redirect->To_site_url( $return_url );
    }

    /**
     * @method	Load_view
     * @access	public
     * @desc    Load the full view with layout and render it
     * @author	Cousin Béla
     *
     * @param   string                      $view_name                  - name of the template to load
     * @param   array                       $params                     - variables as array for this template
     * @param   int                         $cache_time                 - how many seconds to cache this template,
     *                                                                      if cache is enabled
     *
     * @version	1.0.0
     * @return  html content
     */
    protected function Load_view( $view_name = "", $params = array(), $cache_time = 0 )
    {
        if( empty( $params ) )
        {
            $params = $this->data;
        }

        return $this->renderer->Load_view( $view_name, $params, $cache_time );
    }

    /**
     * @method	Load_partial_view
     * @access	public
     * @desc    Load the partial view and render it
     * @author	Cousin Béla
     *
     * @param   string                      $view_name                  - name of the template to load
     * @param   array                       $params                     - variables as array for this template
     * @param   int                         $cache_time                 - how many seconds to cache this template,
     *                                                                      if cache is enabled
     *
     * @version	1.0.0
     * @return  html content
     */
    public function Load_partial_view( $view_name = "", $params = array(), $cache_time = 0 )
    {
        if( empty( $params ) )
        {
            $params = $this->data;
        }

        return $this->renderer->Load_partial_view( $view_name, $params, $cache_time );
    }

    /*******************************************************************************************************************
     * COMMON FUNCTIONS
     ******************************************************************************************************************/

    protected function Save_visitor()
    {
        if( ! $this->config->save_visitors || $this->session->Has( "page_visited" ) || $this->request->Is_robot() )
        {
            return;
        }
    }

    /**
     * @method  Show_result_with_json
     * @access  public
     * @desc    Turn off the profiler and Shows the result with json
     * @author  Cousin Bela
     *
     * @param   array                       $result                     - the result array to show
     *
     * @version 1.0.0
     */
    protected function Show_result_with_json( $result )
    {
        // Turn off the profiler
        $this->profiler->Turn_off();

        $this->data["result"] = $result;

        echo $this->Load_partial_view( "//ajax/json" );

        App()->Finish();
    }

    protected function Validate_paginaton_parameters()
    {
        $validation             = new Pagination_parameters_validation();
        $validation->attributes = $_GET;

        if( ! $validation->Validate() )
        {
            Message_helper::Add_error_flash_message( $this->lang->Get( "Parameters_error" ) );

            if( ! empty( $this->last_listing_url ) )
            {
                $this->redirect->To_site_url( $this->last_listing_url );
            }

            $this->redirect->To_site_url( "" );
        }

        $this->data["limit"] = Get_helper::Get( "limit" );

        if( empty( $this->data["limit"] ) )
        {
            $this->data["limit"] = $this->limit_per_page;
        }

        $this->data["keywords"] = Get_helper::Get( "keywords" );
    }

    protected function Set_actual_page_and_limit_from()
    {
        $actual_page = Get_helper::Get_integer( "page" );
        if( $actual_page - 1 < 0 )
        {
            $actual_page = 1;
        }

        $limit_from                     = ( $actual_page - 1 ) * $this->data["limit"];
        $this->data["actual_page"]      = $actual_page;
        $this->limit_per_page           = $this->data["limit"];
        $this->data["limit_per_page"]   = $this->data["limit"];
        $this->data["limit_from"]       = $limit_from;
    }

    /**
     * @method  Is_captcha_good
     * @access  public
     * @desc    Check if the captcha code is good or not
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  bool
     */
    protected function Load_module_libraries( $module_name )
    {
        $module_name    = ucfirst( strtolower( $module_name ) );
        $path           = MODULEPATH.$module_name."Libraries";

        General_functions::Include_files_from_folder_and_subfolders( $path );
    }

    /**
     * @method  Is_captcha_good
     * @access  public
     * @desc    Check if the captcha code is good or not
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  bool
     */
    protected function Is_captcha_good()
    {
        return Captcha_helper::Is_captcha_good( Post_helper::Get( "captcha" ) );
    }

    /**
     * @method  Is_captcha_good
     * @access  public
     * @desc    Check if the captcha code is good or not
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  bool
     */
    protected function Set_captcha()
    {
        Captcha_helper::Create_captcha();
    }

    /*******************************************************************************************************************
     * COMMON LOADER FUNCTIONS
     ******************************************************************************************************************/
    public function Load_themes()
    {
        $this->data["themes"]  = Themes_model::Get_model()->Get_list_where_not_deleted();
    }

    public function Get_module_details( $module_code )
    {
        if( empty( $this->data["modules"] ) )
        {
            return FALSE;
        }

        foreach( $this->data["modules"]->Get_result() as $module )
        {
            if( $module->link != strtolower( $module_code ) )
            {
                continue;
            }

            return $module;
        }
    }
}