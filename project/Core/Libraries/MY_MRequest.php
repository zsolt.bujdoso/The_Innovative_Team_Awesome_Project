<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MY_MRequest
 *
 * @version     1.0.0
 */
class MY_MRequest extends MRequest
{
    private $system_modules;
    private $custom_modules;

    public function __construct()
    {
        parent::__construct();
    }

    protected function Load_and_process_url()
    {
        // Load other modules
        $this->Load_system_modules();
        $this->Load_custom_modules();
        $this->Load_url();

        $this->url        = trim( $this->url, "/" );
        if( preg_match( "/^([a-zA-Z]{2})\/(.*)/", $this->url ) || preg_match( "/^([a-zA-Z]{2}\/?)$/", $this->url ) )
        {
            $this->url    = preg_replace( "/^([a-zA-Z]{2})\/(.*)/", "$2", $this->url );
        }
        $this->url        = preg_replace( "|([\/]+)|", "/", $this->url );

        $this->Process_url();
    }

    /**
     * @method  Load_system_modules
     * @access  private
     * @desc    Load enabled system modules from database
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    private function Load_system_modules()
    {
        $this->system_modules = Main_modules_model::Get_model()->Get_list_by_module_type( "system" );

        if( ! $this->system_modules )
        {
            return;
        }

        foreach( $this->system_modules->Get_result() as $module )
        {
            App()->config->module[] = $module->link;

            General_functions::Include_module( $module->link );
        }
    }

    /**
     * @method  Load_custom_modules
     * @access  private
     * @desc    Load enabled custom modules from database
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    private function Load_custom_modules()
    {
        $this->custom_modules = Main_modules_model::Get_model()->Get_list_by_module_type( "custom" );

        if( ! $this->custom_modules )
        {
            return;
        }

        foreach( $this->custom_modules->Get_result() as $module )
        {
            App()->config->module[] = $module->link;

            General_functions::Include_module( $module->link );
        }
    }

    /**
     * @method  Load_url
     * @access  private
     * @desc    Load and parse the selected url
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    protected function Load_url()
    {
        $type       = ( isset( App()->config->url["type"] ) ? App()->config->url["type"] : "PATH" );
        $parameter  = ( isset( App()->config->url["parameter"] ) ? App()->config->url["parameter"] : "r" );

        if( strtoupper( $type ) == "GET" )
        {
            $this->url = "";

            if( isset( $_GET[$parameter] ) )
            {
                $this->url = @urldecode( $_GET[$parameter] );
            }

            return;
        }

        $url        = @urldecode( Server_helper::Get( "REQUEST_URI" ) );
        $parse_url  = parse_url( $url );
        $this->url  = $parse_url["path"];

        if( ! empty( $parse_url["query"] ) )
        {
            $this->query_parameters = $parse_url["query"];
        }

        $this->Remove_script_path();
    }
}