<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Common_model
 *
 * @version     1.0.0
 */
class Common_model extends MY_MModel
{
    /*******************************************************************************************************************
     * ADMIN FUNCTIONS
     ******************************************************************************************************************/
    //<editor-fold desc="ADMIN FUNCTIONS">

    /**
     * @method  Get_list_for_admin
     * @access  public
     * @desc    Get list of albums for admin listing album, limited with param limit
     * @author  Cousin Bela
     *
     * @param   int                         $limit                      - how many products to return
     * @param   int                         $limit_from                 - start the list from
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  MDB_Result
     */
    public function Get_list_for_admin( $limit = 50, $limit_from = 0, $search_filters = null )
    {
        $filters = $this->Get_filters();

        $filters->Distinct( TRUE );
        $this->Process_search( $filters, $search_filters );

        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );
        $filters->Limit( $limit, $limit_from );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Get_filters
     * @access  private
     * @desc    This method create filters for selecting items with details
     * @author  Cousin Bela
     *
     * @param   string                      $language_code              - selected language code
     *
     * @version 1.0.0
     * @return  MDB_Filter
     */
    protected function Get_filters( $language_code = "" )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters_for_count( $language_code );

        return $filters;
    }

    /**
     * @method  Get_filters_for_count
     * @access  private
     * @desc    This method create filters for counting items with details
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  MDB_Filter
     */
    protected function Get_filters_for_count( $language_code = "" )
    {
        $filters = new MDB_Filter();

        $filters->Alias( trim( $this->alias, "." ) );
        $filters->Where_equal( $this->alias."is_deleted", "0" );

        return $filters;
    }

    /**
     * @method  Process_search
     * @access  protected
     * @desc    This method filters the item(s) by given keyword(s)
     * @author  Cousin Bela
     *
     * @param   MDB_Filter                  $filters                    - the filter for item
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     */
    protected function Process_search( & $filters, $search_filters = null )
    {
        if( empty( $search_filters ) )
        {
            return;
        }

        $this->Process_search_string( $filters, $search_filters );
        $this->Process_search_array( $filters, $search_filters );
    }

    /**
     * @method  Process_search_string
     * @access  protected
     * @desc    This method filters the item(s) by given keyword(s)
     * @author  Cousin Bela
     *
     * @param   MDB_Filter                  $filters                    - the filter for item
     * @param   string                      $search_filters             - string with search filters
     *
     * @version 1.0.0
     */
    protected function Process_search_string( & $filters, $search_filters = null )
    {
        if( ! is_string( $search_filters ) )
        {
            return;
        }

        $alias = trim( $this->alias, "." );

        $filters->Or_like( $alias."d.name", $search_filters );
        $filters->Or_like( $this->alias."code", $search_filters );
        $filters->Or_like( $alias."d.description", $search_filters );
    }

    /**
     * @method  Count_for_admin
     * @access  public
     * @desc    Count albums for admin listing album
     * @author  Cousin Bela
     *
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  int
     */
    public function Count_for_admin( $search_filters = null )
    {
        $filters = $this->Get_filters_for_count();

        $this->Process_search( $filters, $search_filters );

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Get_list_for_admin_with_subcategories_by_parent_id
     * @access  public
     * @desc    Get all categories with subcategories by parent id
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id to select subcategories start from
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_for_admin_with_subcategories_by_parent_id( $parent_id = 0 )
    {
        $categories = $this->Get_list_for_admin_by_parent_id_where_could_subcategories( $parent_id );

        if( $categories )
        {
            foreach( $categories->Get_result() as $category )
            {
                $category->subcategories = $this->Get_list_for_admin_with_subcategories_by_parent_id( $category->id );
            }
        }

        return $categories;
    }

    /**
     * @method  Get_list_for_admin_with_all_subcategories_by_parent_id
     * @access  public
     * @desc    Get all categories with subcategories by parent id
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id to select subcategories start from
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_for_admin_with_all_subcategories_by_parent_id( $parent_id = 0 )
    {
        $categories = $this->Get_list_for_admin_by_parent_id( $parent_id );

        if( $categories )
        {
            foreach( $categories->Get_result() as $category )
            {
                $category->subcategories = $this->Get_list_for_admin_with_all_subcategories_by_parent_id( $category->id );
            }
        }

        return $categories;
    }

    /**
     * @method  Get_list_for_admin_by_parent_id
     * @access  public
     * @desc    Get a list of categories by parent_id
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id for this category list
     * @param   int                         $offset                     - return items from
     * @param   int                         $limit                      - how many items to return
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_for_admin_by_parent_id( $parent_id = 0, $offset = null, $limit = null )
    {
        $filters = $this->Get_filters( App()->lang->Get_language_code() );

        $filters->Where_equal( $this->alias."parent_id", $parent_id );

        $filters->Limit( $limit, $offset );
        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Get_list_for_admin_by_parent_id_where_could_subcategories
     * @access  public
     * @desc    Get a list of categories by parent_id where subcategories are not disabled
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id for this category list
     * @param   int                         $offset                     - return items from
     * @param   int                         $limit                      - how many items to return
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_for_admin_by_parent_id_where_could_subcategories( $parent_id = 0, $offset = null, $limit = null )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( $this->alias."parent_id", $parent_id );
        $filters->Where_equal( $this->alias."disable_subcategories", "0" );

        $filters->Limit( $limit, $offset );
        $filters->Order_by( $this->alias.$this->primary_key );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Count_for_admin_by_parent_id
     * @access  public
     * @desc    Count article categories by parent_id
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id for this category list
     *
     * @version 1.0
     * @return  int
     */
    public function Count_for_admin_by_parent_id( $parent_id = 0 )
    {
        $filters = $this->Get_filters_for_count();

        $filters->Where_equal( $this->alias."parent_id", $parent_id );

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Get_quick_list_for_admin
     * @access  public
     * @desc    Get last items, limited with param limit
     * @author  Cousin Bela
     *
     * @param   int                         $limit                      - how many items to return
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_quick_list_for_admin( $limit )
    {
        $filters = $this->Get_filters();

        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );
        $filters->Limit( $limit );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Get_list_for_ajax_search
     * @access  public
     * @desc    Get list of items, limited
     * @author  Cousin Bela
     *
     * @param   mixed                       $search_filters             - an array or string with search filters
     * @param   string                      $language_code              - selected language code
     * @param   int                         $limit                      - how many products to return
     *
     * @version 1.0.0
     * @return  array
     */
    public function Get_list_for_ajax_search( $search_filters, $language_code, $limit = 50 )
    {
        $filters    = $this->Get_filters();
        $alias      = trim( $this->alias, "." );

        $this->Process_search( $filters, $search_filters );
        $filters->Where_equal( $alias."d.language_code", $language_code );

        $filters->Order_by( $this->alias.$this->primary_key );
        $filters->Limit( $limit );

        $items = $this->Get_list_by_filters( $filters );

        if( empty( $items ) )
        {
            return array();
        }

        $result = array();
        foreach( $items->Get_result() as $item )
        {
            $result []= array(
                "label"     => $item->name,
                "value"     => $item->name,
                "item_id"   => $item->id,
            );
        }

        return $result;
    }

    /**
     * @method  Get_by_item_id_for_admin
     * @access  public
     * @desc    Get tooltip by primary key for admin
     * @author  Cousin Bela
     *
     * @param   int                         $primary_key                - id of tooltip to return
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_id_for_admin( $primary_key, $search_filters = null )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( $this->alias.$this->primary_key, $primary_key );

        $this->Process_search( $filters, $search_filters );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Check_if_code_exists
     * @access  public
     * @desc    Check if a code exists or not
     * @author  Cousin Bela
     *
     * @param   string                      $code                       - code to check
     * @param   int                         $item_id                    - id of primary key to exclude
     *
     * @version 1.0.0
     * @return  int
     */
    public function Check_if_code_exists( $code = "", $item_id = null )
    {
        $filters = new MDB_Filter();

        $filters->Select( $this->primary_key );
        $filters->Where_equal( "code", $code );
        $filters->Where_not_equal( $this->primary_key, $item_id );

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Check_if_code_exists_by_domain_excluded
     * @access  public
     * @desc    Check if a code exists or not, excluded item by id
     * @author  Cousin Bela
     *
     * @param   string                      $code                       - code to check
     * @param   int                         $domain_id                  - id of domain to filter items
     * @param   int                         $item_id                    - id of primary key to exclude
     *
     * @version 1.0.0
     * @return  int
     */
    public function Check_if_code_exists_by_domain_excluded( $code, $domain_id = 0, $item_id = null )
    {
        $filters = $this->Get_filters_for_count();

        $filters->Where_equal( $this->alias."code", $code );
        $filters->Where_equal( $this->alias."domain_id", $domain_id );

        if( ! empty( $item_id ) )
        {
            $filters->Where_not_equal( $this->alias.$this->primary_key, $item_id );
        }

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Check_if_code_exists_excluded
     * @access  public
     * @desc    Check if a code exists or not, excluded item by id
     * @author  Cousin Bela
     *
     * @param   string                      $code                       - code to check
     * @param   int                         $item_id                    - id of primary key to exclude
     *
     * @version 1.0.0
     * @return  int
     */
    public function Check_if_code_exists_excluded( $code, $item_id = null )
    {
        $filters = new MDB_Filter();

        $filters->Where_equal( "code", $code );

        if( ! empty( $item_id ) )
        {
            $filters->Where_not_equal( $this->primary_key, $item_id );
        }

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Check_if_email_exists
     * @access  public
     * @desc    Checks if email already exists
     * @author  Cousin Bela
     *
     * @param   string                  $email              - email address to check
     * @param   int                     $item_id            - id of item to exclude
     *
     * @version 1.0.0
     * @return  int
     */
    public function Check_if_email_exists( $email = "", $item_id = 0 )
    {
        $filters = new MDB_Filter();

        $filters->Where_equal( "is_deleted", "0" );
        $filters->Where_equal( "email", $email );

        if( ! empty( $item_id ) )
        {
            $filters->Where_not_equal( $this->primary_key, $item_id );
        }

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Check_if_name_exists
     * @access  public
     * @desc    Checks if name already exists
     * @author  Cousin Bela
     *
     * @param   string                      $name                       - code to check
     * @param   int                         $order_status_id            - id of order status to exclude
     *
     * @version 1.0.0
     * @return  int
     */
    public function Check_if_name_exists( $name = "", $order_status_id = 0 )
    {
        $filters = $this->Get_filters_for_count();

        $filters->Where_equal( $this->alias."name", $name );

        if( ! empty( $order_status_id ) )
        {
            $filters->Where_not_equal( $this->alias.$this->primary_key, $order_status_id );
        }

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Get_list_where_not_deleted
     * @access  public
     * @desc    Get list of items for admin listing, limited with param limit
     * @author  Cousin Bela
     *
     * @param   int                         $limit                      - how many products to return
     * @param   int                         $limit_from                 - start the list from
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  MDB_Result
     */
    public function Get_list_where_not_deleted( $limit = 50, $limit_from = 0, $search_filters = null )
    {
        $this->db->Set_cache_to_file();
        $result = $this->Get_list_for_admin( $limit, $limit_from, $search_filters );
        $this->db->Set_cache_back_to_last_state();

        return $result;
    }

    /**
     * @method  Count_where_not_deleted
     * @access  public
     * @desc    Count of items for admin listing album, limited with param limit
     * @author  Cousin Bela
     *
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  integer
     */
    public function Count_where_not_deleted( $search_filters = null )
    {
        return $this->Count_for_admin( $search_filters );
    }

    /**
     * @method  Update_to_deleted_by_primary_key
     * @access  public
     * @desc    Checks if name already exists
     * @author  Cousin Bela
     *
     * @param   int                         $item_id                    - id of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_to_deleted_by_primary_key( $item_id = 0 )
    {
        $filters = new MDB_Filter();

        $filters->Alias( trim( $this->alias, "." ) );
        $filters->Set( "modified_user_id", User_helper::Get_user_id() );
        $filters->Set( "modify_date", date( "Y-m-d" ) );
        $filters->Set( "is_deleted", 1 );
        $filters->Where_equal( $this->alias.$this->primary_key, $item_id );

        return $this->Update_by_filters( $filters );
    }

    /**
     * @method  Update_to_modified_by_primary_key
     * @access  public
     * @desc    This method updates a table row to modified
     * @author  Csenteri Attila
     *
     * @param   int                         $item_id                    - id of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_to_modified_by_primary_key( $item_id = 0 )
    {
        $filters = new MDB_Filter();

        $filters->Alias( trim( $this->alias, "." ) );
        $filters->Set( "modified_user_id", User_helper::Get_user_id() );
        $filters->Set( "modify_date", date( "Y-m-d" ) );
        $filters->Where_equal( $this->alias.$this->primary_key, $item_id );

        return $this->Update_by_filters( $filters );
    }

    /**
     * @method  Update_to_deleted_by_primary_keys
     * @access  public
     * @desc    Checks if name already exists
     * @author  Cousin Bela
     *
     * @param   array                       $item_ids                   - ids of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_to_deleted_by_primary_keys( $item_ids = array() )
    {
        $filters = new MDB_Filter();

        $filters->Alias( trim( $this->alias, "." ) );
        $filters->Set( "modified_user_id", User_helper::Get_user_id() );
        $filters->Set( "modify_date", date( "Y-m-d" ) );
        $filters->Set( "is_deleted", 1 );
        $filters->Where_in( $this->alias.$this->primary_key, $item_ids );

        return $this->Update_by_filters( $filters );
    }

    //</editor-fold>
    
    /*******************************************************************************************************************
     * FRONT FUNCTIONS
     *******************************************************************************************************************/
    //<editor-fold desc="FRONT FUNCTIONS">

    /**
     * @method  Get_list_where_is_enabled
     * @access  public
     * @desc    Get list of active albums, limited with param limit
     * @author  Cousin Bela
     *
     * @param   int                         $limit                      - how many products to return
     * @param   int                         $limit_from                 - start the list from
     * @param   array                       $search_filters             - array with search filters
     *
     * @version 1.0.0
     * @return  MDB_Result
     */
    public function Get_list_where_is_enabled( $limit = 50, $limit_from = 0, $search_filters = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters();

        $filters->Where_equal( $alias."d.is_enabled", "1" );

        $this->Process_search( $filters, $search_filters );

        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );
        $filters->Limit( $limit, $limit_from );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Count_where_is_enabled
     * @access  public
     * @desc    Count albums
     * @author  Cousin Bela
     *
     * @param   array                       $search_filters             - array with search filters
     *
     * @version 1.0.0
     * @return  int
     */
    public function Count_where_is_enabled( $search_filters = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters_for_count();

        $filters->Where_equal( $alias."d.is_enabled", "1" );

        $this->Process_search( $filters, $search_filters );

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Get_list_by_language
     * @access  public
     * @desc    Get list of active items by languages, limited with param limit
     * @author  Cousin Bela
     *
     * @param   string                      $language_code              - language code
     * @param   int                         $limit                      - how many course categories to return
     * @param   int                         $limit_from                 - start the list from
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  MDB_Result
     */
    public function Get_list_by_language( $language_code, $limit = 50, $limit_from = 0, $search_filters = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters( $language_code );

        $filters->Where_equal( $alias."d.is_enabled", "1" );
        $this->Process_search( $filters, $search_filters );

        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );
        $filters->Limit( $limit, $limit_from );

        $this->db->Set_cache_to_file();
        $result = $this->Get_list_by_filters( $filters );
        $this->db->Set_cache_back_to_last_state();

        return $result;
    }

    /**
     * @method  Count_by_language
     * @access  public
     * @desc    Count active items by language
     * @author  Cousin Bela
     *
     * @param   string                      $language_code              - language code
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  int
     */
    public function Count_by_language( $language_code, $search_filters = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters_for_count( $language_code );

        $filters->Where_equal( $alias."d.is_enabled", "1" );

        $this->Process_search( $filters, $search_filters );

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Count_for_admin_by_language
     * @access  public
     * @desc    Count all items by language
     * @author  Cousin Bela
     *
     * @param   string                      $language_code              - language code
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  int
     */
    public function Count_for_admin_by_language( $language_code, $search_filters = null )
    {
        $filters = $this->Get_filters_for_count( $language_code );

        $this->Process_search( $filters, $search_filters );

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Get_list_by_language_and_domain_id
     * @access  public
     * @desc    Get list of active items by languages and domain, limited with param limit
     * @author  Cousin Bela
     *
     * @param   string                      $language_code              - language code
     * @param   int                         $domain_id                  - id of domain to filter items
     * @param   int                         $limit                      - how many course categories to return
     * @param   int                         $limit_from                 - start the list from
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  MDB_Result
     */
    public function Get_list_by_language_and_domain_id( $language_code, $domain_id = 0, $limit = 50, $limit_from = 0, $search_filters = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters( $language_code );

        if( is_numeric( $domain_id ) )
        {
            $filters->Where( "( ".$this->alias."domain_id = 0 OR ".$this->alias."domain_id = ".$domain_id." )" );
        }

        $filters->Where_equal( $alias."d.is_enabled", "1" );

        $this->Process_search( $filters, $search_filters );

        $filters->Order_by( $this->alias."modify_date", "DESC" );
        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );
        
        $filters->Limit( $limit, $limit_from );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Count_by_language_and_domain_id
     * @access  public
     * @desc    Count active items by language and domain
     * @author  Cousin Bela
     *
     * @param   string                      $language_code              - language code
     * @param   int                         $domain_id                  - id of domain to filter items
     * @param   mixed                       $search_filters             - an array or string with search filters
     *
     * @version 1.0.0
     * @return  int
     */
    public function Count_by_language_and_domain_id( $language_code, $domain_id = 0, $search_filters = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters_for_count( $language_code );

        if( is_numeric( $domain_id ) )
        {
            $filters->Where( "( ".$this->alias."domain_id = 0 OR ".$this->alias."domain_id = ".$domain_id." )" );
        }

        $filters->Where_equal( $alias."d.is_enabled", "1" );

        $this->Process_search( $filters, $search_filters );

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Get_list_by_parent_id
     * @access  public
     * @desc    Get a list of categories by parent_id
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id for this category list
     * @param   int                         $offset                     - return items from
     * @param   int                         $limit                      - how many items to return
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_by_parent_id( $parent_id = 0, $offset = null, $limit = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters();

        $filters->Where_equal( $this->alias."parent_id", $parent_id );
        $filters->Where_equal( $alias."d.is_enabled", "1" );

        $filters->Limit( $limit, $offset );
        $filters->Order_by( $this->alias.$this->primary_key );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Count_by_parent_id
     * @access  public
     * @desc    Count categories by parent_id
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id for this category list
     *
     * @version 1.0
     * @return  int
     */
    public function Count_by_parent_id( $parent_id = 0 )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters_for_count();

        $filters->Where_equal( $this->alias."parent_id", $parent_id );
        $filters->Where_equal( $alias."d.is_enabled", "1" );

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Get_list_by_parent_id_and_domain_id
     * @access  public
     * @desc    Get a list of categories by parent_id
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id for this category list
     * @param   int                         $domain_id                  - id of domain to filter items
     * @param   int                         $offset                     - return items from
     * @param   int                         $limit                      - how many items to return
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_by_parent_id_and_domain_id( $parent_id = 0, $domain_id = 0, $offset = null, $limit = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters();

        $filters->Where_equal( $this->alias."parent_id", $parent_id );
        $filters->Where_equal( $alias."d.is_enabled", "1" );

        if( is_numeric( $domain_id ) )
        {
            $filters->Where( "( ".$this->alias."domain_id = 0 OR ".$this->alias."domain_id = ".$domain_id." )" );
        }

        $filters->Limit( $limit, $offset );
        $filters->Order_by( $this->alias.$this->primary_key );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Count_by_parent_id_and_domain_id
     * @access  public
     * @desc    Count article categories by parent_id
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id for this category list
     * @param   int                         $domain_id                  - id of domain to filter items
     *
     * @version 1.0
     * @return  int
     */
    public function Count_by_parent_id_and_domain_id( $parent_id = 0, $domain_id = 0 )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters_for_count();

        $filters->Where_equal( $this->alias."parent_id", $parent_id );
        $filters->Where_equal( $alias."d.is_enabled", "1" );

        if( is_numeric( $domain_id ) )
        {
            $filters->Where( "( ".$this->alias."domain_id = 0 OR ".$this->alias."domain_id = ".$domain_id." )" );
        }

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Get_list_by_parent_id_and_language_and_domain_id
     * @access  public
     * @desc    Get a list of categories by parent_id
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id for this category list
     * @param   string                      $language_code              - language code
     * @param   int                         $domain_id                  - id of domain to filter items
     * @param   int                         $offset                     - return items from
     * @param   int                         $limit                      - how many items to return
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_by_parent_id_and_language_and_domain_id( $parent_id = 0, $language_code, $domain_id = 0, $offset = null, $limit = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters( $language_code );

        $filters->Where_equal( $this->alias."parent_id", $parent_id );
        $filters->Where_equal( $alias."d.is_enabled", "1" );

        if( is_numeric( $domain_id ) )
        {
            $filters->Where( "( ".$this->alias."domain_id = 0 OR ".$this->alias."domain_id = ".$domain_id." )" );
        }

        $filters->Order_by( $this->alias."modify_date", "DESC" );
        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );

        $filters->Limit( $limit, $offset );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Count_by_parent_id_and_language_and_domain_id
     * @access  public
     * @desc    Get a list of categories by parent_id
     * @author  Cousin Bela
     *
     * @param   int                         $parent_id                  - parent id for this category list
     * @param   string                      $language_code              - language code
     * @param   int                         $domain_id                  - id of domain to filter items
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Count_by_parent_id_and_language_and_domain_id( $parent_id = 0, $language_code, $domain_id = 0 )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters_for_count( $language_code );

        $filters->Where_equal( $this->alias."parent_id", $parent_id );
        $filters->Where_equal( $alias."d.is_enabled", "1" );

        if( is_numeric( $domain_id ) )
        {
            $filters->Where( "( ".$this->alias."domain_id = 0 OR ".$this->alias."domain_id = ".$domain_id." )" );
        }

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Get_list_by_order_id
     * @access  public
     * @desc    Get last orders, limited with param limit
     * @author  Cousin Bela
     *
     * @param   int                         $order_id                   - id of order to find
     * @param   int                         $limit                      - how many orders to return
     *
     * @version 1.0
     * @return  MDB_Result
     */
    public function Get_list_by_order_id( $order_id, $limit = 0 )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( $this->alias."order_id", $order_id );
        $filters->Limit( $limit );
        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );

        return $this->Get_list_by_filters( $filters );
    }

    /**
     * @method  Get_list_for_ajax_search_where_is_enabled
     * @access  public
     * @desc    Get list of items, limited
     * @author  Cousin Bela
     *
     * @param   mixed                       $search_filters             - an array or string with search filters
     * @param   string                      $language_code              - selected language code
     * @param   int                         $limit                      - how many products to return
     *
     * @version 1.0.0
     * @return  array
     */
    public function Get_list_for_ajax_search_where_is_enabled( $search_filters, $language_code, $limit = 50 )
    {
        $filters    = $this->Get_filters();
        $alias      = trim( $this->alias, "." );

        $this->Process_search( $filters, $search_filters );
        $filters->Where_equal( $alias."d.language_code", $language_code );
        $filters->Where_equal( $alias."d.is_enabled", "1" );

        $filters->Order_by( $this->alias.$this->primary_key );
        $filters->Limit( $limit );

        $items = $this->Get_list_by_filters( $filters );

        if( empty( $items ) )
        {
            return array();
        }

        $helper_name = String_helper::Get_helper_name( $this->table_name );
        
        $result = array();
        foreach( $items->Get_result() as $item )
        {
            $result []= array(
                "label"     => $item->name,
                "value"     => $item->name,
                "item_id"   => $item->id,
                "link"      => $helper_name::Get_url( $item ),
            );
        }

        return $result;
    }

    public function Count_viewed_for_admin()
    {
        $filters = new MDB_Filter();

        $filters->Select_sum( "viewed", "nr_viewed" );
        $filters->Where_equal( "is_deleted", 0 );

        $result = $this->Get_by_filters( $filters );

        return intval( $result->nr_viewed );
    }

    /**
     * @method  Load_breadcrumbs
     * @access  public
     * @desc    Get all categories by parent id
     * @author  Cousin Bela
     *
     * @param   int                         $category_id                - category id to select subcategories start from
     * @param   string                      $language_code              - language of content to get
     *
     * @version 1.0
     * @return  array
     */
    public function Load_breadcrumbs( $category_id, $language_code = "" )
    {
        $filters = $this->Get_filters( $language_code );

        $filters->Where_equal( $this->alias.$this->primary_key, $category_id );
        $filters->Order_by( $this->alias.$this->primary_key );

        $category_details = $this->Get_by_filters( $filters );

        if( empty( $category_id ) || ! $category_details )
        {
            return FALSE;
        }

        $result = array();

        if( $category_details->parent_id != $category_details->id )
        {
            $parent_categories = $this->Load_breadcrumbs( $category_details->parent_id, $language_code );

            if( $parent_categories != FALSE )
            {
                $result = $parent_categories;
            }
        }

        $result []= $category_details;

        return $result;
    }

    /**
     * @method  Get_by_item_id
     * @access  public
     * @desc    Get item by id for admin navigations
     * @author  Cousin Bela
     *
     * @param   int                         $item_id                    - id of item to return
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_id( $item_id )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( $this->alias.$this->primary_key, $item_id );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_by_item_id_and_details_id
     * @access  public
     * @desc    Get item data with details for admin navigations
     * @author  Cousin Bela
     *
     * @param   int                         $item_id                    - id of album to return
     * @param   int                         $details_id                 - id of album details
     * @param   array                       $search_filters             - array with search filters
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_id_and_details_id( $item_id, $details_id, $search_filters = null  )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters();

        $filters->Select( $alias."d.viewed", "details_viewed" );
        // This check is needed because when the category does not have details id will be a cycle that never ends
        if( ! empty( $details_id ) )
        {
            $filters->Where_equal( $alias."d.id", $details_id );
        }
        $filters->Where_equal( $this->alias.$this->primary_key, $item_id );

        $this->Process_search( $filters, $search_filters );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_by_item_id_and_language
     * @access  public
     * @desc    Get item with album details for admin navigations
     * @author  Cousin Bela
     *
     * @param   int                         $item_id                    - id of album to return
     * @param   string                      $language_code              - language for filter products
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_id_and_language( $item_id, $language_code )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters( $language_code );

        $filters->Where_equal( $this->alias.$this->primary_key, $item_id );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @return Common_model
     */
    public function Get_by_item_id_where_is_enabled( $id )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters();

        $filters->Where_equal( $this->alias.$this->primary_key, $id );
        $filters->Where_equal( $alias."d.is_enabled", "1" );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_by_item_code
     * @access  public
     * @desc    Get coupon data by code
     * @author  Cousin Bela
     *
     * @param   string                      $item_code                  - code of product to return
     * @param   bool                        $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_code( $item_code, $force_from_db = FALSE )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( $this->alias."code", $item_code );

        return $this->Get_by_filters( $filters, $force_from_db );
    }

    /**
     * @method  Get_by_item_code_where_is_enabled
     * @access  public
     * @desc    Get item by language code
     * @author  Cousin Bela
     *
     * @param   string                      $code                       - the language code for filter elements
     * @param   array                       $search_filters             - array with search filters
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_code_where_is_enabled( $code, $search_filters = null )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters();

        $filters->Where_equal( $this->alias."code", $code );
        $filters->Where_equal( $alias."d.is_enabled", "1" );

        $this->Process_search( $filters, $search_filters );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_by_item_code_and_language
     * @access  public
     * @desc    Get article data with article details by code and language
     * @author  Cousin Bela
     *
     * @param   int                         $item_code                  - code of article to return
     * @param   string                      $language_code              - language for filter products
     * @param   bool                        $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_code_and_language( $item_code, $language_code = "", $force_from_db = FALSE )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters( $language_code );

        $filters->Where_equal( $this->alias."code", $item_code );

        return $this->Get_by_filters( $filters, $force_from_db );
    }

    /**
     * @method  Get_by_item_code_and_language_where_is_enabled
     * @access  public
     * @desc    Get article data with article details by code and language where is enabled
     * @author  Cousin Bela
     *
     * @param   int                         $item_code                  - code of article to return
     * @param   string                      $language_code              - language for filter products
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_code_and_language_where_is_enabled( $item_code, $language_code )
    {
        $alias      = trim( $this->alias, "." );
        $filters    = $this->Get_filters( $language_code );

        $filters->Where_equal( $alias."d.is_enabled", 1 );
        $filters->Where_equal( $this->alias."code", $item_code );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_by_item_code_and_language_and_domain
     * @access  public
     * @desc    Get item data with item details for the front side
     * @author  Cousin Bela
     *
     * @param   int                         $item_code                  - code of item to return
     * @param   string                      $language_code              - language for filter products
     * @param   int                         $domain_id                  - domain id
     * @param   bool                        $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_code_and_language_and_domain( $item_code, $language_code, $domain_id = 0, $force_from_db = FALSE )
    {
        $filters = $this->Get_filters( $language_code );

        $filters->Where_equal( $this->alias."code", $item_code );
        $filters->Where_equal( $this->alias."domain_id", $domain_id );

        $item = $this->Get_by_filters( $filters );

        if( ! empty( $item->is_enabled )
            || ( ! empty( $item ) && ! $item->is_enabled && ( User_helper::Is_admin_logged_in() || $force_from_db ) ) )
        {
            return $item;
        }

        if( empty( $item->is_enabled ) && ! empty( $domain_id ) )
        {
            return $this->Get_by_item_code_and_language_and_domain( $item_code, $language_code, 0, $force_from_db );
        }

        return FALSE;
    }

    /**
     * @method  Get_by_item_name
     * @access  public
     * @desc    Get coupon data by name
     * @author  Cousin Bela
     *
     * @param   string                      $item_name                  - name of item to return
     * @param   bool                        $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_name( $item_name, $force_from_db = FALSE )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( $this->alias."name", $item_name );

        return $this->Get_by_filters( $filters, $force_from_db );
    }

    /**
     * @method  Get_by_item_name_where_is_enabled
     * @access  public
     * @desc    Get coupon data by name
     * @author  Cousin Bela
     *
     * @param   string                      $item_name                  - name of item to return
     * @param   bool                        $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_item_name_where_is_enabled( $item_name, $force_from_db = FALSE )
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( $this->alias."name", $item_name );
        $filters->Where_equal( $this->alias."is_enabled", "1" );

        return $this->Get_by_filters( $filters, $force_from_db );
    }

    /**
     * @method  Get_first_by_sort_order_where_is_enabled
     * @access  public
     * @desc    Get first payment status by sort order for new orders
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_first_by_sort_order_where_is_enabled()
    {
        $filters = $this->Get_filters();

        $filters->Where_equal( $this->alias."is_enabled", "1" );

        $filters->Order_by( $this->alias."sort_order", "ASC" );
        $filters->Limit( 1 );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_by_order_id_and_type
     * @access  public
     * @desc    Get order user data with order details
     * @author  Cousin Bela
     *
     * @param   int                         $item_id                    - id of order to return
     * @param   string                      $type                       - type of data
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_by_order_id_and_type( $item_id, $type = "" )
    {
        if( empty( $type ) )
        {
            $type = "invoice";
        }

        $filters = $this->Get_filters();

        $filters->Where_equal( $this->alias."order_id", $item_id );
        $filters->Where_equal( $this->alias."type", $type );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_next_item
     * @access  public
     * @desc    Get next item for admin listing
     * @author  Csenteri Attila
     *
     * @param   int                         $item_id                    - id of item
     * @param   int                         $details_id                 - id of item details
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_next_item( $item_id, $details_id = 0 )
    {
        $filters = $this->Get_filters();

        if( ! empty( $details_id ) )
        {
            $alias = trim( $this->alias, "." );

            $filters->Where_is_lesser_than( $alias."d.id", $details_id );

            $filters->Order_by( $alias."d.id", "DESC" );
        }
        else
        {
            $filters->Where_is_lesser_than( $this->alias . $this->primary_key, $item_id );
        }

        $filters->Order_by( $this->alias.$this->primary_key, "DESC" );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_prev_item
     * @access  public
     * @desc    Get previous item for admin listing
     * @author  Csenteri Attila
     *
     * @param   int                         $item_id                    - id of item
     * @param   int                         $details_id                 - id of item details
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_prev_item( $item_id, $details_id = 0 )
    {
        $filters = $this->Get_filters();

        if( ! empty( $details_id ) )
        {
            $alias = trim( $this->alias, "." );

            $filters->Where_is_greater_than( $alias."d.id", $details_id );

            $filters->Order_by( $alias."d.id", "ASC" );
        }
        else
        {
            $filters->Where_is_greater_than( $this->alias . $this->primary_key, $item_id );
        }

        $filters->Order_by( $this->alias.$this->primary_key, "ASC" );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Get_last_insert_row
     * @access  public
     * @desc    Get last inserted row
     * @author  Csenteri Attila
     *
     * @version 1.0.0
     * @return  Common_model
     */
    public function Get_last_insert_row()
    {
        $filters = $this->Get_filters();

        $filters->Order_by( $this->alias.$this->primary_key, "ASC" );

        return $this->Get_by_filters( $filters );
    }

    /**
     * @method  Count_by_insert_date
     * @access  public
     * @desc    Count rows from insert date
     * @author  Csenteri Attila
     *
     * @param   string                      $insert_date                - the insert date for items
     * @param   int                         $domain_id                  - id of the domain to be verified
     *
     * @version 1.0.0
     * @return  int
     */
    public function Count_by_insert_date( $insert_date, $domain_id = 0 )
    {
        $filters = $this->Get_filters_for_count();

        $filters->Where_is_greater_or_equal_than( $this->alias."insert_date", $insert_date );

        if( ! empty( $domain_id ) )
        {
            $filters->Where_equal( $this->alias."domain_id", $domain_id );
        }

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Count_by_column_from_start_date
     * @access  public
     * @desc    Count platform by the type of the platform
     * @author  Csenteri Attila
     *
     * @param   string                      $column_name                - name of the column
     * @param   string                      $column_value               - value of the column
     * @param   datetime                    $date_start                 - the start date for count
     *
     * @version 1.0.0
     * @return  int
     */
    public function Count_by_column_from_start_date( $column_name, $column_value = "", $date_start = null )
    {
        $filters = $this->Get_filters_for_count();

        $filters->Where_equal( $this->alias.$column_name, $column_value );

        if( ! empty( $date_start ) )
        {
            $filters->Where_is_greater_or_equal_than( $this->alias."insert_date", $date_start );
        }

        return $this->Count_by_filters( $filters );
    }

    /**
     * @method  Count_views_by_domain
     * @access  public
     * @desc    count views by domain
     * @author  Csenteri Attila
     *
     * @param   int                         $domain_id                  - primary key of the domain
     *
     * @version 1.0.0
     * @return  int
     */
    public function Count_views_by_domain( $domain_id = 0  )
    {
        $filters = $this->Get_filters();

        $filters->Reset_select();
        $filters->Select_sum( $this->alias."viewed", "total_views" );

        if( ! empty( $domain_id ) )
        {
            $filters->Where_equal( $this->alias . "domain_id", $domain_id );
        }

        $result = $this->Get_by_filters( $filters );


        if( empty( $result ) )
        {
            return 0;
        }

        return $result->total_views;
    }

    //</editor-fold>
}

/* End of file Common_model.php */
/* Location: ./Core/Libraries/ */