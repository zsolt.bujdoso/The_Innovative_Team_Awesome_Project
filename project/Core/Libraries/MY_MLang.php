<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MY_MLang
 *
 * Load and set languages from Database
 *
 * @version     1.0.0
 */
class MY_MLang extends MLang
{
    private $url;
    private $domain_id      = 0;
    private $domain_details = null;

    public function __construct()
    {
        $this->Set_language();
        $this->Set_default_language();
        $this->Verify_language_code();
        $this->Load_languages();

        $this->language         = $this->languages[$this->language_code]["name"];
        $this->language_details = $this->languages[$this->language_code];

        $this->Load_language_variables();
        $this->Load_language_variables_from_module();
    }

    public function Set_language()
    {
        $this->Load_default_language_from_settings();
        $this->Try_to_get_lang_from_url();
        $this->Try_to_get_lang_from_GET();
    }

    private function Load_default_language_from_settings()
    {
        if( ! empty( $this->language_code ) )
        {
            return;
        }

        $default_language = Main_settings_model::Get_model()->Get_by_item_code_and_domain(
            "default_language",
            $this->domain_id
        );

        // If no default language set in database for this domain, try it for all
        if( empty( $default_language ) && $this->domain_id )
        {
            $default_language = Main_settings_model::Get_model()->Get_by_item_code_and_domain( "default_language", "0" );
        }

        if( ! empty( $default_language ) )
        {
            App()->config->language["code"] = $default_language->value;
            $this->language_code            = $default_language->value;
        }
    }

    private function Try_to_get_lang_from_url()
    {
        $this->url = $this->Calculate_url();

        if( preg_match( "/^([a-zA-Z]{2})\/(.*)/", $this->url ) || preg_match( "/^([a-zA-Z]{2}\/?)$/", $this->url ) )
        {
            $this->language_code = preg_replace( "/^([a-zA-Z]{2})(.*)/", "$1", $this->url );
        }
    }

    protected function Calculate_url()
    {
        $url                = $_SERVER["REQUEST_URI"];
        $script_file_path   = @dirname( $_SERVER["SCRIPT_FILENAME"] )."/";
        $script_path        = @str_replace( $_SERVER["DOCUMENT_ROOT"], "", $script_file_path );

        if( ! empty( $script_path ) && $script_path != "/" )
        {
            $url = str_replace( $script_path, "", $url );
        }

        return ltrim( $url, "/" );
    }

    private function Try_to_get_lang_from_GET()
    {
        if( isset( $_GET["lang"] ) )
        {
            $this->language_code = $_GET["lang"];
        }
    }

    private function Set_default_language()
    {
        if( ! empty( $this->language_code )  )
        {
            return;
        }

        $this->language_code = App()->config->language["default"];
    }

    private function Verify_language_code()
    {
        $language_details = Main_languages_model::Get_model()->Get_by_item_code_and_language(
            $this->language_code,
            $this->language_code
        );

        // If language does not exist or it's not enabled
        if( empty( $language_details ) )
        {
            throw new MLang_exception( "Language with code '".$this->language_code."' is missing" );
        }

        if( ! $this->Check_if_language_is_enabled( $language_details ) )
        {
            throw new MLang_exception( "Language with code '".$this->language_code."' is not enabled" );
        }
    }

    private function Check_if_language_is_enabled( & $language_details )
    {
        if( ! $language_details->is_enabled )
        {
            return FALSE;
        }

        $is_enabled = TRUE;

        if( ! $language_details->is_enabled_on_front )
        {
            $is_enabled = FALSE;
        }

        // enable on admin side
        if( preg_match( "/^([a-zA-Z]{2}\/)?admin\/?(.*)/", $this->url ) )
        {
            $is_enabled = TRUE;
        }

        return $is_enabled;
    }

    private function Load_languages()
    {
        if( preg_match( "/^([a-zA-Z]{2}\/?)admin\/?(.*)/", $this->url ) )
        {
            $languages = Main_languages_model::Get_model()->Get_list_by_language( $this->language_code );
        }
        // get languages for front
        else
        {
            $languages = Main_languages_model::Get_model()->Get_list_by_language_where_is_enabled_on_front( $this->language_code );
        }

        if( ! $languages )
        {
            return;
        }

        foreach( $languages->Get_result() as $language )
        {
            $this->languages[$language->code]   = array(
                "folder"                => strtolower( $language->name ),
                "name"                  => $language->name,
                "image"                 => $language->image,
                "is_enabled_on_front"   => $language->is_enabled_on_front,
                "date_format"           => $language->date_format
            );
        }

        App()->data["languages"] = $languages;
    }
}