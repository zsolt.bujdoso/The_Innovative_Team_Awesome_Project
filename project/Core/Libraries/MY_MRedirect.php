<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MY_MRedirect
 *
 * Overwrite redirect functions
 *
 * @version     1.0.0
 */
class MY_MRedirect extends MRedirect
{
    /**
     * @method  To_site_url
     * @access  public
     * @desc    This method redirect the page to a specified URL on this site
     * @author  Zoltan Jozsa
     *
     * @param   string                      $url                        - the url to redirect to
     *
     * @version 1.0.0
     */
    public function To_site_url( $url )
    {
        $url = ltrim( $url, "/" );

        if( ! preg_match( "/^(http\:\/\/|https\:\/\/|www\.).*/i", $url ) )
        {
            $url = MY_Url_helper::Site_url().$url;
        }

        return $this->Redirect_to_url( $url );
    }

    /**
     * @method  To_last_url_or_home
     * @access  public
     * @desc    This method redirect the page to last URL or home page
     * @author  Zoltan Jozsa
     *
     * @version 1.0.0
     */
    public function To_last_url_or_home()
    {
        $url = Server_helper::Get_last_visited_url();

        if( ! String_helper::Is_link( $url ) )
        {
            $url = MY_Url_helper::Site_url();
        }

        $this->Redirect_to_url( $url );
    }

    /**
     * @method  To_last_url_or_home
     * @access  public
     * @desc    This method redirect the page to last URL or a specified URL on this site
     * @author  Zoltan Jozsa
     *
     * @param   string                      $url                        - the url to redirect to
     *
     * @version 1.0.0
     */
    public function To_last_url( $url = "" )
    {
        $last_url = Server_helper::Get_last_visited_url();

        if( String_helper::Is_link( $url ) )
        {
            $last_url = $url;
        }

        $this->Redirect_to_url( $last_url );
    }
}