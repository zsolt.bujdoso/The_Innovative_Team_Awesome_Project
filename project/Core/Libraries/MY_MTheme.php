<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MTheme
 *
 * @version     1.0.0
 */
class MY_MTheme extends MTheme
{
    private $domain_id      = 0;
    public $domain_details  = null;

    public function __construct()
    {
        $this->default_path = BASEPATH."Themes".DIRECTORY_SEPARATOR;
        $type               = "front";
        $controller         = App()->router->Get_controller();

        if( preg_match( "/^admin(.*)/", $controller ) || preg_match( "/^([a-z]{2}\/)?admin(.*)/", $_SERVER["QUERY_STRING"] ) )
        {
            $type = "admin";
        }

        $this->Set_theme_for_type( $type );
    }

    public function Set_theme_for_type( $type )
    {
        // Get theme from domain settings
        if( ! empty( $theme_id ) )
        {
            $theme = Main_themes_model::Get_model()->Get_by_primary_key_and_type( $theme_id, $type );
        }

        // Get theme by default for selected type
        if( empty( $theme ) )
        {
            $theme = Main_themes_model::Get_model()->Get_by_type_where_is_default( $type );
        }

        // If no theme set it to test
        if( empty( $theme ) )
        {
            $this->Set_theme( "test" );
            return;
        }

        $this->default_path = BASEPATH."Themes".DIRECTORY_SEPARATOR;
        $sub_directory      = $type.DIRECTORY_SEPARATOR;
        $this->default_path .= $sub_directory;
        $this->path         = $this->default_path;

        $this->Set_theme( $theme->code );
        $this->Set_theme_id( $theme->id );
    }
}

/* End of file MY_MTheme.php */
/* Location: ./project/core/Libraries/ */