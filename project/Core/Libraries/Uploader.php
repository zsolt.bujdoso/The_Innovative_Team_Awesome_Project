<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Uploader
{
    protected static $available_types = array(
        "doc"   => 1,
        "docx"  => 1,
        "xls"   => 1,
        "odt"   => 1,
        "pdf"   => 1,
        "txt"   => 1,
        "gif"   => 1,
        "png"   => 1,
        "jpg"   => 1,
        "jpeg"  => 1,
        "zip"   => 1,
        "sql"   => 1,
        "ico"   => 1,
    );

    protected static $error_message = "";

    /**
     * @method  Upload
     * @access  public
     * @desc    Upload files for gallery
     * @author  Cousin Bela
     *
     * @param   string                      $files_name           - the code of the item
     * @param   string                      $path_to              - location to
     *
     * @version 1.0
     * @return  bool
     */
    protected static function Upload( $files_name, $path_to )
    {
        if( empty( $_FILES[$files_name] ) )
        {
            return FALSE;
        }

        if( isset( $_FILES[$files_name]["tmp_name"] ) && is_array( $_FILES[$files_name]["tmp_name"] ) )
        {
            $new_file_name = "";
            foreach( $_FILES[$files_name]["name"] as $key => $file )
            {
                if( $_FILES[$files_name]["error"][$key] )
                {
                    continue;
                }

                $name           = $_FILES[$files_name]["name"][$key];
                $tmp_name       = $_FILES[$files_name]["tmp_name"][$key];
                $ext            = pathinfo( $name, PATHINFO_EXTENSION );

                // If extension can't be saved
                if( ! array_key_exists( strtolower( $ext ), self::$available_types ) )
                {
                    self::$error_message = sprintf(
                        App()->lang->Get( "Invalid_file_type" ),
                        $ext,
                        implode( ",", self::$available_types )
                    );

                    return FALSE;
                }

                // Upload the file and get the file name
                $new_file_name  = Upload_helper::Move_uploaded_file_with_auto_name( $tmp_name, $path_to, $name );

                // If no file name, than upload wasn't success
                if( ! $new_file_name )
                {
                    continue;
                }
            }

            return ( $new_file_name ? $new_file_name : FALSE );
        }

        if( ! empty( $_FILES[$files_name]["error"] ) )
        {
            self::$error_message = sprintf(
                App()->lang->Get( "Upload_error" ),
                $_FILES[$files_name]["error"]
            );

            return FALSE;
        }

        $name   = $_FILES[$files_name]["name"];
        $ext    = pathinfo( $name, PATHINFO_EXTENSION );

        // If extension can't be saved
        if( ! array_key_exists( $ext, self::$available_types ) )
        {
            self::$error_message = sprintf(
                App()->lang->Get( "Invalid_file_type" ),
                implode( ",", self::$available_types )
            );

            return FALSE;
        }

        // Upload the file and get the file name
        $file_name  = Upload_helper::Move_uploaded_file_with_auto_name(
            $_FILES[$files_name]["tmp_name"], $path_to, $name
        );

        // If no file name, than upload wasn't success
        if( ! $file_name )
        {
            self::$error_message = sprintf(
                App()->lang->Get( "Upload_error" ),
                $name
            );

            return FALSE;
        }

        return $file_name;
    }

    /**
     * @method  Upload_documents
     * @access  public
     * @desc    Upload files for gallery
     * @author  Cousin Bela
     *
     * @param   string                      $gallery_type               - type of the gallery, ex: pages, products
     * @param   string                      $gallery_id                 - the code of the item
     * @param   string                      $file_type                  - the file type for saving
     * @param   bool                        $save                       - save the filename into database or not
     *
     * @version 1.0
     * @return  bool
     */
    public static function Upload_documents( $gallery_type, $gallery_id, $file_type = "", $save = FALSE )
    {
        $files_name = "files";

        if( empty( $_FILES[$files_name] ) )
        {
            return FALSE;
        }

        $path_to = BASEPATH."upload/".$gallery_type."/".$gallery_id."/".$file_type."s/";

        if( isset( $_FILES[$files_name]["tmp_name"] ) && is_array( $_FILES[$files_name]["tmp_name"] ) )
        {
            $new_file_name = "";
            foreach( $_FILES[$files_name]["name"] as $key => $file )
            {
                $file_details = array(
                    "name"      => $_FILES[$files_name]["name"][$key],
                    "tmp_name"  => $_FILES[$files_name]["tmp_name"][$key],
                    "error"     => $_FILES[$files_name]["error"][$key],
                );

                $new_file_name = self::Upload_file( $file_details, $path_to, $gallery_type, $gallery_id, $file_type, $save );

                /*if( $_FILES[$files_name]["error"][$key] )
                {
                    self::$error_message = sprintf(
                        App()->lang->Get( "Upload_error" ),
                        $_FILES[$files_name]["name"]
                    );

                    continue;
                }

                $name           = $_FILES[$files_name]["name"][$key];
                $tmp_name       = $_FILES[$files_name]["tmp_name"][$key];
                $ext            = pathinfo( $name, PATHINFO_EXTENSION );

                // If extension can't be saved
                if( ! array_key_exists( strtolower( $ext ), self::$available_types ) )
                {
                    self::$error_message = sprintf(
                        App()->lang->Get( "Invalid_file_type" ),
                        $ext,
                        implode( ", ", array_keys( self::$available_types ) )
                    );

                    return FALSE;
                }

                // Upload the file and get the file name
                $new_file_name  = Upload_helper::Move_uploaded_file_with_auto_name( $tmp_name, $path_to, $name );

                // If no file name, than upload wasn't success
                if( ! $new_file_name )
                {
                    continue;
                }

                if( $save )
                {
                    if( self::Save_file( $name, $new_file_name, $path_to, $gallery_type, $gallery_id, $file_type ) )
                    {
                        return $new_file_name;
                    }

                    self::$error_message = App()->lang->Get( "Save_error_on_upload" );

                    return FALSE;
                }*/
            }

            return ( $new_file_name ? $new_file_name : FALSE );
        }

        return self::Upload_file( $_FILES[$files_name], $path_to, $gallery_type, $gallery_id, $file_type, $save );

        /*if( ! empty( $_FILES[$files_name]["error"] ) )
        {
            self::$error_message = sprintf(
                App()->lang->Get( "Upload_error" ),
                $_FILES[$files_name]["error"]
            );

            return FALSE;
        }

        $name   = $_FILES[$files_name]["name"];
        $ext    = pathinfo( $name, PATHINFO_EXTENSION );

        // If extension can't be saved
        if( ! array_key_exists( $ext, self::$available_types ) )
        {
            self::$error_message = sprintf(
                App()->lang->Get( "Invalid_file_type" ),
                $name,
                implode( ", ", array_keys( self::$available_types ) )
            );

            return FALSE;
        }

        // Upload the file and get the file name
        $file_name  = Upload_helper::Move_uploaded_file_with_auto_name(
            $_FILES[$files_name]["tmp_name"], $path_to, $name
        );

        // If no file name, than upload wasn't success
        if( ! $file_name )
        {
            self::$error_message = sprintf(
                App()->lang->Get( "Upload_error" ),
                $name
            );

            return FALSE;
        }

        if( $save )
        {
            if( self::Save_file( $name, $file_name, $path_to, $gallery_type, $gallery_id, $file_type ) )
            {
                return $file_name;
            }

            self::$error_message = App()->lang->Get( "Save_error_on_upload" );

            return FALSE;
        }

        return TRUE;*/
    }

    private static function Upload_file( $file_details, $path_to, $gallery_type, $gallery_id, $file_type = "", $save = FALSE )
    {
        if( ! empty( $file_details["error"] ) )
        {
            self::$error_message = sprintf(
                App()->lang->Get( "Upload_error" ),
                $file_details["error"]
            );

            return FALSE;
        }

        $name       = $file_details["name"];
        $tmp_name   = $file_details["tmp_name"];
        $ext        = pathinfo( $name, PATHINFO_EXTENSION );

        // If extension can't be saved
        if( ! array_key_exists( $ext, self::$available_types ) )
        {
            self::$error_message = sprintf(
                App()->lang->Get( "Invalid_file_type" ),
                $name,
                implode( ", ", array_keys( self::$available_types ) )
            );

            return FALSE;
        }

        // Upload the file and get the file name
        $file_name  = Upload_helper::Move_uploaded_file_with_auto_name( $tmp_name, $path_to, $name );

        // If no file name, than upload wasn't success
        if( ! $file_name )
        {
            self::$error_message .= sprintf(
                App()->lang->Get( "Upload_error" ),
                $name
            )."\r\n";

            return FALSE;
        }

        if( $save )
        {
            if( self::Save_file( $name, $file_name, $path_to, $gallery_type, $gallery_id, $file_type ) )
            {
                return $file_name;
            }

            self::$error_message .= App()->lang->Get( "Save_error_on_upload" )."\r\n";

            return FALSE;
        }

        return $file_name;
    }

    /**
     * @method  Save_file
     * @access  public
     * @desc    Save file for gallery
     * @author  Cousin Bela
     *
     * @param   string                      $original_name              - original file name
     * @param   string                      $new_file_name              - file name
     * @param   string                      $path_to                    - path for the selected file
     * @param   string                      $gallery_type               - type of the gallery, ex: pages, products
     * @param   string                      $gallery_id                 - the code of the item
     * @param   int                         $document_id                - id of an existing document
     *
     * @version 1.0
     * @return  bool
     */
    private static function Save_file( $original_name, $new_file_name, $path_to, $gallery_type, $gallery_id, $file_type, $document_id = null )
    {
        $path_to = str_replace( BASEPATH, "", $path_to );

        $data   = array(
            "item_id"       => $gallery_id,
            "item_type"     => $gallery_type,
            "type"          => $file_type,
            "name"          => $new_file_name,
            "original_name" => $original_name,
            "path"          => $path_to,
            "insert_date"   => Date_helper::Get_date_time(),
        );

        if( empty( $document_id ) )
        {
            return Documents_model::Get_model()->Insert( $data );
        }

        return Documents_model::Get_model()->Update_by_primary_key( $data, $document_id );
    }

    public static function Get_error_message()
    {
        return self::$error_message;
    }
}

/* End of file Uploader.php */
/* Location: ./Core/Libraries/ */