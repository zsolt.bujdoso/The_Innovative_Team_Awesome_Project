<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MY_MRouter
 *
 * @property    MRequest            $request
 * @property    MSession            $session
 * @property    MConfig             $config
 *
 * @version     1.0.0
 */
class MY_MRouter extends MRouter
{
    public function __construct()
    {
        $this->url = App()->request->Get_url();

        $this->Load_user();

        parent::__construct();
    }

    /**
     * @method  Load_user
     * @access  private
     * @desc    Load enabled modules from database
     * @author  Desc
     *
     * @version 1.0
     */
    private function Load_user()
    {
        $session_name               = ( $this->Is_admin() ? App()->config->admin_session_id : App()->config->user_session_id );
        App()->data["session_name"] = $session_name;

        if( App()->session->Has( $session_name ) )
        {
            $user_id                    = App()->session->Get( $session_name );
            App()->data[$session_name]  = $user_id;
            App()->data["user"]         = Main_users_model::Get_model()->Get_by_primary_key( $user_id );

            if( ! App()->data["user"] )
            {
                App()->session->Remove( $session_name );
            }
        }
    }

    public function Is_admin()
    {
        return ( preg_match( "/^([a-zA-Z]+\/)?admin\/?(.*)$/", $this->url ) );
    }
}

/* End of file MY_MRouter.php */
/* Location: ./Core/Libraries/ */