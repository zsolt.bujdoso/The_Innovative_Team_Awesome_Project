<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "validation_cron_year"              => "Helytelen az év mező értéke, kérjük próbálja újra",
    "validation_cron_month"             => "Helytelen a hónap mező értéke, kérjük próbálja újra",
    "validation_cron_day"               => "Helytelen a nap mező értéke, kérjük próbálja újra",
    "validation_cron_hour"              => "Helytelen az óra mező értéke, kérjük próbálja újra",
    "validation_cron_minute"            => "Helytelen a perc mező értéke, kérjük próbálja újra",
    "validation_result"                 => "A %s mező kötelezően kell tartalmazza az eredményt ( 0-0 )",
);

/* End of file Front_lang.php */
/* Location: ./Core/Languages/en/ */