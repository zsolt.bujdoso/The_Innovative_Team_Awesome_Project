<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Registration_error"                => "Sajnáljuk, jelen pillanatban nem tudunk beregisztrálni, kérjük próbálja újra később",
    "You_need_to_login"                 => "Sajnáljuk, be kell jelentkeznie ahhoz, hogy megtekinthesse ezt az oldalt",
    "You_need_to_logout"                => "Sajnáljuk, ki kell jelentkeznie ahhoz, hogy megtekinthesse ezt az oldalt",
    "Email_send_error"                  => "Sajnáljuk, jelen pillanatban nem tudunk emailt küldeni, kérjük próbálja újra később",
    "No_item_in_category"               => "Sajnáljuk, jelenleng nincs egyetlen elem sem ebben a kategóriában",
    "You_dont_have_permission_to_view"  => "Sajnáljuk, jelenleg nem rendelkezik elegendő joggal ahhoz, hogy megtekintse ezt az oldalt",
    "Comment_item_not_found"            => "Sajnáljuk, ez az elem nem található",
    "All_question_needs_an_answer"      => "Sajnáljuk, minden kérdésre válaszolnod kell",
    "validation_emails"                 => "Sajnáljuk, a beírt email címek között van helytelen, töltse ki az alábbi mezőt "
                                            ."a barátai email címével, soronkét egy címet írjon be",
    "Helpful_login_error"               => "Sajnáljuk, az értékeléshez bejelentkezés szükséges",
    "Favorite_add_error"                => "Sajnáljuk, a kedvencekhez adáshoz bejelentkezés szükséges",
    "Wrong_captcha_code"                => "Sajnáljuk, rossz ellenőrző számot írt be",
    "You_have_not_access_to_download"   => "Sajnáljuk, nem rendelkezik elegendő joggal, hogy letöltse ezt a fájlt",
    "Cart_item_add_error"               => "Sajnáljuk, hiba történt a termék hozzáadása közben, kérjük próbálja meg újra",
    "Cart_item_modify_error"            => "Sajnáljuk, hiba történt a termék módosítása közben, kérjük próbálja meg újra",
    "Cart_item_delete_error"            => "Sajnáljuk, hiba történt a termék törlése közben, kérjük próbálja meg újra",
    "You_have_not_access_to_delete_this_address"  => "Sajnáljuk nincs jogod törölni ezt a címet",
    "You_have_not_access_to_delete_this_user"     => "Sajnáljuk nincs jogod törölni ezt a felhasználót",
    "Report_type_not_found"             => "Jelentés nem található",
    "Type_not_found"                    => "Ez a típus nem található",
    "Item_cant_add_to_cart"             => "Sajnáljuk, ezt az elemet nem lehet a kosárba rakni",
    "You_have_broken_the_payment"       => "Ön megszakította a fizetés menetét",
    "Order_already_payed"               => "A rendelés már ki van fizetve",
    "Invalid_user_address"              => "Sajnáljuk, a kiválasztott cím helytelen",
    "Product_not_enough_on_stock"       => "Sajnáljuk, az adott termékből jelenleg csak %d van raktáron",
    "Order_not_paid"                    => "Sajnáljuk, a rendelés nincs kifizetve",
    "Order_minimum_price_must_be_x"     => "Sajnáljuk, A rendeléshez a termékek összértéke nagyobb vagy egyenlő, mint %d",
    "Order_need_login"                  => "Sajnáljuk, bejelentkezés szükséges a rendelés véglegesítéséhez",
    "Invalid_vat_number"                => "Sajnáljuk, ez a TVA szám helytelen",
    "Cron_code_exists"                  => "Sajnáljuk, ez a cron már létezik",
    "Cron_could_not_delete"             => "Sajnáljuk, a cron(ok) nem törölhető(ek)",
    "Selected_address_doesnt_have_your_identity_number"   => "A kiválasztott cím nem rendekezik személyi számmal, kérjük állítsa be a profil menüben",
    "Select_min_x_from_category_x"      => "Kérjük válasszon ki minimum %d terméket a %s kategóriából",
    "This_package_has_no_combinations"  => "Sajnáljuk, ehhez a csomaghoz még nincs beállítva semmi megvásárolható termék",
    "IP_address_not_allowed"            => "Sajnáljuk, az ön IP címe nem engedélyezett",
    "License_code_exists"               => "Sajnáljuk, ez a licensz már létezik",
    "License_could_not_delete"          => "Sajnáljuk, a licensz(ek) nem törölhető(ek)",
    "No_item_in_this_page"              => "Sajnáljuk, jelenleg egyetlen elem sem található ezen az oldalon",
    "Only_teams_can_subscribe"          => "Sajnáljuk, csak csapatok íratkozhatnak a versenyre",
    "Subscriber_already_added"          => "Sajnáljuk, ön már fel van íratkozva",
    "Name_field_is_incorrect"           => "A név mező nem megfelelő",
    "Team_member_already_exists"        => "A csapat tag már hozzá volt adva a csapathoz",
    "Please_select_user_from_list_appear"   => "Kérem válassza ki a felhasználót a listából",
    "Nr_of_groups_required"             => "Csoportok száma kötelező",
    "Result_already_exists"             => "Sajnáljuk, ez az eredmény már létezik",
    "Award_already_in_added_to_participant" => "Sajnáljuk, a díj már létezik",
    "Point_not_found"                   => "Sajnáljuk, a pont nem található",
    "Event_already_exists"              => "Sajnáljuk, ez a esemény már létezik",
    "Result_is_finished"                => "Sajnáljuk, ez a eredmény már be van fejezve",
    "Lucky_looser_add_error"            => "Sajnáljuk, nem lehet szerencsés vesztest hozzáadni",
    "Not_subscribed"                    => "Sajnáljuk, nincs feliratkozva",
);

/* End of file Front_errors_lang.php */
/* Location: ./Core/Languages/hu/ */