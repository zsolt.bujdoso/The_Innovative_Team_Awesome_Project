<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Required_parameters_missing"       => "Some required parameters are not set",
    "Something_went_wrong"              => "Something went wrong, please try again",
    "We_cant_get_the_parameters"        => "We can't get the parameters, please try again",
    "We_cant_get_your_details"          => "We can't get your details, please try again",
    "Error_in_parameters"               => "Error in parameters",
    "The_user_cancelled"                => "The user cancelled this request",
    "Service_not_exists"                => "This services doesn't exist, please try another",
    "Access_token_not_found"            => "Access token not found, please try again",
    "Auth_code_not_found"               => "Auth code not found, please try again",
    "User_not_yours"                    => "Sorry, this user not exists or has registered an other way, please try again with simple login if it's yours",
);

/* End of file Third_party_logins_lang.php */
/* Location: ./Core/Languages/en/ */