<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Registration_error"                => "Sorry, at this moment we can't register you, please try again later",
    "You_need_to_login"                 => "Sorry, you need to login",
    "You_need_to_logout"                => "Sorry, you need to logout before you can use this",
    "Email_send_error"                  => "Sorry, we can't send email now, please try again later",
    "No_item_in_category"               => "Sorry, we can't find any item in this category yet",
    "You_dont_have_permission_to_view"  => "Sorry, you don't have permission to view this",
    "Comment_item_not_found"            => "Sorry, item not found",
    "All_question_needs_an_answer"      => "Sorry, all questions needs an answer",
    "validation_emails"                 => "Sorry, there is some email address invalid, please enter your friends email addresses separated with new line",
    "Helpful_login_error"               => "Sorry, you need to login before rate this item",
    "Favorite_add_error"                => "Sorry, you have to be logged in before add items to favorites",
    "Wrong_captcha_code"                => "Sorry, you have entered a wrong result in verification field",
    "You_have_not_access_to_download"   => "Sorry, You don't have access to download this file",
    "Cart_item_add_error"               => "Sorry, error encountered while adding product to cart, please try again",
    "Cart_item_modify_error"            => "Sorry, error encountered while modifing cart item, please try again",
    "Cart_item_delete_error"            => "Sorry, error encountered while deleting product from cart, please try again",
    "You_have_not_access_to_delete_this_address"  => "Sorry, You don't have access to delete this address",
    "You_have_not_access_to_delete_this_user"     => "Sorry, You don't have access to delete this user",
    "Report_type_not_found"             => "Sorry, Report type not found",
    "Type_not_found"                    => "Sorry, This type not found",
    "Item_cant_add_to_cart"             => "Sorry, This item can not be added to cart",
    "You_have_broken_the_payment"       => "Sorry, You have broken the payment of order",
    "Order_already_payed"               => "Sorry, Order already payed",
    "Invalid_user_address"              => "Sorry, Invalid address",
    "Product_not_enough_on_stock"       => "Sorry, we have only %d product(s) on stock",
    "Order_not_paid"                    => "Sorry, Order not paid",
    "Order_minimum_price_must_be_x"     => "Sorry, The product(s) price for order must be greater or equal than %d",
    "Order_need_login"                  => "Sorry, log in needed to order items",
    "Invalid_vat_number"                => "Sorry, this VAT number is invalid",
    "Cron_code_exists"                  => "Sorry, this cron already exists",
    "Cron_could_not_delete"             => "Sorry, cron(s) could not be deleted",
    "Selected_address_doesnt_have_your_identity_number"   => "The selected address doesn't have your identity number, you can set it in profile menu",
    "Select_min_x_from_category_x"      => "Please select minimum %d products from category: %s",
    "This_package_has_no_combinations"  => "Sorry, this package has no combinations set",
    "IP_address_not_allowed"            => "Sorry, this ip address is not allowed",
    "License_code_exists"               => "Sorry, this license already exists",
    "License_could_not_delete"          => "Sorry, license(s) could not be deleted",
    "No_item_in_this_page"              => "Sorry, we can't find any item in this page yet",
    "Only_teams_can_subscribe"          => "Sorry, only teams can subscribe to this competition",
    "Subscriber_already_added"          => "Sorry, you have already subscribed",
    "Name_field_is_incorrect"           => "Name field is incorrect",
    "Team_member_already_exists"        => "Team member already exists",
    "Please_select_user_from_list_appear"   => "Please select user from list appear",
    "Nr_of_groups_required"             => "Number of groups required",
    "Result_already_exists"             => "Sorry, result already exists",
    "Award_already_in_added_to_participant" => "Sorry, award already added to participant",
    "Point_not_found"                   => "Sorry, the point not found",
    "Event_already_exists"              => "Sorry, this event already exists",
    "Result_is_finished"                => "Sorry, the result is already finished",
    "Lucky_looser_add_error"            => "Sorry, adding lucky looser is impossible",
    "Not_subscribed"                    => "Sorry, you are not subscribed to this team",
);

/* End of file Front_errors_lang.php */
/* Location: ./Core/Languages/en/ */