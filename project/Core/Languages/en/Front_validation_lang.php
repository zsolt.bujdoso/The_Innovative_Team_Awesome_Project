<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "validation_cron_year"              => "Incorrect value of year, please try again",
    "validation_cron_month"             => "Incorrect value of month, please try again",
    "validation_cron_day"               => "Incorrect value of day, please try again",
    "validation_cron_hour"              => "Incorrect value of hour, please try again",
    "validation_cron_minute"            => "Incorrect value of minute, please try again",
    "validation_result"                 => "The %s field must contains a result ( 0-0 )",
);

/* End of file Front_lang.php */
/* Location: ./Core/Languages/en/ */