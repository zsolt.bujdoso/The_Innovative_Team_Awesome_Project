<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Registration_error"                => "Ne pare rău, în acest moment nu putem să vă înregistrăm, vă rugăm să încercați mai târziu",
    "You_need_to_login"                 => "Ne pare rău, trebuie să intrați ca să puteți vizualiza această pagină",
    "You_need_to_logout"                => "Ne pare rău, trebuie să ieșiți ca să puteți vizualiza această pagină",
    "Email_send_error"                  => "Ne pare rău, în acest moment nu putem să vă trimitem emailul, vă rugăm încercați mai târziu",
    "No_item_in_category"               => "Ne pare rău, în acest moment nu este nici un item în această categorie",
    "You_dont_have_permission_to_view"  => "Ne pare rău, în acest moment nu aveți acces pentru a vizualiza această pagină",
    "Comment_item_not_found"            => "Ne pare rău, acest element nu poate fi găsită",
    "All_question_needs_an_answer"      => "Ne pare rău, dar trebuie să răspundeți la toate întrebări",
    "validation_emails"                 => "Vă rugăm să verificați adresele completate, corectați câmpurile de mai jos "
                                            ."cu adresele prieteniilor Dvs., în fiecare rând să existe câte o adresă",
    "Helpful_login_error"               => "Ne pare rău, dar trebuie să fiți logat înainte de a evalua această pagină",
    "Favorite_add_error"                => "Ne pare rău, dar trebuie să fiți logat înainte de a adăuga între favorite",
    "Wrong_captcha_code"                => "Ne pare rău, număr introdus în câmpul de verificare este incorect",
    "You_have_not_access_to_download"   => "Ne pare rău, nu aveți acces pentru a descărca acest fișier",
    "Cart_item_add_error"               => "Ne pare rău, s-a enumerat o eroare în adăugare, vă rugăm încercați din nou",
    "Cart_item_modify_error"            => "Ne pare rău, s-a enumerat o eroare în modificare, vă rugăm încercați din nou",
    "Cart_item_delete_error"            => "Ne pare rău, s-a enumerat o eroare în ștergere, vă rugăm încercați din nou",
    "You_have_not_access_to_delete_this_address"  => "Ne pare rău, dar nu ai suficient acces pentru a șterge adresa",
    "You_have_not_access_to_delete_this_user"     => "Ne pare rău, dar nu ai suficient acces pentru a șterge utilizatorul",
    "Report_type_not_found"             => "Tip de raport negăsit",
    "Type_not_found"                    => "Tip negăsit",
    "Item_cant_add_to_cart"             => "Ne pare rău, nu se poate pune acest element în coș",
    "You_have_broken_the_payment"       => "A-ți abandonat plata comenzii",
    "Order_already_payed"               => "Comandă a fost plătită",
    "Invalid_user_address"              => "Ne pare rău, adresa selectată este incorectă",
    "Product_not_enough_on_stock"       => "Ne pare rău, din acest produs avem doar %d în stock",
    "Order_not_paid"                    => "Ne pare rău, comadă nu a fost plătită",
    "Order_minimum_price_must_be_x"     => "Ne pare rău, prețul produselor în comandă trebuie să fie mai mare decât %d",
    "Order_need_login"                  => "Ne pare rău, intrarea este necesar pentru punerea comenzii",
    "Invalid_vat_number"                => "Ne pare rău, acest număr VAT este invalid",
    "Cron_code_exists"                  => "Ne pare rău, acest cron deja există",
    "Cron_could_not_delete"             => "Ne pare rău, cron(uri) nu poate fi șters",
    "Selected_address_doesnt_have_your_identity_number"   => "Adresa selectată nu conține CNP, puteți seta în meniu profil",
    "Select_min_x_from_category_x"      => "Vă rugăm să selectați minim %d produse din categoria: %s",
    "This_package_has_no_combinations"  => "Ne pare rău, la acest pachet încă nu sunt setat nici un produs disponibil",
    "IP_address_not_allowed"            => "Ne pare rău, dar adresa de IP al Dvs. nu este acceptat",
    "License_code_exists"               => "Ne pare rău, această licență deja există",
    "License_could_not_delete"          => "Ne pare rău, licenț(e) nu poate fi șters",
    "No_item_in_this_page"              => "Ne pare rău, în acest moment nu este nici un item în această pagină",
    "Only_teams_can_subscribe"          => "Ne pare rău, numai echipe au voie să se înscrie la această competiție",
    "Subscriber_already_added"          => "Ne pare rău, ați înscris deja",
    "Name_field_is_incorrect"           => "Numele câmpului este incorect",
    "Team_member_already_exists"        => "Membru al echipei a fost adaugată",
    "Please_select_user_from_list_appear"   => "Selectați utilizatorul din listă",
    "Nr_of_groups_required"             => "Numărul grupelor este necesar",
    "Result_already_exists"             => "Ne pare rău, rezultatul deja există",
    "Award_already_in_added_to_participant" => "Ne pare rău, premiul deja există",
    "Point_not_found"                   => "Ne pare rău, punctul nu există",
    "Event_already_exists"              => "Ne pare rău, acest eveniment deja există",
    "Result_is_finished"                => "Ne pare rău, acest rezultat deja s-a terminat",
    "Lucky_looser_add_error"            => "Ne pare rău, adăugarea lucky looserului este imposibilă",
    "Not_subscribed"                    => "Ne pare rău, nu urmăriți această echipă",
);

/* End of file Front_errors_lang.php */
/* Location: ./Core/Languages/ro/ */