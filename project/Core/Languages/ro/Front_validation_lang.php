<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "validation_cron_year"              => "Valoarea incorectă în câmpul an, vă rugăm încercați din nou",
    "validation_cron_month"             => "Valoarea incorectă în câmpul lună, vă rugăm încercați din nou",
    "validation_cron_day"               => "Valoarea incorectă în câmpul ziua, vă rugăm încercați din nou",
    "validation_cron_hour"              => "Valoarea incorectă în câmpul ora, vă rugăm încercați din nou",
    "validation_cron_minute"            => "Valoarea incorectă în câmpul minut, vă rugăm încercați din nou",
    "validation_result"                 => "Câmpul %s trebuie să conțină un rezultat (0-0)",
);

/* End of file Front_lang.php */
/* Location: ./Core/Languages/en/ */