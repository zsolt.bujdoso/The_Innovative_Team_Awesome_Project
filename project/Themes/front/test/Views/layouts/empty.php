<!DOCTYPE html>
<html lang="<?php echo App()->lang->Get_language_code(); ?>">
    <head>
        <?php echo App()->controller->Load_partial_view( "//partials/header_clear" ); ?>
    </head>
    <body>
        <?php echo $content; ?>
    </body>
</html>

<?php
/* End of file empty.php */
/* Location: ./themes/test/Views/layouts/ */
