<!DOCTYPE html>
<html lang="<?php echo App()->lang->Get_language_code(); ?>">
    <head>
        <?php echo App()->controller->Load_partial_view( "//partials/header" ); ?>
    </head>
    <body>
        <?php
        echo App()->controller->Load_partial_view( "//partials/header_content" );
        echo App()->controller->Load_partial_view( "//partials/notices" );
        echo App()->controller->Load_partial_view( "//partials/notices_admin" );
        
        echo $content;

        echo App()->controller->Load_partial_view( "//partials/footer_content" );

        // Load jQuery
        echo Html_helper::Js( 'jquery-3.2.1.min', FALSE );
        echo Html_helper::Js( 'jquery/jquery-ui.1.12.1.min', FALSE );
        echo Html_helper::Js( "fancybox/jquery.fancybox" );
        echo Html_helper::Js( "fancybox/jquery.fancybox-buttons" );
        echo Html_helper::Js( "fancybox/jquery.fancybox-media" );
        echo Html_helper::Js( "fancybox/jquery.fancybox-thumbs" );

        echo Html_helper::Js( 'page/index' );
        //echo Html_helper::Js( 'ckeditor/ckeditor' );
        echo Html_helper::Js( 'jquery/jquery-cycle2' );
        echo Html_helper::Js( 'main' );
        echo ( ! empty( $view_scripts ) ? App()->controller->Load_partial_view( $view_scripts ) : "" );
        echo ( ! empty( $extension_scripts ) ? $extension_scripts : "" );

        if( App()->config->debug_mode )
        {
            App()->profiler->Show_result();
        };
        ?>
    </body>
</html>

<?php
/* End of file index.php */
/* Location: ./themes/test/Views/layouts/ */
