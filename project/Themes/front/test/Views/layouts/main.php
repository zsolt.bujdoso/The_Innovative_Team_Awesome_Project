<!DOCTYPE html>
<html lang="<?php echo App()->lang->Get_language_code(); ?>">
    <head>
        <title><?php echo ( ! empty( $meta_title ) ? $meta_title : App()->config->site_name ); ?></title>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <?php
        // Load jQuery
        //echo Html_helper::Js( 'jquery-1.7.1.min.js' );
        echo Html_helper::Css( 'bootstrap.min' );
        echo Html_helper::Css( 'main' );
        echo Html_helper::Css( 'responsive' );

        $base_url = !empty($base_url ) ? $base_url : "";

        ?>

        <script type="text/javascript">
            var base_url                = "<?php echo $base_url; ?>";
            var page_language           = "<?php echo App()->lang->Get_language_code(); ?>";
        </script>

        <!-- Favicons -->
        <link rel="shortcut icon" href="<?php echo $base_url; ?>favicon.ico" />
        <link rel="apple-touch-icon" href="<?php echo $base_url; ?>logo-ico.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $base_url; ?>images/logo-ico.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $base_url; ?>images/logo-ico.png">

        <?php echo ( ! empty( $view_styles ) ? App()->controller->Load_partial_view( $view_styles ) : "" ); ?>
    </head>
    <body>
        <div class = "container-fluid">
            <div class="header row">
                <div class = "col-lg-12 col-md-12 col-xs-12">
                    <?php echo App()->controller->Load_partial_view( "//partials/header_content" ); ?>
                </div>
            </div>
            <div class="row mt-5">
                <aside class="col-lg-3 mx-0 px-0">
                    <?php echo App()->controller->Load_partial_view( "//partials/aside" ); ?>
                </aside>
                <main class="col-lg-9">
                    <?php echo $content; ?>
                </main>
            </div>
            <div class="footer row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <?php echo App()->controller->Load_partial_view( "//partials/footer_content" ); ?>
                </div>
            </div>
        </div>
        <?php 


            echo Html_helper::Js ( 'jquery.min' ); 
            echo Html_helper::Js ( 'bootstrap.min' );
            
            echo ( ! empty( $view_scripts ) ? App()->controller->Load_partial_view( $view_scripts ) : "" );
        ?>
    </body>
</html>

<?php
/* End of file main.php */
/* Location: ./themes/test/Views/layouts/ */
