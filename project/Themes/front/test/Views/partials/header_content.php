<nav class="navbar navbar-expand navbar-dark bg-dark fixed-top" id="mainNav">
    <button class="navbar-toggler d-none" type="button" data-toggle="collapse"
            data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav">
        <li class="nav-item">
            <a href="" class="nav-link">
                Főoldal
            </a>
        </li>
        <li class="nav-item">
            <a href="" class="nav-link">
                Saját adatok
            </a>
        </li>
        <li class="nav-item">
            <a href="" class="nav-link">
                Aktuális ügyek
            </a>
        </li>
        <li class="nav-item">
            <a href="" class="nav-link">
                Lezárt ügyek
            </a>
        </li>
        <li class="nav-item">
            <a href="" class="nav-link">
                E-tananyag
            </a>
        </li>
    </ul>
</nav>