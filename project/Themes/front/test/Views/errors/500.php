<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo Html_helper::Css( "main_required.css" ); ?>
    <title>Error 500</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <?php
    echo Html_helper::Get_css_loader_script( "jquery-ui.structure.css" );
    echo Html_helper::Get_css_loader_script( "jquery-ui.theme.css" );
    echo Html_helper::Get_css_loader_script( "main.css" );
    echo Html_helper::Get_css_loader_script( "learning.css" );
    //echo Html_helper::Get_css_loader_script( 'responsive' );
    ?>
</head>
<body>
    <?php $is_page_using_full_screen = Config_helper::Get( "is_page_using_full_screen" ); ?>
    <div class="wrapper <?php echo ( ! empty( $is_page_using_full_screen ) ? "full-screen" : "max-width-1200" ); ?>">
        <div class="header">
            <div class="header_login">
                <div class="logo">
                    <a href="<?php echo Url_helper::Base_url(); ?>">
                        <img src="<?php echo Url_helper::Theme_url(); ?>assets/images/static/logo.png" alt="logo.png"/>
                    </a>
                </div>
                <div class="row">
                    <div>
                        <?php echo App()->renderer->Load_partial_view( "//partials/header_buttons_listing" ); ?>
                    </div>
                </div>
            </div>

            <div class="nav">
                <div class="icon_nav"></div>
                <?php
                $navigations = ( class_exists( "Navigation_helper", FALSE ) ?
                    Navigation_helper::Get_items_by_category_code( "header" ) : FALSE );

                if( empty( $navigations ) )
                {
                ?>
                    <ul class="first_nav">
                        <li>
                            <a href="<?php echo MY_Url_helper::Base_url(); ?>">
                                <?php echo App()->lang->Get( "Home" ); ?>
                            </a>
                        </li>
                    </ul>
                <?php
                }
                else
                {
                    echo App()->renderer->Load_partial_view(
                        "//navigations/extensions/always/menu_items_listing",
                        array(
                            "menu_items"    => $navigations,
                            "class"         => "first_nav",
                        )
                    );
                }
                ?>
                <div class="clear-both"></div>
            </div>
        </div>

        <?php 
        echo App()->renderer->Load_partial_view( "//partials/notices" ); 
        echo App()->renderer->Load_partial_view( "//partials/notices_admin" ); 
        ?>

        <div>
            <div class="content">
                <div class="container">
                    <div class="error_404_container">
                        <div>
                            <div class="icon_error_500">
                                <i></i>
                            </div>
                            <div class="padding_bottom_20">
                                <h1>
                                    <?php echo App()->lang->Get( "Sorry_we_had_technical_problems" ); ?>!
                                </h1>
                            </div>
                            <div class="padding_bottom_30">
                                <h2>
                                    <?php echo App()->lang->Get( "Please_try_back_later" ); ?>.
                                </h2>
                            </div>
                            <div class="btn_back">
                                <a href="<?php echo MY_Url_helper::Site_url(); ?>page/home">
                                    <?php echo App()->lang->Get( "Back_to_home" );   ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="row-1">
                <div>
                    <div class="margin_top_0">
                        <h2><?php echo App()->lang->Get( "Browse" ); ?></h2>
                        <?php
                        echo App()->renderer->Load_partial_view(
                            "//navigations/extensions/always/menu_items_listing",
                            array(
                                "menu_items"    => ( class_exists( "Navigation_helper", FALSE ) ?
                                                    Navigation_helper::Get_items_by_category_code( "footer" ) : FALSE ),
                                "selected_menu" => ( ! empty( $selected_menu ) ? $selected_menu : "" ),
                                "class"         => "",
                            )
                        );
                        ?>
                    </div>
                    <div class="margin_top_0">
                        <h2><?php echo App()->lang->Get( "Informations" ); ?></h2>
                        <?php
                        echo App()->renderer->Load_partial_view(
                            "//navigations/extensions/always/menu_items_listing",
                            array(
                                "menu_items"    => ( class_exists( "Navigation_helper", FALSE ) ?
                                                    Navigation_helper::Get_items_by_category_code( "footer-browse" ) : FALSE ),
                                "selected_menu" => ( ! empty( $selected_menu ) ? $selected_menu : "" ),
                                "class"         => ""
                            )
                        );
                        ?>
                    </div>
                    <div>
                        <h2><?php echo App()->lang->Get( "About_learn" ); ?></h2>
                        <?php
                        echo App()->renderer->Load_partial_view(
                            "//navigations/extensions/always/menu_items_listing",
                            array(
                                "menu_items"    => ( class_exists( "Navigation_helper", FALSE ) ?
                                                    Navigation_helper::Get_items_by_category_code( "footer-about-learn" ) : FALSE ),
                                "selected_menu" => ( ! empty( $selected_menu ) ? $selected_menu : "" ),
                                "class"         => ""
                            )
                        );
                        ?>
                    </div>
                    <div class="availability">
                        <?php /*
                    <div>
                        <div>
                            <a href="<?php echo App()->config->facebook_link; ?>" target="_blank">
                                <i class="icon_facebook"></i>
                            </a>
                        </div>
                        <div>
                            <a href="<?php echo App()->config->twitter_link; ?>" target="_blank">
                                <i class="icon_twitter"></i>
                            </a>
                        </div>
                        <div>
                            <a href="<?php echo App()->config->google_link; ?>" target="_blank">
                                <i class="icon_google"></i>
                            </a>
                        </div>
                        <div class="clear-both"></div>
                    </div>
                    */ ?>
                        <div>
                            <p>
                                <?php echo App()->lang->Get( "Do_you_have_questions" ); ?>
                                <a href="<?php echo MY_Url_helper::Site_url(); ?>page/contact">
                                    <?php echo ( class_exists( "String_helper", FALSE ) ?
                                        String_helper::Encode_email( Config_helper::Get( "site_email" ) ) : FALSE ); ?>
                                </a>
                            </p>
                        </div>

                        <div class="report-bug">
                            <a href="<?php echo MY_Url_helper::Site_url() ?>reports/default/add/bug">
                                <?php echo App()->lang->Get( "Report_bug" ); ?>
                            </a>
                        </div>
                    </div>
                    <div class="clear-both"></div>
                </div>
            </div>

            <p>© 2015</p>
        </div>

        <?php
        echo Html_helper::Js( 'jquery-1.11.1', FALSE );
        echo Html_helper::Js( 'jquery-ui', FALSE );

        //echo Html_helper::Js( 'page/index' );
        //echo Html_helper::Js( 'main' );
        ?>
    </div>
</body>
</html>
<?php
/* End of file 500.php */
/* Location: ./themes/test/views/errors/ */