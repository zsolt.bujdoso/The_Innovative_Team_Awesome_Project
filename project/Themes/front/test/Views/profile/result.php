<?php
if( App()->config->profile["show_time"] )
{
?>
    <div>
        <?php
        if( App()->config->profile["show_pre_controller_time"] )
        {
        ?>
            <div>
                <label for="">Pre controller execution time:</label>
                <span class="bold"><?php echo $pre_controller_time; ?></span>
            </div>
        <?php
        }

        if( App()->config->profile["show_controller_time"] )
        {
        ?>
            <div>
                <label for="">Controller execution time:</label>
                <span class="bold"><?php echo $controller_time; ?></span>
            </div>
        <?php
        }

        if( App()->config->profile["show_system_time"] )
        {
        ?>
            <div>
                <label for="">System execution time:</label>
                <span class="bold"><?php echo $system_time; ?></span>
            </div>
        <?php
        }
        ?>
    </div>
<?php
}

if( App()->config->profile["show_memory"] )
{
?>
    <div>
        <label for="">Memory used:</label>
        <span class="bold"><?php echo $memory_size; ?></span>
    </div>
<?php
}

if( App()->config->profile["show_number_of_queries"] )
{
?>
    <div>
        <label for="">Number of queries used:</label>
        <span class="bold"><?php echo count( $sql_queries ); ?></span>
    </div>
<?php
}

if( App()->config->profile["show_duplicated_queries"] )
{
?>
    <div>
        <label for="">Queries need optimization:</label>
        <div>
            <?php
            if( ! empty( $sql_queries ) )
            {
            ?>
                <ul class="executed_queries">
                    <?php
                    $nr_queries                 = count( $sql_queries );
                    $queries_need_optimization  = array();

                    foreach( $sql_queries as $key => $query )
                    {
                        // if query was put into this array then already need optimization
                        if( ! empty( $queries_need_optimization[$key] ) )
                        {
                            continue;
                        }

                        $nr_of_executed = 1;
                        for( $i = ( $key + 1 ); $i < $nr_queries; ++$i )
                        {
                            if( $query["sql"] == $sql_queries[$i]["sql"] )
                            {
                                $queries_need_optimization[$i] = 1;
                                $nr_of_executed++;
                            }
                        }

                        if( $nr_of_executed > 1 )
                        {
                        ?>
                            <li>
                                <div>
                                    <label for="">Query:</label>
                                    <span class="bold"><?php echo $query["sql"]; ?></span>
                                </div>
                                <div>
                                    <label for="">Executed times:</label>
                                    <span class="bold"><?php echo $nr_of_executed; ?></span>
                                </div>
                                <div class="clear-both">&nbsp;</div>
                            </li>
                        <?php
                        }
                    }
                    ?>
                </ul>
            <?php
            }
            ?>
        </div>
    </div>
<?php
}

if( App()->config->profile["show_queries"] )
{
?>
    <div>
        <label for="">Executed queries:</label>
        <div>
            <?php
            if( ! empty( $sql_queries ) )
            {
            ?>
                <ul class="executed_queries">
                    <?php
                    foreach( $sql_queries as $query )
                    {
                    ?>
                        <li>
                            <div>
                                <label for="">Query:</label>
                                <span class="bold"><?php echo $query["sql"]; ?></span>
                            </div>
                            <div>
                                <label for="">Execution time:</label>
                                <span class="bold"><?php echo $query["time"]; ?></span>
                            </div>
                            <div>
                                <label for="">Rows affected:</label>
                                <span class="bold"><?php echo $query["rows"]; ?></span>
                            </div>
                            <div class="clear-both">&nbsp;</div>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            <?php
            }
            ?>
        </div>
    </div>
<?php
}


/* End of file result.php */
/* Location: ./themes/test/views/profile/ */