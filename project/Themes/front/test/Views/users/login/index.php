<div class = "row mt-5">
    <div class = "col-lg-2">
    </div>
    <div class = "col-lg-8">
        <div class = "card card-container">
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png">
            <div class="form-group">
                <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Felhasználó név" value="">
            </div>
            <div class="form-group">
                <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Jelszó">                </div>
            <div class="col-xs-6 form-group pull-left checkbox">
                <input id="checkbox1" type="checkbox" name="remember">
                <label for="checkbox1">Emlékezz rám.</label>   
            </div>
            <div class = "row ml-auto">
                <div class="col-xs-6">     
                    <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Bejelentkezés">
                </div>
                <div class="col-xs-6">
                    <input type="submit" name="login-submit" id="registration-btn" tabindex="4" class="form-control btn btn-login" value="Regisztráció" onclick="window.location='registration';">
                </div>
            </div>
        </div>
    </div>
    <div class = "col-lg-2">
    </div>
</div>