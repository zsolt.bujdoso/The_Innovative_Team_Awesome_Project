<?php

/*
 *
 * Modulok
- todo: Update and update checker
- todo: Termékek vásárlásánál termékfüggő feladatok végrehajtása ( pl bérletek létrehozása )

************************************************************************************************************************
COUSIN
************************************************************************************************************************
- todo: Figyelmeztető emaileket lehessen létrehozni és automatikusan küldje ki a rendszer, pl ha tettünk terméket  a kosárba és nem vásároltuk meg
- todo: Front oldalon létrehozni egy felületet, ahol termékekből lehetne megvásárolni ajándék kuponokat
- todo: A különleges termék csomagoknál mikor a csomagnak adom hozzá a kombinációt pl magyar nyelven akkor hozzá adjam más nyelven is ha létre van hozva

- todo: Cousin: Csomó tooltip szövege hiányzik
- todo: User profile order megtekintésénél a breadcrumbsban a "Rendelések" gomb a user/profile_orders-re kellene visszavigyen
- todo: [10:30:25 AM] Statisztikába:
- todo: [10:38:15 AM] Zoltán - SakerSoft: A 10 legtöbbet bejelentkezett usernek az ID-ját menteni (Ezt csak a havi statisztikában majd)
- todo: [10:41:58 AM] Zoltán - SakerSoft: A 10 legtöbbet megvásárolt termék ID-ja (Csak a havi adatokhoz)
- todo: A leírás közé lehessen reklámot beszúrni
- todo: egy membernek nem lehet kommentet írni


************************************************************************************************************************
Amit még be lehet tenni
************************************************************************************************************************

- todo: jelenjen meg visszaszámláló az olyan akciós termékeken amelyeknek be van állítva h hány napig érvényes az akció
- todo: a felhasználó tudjon képet feltölteni a profil oldalán
- megoldva: behozni a beállításokba h a kereső csak akkor jelenjen meg ha engedélyezve van

************************************************************************************************************************
RÉKA
************************************************************************************************************************

- todo: a team membernél kötelező kell legyen a user_id
- todo: controllereket át kell írni
- todo: román nyelven a termékeknél be töltődik a special termék lista, pedig nincs is kategória
- todo: special termék listánál a menüből hiányzik a login, logout, regi, stb, currency-k
- todo: special termék listánál ha nincs egyetlen elem sem, ki kellene írja, hogy nincs itt semmi
- todo: admin témákban termék kombinációknál nem írja a termék nevét ki
- todo: admin témákban termék kombinációknál nincs enable/disable gomb
- todo: admin témákban termék kombinációknál a tőrlés gombot be kellene tenni a lista alá is
- todo: admin test témák termék kombinációknál valamiért nem írja ki a kombináció nevét amikor be húzzuk az új hozzáadáshoz
- todo: front flipmart témában (lehet máshol is), a termék oldalon a termék "disponibilate" alá kell megjeleníteni a kombináció tulajdonságokat
- todo: flipmart designt át kell nézni s ahol van plusz item details pl. termékek, kuzusok... azokba a könyvtárakba kell külön tabs_for_item_details

************************************************************************************************************************
HUNI
************************************************************************************************************************

- todo: csapat hozzáadásánál a versenyhez ki lehessen választani a csapattagokat akik ezen a versenyen résztvesznek esetleg újak hozzáadása
- todo: ide bejöhet egy opció, hogy mentse - e az új játékosokat a csapatba vagy ne
- todo: csapatok hozzáadásánál legyen egy gomb, hogy új csapat (ami még nincs benne a rendszerbe)
- todo: új csapat gombra lépve lehessen megadni a csapat adatait és tagjait majd mentse el a rendszer
- todo: kiesett csapatokbol az 5,6... helyek kiszamitasa + tablazat

- todo: Verseny megtekintesenel lehessen megtekinteni a golokat
- todo: Verseny Adminisztracional a resztvevok hozzaadasanal a megse gomb nem mukodik
- todo: Verseny Adminisztracional a resztvevok hozzaadasanal bal oldalt kilistaz mindenkit
- todo: Verseny Pontok beirasahoz be tenni egy gombot ami finish-re allitana az adott eredemneyt (merkozest)
- todo: Verseny adinisztracio felhasznalok tabon a delete csak az adott sort torolje
- todo: A generatort meg kell modositani hogy a listat vegye aszerint ahogy a csoport resztvevok hozzaadasnal
- todo: Verseny adminisztracional a vissza gomb nem jo helyre visz


***********************************************************************************************************************/

define( "BASEPATH", __DIR__.DIRECTORY_SEPARATOR );

include( "../core/Core.php" );
new Web_application();