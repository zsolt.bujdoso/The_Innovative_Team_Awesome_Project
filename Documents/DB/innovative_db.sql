/*
SQLyog Enterprise v13.0.0 (64 bit)
MySQL - 10.1.21-MariaDB : Database - innovative
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`innovative` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `innovative`;

/*Table structure for table `m_language_details` */

DROP TABLE IF EXISTS `m_language_details`;

CREATE TABLE `m_language_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` bigint(20) unsigned NOT NULL,
  `modified_user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `language_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modify_date` datetime DEFAULT '0000-00-00 00:00:00',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `langd_deleted` (`is_deleted`),
  KEY `langd_enabled_deleted` (`is_enabled`,`is_deleted`),
  KEY `langd_id_deleted` (`id`,`is_deleted`),
  KEY `langd_id_enabled_deleted` (`id`,`is_enabled`,`is_deleted`),
  KEY `langd_lid_deleted` (`language_id`,`is_deleted`),
  KEY `langd_lid_enabled_deleted` (`language_id`,`is_enabled`,`is_deleted`),
  KEY `langd_lid_language_deleted` (`language_id`,`language_code`,`is_deleted`),
  KEY `langd_lid_language_enabled_deleted` (`language_id`,`language_code`,`is_enabled`,`is_deleted`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `m_language_details` */

insert  into `m_language_details`(`id`,`language_id`,`modified_user_id`,`name`,`language_code`,`insert_date`,`modify_date`,`is_enabled`,`is_deleted`) values 
(1,1,1,'Magyar','hu','2016-04-06 16:48:18','0000-00-00 00:00:00',1,0),
(2,1,1,'Maghiară','ro','2016-04-06 16:51:06','0000-00-00 00:00:00',1,0),
(3,1,1,'Hungarian','en','2016-04-06 16:52:40','0000-00-00 00:00:00',1,0),
(4,2,2,'Angol','hu','2016-04-06 16:53:43','2016-04-06 19:23:21',1,0),
(5,2,1,'Engleză','ro','2016-04-06 16:53:58','0000-00-00 00:00:00',1,0),
(6,2,2,'English','en','2016-04-06 16:54:14','2016-04-06 19:22:14',1,0),
(7,3,1,'Román','hu','2016-04-06 16:54:31','2016-05-08 14:12:27',1,0),
(8,3,1,'Română','ro','2016-04-06 16:54:51','2016-05-08 14:12:27',1,0),
(9,3,1,'Romanian','en','2016-04-06 16:55:05','2016-05-08 14:12:27',1,0),
(10,5,1,'Română','ro','2016-09-15 16:27:16','2016-09-15 16:27:36',1,1),
(11,5,1,'Romanian','en','2016-09-15 16:27:16','2016-09-15 16:27:33',1,1),
(12,5,1,'Román','hu','2016-09-15 16:27:16','2016-09-15 16:27:29',1,1);

/*Table structure for table `m_languages` */

DROP TABLE IF EXISTS `m_languages`;

CREATE TABLE `m_languages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `modified_user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `date_format` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_enabled_on_front` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sort_order` int(10) unsigned NOT NULL DEFAULT '99',
  `insert_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modify_date` datetime DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lang_deleted` (`is_deleted`),
  KEY `lang_id_deleted` (`id`,`is_deleted`),
  KEY `lang_enabled_deleted` (`id`,`is_enabled_on_front`,`is_deleted`),
  KEY `lang_code_deleted` (`code`,`is_deleted`),
  KEY `lang_code_enabled_deleted` (`code`,`is_enabled_on_front`,`is_deleted`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `m_languages` */

insert  into `m_languages`(`id`,`modified_user_id`,`code`,`image`,`date_format`,`is_enabled_on_front`,`sort_order`,`insert_date`,`modify_date`,`is_deleted`) values 
(1,2,'hu','upload/languages/undefined/images/rs2UGeLy.jpg','Y-m-d',1,0,'0000-00-00 00:00:00','2016-04-06 14:22:03',0),
(2,1,'en','upload/languages/undefined/images/fwdkKLdz.jpg','d-m-Y',1,1,'0000-00-00 00:00:00','2016-06-08 16:22:55',0),
(3,1,'ro','upload/languages/undefined/images/3t5hbDB9.jpg','d-m-Y',1,2,'0000-00-00 00:00:00','2017-04-03 15:15:02',0),
(4,2,'de','upload/languages/undefined/images/cOL3Kc3I.jpg','Y-m-d',0,3,'2015-09-08 08:19:57','2016-04-06 19:23:52',0),
(5,1,'rr','upload/languages/undefined/images/3t5hbDB9.jpg',NULL,0,99,'2016-09-15 16:27:16','2016-09-15 16:27:36',1);

/*Table structure for table `m_modules` */

DROP TABLE IF EXISTS `m_modules`;

CREATE TABLE `m_modules` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `modified_user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `helper` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('custom','system') COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `show_in_menu` tinyint(1) NOT NULL DEFAULT '1',
  `show_as_visited` tinyint(1) NOT NULL DEFAULT '0',
  `can_be_shown_in_quick_access` tinyint(1) NOT NULL DEFAULT '0',
  `can_be_added_to_cart` tinyint(1) NOT NULL DEFAULT '0',
  `can_have_items_for_home` tinyint(1) NOT NULL DEFAULT '0',
  `has_items_for_home` tinyint(1) NOT NULL DEFAULT '0',
  `has_categories` tinyint(1) NOT NULL DEFAULT '0',
  `has_statistics` tinyint(1) NOT NULL DEFAULT '0',
  `has_combinations` tinyint(1) NOT NULL DEFAULT '0',
  `show_categories_on_home` tinyint(1) NOT NULL DEFAULT '1',
  `has_api_communication` tinyint(1) NOT NULL DEFAULT '0',
  `tables` text COLLATE utf8_unicode_ci,
  `version` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1.0.0',
  `update` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` smallint(6) NOT NULL DEFAULT '9999',
  `insert_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modify_date` datetime DEFAULT '0000-00-00 00:00:00',
  `update_date` datetime DEFAULT '0000-00-00 00:00:00',
  `can_be_imported_exported` tinyint(1) NOT NULL DEFAULT '0',
  `is_installed` tinyint(1) NOT NULL DEFAULT '0',
  `is_updated` tinyint(1) NOT NULL DEFAULT '0',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `enabled_deleted` (`is_enabled`,`is_deleted`),
  KEY `primary_deleted` (`id`,`is_deleted`),
  KEY `primary_enabled_deleted` (`id`,`is_enabled`,`is_deleted`),
  KEY `show_in_menu_enabled_deleted` (`show_in_menu`,`is_enabled`,`is_deleted`),
  KEY `can_be_show_quick_enabled_deleted` (`can_be_shown_in_quick_access`,`is_enabled`,`is_deleted`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `m_modules` */

insert  into `m_modules`(`id`,`modified_user_id`,`name`,`link`,`helper`,`type`,`image`,`show_in_menu`,`show_as_visited`,`can_be_shown_in_quick_access`,`can_be_added_to_cart`,`can_have_items_for_home`,`has_items_for_home`,`has_categories`,`has_statistics`,`has_combinations`,`show_categories_on_home`,`has_api_communication`,`tables`,`version`,`update`,`sort_order`,`insert_date`,`modify_date`,`update_date`,`can_be_imported_exported`,`is_installed`,`is_updated`,`is_enabled`,`is_deleted`) values 
(1,0,'Menu','menu',NULL,'system',NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,'1.0.0','',9999,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,1,0,1,0);

/*Table structure for table `m_settings` */

DROP TABLE IF EXISTS `m_settings`;

CREATE TABLE `m_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `modified_user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `domain_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `validation` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value_text` text COLLATE utf8_unicode_ci,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `insert_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modify_date` datetime DEFAULT '0000-00-00 00:00:00',
  `can_be_modified` tinyint(1) NOT NULL DEFAULT '1',
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_domain_uniq` (`code`,`domain_id`),
  KEY `settings_domainid_code_deleted` (`domain_id`,`code`,`is_deleted`),
  KEY `settings_domainid_code_visible_deleted` (`domain_id`,`code`,`is_visible`,`is_deleted`),
  KEY `settings_deleted` (`is_deleted`)
) ENGINE=MyISAM AUTO_INCREMENT=194 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `m_settings` */

insert  into `m_settings`(`id`,`modified_user_id`,`domain_id`,`name`,`code`,`validation`,`value`,`value_text`,`comment`,`type`,`insert_date`,`modify_date`,`can_be_modified`,`is_visible`,`is_deleted`) values 
(1,0,0,'Az oldalt betölthetik más oldalak?','header_X-Frame-Options','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','0000-00-00 00:00:00','2015-06-26 08:58:02',1,1,0),
(2,0,2,'A google analytics kód a tizbani.no-ip.org domain-nek','analytics_code','alpha_dash|max_length[50]','','','Csak a kódot kell beírni, nem a teljes scriptet','text','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(3,0,0,'Kereső botok','default_meta_robots','max_length[20]','index,follow','','','text','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(4,1,0,'Az oldalnak több nyelve van-e?','page_has_more_languages','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2017-04-03 16:05:14',1,1,0),
(5,1,0,'A google analytics kód minden más esetben','analytics_code','alpha_dash|max_length[50]','','','Csak a kódot kell beírni, nem a teljes scriptet','text','0000-00-00 00:00:00','2017-11-24 09:14:39',1,1,0),
(6,0,0,'A termékeknél használjuk az intro-t?','product_use_intro','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(7,0,0,'A termékeknél használjuk a leírást?','product_use_description','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(8,0,0,'A termékeknél használjuk a fő képet?','product_use_image','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(9,0,0,'A termékeknél használjuk a kép galériát?','product_use_images','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(10,0,0,'A termékeknél használjuk a fájl galériát?','product_use_files','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(11,0,0,'A termékeknél használjuk a plussz tulajdonságokat?','product_use_properties','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(12,0,0,'A termékeknél használjuk a kombinációkat ár kalkuláláshoz?','product_use_combinations','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(13,0,0,'A termék kategóriáknál tiltjuk az alkategóriákat?','product_disable_subcategories','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(14,0,0,'Az oldalaknál használjuk az intro-t?','page_use_intro','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(15,0,0,'Az oldalaknál használjuk a leírást?','page_use_description','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(16,0,0,'Az oldalaknál használjuk a fő képet?','page_use_image','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(17,0,0,'Az oldalaknál használjuk a kép galériát?','page_use_images','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','0000-00-00 00:00:00','2015-05-19 11:09:52',1,1,0),
(18,0,0,'A cikkeknél tiltjuk az alkategóriákat?','article_disable_subcategories','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-21 22:52:02','2015-05-19 11:09:52',1,1,0),
(19,0,0,'A cikkeknél használjuk a plussz tulajdonságokat?','article_use_properties','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-21 22:52:39','2015-05-19 11:09:52',1,1,0),
(20,0,0,'A cikkeknél használjuk a fájl galériát?','article_use_files','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-21 22:52:57','2015-05-19 11:09:52',1,1,0),
(21,0,0,'A cikkeknél használjuk a kép galériát?','article_use_images','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-21 22:53:24','2015-05-19 11:09:52',1,1,0),
(22,0,0,'A cikkeknél használjuk a fő képet?','article_use_image','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-21 22:53:42','2015-05-19 11:09:52',1,1,0),
(23,0,0,'A cikkeknél használjuk a leírást?','article_use_description','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-21 22:54:07','2015-05-19 11:09:52',1,1,0),
(24,0,0,'A cikkeknél használjuk az intro-t?','article_use_intro','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-21 22:54:24','2015-05-19 11:09:52',1,1,0),
(25,0,0,'Az albumoknál használjuk az intro-t?','album_use_intro','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-22 09:23:57','2015-05-19 11:09:52',1,1,0),
(26,0,0,'Az albumoknál használjuk a leírást?','album_use_description','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-22 09:24:15','2015-05-19 11:09:52',1,1,0),
(27,0,0,'Az albumoknál használjuk a fő képet?','album_use_image','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-22 09:24:23','2015-05-19 11:09:52',1,1,0),
(28,0,0,'Az albumoknál használjuk a kép galériát?','album_use_images','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-09-22 09:24:31','2015-05-19 11:09:52',1,1,0),
(29,0,0,'A blokkoknál használjuk az intro-t?','block_use_intro','integer|between[0,1]','0','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-04 09:12:49','0000-00-00 00:00:00',1,1,1),
(30,0,0,'A blokkoknál használjuk a kép galériát? ','block_use_images','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-04 13:58:15','0000-00-00 00:00:00',1,1,1),
(31,0,0,'Az oldal nem elérhető','page_isnt_visible_hu','normal|max_length[2000]','','<p>Teszt 123</p>\r\n','','textarea','2014-10-22 22:25:51','2015-05-19 11:09:52',1,0,0),
(32,0,0,'Az oldal nem elérhető','page_isnt_visible_en','normal|max_length[2000]','','<p>Teszt 12345</p>\r\n','','textarea','2014-10-22 22:41:22','2015-05-19 11:09:52',1,0,0),
(33,0,0,'Az oldal nem elérhető','page_isnt_visible_ro','normal|max_length[2000]','','<p>RO Teszt</p>\r\n','','textarea','2014-10-22 22:41:59','2015-05-19 11:09:52',1,0,0),
(34,0,0,'Látogató kitiltva','visitor_is_banned_hu','normal|max_length[2000]','','<p>Alma</p>\r\n','','textarea','2014-10-22 22:43:59','2015-05-19 11:09:52',1,0,1),
(35,0,0,'Látogató kitiltva','visitor_is_banned_en','normal|max_length[2000]','','','','textarea','2014-10-22 22:44:22','2015-05-19 11:09:52',1,0,1),
(36,0,0,'Látogató kitiltva','visitor_is_banned_ro','normal|max_length[2000]','','','','textarea','2014-10-22 22:44:46','2015-05-19 11:09:52',1,0,1),
(37,0,0,'A kurzusoknál használjuk az intro-t?','course_use_intro','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:44:55','2015-05-19 11:09:52',1,1,0),
(38,0,0,'A kurzusoknál használjuk a leírást?','course_use_description','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:45:31','2015-05-19 11:09:52',1,1,0),
(39,0,0,'A kurzusoknál használjuk a fő képet?','course_use_image','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:46:13','2015-05-19 11:09:52',1,1,0),
(40,0,0,'A kurzusoknál használjuk a kép galériát?','course_use_images','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:46:44','2015-05-19 11:09:52',1,1,0),
(41,0,0,'A kurzusoknál használjuk a fájl galériát?','course_use_files','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:47:13','2015-05-19 11:09:52',1,1,0),
(42,0,0,'A kurzusoknál használjuk a plussz tulajdonságokat?','course_use_properties','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:47:25','2015-05-19 11:09:52',1,1,0),
(43,0,0,'A kurzusoknál használjuk a kombinációkat ár kalkuláláshoz?','course_use_combinations','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:48:01','2015-05-19 11:09:52',1,1,0),
(44,0,0,'A kurzus kategóriáknál tiltjuk az alkategóriákat?','course_disable_subcategories','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:48:18','2015-05-19 11:09:52',1,1,0),
(45,0,0,'A példáknál használjuk az intro-t?','example_use_intro','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:48:37','2015-05-19 11:09:52',1,1,0),
(46,0,0,'A példáknál használjuk a leírást?','example_use_description','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:48:57','2015-05-19 11:09:52',1,1,0),
(47,0,0,'A példáknál használjuk a fő képet?','example_use_image','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:49:19','2015-05-19 11:09:52',1,1,0),
(48,0,0,'A példáknál használjuk a kép galériát?','example_use_images','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:49:37','2015-05-19 11:09:52',1,1,0),
(49,0,0,'A példa kategóriáknál tiltjuk az alkategóriákat?','example_disable_subcategories','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:49:56','2015-05-19 11:09:52',1,1,0),
(50,0,0,'A példáknál használjuk a fájl galériát?','example_use_files','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:50:14','2015-05-19 11:09:52',1,1,0),
(51,0,0,'A példáknál használjuk a plussz tulajdonságokat?','example_use_properties','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-10-25 22:50:36','2015-05-19 11:09:52',1,1,0),
(52,1,0,'Felhasználok automatikus engedélyezése regisztrációnál','enable_user_on_registration','integer|between[0,1]','','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-11-29 19:42:59','2017-11-24 09:42:42',1,1,0),
(53,1,0,'Engedélyezett a regisztráció?','is_registration_enabled','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2014-11-29 20:22:40','2017-04-03 16:04:38',1,1,0),
(54,0,0,'Felhasználó típus regisztrációnál','user_type_id_on_registration','positive|max_length[20]','4','','','text','2014-11-29 20:48:13','2015-05-19 11:09:52',1,1,0),
(55,1,0,'Az oldal neve','site_name','name|max_length[100]','Player ranks','','','text','2014-11-30 19:03:04','2017-11-24 17:07:31',1,1,0),
(56,1,0,'Az oldal facebook oldala','facebook_link','link|max_length[150]','https://www.facebook.com/Player-Ranks-608850859452902/','','Facebook oldalra vezető link','text','2015-02-25 09:34:34','2018-04-23 20:46:31',1,1,0),
(57,0,0,'Az oldal twitter oldala','twitter_link','link|max_length[150]','','','Twitter oldalra vezető link','text','2015-02-25 09:36:29','2015-05-19 11:09:52',1,1,0),
(58,0,0,'Az oldal google oldala','google_link','link|max_length[150]','','','Google oldalra vezető link','text','2015-02-25 09:38:42','2015-05-19 11:09:52',1,1,0),
(59,0,0,'A teszt kategóriáknál tiltjuk az alkategóriákat?','test_disable_subcategories','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-03-11 14:51:20','2015-05-19 11:09:52',1,1,0),
(60,0,0,'A teszteknél használjuk az intro-t?','test_use_intro','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-03-11 15:13:14','2015-05-19 11:09:52',1,1,0),
(61,0,0,'A teszteknél használjuk a leírást?','test_use_description','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-03-11 15:13:58','2015-05-19 11:09:52',1,1,0),
(62,0,0,'A teszteknél használjuk a kép galériát?','test_use_images','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-03-11 15:14:26','2015-05-19 11:09:52',1,1,0),
(63,0,0,'A teszteknél használjuk a fájl galériát?','test_use_files','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-03-11 15:14:55','2015-05-19 11:09:52',1,1,0),
(64,0,0,'A teszteknél használjuk a fő képet?','test_use_image','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-03-11 15:15:20','2015-05-19 11:09:52',1,1,0),
(65,1,0,'Felhasználók meghívhatnak más felhasználókat','is_invite_user_allowed','integer|between[0,1]','','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-03-26 16:25:40','2016-04-29 00:16:31',1,1,0),
(66,0,0,'Hány meghívott felhasználó kell a kurzusok megtekintéséhez','invites_needed_for_access','integer|max_length[2]','2','','Egy számot kell beírni értékként','text','2015-03-29 15:59:00','2015-05-19 11:09:52',1,1,0),
(67,0,0,'Az oldal/cég email címe','site_email','email|max_length[150]','contact@sakersoft.ro','','Az oldalhoz tartozó email cím','text','2015-04-10 15:34:07','2015-05-19 11:09:52',1,1,0),
(68,1,0,'Az oldal/cég telefonszáma','site_phone','numeric_phone|max_length[20]','+40 746 332 581','','A cég/tulajdonos telefonszáma','text','2015-04-10 16:08:38','2017-11-24 09:14:59',1,1,0),
(69,1,0,'Az oldal/cég címe','site_address','address|max_length[150]','Targu Mures, Romania','','A cég/oldal címe, ahol elérhető személyesen is','text','2015-04-10 16:18:12','2017-11-24 09:15:12',1,1,0),
(70,0,0,'A látogatók mentése','save_visitors','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-16 19:18:18','0000-00-00 00:00:00',1,1,0),
(71,0,0,'A látogatók által megtekintett oldalak mentése','save_visited_links','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-16 19:19:11','0000-00-00 00:00:00',1,1,0),
(72,0,0,'Az emaileket az oldal egy cron segítségével küldi?','send_emails_with_cron','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-17 07:50:02','0000-00-00 00:00:00',1,1,0),
(73,1,0,'Sitemap tartalmazza az oldalakat','generate_pages_into_sitemap','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-22 20:21:11','2015-09-20 21:54:40',1,1,0),
(74,1,0,'Sitemap tartalmazza a termékeket','generate_products_into_sitemap','integer|between[0,1]','','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-22 20:22:13','2017-11-24 09:43:03',1,1,0),
(75,1,0,'Sitemap tartalmazza a teszteket','generate_tests_into_sitemap','integer|between[0,1]','','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-22 20:23:41','2017-11-24 09:43:05',1,1,0),
(76,1,0,'Sitemap tartalmazza a hírleveleket','generate_newsletters_into_sitemap','integer|between[0,1]','','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-22 20:25:53','2017-11-24 09:43:08',1,1,0),
(77,1,0,'Sitemap tartalmazza a példákat','generate_examples_into_sitemap','integer|between[0,1]','','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-22 20:27:02','2017-11-24 09:43:10',1,1,0),
(78,1,0,'Sitemap tartalmazza a kurzusokat','generate_courses_into_sitemap','integer|between[0,1]','','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-22 20:27:51','2017-11-24 09:43:12',1,1,0),
(79,1,0,'Sitemap tartalmazza a cikkeket','generate_articles_into_sitemap','integer|between[0,1]','','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-22 20:29:04','2017-11-24 09:43:14',1,1,0),
(80,1,0,'Sitemap tartalmazza az albumokat','generate_albums_into_sitemap','integer|between[0,1]','','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-22 20:29:53','2017-11-24 09:43:15',1,1,0),
(81,1,0,'JS cache fájlok újragenerálása','regenerate_js_cache','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-25 08:46:32','2015-09-20 21:27:38',1,1,0),
(82,1,0,'CSS cache fájlok újragenerálása','regenerate_css_cache','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-06-29 18:27:01','2015-08-12 23:44:51',1,1,0),
(84,1,1,'A google analytics kód a localhost domain-nek','analytics_code','alpha_dash|max_length[50]','','','Csak a kódot kell beírni, nem a teljes scriptet','text','2015-07-26 18:33:39','0000-00-00 00:00:00',1,1,0),
(85,1,0,'Tooltip információk megjelenítése','show_tooltips','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2015-07-29 20:49:31','0000-00-00 00:00:00',1,1,0),
(86,0,0,'Az oldal alap pénzneme','currency','alpha|max_length[3]','RON','','A pénznem kódja, pl: EUR','text','2015-08-23 22:46:02','0000-00-00 00:00:00',1,1,0),
(87,1,0,'Az áraknál használt tizedesek száma','decimals_for_prices','positive|max_length[1]','2','','Csak egy számot kell megadni, például: 2','text','2015-08-28 22:29:23','0000-00-00 00:00:00',1,1,0),
(88,0,0,'Az oldal alap domain címe','main_domain','alpha_dash|max_length[100]','playerranks.net','','Az alap domain értéke, pl: wannacode.com','text','2015-09-21 13:10:49','0000-00-00 00:00:00',0,1,0),
(89,0,0,'Reportok email címe','report_email','email|max_length[150]','contact@sakersoft.ro','','','text','2015-09-21 17:24:40','0000-00-00 00:00:00',0,1,0),
(90,1,0,'Kiválasztott téma','theme','alpha_dash|max_length[20]','vewolab','','A kiválasztott téma kódja','text','2015-09-26 19:36:37','0000-00-00 00:00:00',0,0,0),
(91,1,0,'Kiválasztott téma az admin felülethez','admin_theme','alpha_dash|max_length[20]','test','','A kiválasztott téma kódja','text','2015-09-26 19:37:26','0000-00-00 00:00:00',0,0,0),
(92,1,0,'Az oldal mottója','site_slogan','max_length[100]','','','','text','2015-10-21 12:57:06','2017-11-24 10:40:03',1,1,0),
(93,1,0,'Főoldalon listázandó elemek száma','limit_items_on_front_page','positive|between[1,50]','20','','A megjelenítendő elemek száma','text','2015-11-28 12:06:30','2017-04-03 15:59:28',1,1,0),
(94,0,0,'Főoldalon listázandó kategória elemek száma','limit_categories_on_front_page','positive|between[1,50]','3','','A megjelenítendő elemek száma','text','2015-11-28 12:10:18','0000-00-00 00:00:00',1,1,0),
(95,0,0,'Főoldalon listázandó bannerek száma','limit_banners_on_front_page','positive|between[1,5]','5','','A megjelenítendő elemek száma','text','2015-11-28 12:24:38','0000-00-00 00:00:00',1,1,0),
(96,0,0,'Adatlapokon megjelenő hozzászólások száma','limit_comments_on_page_details','positive|between[1,10]','5','','A megjelenítendő elemek száma','text','2015-11-28 13:10:30','0000-00-00 00:00:00',1,1,0),
(97,0,0,'Főoldalon listázandó hozzászólások száma','limit_comments_on_front_page','positive|between[1,5]','5','','A megjelenítendő elemek száma','text','2015-11-28 16:32:12','0000-00-00 00:00:00',1,1,0),
(98,0,0,'Megengedett hibás bejelentkezések száma 1 perc alatt','login_errors_permitted_in_one_minute','positive|between[1,10]','5','','Maximális hibás login száma','text','2015-11-29 17:51:27','0000-00-00 00:00:00',0,1,0),
(99,0,0,'Megengedett hibás link megtekintések száma 1 perc alatt','link_view_errors_permitted_in_one_minute','positive|between[1,10]','10','','Maximális megtekintett hibás linkek száma','text','2015-11-29 22:19:28','0000-00-00 00:00:00',0,1,0),
(100,1,0,'Az oldal logoja','site_logo','path|max_length[500]','','upload/settings/site_logo/images/PD58VmtR.png','A kiválasztott kép','image_link','2015-11-30 12:06:49','2017-11-27 23:13:36',1,1,0),
(104,0,0,'Sikeres rendelés szövege','order_success_hu','normal|max_length[2000]','','<p>Üdvözlettel Wannacode oldalról</p>\r\n','','textarea','2015-12-09 09:46:59','0000-00-00 00:00:00',1,0,0),
(101,1,0,'Az oldal logoja mini változat','site_logo_mini','path|max_length[500]','','upload/settings/site_logo_mini/images/Cf6cVvog.png','A kiválasztott kép','image_link','2015-11-30 13:03:59','2017-12-01 12:45:32',1,1,0),
(102,1,0,'Az oldal logoja a belépés oldalon','site_logo_login','path|max_length[500]','','','A kiválasztott kép','image_link','2015-11-30 13:04:26','2017-11-24 17:14:20',1,1,0),
(103,1,0,'Az oldal logoja négyzet alakban','site_logo_square','path|max_length[500]','','upload/settings/site_logo_square/images/wtwFWR5u.png','A kiválasztott kép','image_link','2015-11-30 13:04:55','2017-12-01 12:46:03',1,1,0),
(105,0,0,'Sikeres rendelés szövege','order_success_en','normal|max_length[2000]','','','','textarea','2015-12-09 09:46:59','0000-00-00 00:00:00',1,0,0),
(106,0,0,'Sikeres rendelés szövege','order_success_ro','normal|max_length[2000]','','','','textarea','2015-12-09 09:46:59','0000-00-00 00:00:00',1,0,0),
(107,0,0,'Rendelések minimális értéke','orders_minimum_price','positive|max_length[11]','20','','','text','2015-12-09 13:11:48','0000-00-00 00:00:00',1,1,0),
(108,0,0,'Megvásárolhatóak a cikkek?','can_add_articles_to_cart','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2016-01-12 10:01:42','0000-00-00 00:00:00',1,1,0),
(109,0,0,'A képekhez használt vízjel','watermark','path|max_length[500]','','','A kiválasztott kép','image_link','2016-02-08 19:23:20','0000-00-00 00:00:00',1,1,0),
(110,1,0,'Listázandó hasonló elemek száma','limit_is_related','positive|between[1,10]','5','','A megjelenítendő elemek száma','text','2016-02-11 16:00:41','2016-05-08 14:13:53',1,1,0),
(111,1,0,'Az oldal megjelenítendő nyelve','default_language','alpha|max_length[2]','hu','','Megjelenítendő nyelv kódja','text','2016-03-23 16:44:07','0000-00-00 00:00:00',1,1,0),
(112,1,0,'Engedélyezett a bejelentkezés?','is_login_enabled','integer|between[0,1]','1','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2016-03-25 14:46:10','2017-04-03 16:02:07',1,1,0),
(113,1,0,'Időzóna','default_timezone','alpha_dash_slash|max_length[50]','Europe/Bucharest','','Értéknek szöveges formában kell beírni: kontinens/főváros','text','2016-04-06 12:59:15','0000-00-00 00:00:00',1,1,0),
(114,1,0,'Kezdő árak mutatása a Saker Soft design-ban','sakersoft_show_main_prices','integer|between[0,1]','0','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2016-04-07 14:36:32','0000-00-00 00:00:00',1,1,0),
(115,1,0,'Saker Soft design baloldali kezdő ár','sakersoft_left_main_price','alpha_dash_space|max_length[10]','250 €','','Az ár és a pénznem','text','2016-04-07 14:39:50','0000-00-00 00:00:00',1,1,0),
(116,1,0,'Saker Soft design jobboldali kezdő ár','sakersoft_right_main_price','alpha_dash_space|max_length[10]','500 €','','Az ár és a pénznem','text','2016-04-07 14:40:55','0000-00-00 00:00:00',1,1,0),
(117,1,0,'Copyright kezdő év','copyright_date_from','positive|max_length[4]','2017','','Csak az évszám','text','2016-04-07 14:41:57','2017-11-24 09:17:30',1,1,0),
(118,1,0,'Copyright név','copyright_name','name|max_length[100]','SC SAKERSOFT SRL-D','','Copyright szövegben megjelenő név','text','2016-04-07 14:55:54','0000-00-00 00:00:00',1,1,0),
(119,1,0,'Az oldal ikonja a fejlécben','site_favicon','path|max_length[500]','','','Egy 32x32 méretű ico kiterjesztésű fájlt kell beállítani','image_link','2016-04-08 09:38:27','0000-00-00 00:00:00',1,1,0),
(120,1,0,'Appli touch ikon','apple_touch_icon','path|max_length[500]','','','Egy az apple alkalmazásnak megfelelő ikon','image_link','2016-04-08 09:46:25','0000-00-00 00:00:00',1,1,0),
(121,1,0,'Apple touch ikon 72','apple_touch_icon_72','path|max_length[500]','','','Egy az apple alkalmazásnak megfelelő 72x72 méretű ikon','image_link','2016-04-08 09:53:45','0000-00-00 00:00:00',1,1,0),
(122,1,0,'Apple touch ikon 114','apple_touch_icon_114','path|max_length[500]','','','Egy az apple alkalmazásnak megfelelő 114x114 méretű ikon','image_link','2016-04-08 09:54:26','0000-00-00 00:00:00',1,1,0),
(123,1,0,'Rendelésnél jelenjen meg az állam','is_state_visible','integer|between[0,1]','','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2016-04-19 10:55:18','2017-04-03 16:03:21',1,1,0),
(124,1,0,'Látható a hírlevélre feliratkozó form?','is_subscribe_form_visible','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2016-04-26 09:20:12','2017-04-03 16:03:32',1,1,0),
(125,0,0,'Email csatolmányok max száma','max_attachment_numbers','positive|between[1,10]','3',NULL,'Egy pozitív szám','text','2016-04-26 16:40:58','0000-00-00 00:00:00',1,1,0),
(126,1,0,'Összes nyelvű kommentek megjelenítése az adatlapokon','show_comments_on_any_language','integer|between[0,1]','0','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2016-04-29 11:16:52','0000-00-00 00:00:00',1,1,0),
(127,1,0,'','default_image','path|max_length[500]','','','','image_link','2016-05-04 11:47:06','0000-00-00 00:00:00',1,1,0),
(128,1,0,'','is_tracking_needed_for_order','positive|between[0,1]','','','','checkbox','2016-05-04 11:48:32','2017-11-24 09:43:45',1,1,0),
(129,1,0,'','tag_manager_code','alpha_dash|max_length[50]','GTM-TRZS92R','','','text','2016-06-01 11:22:44','0000-00-00 00:00:00',1,1,0),
(130,1,2,'','tag_manager_code','alpha_dash|max_length[50]','','','','text','2016-06-01 11:23:37','0000-00-00 00:00:00',1,1,0),
(131,1,1,'','tag_manager_code','aplha_dash|max_length[50]','','','','text','2016-06-01 12:59:25','0000-00-00 00:00:00',1,1,0),
(155,0,0,'Az oldalra be lehessen jelentkezni facebook konttal','use_facebook_login','integer|between[0,1]','0','','','checkbox','2016-09-09 09:43:42','0000-00-00 00:00:00',1,1,0),
(132,2,0,'Az oldal pinterest oldala','pinterest_link','link|max_length[150]','','','Pinterest oldalra veztő link','text','2016-06-01 16:24:36','0000-00-00 00:00:00',1,1,0),
(133,2,0,'Az oldal instagram oldala','instagram_link','link|max_length[150]','','','Instagram oldalra veztő link','text','2016-06-01 16:25:42','0000-00-00 00:00:00',1,1,0),
(134,1,0,'','generate_news_into_sitemap','integer|between[0,1]','0','','','checkbox','2016-06-10 17:04:33','0000-00-00 00:00:00',1,1,0),
(135,1,0,'','is_login_required_to_order','integer|between[0,1]','','','','checkbox','2016-06-13 23:04:09','2017-04-03 16:04:05',1,1,0),
(136,1,0,'','new_disable_subcategories','integer|between[0,1]','0','','','checkbox','2016-06-13 23:36:23','0000-00-00 00:00:00',1,1,0),
(137,1,0,'','new_use_intro','integer|between[0,1]','1','','','checkbox','2016-06-13 23:37:10','0000-00-00 00:00:00',1,1,0),
(138,1,0,'','new_use_description','integer|between[0,1]','1','','','checkbox','2016-06-13 23:38:17','0000-00-00 00:00:00',1,1,0),
(139,1,0,'','new_use_image','integer|between[0,1]','1','','','checkbox','2016-06-13 23:39:04','0000-00-00 00:00:00',1,1,0),
(140,1,0,'','new_use_images','integer|between[0,1]','1','','','checkbox','2016-06-13 23:39:54','0000-00-00 00:00:00',1,1,0),
(141,1,0,'','new_use_files','integer|between[0,1]','1','','','checkbox','2016-06-13 23:40:33','0000-00-00 00:00:00',1,1,0),
(142,0,0,'Megengedett hibás regisztrációk száma 1 perc alatt','registration_errors_permitted_in_one_minute','positive|between[1,10]','5','','Maximális hibás regisztrációk száma','text','2016-06-17 14:42:14','0000-00-00 00:00:00',1,1,0),
(143,1,0,'Tax/TVA','tax','integer|between[1,100]','20','','','text','2016-06-21 16:28:34','0000-00-00 00:00:00',1,1,0),
(144,1,0,'','show_pagination','integer|between[0,1]','1','','','checkbox','2016-06-22 13:51:41','2017-04-03 16:05:29',1,1,0),
(145,0,0,'','is_https_enabled','integer|beetwen[0,1]','0','','','checkbox','2016-06-22 14:55:29','0000-00-00 00:00:00',1,1,0),
(146,1,0,'Rendelés sikeres statusz ID','order_success_status_id','numeric|max_length[20]','2','','','text','2016-06-24 09:07:10','0000-00-00 00:00:00',1,1,0),
(147,1,0,'Könyvtár létrehozásnál alap jogok','chmod_for_created_folders','numeric|max_length[4]','0777','','','text','2016-06-24 09:11:59','0000-00-00 00:00:00',1,1,0),
(148,1,0,'Fájl létrehozásnál alap jogok','chmod_for_created_files','numeric|max_length[4]','0777','','','text','2016-06-24 09:11:59','0000-00-00 00:00:00',1,1,0),
(149,1,0,'Szerver IP címe','server_ip_address','ip|max_length[15]','81.18.91.114','','A szerver IP címe','text','2016-06-28 09:23:19','2017-11-24 09:20:46',1,1,0),
(150,1,0,'IP cím ellenőrzése belépésnél','check_ip_address_for_login','integer|between[0,1]','0','','','checkbox','2016-06-30 09:18:24','0000-00-00 00:00:00',1,1,0),
(151,1,5,'Google oldal ellenőrző kód','google_site_verification_code','alpha_dash|max_length[50]','3_YjNGKGl4qDrJP-H0w7bwhvKTJyohjMzStB25j4PCs','','A googletől kapott kód','text','2016-07-04 15:28:13','2017-11-24 09:40:27',1,1,0),
(152,1,0,'Pénzem frissítő link','currency_update_exchange_value_link','link|xss_clean|max_length[255]','http://www.bnro.ro/nbrfxrates.xml','','Az xmlt vagy jsont visszaadó link','text','2016-07-06 16:28:56','0000-00-00 00:00:00',0,0,0),
(153,1,0,'TVA-kat frissítő link','vat_rates_update_link','link|xss_clean|max_length[255]','https://euvatrates.com/rates.json','','A jsont visszaadó link','text','2016-07-06 16:31:29','0000-00-00 00:00:00',0,0,0),
(154,0,0,'VAT szám validáló link','vat_number_validation_link','link|css_clean|max_length[255]','http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl','','A validáláshoz szükséges link','text','2016-07-07 13:47:49','0000-00-00 00:00:00',0,0,0),
(156,0,0,'Az oldalra be lehessen jelentkezni google konttal','use_google_login','integer|beetwen[0,1]','0','','','checkbox','2016-09-09 09:45:05','0000-00-00 00:00:00',1,1,0),
(157,1,0,'','event_disable_subcategories','integer|beetwen[0,1]','0','','','checkbox','2016-10-08 17:32:12','0000-00-00 00:00:00',1,1,0),
(158,1,0,'','event_use_intro','integer|beetwen[0,1]','0','','','checkbox','2016-10-08 17:34:35','0000-00-00 00:00:00',1,1,0),
(159,1,0,'','event_use_description','integer|beetwen[0,1]','0','','','checkbox','2016-10-08 17:35:20','0000-00-00 00:00:00',1,1,0),
(160,1,0,'','event_use_image','integer|beetwen[0,1]','0','','','checkbox','2016-10-08 17:35:58','0000-00-00 00:00:00',1,1,0),
(161,1,0,'','event_use_images','integer|beetwen[0,1]','0','','','checkbox','2016-10-08 17:36:39','0000-00-00 00:00:00',1,1,0),
(162,1,0,'','event_use_files','integer|beetwen[0,1]','0','','','checkbox','2016-10-08 17:37:25','0000-00-00 00:00:00',1,1,0),
(163,1,0,'Rendelések sikeres fizetésének státusza','payment_success_status_id','positive|max_length[20]','2','','A sikeres státusznak az ID-ja','text','2016-11-02 12:52:40','0000-00-00 00:00:00',1,1,0),
(164,0,0,'Események ellenőrzésének időintervalluma percben','admin_check_event_interval','positive|max_length[1]','5','','Az automatikus ellenőrzés intervalluma percben','text','2016-11-17 10:33:39','0000-00-00 00:00:00',1,1,0),
(165,1,0,'Az oldal mutatása teljes képernyőn','is_page_using_full_screen','integer|between[0,1]','','','','checkbox','2016-11-21 10:49:36','2017-04-03 16:01:26',1,1,0),
(166,1,0,'A rendszerről','about_the_system_hu','normal|max_length[5000]','','<h1>M.Y.S.E</h1>\r\n\r\n<p><strong>Verzió</strong>: 1.0.0, <strong>Készítette</strong>: <a href=\\\"http://www.sakersoft.hu\\\"> Saker Soft SRL</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>A rendszer használata egy valós licensz kulcsot igényel.<br />\r\nAz ön kulcsa: {license_key}</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ez a felület ellenőrzi az ön számára a rendszer frissességét. Amennyiben a rendszer valamelyik részéből létezik újabb változat azt az alábbi listában láthatja. A rendszerelemek újabb változatának telepítéséhez a következő lépések szükségesek:</p>\r\n\r\n<ol>\r\n	<li>Az újabb változat letöltése, melyet a <strong>letöltés</strong> gombra kattinva tehet meg</li>\r\n	<li>A letöltés után az elem kicsomagolása és telepítése, melyet a <strong>telepítés</strong> gombra kattintva tehet meg</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','','textarea','2016-11-24 16:44:28','0000-00-00 00:00:00',1,0,0),
(167,1,0,'A rendszer frissítések kereséséhez a domain','update_checker_link','link|max_length[255]','http://www.sakersoft.ro/','','A frissítéseket tartalmazó weboldal címe','text','2016-11-25 13:40:36','0000-00-00 00:00:00',0,0,0),
(168,1,0,'Csapat kategóriáknál tiltjuk az alkategóriákat?','team_disable_subcategories','integer|between[0,1]','0','','Értékek: Van pipa: Igen, Nincs pipa: Nem','checkbox','2016-11-29 12:27:07','0000-00-00 00:00:00',1,1,0),
(169,1,0,'Csapatoknál bemutató szöveg használata','team_use_intro','integer|between[0,1]','1','','Értékek: Van pipa: Igen, Nincs pipa: Nem','checkbox','2016-11-29 12:28:39','0000-00-00 00:00:00',1,1,0),
(170,1,0,'Csapatoknál leírás használata','team_use_description','integer|between[0,1]','1','','Értékek: Van pipa: Igen, Nincs pipa: Nem','checkbox','2016-11-29 12:28:39','0000-00-00 00:00:00',1,1,0),
(171,1,0,'Csapatoknál használjuk a fő képet?','team_use_image','integer|between[0,1]','1','','Értékek: Van pipa: Igen, Nincs pipa: Nem','checkbox','2016-11-29 12:28:39','0000-00-00 00:00:00',1,1,0),
(172,1,0,'Csapatoknál használjuk a kép galériát?','team_use_images','integer|between[0,1]','1','','Értékek: Van pipa: Igen, Nincs pipa: Nem','checkbox','2016-11-29 12:28:39','0000-00-00 00:00:00',1,1,0),
(173,1,0,'Csapatoknál használjuk a fájl galériát?','team_use_files','integer|between[0,1]','1','','Értékek: Van pipa: Igen, Nincs pipa: Nem','checkbox','2016-11-29 12:28:39','0000-00-00 00:00:00',1,1,0),
(174,1,0,'Admin IP címe','admin_ip_address','ip|max_length[15]','82.79.184.244','','','text','2016-12-01 11:55:53','2017-11-24 09:36:30',1,1,0),
(175,1,0,'Sitemap tartalmazza a csapatokat','generate_teams_into_sitemap','integer|between[0,1]','0','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2017-02-02 15:15:23','0000-00-00 00:00:00',1,1,0),
(176,1,0,'Sitemap tartalmazza az eseményeket','generate_events_into_sitemap','integer|between[0,1]','0','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2017-02-02 15:15:23','0000-00-00 00:00:00',1,1,0),
(177,1,0,'Ecommerce engedélyezés','is_ecommerce_enabled','integer|between[0,1]','0','','Értékek: Van pipa - Igen, Nincs pipa - Nem','checkbox','2017-02-10 11:39:00','0000-00-00 00:00:00',1,1,0),
(178,1,0,'','license_code','alpha_numeric|max_length[64]','','','Licensz kód','text','2017-02-27 10:03:40','2017-09-13 12:26:14',0,1,0),
(179,1,0,'','license_expire_date','datetime','2020-12-31','','Lejárati dátum','text','2017-02-27 10:08:28','0000-00-00 00:00:00',0,1,0),
(180,1,0,'A kapcsolat oldalon térkép megjelenítése','contact_map','link|max_length[1000]','','','Kapcsolat oldalon megjelenő térkép','text','2017-03-10 12:36:49','0000-00-00 00:00:00',1,1,0),
(181,1,4,'Google oldal ellenőrző kód','google_site_verification_code','alpha_dash|max_length[50]','','','A googletől kapott kód','text','2017-04-24 09:54:12','0000-00-00 00:00:00',1,1,0),
(182,1,0,'','limit_items_on_front_home_page','integer|between[1,10]','5','','Az elemek száma','text','2017-05-11 15:09:53','0000-00-00 00:00:00',1,1,0),
(183,1,0,'Facebook megosztás használata','use_facebook_share','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2017-05-16 14:38:12','2018-03-23 16:14:03',1,1,0),
(184,1,0,'Google megosztás használata','use_google_share','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2017-05-16 14:39:01','2018-03-23 16:14:03',1,1,0),
(185,1,0,'Twitter megosztás használata','use_twitter_share','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2017-05-16 14:39:38','2018-03-23 16:14:04',1,1,0),
(186,1,0,'Kategória megjelenítése a főoldalon','show_product_categories_on_front_page','integer|beetween[0,1]','','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2017-04-27 10:57:04','2017-11-24 09:44:42',1,1,0),
(187,1,0,'A blog kategóriánál tiltjuk az alkategóriákat?','blog_disable_subcategories','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2017-06-26 15:44:16','0000-00-00 00:00:00',1,1,0),
(188,1,0,'A blogoknál használjuk a leírást?','blog_use_description','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2017-06-26 15:46:18','0000-00-00 00:00:00',1,1,0),
(189,1,0,'A blogoknál használjuk az intro-t?','blog_use_intro','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2017-06-26 15:47:45','0000-00-00 00:00:00',1,1,0),
(190,1,0,'A blogoknál használjuk fő képet?','blog_use_image','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2017-06-26 15:50:29','0000-00-00 00:00:00',1,1,0),
(191,1,0,'A blogoknál használjuk a kép galériát?','blog_use_images','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2017-06-26 15:52:22','0000-00-00 00:00:00',1,1,0),
(192,1,0,'A blogoknál használjuk a fájl galériát?','blog_use_files','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2017-06-26 15:54:19','0000-00-00 00:00:00',1,1,0),
(193,1,0,'Kereső megjelenítése','is_search_visible','integer|between[0,1]','1','','Értékek: Van pipa - Engedélyezzük, Nincs pipa - Tiltjuk','checkbox','2018-03-06 10:56:14','0000-00-00 00:00:00',1,1,0);

/*Table structure for table `m_themes` */

DROP TABLE IF EXISTS `m_themes`;

CREATE TABLE `m_themes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `modified_user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `is_for_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_for_front` tinyint(1) NOT NULL DEFAULT '0',
  `can_be_deleted` tinyint(1) NOT NULL DEFAULT '1',
  `insert_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modify_date` datetime DEFAULT '0000-00-00 00:00:00',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_enabled_on_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_enabled_on_front` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `themes_id_deleted` (`id`,`is_deleted`),
  KEY `themes_id_front_deleted` (`id`,`is_enabled_on_front`,`is_deleted`),
  KEY `themes_id_admin_deleted` (`id`,`is_enabled_on_admin`,`is_deleted`),
  KEY `themes_id_default_deleted` (`id`,`is_default`,`is_deleted`),
  KEY `themes_code_deleted` (`code`,`is_deleted`),
  KEY `themes_code_front_deleted` (`code`,`is_enabled_on_front`,`is_deleted`),
  KEY `themes_code_front_enabled_deleted` (`code`,`is_for_front`,`is_enabled_on_front`,`is_deleted`),
  KEY `themes_id_front_enabled_deleted` (`id`,`is_for_front`,`is_enabled_on_front`,`is_deleted`),
  KEY `themes_deleted` (`is_deleted`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `m_themes` */

insert  into `m_themes`(`id`,`modified_user_id`,`name`,`code`,`image`,`is_for_admin`,`is_for_front`,`can_be_deleted`,`insert_date`,`modify_date`,`is_default`,`is_enabled_on_admin`,`is_enabled_on_front`,`is_deleted`) values 
(1,1,'Test','test','upload/themes/test/images/test.png',1,1,0,'2015-10-13 09:39:34','2016-05-08 13:47:18',1,1,1,0);

/*Table structure for table `m_users` */

DROP TABLE IF EXISTS `m_users`;

CREATE TABLE `m_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `added_user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_type_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `invited_user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `modified_user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `domain_id` bigint(20) NOT NULL DEFAULT '0',
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'hu',
  `last_login_ip` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_receiving_newsletter` tinyint(1) NOT NULL DEFAULT '1',
  `insert_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modify_date` datetime DEFAULT '0000-00-00 00:00:00',
  `last_login_date` datetime DEFAULT '0000-00-00 00:00:00',
  `is_third_party_login` tinyint(1) NOT NULL DEFAULT '0',
  `third_party_login_from` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `third_party_user_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `users_domain_email` (`domain_id`,`email`,`is_enabled`,`is_deleted`),
  KEY `users_domain_email_password` (`domain_id`,`email`,`password`,`is_enabled`,`is_deleted`),
  KEY `users_domain_email_is_third_party` (`domain_id`,`email`,`is_third_party_login`,`is_enabled`,`is_deleted`),
  KEY `users_email_is_third_party` (`email`,`is_third_party_login`,`is_enabled`,`is_deleted`),
  KEY `users_email_password` (`email`,`password`,`is_enabled`,`is_deleted`),
  KEY `users_id_enabled_deleted` (`id`,`is_enabled`,`is_deleted`),
  KEY `users_id_deleted` (`id`,`is_deleted`),
  KEY `users_deleted` (`is_deleted`)
) ENGINE=MyISAM AUTO_INCREMENT=131015 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `m_users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
