if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class %FILE_NAME%
 *
 %PROPERTIES%
 *
 * @version 1.0
 */
class %FILE_NAME% extends %EXTENDED_MODEL%
{
    public function __construct()
    {
        parent::__construct();

        $this->table_name = "%TABLE_NAME%";
        $this->primary_key= "%PRIMARY_KEY%";
    }

    /**
     * @return %FILE_NAME%
     */
    public static function Get_model()
    {
        return parent::Get_model();
    }
}

/* End of file %FILE_NAME%.php */
/* Location: %FILE_PATH% */