<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// 
//	This variable have the path to the framework folder, 
//	In this COREPATH will be all folder and files,
//	needed for the framework
//	examples: libraries, models, etc
//
define( "COREPATH",     __DIR__.DIRECTORY_SEPARATOR );
define( "MODULEPATH",   BASEPATH."Core".DIRECTORY_SEPARATOR."Modules".DIRECTORY_SEPARATOR );
define( "PLUGINPATH",   BASEPATH."Core".DIRECTORY_SEPARATOR."Plugins".DIRECTORY_SEPARATOR );


/*****************************************************
*	SET MAX EXECUTION TIME
*****************************************************/
// 
//	This execution time is needed for all request, 
//	if it is needed more than this number for request, 
//	the request will be terminated
//
ini_set( 'max_execution_time',	360 );


/*****************************************************
*	SET SHORT OPEN TAGS
*****************************************************/
// 
//	This short open tags allows to the php to execute
//	the php tags if these are starting with <? and not
//	with <?php, and for the rss file, this must be
//	set to off 
//
ini_set( 'short_open_tag',      'Off' );


/*****************************************************
*	Gzip compression
*****************************************************/
//
//	Enable the gzip compression for html files
//
//ini_set( 'zlib.output_compression', 'On' );
    

/*****************************************************
*	Include required files
*****************************************************/
require_once COREPATH."Base".DIRECTORY_SEPARATOR."Config".DIRECTORY_SEPARATOR."Includes.php";


/*****************************************************
*	AUTOLOAD REGISTER
*****************************************************/
// 
//	Set the autoload register, for autoload classes
//	when we will instanciate or load them
//
spl_autoload_register( "General_functions::Autoload_class", TRUE );


/*****************************************************
*	ERROR HANDLER
*****************************************************/
// 
//	Set the error handler for php errors
//
set_error_handler( "General_functions::Error_handler" );