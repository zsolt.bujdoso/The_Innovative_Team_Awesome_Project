<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Email_address"                 => "Email address",
    "validation_required"           => "Het veld is verplicht '%s'",
    "validation_email"              => "Het veld '%s' moet een geldig e-mailadres bevatten",
    "validation_length"             => "Het veld '%s' moet precies %d teken bevatten",
    "validation_min_length"		    => "Het veld '%s' moet minimaal %d teken bevatten",
    "validation_max_length"		    => "Het veld '%s' moet kleiner zijn dan %d tekens bevatten",
    "validation_match"              => "Het veld '%s' is niet gelijk met het veld '%s'",
    "validation_numeric"            => "Voor het veld '%s' is alleen numerieke waarden toegestaan",
    "validation_integer"            => "Het veld '%s' moet een integer waarde",
    "validation_address"            => "Het veld '%s' moet een geldig adres bevatten",
    "validation_alpha"              => "Het veld '%s' mag alleen alfanumerieke tekens bevatten",
    "validation_alpha_numeric"	    => "Het veld '%s' mag alleen alfanumerieke en numerieke tekens bevatten",
    "validation_alpha_dash"		    => "Het veld '%s' mag alleen bevatten alfanumeriek, numeriek en ._- tekens bevatten",
    "validation_alpha_dash_slash"	=> "Het veld '%s' mag alleen bevatten alfanumeriek, numeriek en ._-/ tekens bevatten",
    "validation_alpha_dash_slash_space"	=> "Het veld '%s' mag alleen bevatten alfanumeriek, numeriek, ruimte en ._-/ tekens bevatten",
    "validation_alpha_dash_space"   => "Het veld '%s' mag alleen bevatten alfanumeriek, numeriek, ruimte en ._- tekens bevatten",
    "validation_ip"                 => "Het veld '%s' moet een geldig IP-adres bevatten",
    "validation_xss"                => "Het veld '%s' kan geen scripts of een deel van scripts bevatten",
    "validation_normal"             => "Het veld '%s' mag alleen normale tekens bevatten",
    "validation_date"               => "Het veld '%s' moet een geldige datum formaat. Bijv.: ".date( "Y-m-d" ),
    "validation_datetime"           => "Het veld '%s' moet een geldige datum en tijd formaat. Bijv.: ".date( "Y-m-d H:i:s" ),
    "validation_min_date"           => "Voor het veld '%s' de waarde moet groter zijn dan de huidige datum. Bijv.: ".date( "Y-m-d" ),
    "validation_min_datetime"       => "Voor het veld '%s' de waarde moet groter zijn dan de actuele datum en tijd. Bijv.: ".date( "Y-m-d H:i:s" ),
    "validation_max_date"           => "Voor het veld '%s' de waarde moet lager zijn dan de huidige datum. Bijv.: ".date( "Y-m-d" ),
    "validation_max_datetime"       => "Voor het veld '%s' de waarde moet lager zijn dan de actuele datum en tijd. Bijv.: ".date( "Y-m-d H:i:s" ),
    "validation_number"             => "Het veld '%s' moet een numerieke waarde bevatten",
    "validation_password_a"         => "Het wachtwoord moet een kleine letter bevatten",
    "validation_password_A"         => "Het wachtwoord moet een hoofdletter bevatten",
    "validation_password_0"         => "Het wachtwoord moet een numerieke teken bevatten",
    "validation_password_white"     => "Het wachtwoord moet een leesteken bevatten (., _-)",
    "validation_link"               => "Het veld '%s' mag alleen tekens die zijn toegestaan in een koppeling bevatten",
    "validation_path"               => "Het veld '%s' moet een geldig pad formaat bevatten",
    "validation_max_value"          => "Het veld '%s' moet waarden lager dan %d bevatten",
    "validation_min_value"          => "Het veld '%s' moet waarden hoger dan %d bevatten",
    "validation_negative"           => "Het veld '%s' alleen negatieve waarden toegestaan (<0)",
    "validation_positive"           => "Het veld '%s' er zijn alleen positieve waarden toegestaan (<0)",
    "validation_between"            => "Het veld '%s' alleen waarden tussen %d en %d mogelijk maakt",
    "validation_phone_length"       => "De lengte van het telefoonnummer onjuist",
    "validation_name"               => "Het veld '%s' mag alleen bevatten alfanumeriek, numeriek, ruimte en -' tekens bevatten",
    "validation_title_name"         => "Het veld '%s' mag alleen bevatten alfanumeriek, numeriek, ruimte en .,+_-\"!:@'?& tekens bevatten",
    "validation_filename"           => "Het veld '%s' mag alleen bevatten alfanumeriek, numeriek en ._- tekens bevatten",
    "validation_module_name"        => "Het veld '%s' mag alleen bevatten alfanumeriek, numeriek en _- tekens bevatten",
    "validation_color"              => "Het veld '%s' mag alleen bevatten alfanumeriek (A-F) en numeriek (0-9)",
    "validation_domain_valid"       => "The domain name in '%s' field doesn't exist",
    "validation_lesser"             => "The field '%s' must contain a number lesser than %d",
    "validation_lesser_or_equal"    => "The field '%s' must contain a number lesser or equal than %d",
    "validation_bigger"             => "The field '%s' must contain a number bigger than %d",
    "validation_bigger_or_equal"    => "The field '%s' must contain a number bigger or equal than %d",
    "Error_with_rules"              => "De regels zijn niet goed gedefinieerd",
);

/* End of file Validation_lang.php */
/* Location: ./core/languages/english/ */