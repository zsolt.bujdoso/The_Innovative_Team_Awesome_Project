<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Email_address"                 => "Email address",
    "validation_required"           => "È necessario il campo '%s'",
    "validation_email"              => "Il campo '%s' deve contenere un indirizzo email valido",
    "validation_length"             => "Il campo '%s' deve contenere esattamente %d carattere",
    "validation_min_length"		    => "Il campo '%s' deve contenere almeno %d carattere",
    "validation_max_length"		    => "Il campo '%s' deve contenere meno di %d caratteri",
    "validation_match"              => "Il campo '%s' non è uguale al campo '%s'",
    "validation_numeric"            => "Per il campo '%s' è solo valori numerici accettati",
    "validation_integer"            => "Il campo '%s' deve essere un valore intero",
    "validation_address"            => "Il campo '%s' deve contenere un indirizzo valido",
    "validation_alpha"              => "Il campo '%s' deve contenere solo caratteri alfanumerici",
    "validation_alpha_numeric"	    => "Il campo '%s' deve contenere solo caratteri alfanumerici e caratteri numerici",
    "validation_alpha_dash"		    => "Il campo '%s' deve contenere solo caratteri alfanumerici, numerici e ._- caratteri",
    "validation_alpha_dash_slash"	=> "Il campo '%s' deve contenere solo caratteri alfanumerici, numerici e ._-/ caratteri",
    "validation_alpha_dash_slash_space"	=> "Il campo '%s' deve contenere solo caratteri alfanumerici, numerici, spazio e ._-/ caratteri",
    "validation_alpha_dash_space"   => "Il campo '%s' deve contenere solo caratteri alfanumerici, numerici, spazio e ._- caratteri",
    "validation_ip"                 => "Il campo '%s' deve contenere un indirizzo IP valido",
    "validation_xss"                => "Il campo '%s' non può contenere script o parte di script",
    "validation_normal"             => "Il campo '%s' può contenere solo caratteri normali",
    "validation_date"               => "Il campo '%s' deve essere un formato data. Es.: ".date( "Y-m-d" ),
    "validation_datetime"           => "Il campo '%s' deve essere un formato data e ora valida. Es.: ".date( "Y-m-d H:i:s" ),
    "validation_min_date"           => "Il valore '%s' deve essere maggiore della data corrente sia Es.: ".date( "Y-m-d" ),
    "validation_min_datetime"       => "Il valore '%s' deve essere maggiore di data e ora corrente. Es.: ".date( "Y-m-d H:i:s" ),
    "validation_max_date"           => "Il valore '%s' deve essere inferiore alla data corrente. Es.: ".date( "Y-m-d" ),
    "validation_max_datetime"       => "Il valore '%s' deve essere inferiore la data e l'ora correnti. Es.: ".date( "Y-m-d H:i:s" ),
    "validation_number"             => "Il campo '%s' deve contenere un valore numerico",
    "validation_password_a"         => "La password deve contenere un carattere minuscolo",
    "validation_password_A"         => "La password deve contenere un carattere maiuscolo",
    "validation_password_0"         => "La password deve contenere un carattere numerico",
    "validation_password_white"     => "La password deve contenere un segno di punteggiatura (., _-)",
    "validation_link"               => "Il campo '%s' può contenere solo caratteri che sono ammessi in un collegamento",
    "validation_path"               => "Il campo '%s' deve contenere un formato di percorso valido",
    "validation_max_value"          => "Il campo '%s' deve contenere valori inferiori a %d",
    "validation_min_value"          => "Il campo '%s' deve contenere valori superiori a %d",
    "validation_negative"           => "Per il campo '%s' solo valori negativi consentito (<0)",
    "validation_positive"           => "Per il campo '%s' Ci sono solo valori positivi consentiti (<0)",
    "validation_between"            => "Per il campo '%s' solo i valori tra %d e %d permette",
    "validation_phone_length"       => "La lunghezza del numero di telefono non è corretto",
    "validation_name"               => "Il campo '%s' deve contenere solo caratteri alfanumerici, numerici, spazio e -' caratteri",
    "validation_title_name"         => "Il campo '%s' deve contenere solo caratteri alfanumerici, numerici, spazio e .,+_-\"!:@&'? caratteri",
    "validation_filename"           => "Il campo '%s' deve contenere solo caratteri alfanumerici, numerici e ._- caratteri",
    "validation_module_name"        => "Il campo '%s' deve contenere solo caratteri alfanumerici, numerici e _- caratteri",
    "validation_color"              => "Il campo '%s' deve contenere solo caratteri alfanumerici (A-F) e numerici (0-9)",
    "validation_domain_valid"       => "The domain name in '%s' field doesn't exist",
    "validation_lesser"             => "The field '%s' must contain a number lesser than %d",
    "validation_lesser_or_equal"    => "The field '%s' must contain a number lesser or equal than %d",
    "validation_bigger"             => "The field '%s' must contain a number bigger than %d",
    "validation_bigger_or_equal"    => "The field '%s' must contain a number bigger or equal than %d",
    "Error_with_rules"              => "Le regole non sono definite correttamente",
);

/* End of file Validation_lang.php */
/* Location: ./core/languages/english/ */