<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Email_address"                 => "Emailadresse",
    "validation_required"           => "Das Feld '%s' ist nicht erforderlich",
    "validation_email"              => "Das Feld '%s' muss eine gültige E-Mail-Adresse enthalten",
    "validation_length"             => "Das Feld '%s' muss genau %d Zeichen enthalten",
    "validation_min_length"		    => "Das Feld '%s' muss mindestens %d Zeichen enthalten",
    "validation_max_length"		    => "Das Feld '%s' muss weniger als %d Zeichen enthalten",
    "validation_match"              => "Das Feld '%s' hat nicht den gleiche Wert wie Feld '%s'",
    "validation_numeric"            => "Für das Feld '%s' sind nur numerische Werte erlaubt",
    "validation_integer"            => "Das Feld '%s' muss ein Integer-Wert sein",
    "validation_address"            => "The field %s must contains a valid address",
    "validation_alpha"              => "Das Feld '%s' darf nur alpha Zeichen enthalten",
    "validation_alpha_numeric"	    => "Das Feld '%s' darf nur buchstaben und numerische Zeichen enthalten",
    "validation_alpha_dash"		    => "Das Feld '%s' darf nur buchstaben, numerische und ._- Zeichen enthalten",
    "validation_alpha_dash_slash"	=> "Das Feld '%s' darf nur buchstaben, numerische und ._-/ Zeichen enthalten",
    "validation_alpha_dash_slash_space"	=> "Das Feld '%s' darf nur buchstaben, numerische, Leerzeichen und ._-/ Zeichen enthalten",
    "validation_alpha_dash_space"   => "Das Feld '%s' darf nur buchstaben, numerische, Leerzeichen und ._- Zeichen enthalten",
    "validation_ip"                 => "Das Feld '%s' muss eine korrekte IP-Adresse enthalten",
    "validation_xss"                => "Das Feld '%s' darf keine Skripte oder Teile von Skripten enthalten",
    "validation_normal"             => "Das Feld '%s' darf nur normale Zeichen enthalten",
    "validation_date"               => "Das Feld '%s' muss ein gültiges Datum-Format sein. Bsp.: ".date( "Y-m-d" ),
    "validation_datetime"           => "Das Feld '%s' muss ein gültiges Datums- und Zeit-Format sein. Bsp .: ".date( "Y-m-d H:i:s" ),
    "validation_min_date"           => "Der Wert '%s' muss größer als das aktuelle Datum sein: Bsp.: ".date( "Y-m-d" ),
    "validation_min_datetime"       => "Der Wert '%s' muss größer sein als das aktuelle Datum und die Zeit. Bsp.: ".date( "Y-m-d H:i:s" ),
    "validation_max_date"           => "Der Wert '%s' muss unter dem aktuellen Datum sein. Bsp.: ".date( "Y-m-d" ),
    "validation_max_datetime"       => "Der Wert '%s' muss niedriger als der aktuelle Datum und die Zeit. Bsp.: ".date( "Y-m-d H:i:s" ),
    "validation_number"             => "Das Feld '%s' muss einen numerischen Wert enthalten",
    "validation_password_a"         => "Das Passwort muss einen Kleinbuchstaben enthalten",
    "validation_password_A"         => "Das Passwort muss einen Grossbuchstaben enthalten",
    "validation_password_0"         => "Das Passwort muss ein numerisches Zeichen enthalten",
    "validation_password_white"     => "Das Password muss ein Zeichen der Interpunktion enthalten (.,_-)",
    "validation_link"               => "Das Feld '%s' darf nur Zeichen enthalten die in einem Link erlaubt sind",
    "validation_path"               => "Das Feld '%s' muss eine gültige Pfadangabe enthalten",
    "validation_max_value"          => "Das Feld '%s' muss einen Wert kleiner %d enthalten",
    "validation_min_value"          => "Das Feld '%s' muss einen Wert grösser %d enthalten",
    "validation_negative"           => "Das Feld '%s' es sind nur negative Werte erlaubt (<0)",
    "validation_positive"           => "Das Feld '%s' es sind nur positive Werte erlaubt (>0)",
    "validation_between"            => "Das Feld '%s' es sind nur Werte zwischen %d und %d erlaubt",
    "validation_phone_length"       => "Die Länge der Telefonnummer ist inkorrekt",
    "validation_name"               => "Das Feld '%s' darf nur buchstaben, numerische, Leerzeichen und -' Zeichen enthalten",
    "validation_title_name"         => "Das Feld '%s' darf nur buchstaben, numerische, Leerzeichen und .,+_-\"!:@&'? Zeichen enthalten",
    "validation_filename"           => "Das Feld '%s' darf nur buchstaben, numerische und ._- Zeichen enthalten",
    "validation_module_name"        => "Das Feld '%s' darf nur buchstaben, numerische und _- Zeichen enthalten",
    "validation_color"              => "Das Feld '%s' darf nur buchstaben zwischen A-F und numerische",
    "validation_domain_valid"       => "The domain name in '%s' field doesn't exist",
    "validation_lesser"             => "The field '%s' must contain a number lesser than %d",
    "validation_lesser_or_equal"    => "The field '%s' must contain a number lesser or equal than %d",
    "validation_bigger"             => "The field '%s' must contain a number bigger than %d",
    "validation_bigger_or_equal"    => "The field '%s' must contain a number bigger or equal than %d",
    "Error_with_rules"              => "Die Regeln sind nicht korrekt definiert",
);

/* End of file Validation_lang.php */
/* Location: ./core/languages/english/ */