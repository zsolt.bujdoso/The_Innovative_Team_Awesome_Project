<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Email_address"                 => "Email cím",
    "validation_required"           => "A(z) '%s' mező kötelező",
    "validation_email"              => "A(z) '%s' mező egy helyes email címet kell tartalmazzon",
    "validation_length"             => "A(z) '%s' mező pontosan %d karaktert kell tartalmazzon",
    "validation_min_length"		    => "A(z) '%s' mező kötelezően kell tartalmazzon %d karaktert",
    "validation_max_length"		    => "A(z) '%s' mező kevesebb mint %d karaktert kell tartalmazzon",
    "validation_match"              => "A(z) '%s' mező nem egyenlő a '%s' mezővel",
    "validation_numeric"            => "A(z) '%s' mező csak számokat tartalmazhat",
    "validation_integer"            => "A(z) '%s' mező csak egy egész számot tartalmazhat",
    "validation_address"            => "A(z) '%s' mező csak egy helyes címet kell tartalmazzon",
    "validation_alpha"              => "A(z) '%s' mező csak betűket tartalmazhat",
    "validation_alpha_numeric"	    => "A(z) '%s' mező csak betűket és számokat tartalmazhat",
    "validation_alpha_dash"		    => "A(z) '%s' mező csak betűket, számokat és ._- karaktereket tartalmazhat",
    "validation_alpha_dash_slash"   => "A(z) '%s' mező csak betűket, számokat és ._-/ karaktereket tartalmazhat",
    "validation_alpha_dash_slash_space"   => "A(z) '%s' mező csak betűket, számokat, szünetet és ._-/ karaktereket tartalmazhat",
    "validation_alpha_dash_space"   => "A(z) '%s' mező csak betűket, számokat, szünetet és ._- karaktereket tartalmazhat",
    "validation_ip"                 => "A(z) '%s' mező csak egy helyes ip címet tartalmazhat",
    "validation_xss"                => "A(z) '%s' mező nem tartalmazhat semmilyen szkriptet vagy szkript részletet",
    "validation_normal"             => "A(z) '%s' mező csak normál használatos karaktereket tartalmazhat",
    "validation_date"               => "A(z) '%s' mező dátum formátum kell legyen. Pl.: ".date( "Y-m-d" ),
    "validation_datetime"           => "A(z) '%s' mező dátum és idő formátum kell legyen. Pl.: ".date( "Y-m-d H:i:s" ),
    "validation_min_date"           => "A(z) '%s' mező nagyobb dátumot kell tartalmazzon mint ".date( "Y-m-d" ),
    "validation_min_datetime"       => "A(z) '%s' mező nagyobb dátumot és órát kell tartalmazzon mint ".date( "Y-m-d H:i:s" ),
    "validation_max_date"           => "A(z) '%s' mező kisebb dátumot kell tartalmazzon mint ".date( "Y-m-d" ),
    "validation_max_datetime"       => "A(z) '%s' mező kisebb dátumot és órát kell tartalmazzon mint ".date( "Y-m-d H:i:s" ),
    "validation_number"             => "A(z) '%s' mező csak szám értéket tartalmazhat",
    "validation_password_a"         => "A jelszó mező kötelezően kell tartalmazzon egy kis betűt",
    "validation_password_A"         => "A jelszó mező kötelezően kell tartalmazzon egy nagy betűt",
    "validation_password_0"         => "A jelszó mező kötelezően kell tartalmazzon egy számot",
    "validation_password_white"     => "A jelszó mező kötelezően kell tartalmazzon egy punktuális karaktert (.,_-)",
    "validation_link"               => "A(z) '%s' mező csak linkben megengedett karaktereket tartalmazhat",
    "validation_path"               => "A(z) '%s' mező csak könyvtár struktúrát tartalmazhat",
    "validation_max_value"          => "A(z) '%s' mező értéke kisebb számot kell tartalmazzon, mint %d",
    "validation_min_value"          => "A(z) '%s' mező értéke nagyobb számot kell tartalmazzon, mint %d",
    "validation_negative"           => "A(z) '%s' mező értéke szigorúan 0-nál kisebb számot kell tartalmazzon (<0)",
    "validation_positive"           => "A(z) '%s' mező értéke szigorúan 0-nál nagyobb számot kell tartalmazzon (>0)",
    "validation_between"            => "A(z) '%s' mező értéke %d és %d között kell legyen",
    "validation_phone_length"       => "A(z) '%s' mező értéke nem tartalmaz elegendő számjegyet",
    "validation_name"               => "A(z) '%s' mező csak betűket, számokat, szünetet és -' karaktereket tartalmazhat",
    "validation_title_name"         => "A(z) '%s' mező csak betűket, számokat, szünetet és .,+_-\"!:@'?& karaktereket tartalmazhat",
    "validation_filename"           => "A(z) '%s' mező csak betűket, számokat és ._- karaktereket tartalmazhat",
    "validation_module_name"        => "A(z) '%s' mező csak betűket, számokat és _- karaktereket tartalmazhat",
    "validation_color"              => "A(z) '%s' mező csak betűket (A-F) és számokat (0-9) tartalmazhat",
    "validation_domain_valid"       => "A(z) '%s' mezőbe írt domén név nem létezik",
    "validation_lesser"             => "A(z) '%s' mező egy kisebb számot kell tartalmazzon, mint %d",
    "validation_lesser_or_equal"    => "A(z) '%s' mező egy kisebb vagy egyenlő számot kell tartalmazzon, mint %d",
    "validation_bigger"             => "A(z) '%s' mező egy nagyobb számot kell tartalmazzon, mint %d",
    "validation_bigger_or_equal"    => "A(z) '%s' mező egy nagyobb vagy egyenlő számot kell tartalmazzon, mint %d",
    "Error_with_rules"              => "Az értékek feltételei nincsenek jól beállítva",
);


/* End of file Validation_lang.php */
/* Location: ./core/Languages/hungarian/Validation_lang.php */