<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Email_address"                 => "Email address",
    "validation_required"           => "The field '%s' is required",
    "validation_email"              => "The field '%s' must contains a valid email address",
    "validation_length"             => "The field '%s' must contains %d character exactly",
    "validation_min_length"		    => "The field '%s' must contains minimum %d character",
    "validation_max_length"		    => "The field '%s' must contains less than %d character",
    "validation_match"              => "The field '%s' is not equal with the field '%s'",
    "validation_numeric"            => "The field '%s' must contains only numeric values",
    "validation_integer"            => "The field '%s' it is needed to be an integer value",
    "validation_address"            => "The field '%s' must contains a valid address",
    "validation_alpha"              => "The field '%s' must contains only alphabetical characters",
    "validation_alpha_numeric"	    => "The field '%s' must contains only alphabetical and numeric characters",
    "validation_alpha_dash"		    => "The field '%s' must contains only alphabetical, numerical and ._- characters",
    "validation_alpha_dash_slash"	=> "The field '%s' must contains only alphabetical, numerical and ._-/ characters",
    "validation_alpha_dash_slash_space"	=> "The field '%s' must contains only alphabetical, numerical, space and ._-/ characters",
    "validation_alpha_dash_space"   => "The field '%s' must contains only alphabetical, numerical, space and ._- characters",
    "validation_ip"                 => "The field '%s' must contains only a valid ip address",
    "validation_xss"                => "The field '%s' can't contains any scripts or part of scripts",
    "validation_normal"             => "The field '%s' must contains only normal characters",
    "validation_date"               => "The field '%s' must be a valid date format. Ex.: ".date( "Y-m-d" ),
    "validation_datetime"           => "The field '%s' must be a valid date and time format. Ex.: ".date( "Y-m-d H:i:s" ),
    "validation_min_date"           => "The field '%s' must be bigger date than actual: ".date( "Y-m-d" ),
    "validation_min_datetime"       => "The field '%s' must be bigger date and time than actual: ".date( "Y-m-d H:i:s" ),
    "validation_max_date"           => "The field '%s' must be lower date than actual: ".date( "Y-m-d" ),
    "validation_max_datetime"       => "The field '%s' must be lower date and time than actual: ".date( "Y-m-d H:i:s" ),
    "validation_number"             => "The field '%s' must contains a number value",
    "validation_password_a"         => "The password must contains a lowercase literal character",
    "validation_password_A"         => "The password must contains an uppercase literal character",
    "validation_password_0"         => "The password must contains a numerical character",
    "validation_password_white"     => "The password must contains a punctual character (.,_-)",
    "validation_link"               => "The field '%s' must contains only characters allowed in a link",
    "validation_path"               => "The field '%s' must contains a valid path format",
    "validation_max_value"          => "The field '%s' must contain lower numbers than %d",
    "validation_min_value"          => "The field '%s' must contain higher numbers than %d",
    "validation_negative"           => "The field '%s' must be strictly negative number (<0)",
    "validation_positive"           => "The field '%s' must be strictly positive number (>0)",
    "validation_between"            => "The field '%s' must contain between %d and %d",
    "validation_phone_length"       => "The length of phone number is incorrect",
    "validation_name"               => "The field '%s' must contains only alphabetical, numerical, space and -' characters",
    "validation_title_name"         => "The field '%s' must contains only alphabetical, numerical, space and .,+_-\"!:@'?& characters",
    "validation_filename"           => "The field '%s' must contains only alphabetical, numerical and ._- characters",
    "validation_module_name"        => "The field '%s' must contains only alphabetical, numerical and _- characters",
    "validation_color"              => "The field '%s' must contains only alphabetical (A-F) and numerical (0-9) characters",
    "validation_domain_valid"       => "The domain name in '%s' field doesn't exist",
    "validation_lesser"             => "The field '%s' must contain a number lesser than %d",
    "validation_lesser_or_equal"    => "The field '%s' must contain a number lesser or equal than %d",
    "validation_bigger"             => "The field '%s' must contain a number bigger than %d",
    "validation_bigger_or_equal"    => "The field '%s' must contain a number bigger or equal than %d",
    "Error_with_rules"              => "The rules are not set correctly",
);

/* End of file Validation_lang.php */
/* Location: ./core/languages/english/ */