<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Email_address"                 => "Email address",
    "validation_required"           => "Se requiere que el ancho de campo '%s'",
    "validation_email"              => "El campo '%s' debe contener una dirección válida de correo electrónico",
    "validation_length"             => "El campo '%s' debe contener exactamente %d carácter",
    "validation_min_length"		    => "El campo '%s' debe contener al menos %d carácter",
    "validation_max_length"		    => "El campo '%s' debe contener menos de %d caracteres",
    "validation_match"              => "El ancho del campo '%s' no es igual con el campo '%s'",
    "validation_numeric"            => "Para el campo '%s' es sólo valores numéricos permitió",
    "validation_integer"            => "El campo '%s' debe ser un valor entero",
    "validation_address"            => "The field %s must contains a valid address",
    "validation_alpha"              => "El campo '%s' sólo debe contener caracteres alfanuméricos",
    "validation_alpha_numeric"	    => "El campo '%s' debe contener solamente caracteres alfanuméricos y numéricos",
    "validation_alpha_dash"		    => "El campo '%s' sólo debe contener caracteres alfanuméricos, numéricos y ._- caracteres",
    "validation_alpha_dash_slash"	=> "El campo '%s' sólo debe contener caracteres alfanuméricos, numéricos y ._-/ caracteres",
    "validation_alpha_dash_slash_space"	=> "El campo '%s' sólo debe contener caracteres alfanuméricos, numéricos, el espacio y ._-/ caracteres",
    "validation_alpha_dash_space"   => "El campo '%s' sólo debe contener caracteres alfanuméricos, numéricos, el espacio y ._- caracteres",
    "validation_ip"                 => "El campo '%s' debe contener una dirección IP válida",
    "validation_xss"                => "El campo '%s' no puede contener scripts o parte de las secuencias de comandos",
    "validation_normal"             => "El campo '%s' sólo puede contener caracteres normales",
    "validation_date"               => "El campo '%s' debe ser un formato de fecha válido. Ej.: ".date( "Y-m-d" ),
    "validation_datetime"           => "El campo '%s' debe ser un formato de fecha y hora válido. Ej.: ".date( "Y-m-d H:i:s" ),
    "validation_min_date"           => "El valor '%s' debe ser mayor que la fecha actual sea Ej.: ".date( "Y-m-d" ),
    "validation_min_datetime"       => "El valor '%s' debe ser mayor que la fecha y hora actuales. Ej.: ".date( "Y-m-d H:i:s" ),
    "validation_max_date"           => "El valor '%s' debe ser inferior a la fecha actual. Ej.: ".date( "Y-m-d" ),
    "validation_max_datetime"       => "El valor '%s' debe ser menor que la fecha y hora actuales. Ej.: ".date( "Y-m-d H:i:s" ),
    "validation_number"             => "El campo '%s' debe contener un valor numérico",
    "validation_password_a"         => "La contraseña debe contener un carácter en minúscula",
    "validation_password_A"         => "La contraseña debe contener un carácter en mayúscula",
    "validation_password_0"         => "La contraseña debe contener un carácter numérico",
    "validation_password_white"     => "La contraseña debe contener un signo de especial (._-,)",
    "validation_link"               => "El campo '%s' sólo puede contener caracteres que se permiten en un enlace",
    "validation_path"               => "El campo '%s' debe contener un formato de ruta válida",
    "validation_max_value"          => "El campo '%s' debe contener valores inferiores a %d",
    "validation_min_value"          => "El campo '%s' debe contener valores superiores a %d",
    "validation_negative"           => "Per il campo '%s' sólo los valores negativos permitido (<0)",
    "validation_positive"           => "Per il campo '%s' hay sólo valores positivos permitidos (<0)",
    "validation_between"            => "Il campo '%s' sólo valores entre %d y %d permite",
    "validation_phone_length"       => "La longitud del número de teléfono es incorrecto",
    "validation_name"               => "El campo '%s' sólo debe contener caracteres alfanuméricos, numéricos, el espacio y -' caracteres",
    "validation_title_name"         => "El campo '%s' sólo debe contener caracteres alfanuméricos, numéricos, el espacio y .,+_-\"!:@'?& caracteres",
    "validation_filename"           => "El campo '%s' sólo debe contener caracteres alfanuméricos, numéricos y ._- caracteres",
    "validation_module_name"        => "El campo '%s' sólo debe contener caracteres alfanuméricos, numéricos y _- caracteres",
    "validation_color"              => "El campo '%s' sólo debe contener caracteres alfanuméricos (A-F) y numéricos (0-9)",
    "validation_domain_valid"       => "The domain name in '%s' field doesn't exist",
    "validation_lesser"             => "The field '%s' must contain a number lesser than %d",
    "validation_lesser_or_equal"    => "The field '%s' must contain a number lesser or equal than %d",
    "validation_bigger"             => "The field '%s' must contain a number bigger than %d",
    "validation_bigger_or_equal"    => "The field '%s' must contain a number bigger or equal than %d",
    "Error_with_rules"              => "Las reglas no están definidas correctamente",
);

/* End of file Validation_lang.php */
/* Location: ./core/languages/english/ */