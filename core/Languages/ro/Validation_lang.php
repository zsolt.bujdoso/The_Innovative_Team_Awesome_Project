<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Email_address"                 => "Adresa de email",
    "validation_required"           => "Câmpul '%s' este necesar",
    "validation_email"              => "Câmpul '%s' trebuie să conține o adresă de email validată",
    "validation_length"             => "Câmpul '%s' trebuie să conține %d caracter(i) exact",
    "validation_min_length"		    => "Câmpul '%s' trebuie să conține minimal %d caracter(i)",
    "validation_max_length"		    => "Câmpul '%s' trebuie să nu conține mai mult de %d caracter(i)",
    "validation_match"              => "Câmpul '%s' nu este egal cu câmpul '%s'",
    "validation_numeric"            => "Câmpul '%s' trebuie să conține numai numări",
    "validation_integer"            => "Câmpul '%s' trebuie să conține un număr natural",
    "validation_address"            => "Câmpul '%s' trebuie să conține o adresă validă",
    "validation_alpha"              => "Câmpul '%s' trebuie să conține numai litere",
    "validation_alpha_numeric"	    => "Câmpul '%s' trebuie să conține numai litere și numere",
    "validation_alpha_dash"		    => "Câmpul '%s' trebuie să conține numai litere, numere și ._- caracteri",
    "validation_alpha_dash_slash"	=> "Câmpul '%s' trebuie să conține numai litere, numere și ._-/ caracteri",
    "validation_alpha_dash_slash_space"	=> "Câmpul '%s' trebuie să conține numai litere, numere, spațiu și ._-/ caracteri",
    "validation_alpha_dash_space"   => "Câmpul '%s' trebuie să conține numai litere, spațiu, numere și ._- caracteri",
    "validation_ip"                 => "Câmpul '%s' trebuie să conține o adresă de ip validată",
    "validation_xss"                => "Câmpul '%s' nu poate conține nici un script",
    "validation_normal"             => "Câmpul '%s' trebuie să conține numai caractere normale",
    "validation_date"               => "Câmpul '%s' trebuie să aibă forma unei date. Ex.: ".date( "Y-m-d" ),
    "validation_datetime"           => "Câmpul '%s' trebuie să aibă forma unei date și ora. Ex.: ".date( "Y-m-d H:i:s" ),
    "validation_min_date"           => "Câmpul '%s' trebuie să conțină o dată validă mai mare decât ".date( "Y-m-d" ),
    "validation_min_datetime"       => "Câmpul '%s' trebuie să conțină o dată și ora validă mai mare decât ".date( "Y-m-d H:i:s" ),
    "validation_max_date"           => "Câmpul '%s' trebuie să conțină o dată validă mai mică decât ".date( "Y-m-d" ),
    "validation_max_datetime"       => "Câmpul '%s' trebuie să conțină o dată și ora validă mai mică decât ".date( "Y-m-d H:i:s" ),
    "validation_number"             => "Câmpul '%s' trebuie să conține numai o valoare de un număr",
    "validation_password_a"         => "Câmpul parolă trebuie să conține un caracter mic",
    "validation_password_A"         => "Câmpul parolă trebuie să conține un caracter mare",
    "validation_password_0"         => "Câmpul parolă trebuie să conține un caracter numeric",
    "validation_password_white"     => "Câmpul parolă trebuie să conține un caracter special (.,_-)",
    "validation_link"               => "Câmpul '%s' trebuie să conține numai caractere care sunt permise în linkuri",
    "validation_path"               => "Câmpul '%s' trebuie să aibă un format de drum valid între foldere",
    "validation_max_value"          => "Câmpul '%s' trebuie să conține un număr mai mic, decât %d",
    "validation_min_value"          => "Câmpul '%s' trebuie să conține un număr mai mare, decât %d",
    "validation_negative"           => "Câmpul '%s' trebuie să conține un număr strict negativ (<0)",
    "validation_positive"           => "Câmpul '%s' trebuie să conține un număr strict pozitiv (>0)",
    "validation_between"            => "Câmpul '%s' trebuie să conține o valoare între %d și %d",
    "validation_phone_length"       => "Pe câmpul Telefon numârul elementelor nu se potrivește",
    "validation_name"               => "Câmpul '%s' trebuie să conține numai litere, numere, spațiu și -' caracteri",
    "validation_title_name"         => "Câmpul '%s' trebuie să conține numai litere, numere, spațiu și .,+_-\"!:@'?& caracteri",
    "validation_filename"           => "Câmpul '%s' trebuie să conține numai litere, numere, și ._- caracteri",
    "validation_module_name"        => "Câmpul '%s' trebuie să conține numai litere, numere, și _- caracteri",
    "validation_color"              => "Câmpul '%s' trebuie să conține numai litere (A-F) și numere (0-9)",
    "validation_domain_valid"       => "Domeniu scris în câmpul '%s' nu există",
    "validation_lesser"             => "Câmpul '%s' trebuie să conține un număr mai mici decât %d",
    "validation_lesser_or_equal"    => "Câmpul '%s' trebuie să conține un număr mai mici decât %d sau egal",
    "validation_bigger"             => "Câmpul '%s' trebuie să conține un număr mai mari decât %d",
    "validation_bigger_or_equal"    => "Câmpul '%s' trebuie să conține un număr mai mari decât %d sau egal",
    "Error_with_rules"              => "Validații nu sunt setate corect",
);

/* End of file Validation_lang.php */
/* Location: ./core/languages/romanian/Validation_lang.php */