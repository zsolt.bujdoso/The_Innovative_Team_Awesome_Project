<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

return array(
    "Email_address"                 => "Email address",
    "validation_required"           => "La '%s' de champ est obligatoire",
    "validation_email"              => "Le champ '%s' doit contenir une adresse email valide",
    "validation_length"             => "Le champ '%s' doit contenir exactement %d caractère",
    "validation_min_length"		    => "Le champ '%s' doit contenir au moins %d caractère",
    "validation_max_length"		    => "Le champ '%s' doit contenir moins de %d personnages",
    "validation_match"              => "La '%s' de champ ne correspond pas avec le champ '%s'",
    "validation_numeric"            => "Pour le champ '%s' est que des valeurs numériques acceptés",
    "validation_integer"            => "Le champ '%s' doit être une valeur entière",
    "validation_address"            => "Le champ '%s' doit contenir une adresse valide",
    "validation_alpha"              => "Le champ '%s' ne doit contenir que des caractères alphanumériques",
    "validation_alpha_numeric"	    => "Le champ '%s' ne doit contenir que des caractères alphanumériques et numériques",
    "validation_alpha_dash"		    => "Le champ '%s' ne doit contenir que des caractères alphanumériques, numériques et ._- contenir des caractères",
    "validation_alpha_dash_slash"	=> "Le champ '%s' ne doit contenir que des caractères alphanumériques, numériques et ._-/ contenir des caractères",
    "validation_alpha_dash_slash_space"	=> "Le champ '%s' ne doit contenir que des caractères alphanumériques, numériques, l'espace et ._-/ contenir des caractères",
    "validation_alpha_dash_space"   => "Le champ '%s' ne doit contenir que des caractères alphanumériques, numériques, l'espace et ._- contenir des caractères",
    "validation_ip"                 => "Le champ '%s' doit contenir une adresse IP valide",
    "validation_xss"                => "Le champ '%s' ne peut pas contenir des scripts ou partie de scripts",
    "validation_normal"             => "Le champ '%s' ne peut contenir que des caractères normaux",
    "validation_date"               => "Le champ '%s' doit être un format de date valide. Ex.: ".date( "Y-m-d" ),
    "validation_datetime"           => "Le champ '%s' doit être un format de date et heure valide. Ex.: ".date( "Y-m-d H:i:s" ),
    "validation_min_date"           => "La valeur '%s' doit être supérieure à la date actuelle soit Ex.: ".date( "Y-m-d" ),
    "validation_min_datetime"       => "La valeur '%s' doit être supérieure à la date et l'heure. Ex.: ".date( "Y-m-d H:i:s" ),
    "validation_max_date"           => "La valeur '%s' doit être inférieure à la date actuelle. Ex.: ".date( "Y-m-d" ),
    "validation_max_datetime"       => "La valeur '%s' doit être inférieure à la date et l'heure. Ex.: ".date( "Y-m-d H:i:s" ),
    "validation_number"             => "Le champ '%s' doit contenir une valeur numérique",
    "validation_password_a"         => "Le mot de passe doit contenir une lettre minuscule",
    "validation_password_A"         => "Le mot de passe doit contenir une lettre majuscule",
    "validation_password_0"         => "Le mot de passe doit contenir un caractère numérique",
    "validation_password_white"     => "Le mot de passe doit contenir un signe de spécial (.,_-)",
    "validation_link"               => "Le champ '%s' ne peut contenir que des caractères autorisés dans un lien",
    "validation_path"               => "Le champ '%s' doit contenir un format de chemin d'accès valide",
    "validation_max_value"          => "Le champ '%s' doit contenir des valeurs inférieures à %d",
    "validation_min_value"          => "Le champ '%s' doit contenir des valeurs supérieures à %d",
    "validation_negative"           => "Le champ '%s' seules les valeur négatives permis (<0)",
    "validation_positive"           => "Le champ '%s' il n'y a que des valeurs positives autorisés (>0)",
    "validation_between"            => "Le champ '%s' seules les valeurs entre %d et %d permet",
    "validation_phone_length"       => "La longueur du numéro de téléphone est incorrect",
    "validation_name"               => "Le champ '%s' ne doit contenir que des caractères alphanumériques, numériques et -' contenir des caractères",
    "validation_title_name"         => "Le champ '%s' ne doit contenir que des caractères alphanumériques, numériques, l'espace et .,+_-\"!:@'?& contenir des caractères",
    "validation_filename"           => "Le champ '%s' ne doit contenir que des caractères alphanumériques, numériques et ._- contenir des caractères",
    "validation_module_name"        => "Le champ '%s' ne doit contenir que des caractères alphanumériques, numériques et _- contenir des caractères",
    "validation_color"              => "Le champ '%s' ne doit contenir que des caractères alphanumériques (A-F) et numériques (0-9)",
    "validation_domain_valid"       => "The domain name in '%s' field doesn't exist",
    "validation_lesser"             => "The field '%s' must contain a number lesser than %d",
    "validation_lesser_or_equal"    => "The field '%s' must contain a number lesser or equal than %d",
    "validation_bigger"             => "The field '%s' must contain a number bigger than %d",
    "validation_bigger_or_equal"    => "The field '%s' must contain a number bigger or equal than %d",
    "Error_with_rules"              => "Les règles ne sont pas correctement définis",
);

/* End of file Validation_lang.php */
/* Location: ./core/languages/english/ */