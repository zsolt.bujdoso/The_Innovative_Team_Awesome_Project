<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MController
 *
 * @version     1.0.0
 */
class MController extends BController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Init()
    {
        parent::Init();

        $this->Validate_values( $_POST );
        $this->Validate_values( $_GET );
    }

    /**
     * @method	Validate_values
     * @access	public
     * @desc    This method check and validate values, if there is a script, remove all values
     * @author	Cousin Béla
     *
     * @param   mixed                       $values                     - values to check
     *
     * @version	1.0.0
     */
    public function Validate_values( & $values )
    {
        if( empty( $values ) )
        {
            return;
        }

        // Validate array values
        if( is_array( $values ) )
        {
            foreach( $values as $key => $value )
            {
                if( is_array( $value ) )
                {
                    $this->Validate_values( $value );
                    continue;
                }

                if( ! $this->Validate_value( $value ) )
                {
                    unset( $values[$key] );
                }
            }

            return;
        }

        // Validate string value
        if( ! $this->Validate_value( $values ) )
        {
            $values = "";
        }
    }

    /**
     * @method	Validate_value
     * @access	public
     * @desc    This method check and validate value
     * @author	Cousin Béla
     *
     * @param   mixed                       $value                      - value to check
     *
     * @version	1.0.0
     * @return  boolean
     */
    public function Validate_value( $value )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        $validation = Validation_helper::Validate_variable( $value, "", "xss_clean" );

        if( $validation === TRUE )
        {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * @method	Redirect
     * @access	public
     * @desc    Redirect the active url to the new url
     * @author	Cousin Béla
     *
     * @param   string                      $url                        - full url to redirect or part without base url
     *
     * @version	1.0.0
     */
    public function Redirect( $url )
    {
        if( ! preg_match( "/^(http\:\/\/|https\:\/\/|www\.).*/", $url ) )
        {
            $url = Url_helper::Base_url().$url;
        }

        header( "Location: ".$url );
        App()->Close_database();
        die();
    }

    /**
     * @method  Load_view
     * @access  public
     * @desc    Load a view by name when rendering the page with layout
     * @author  Cousin Béla
     *
     * @param   string                  $view_name              - the name of template
     * @param   array                   $params                 - parameters array with tokens to replace
     * @param   int                     $cache_time             - if we want with cache, set the time for cache available
     *
     * @version 1.0.0
     * @return  string
     */
    protected function Load_view( $view_name = "", $params = array(), $cache_time = 0 )
    {
        return $this->renderer->Load_view( $view_name, $params, $cache_time );
    }

    /**
     * @method  Load_partial_view
     * @access  public
     * @desc    Load a view by name to rendering
     * @author  Cousin Béla
     *
     * @param   string                  $view_name              - the name of template
     * @param   array                   $params                 - parameters array with tokens to replace
     * @param   int                     $cache_time             - if we want with cache, set the time for cache available
     *
     * @version 1.0.0
     * @return  string
     */
    public function Load_partial_view( $view_name = "", $params = array(), $cache_time = 0 )
    {
        return $this->renderer->Load_partial_view( $view_name, $params, $cache_time );
    }

    /**
     * @method  Set_layout
     * @access  public
     * @desc    Set the layout name
     * @author  Cousin Béla
     *
     * @param   string                  $layout                 - name of the layout with path
     *
     * @version 1.0.0
     */
    protected function Set_layout( $layout )
    {
        $this->renderer->Set_layout( $layout );
    }

    /**
     * @method  Set_layout
     * @access  public
     * @desc    Set the status code in header
     * @author  Cousin Béla
     *
     * @param   int                     $code                   - code of status
     *
     * @version 1.0.0
     */
    public function Set_status_in_header( $code )
    {
        if( empty( $code ) )
        {
            return;
        }

        if( function_exists( "http_status_code" ) )
        {
            http_status_code( $code );
            return;
        }

        header( Array_helper::Get_status_by_code( $code ) );
    }
}

/* End of file MController.php */
/* Location: ./core/Libraries/ */