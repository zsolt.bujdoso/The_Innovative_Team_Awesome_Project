<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * class MDatabase_exception
 *
 * @propery     Logger      $logger
 *
 * @version     1.0.0
 */
class MDatabase_exception extends MException
{
    public function __construct( $message = "", $exception_type = "error" )
    {
        $this->error_code               = Settings::STATUS_INTERNAL_SERVER_ERROR;
        $this->exception_type_message   = "An error occured while executing SQL command";

        parent::__construct( $message, $exception_type );

        $this->Handle( $exception_type );
    }
}

/* End of file Exception.php */
/* Location: ./core/Libraries/ */