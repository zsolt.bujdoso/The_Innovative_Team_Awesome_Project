<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * class MModule_not_found_exception
 *
 * @propery     Logger      $logger
 *
 * @version     1.0.0
 */
class MModule_not_found_exception extends MException
{
    public function __construct( $message = "" )
    {
        $this->error_code               = Settings::STATUS_NOT_FOUND;
        $this->exception_type_message   = "An error occured while loading module";

        parent::__construct( $message, "error" );

        $this->Handle( "error" );
    }
}

/* End of file MModule_not_found_exception.php */
/* Location: ./core/Libraries/Exceptions/ */