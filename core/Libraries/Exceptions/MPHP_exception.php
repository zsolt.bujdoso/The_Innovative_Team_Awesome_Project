<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * class MPHP_exception
 *
 * @propery     Logger      $logger
 *
 * @version     1.0.0
 */
class MPHP_exception extends MException
{
    public function __construct( $errno = 0, $errstr = "", $errfile = "", $errline = 0, $error_context = array() )
    {
        $this->code = $errno;
        $this->file = $errfile;
        $this->line = $errline;

        $this->error_code               = Settings::STATUS_INTERNAL_SERVER_ERROR;
        $this->exception_type_message   = "An error occured on page";

        parent::__construct( $errstr, "error" );

        $this->Handle( "error" );
    }
}

/* End of file MPHP_exception.php */
/* Location: ./core/Libraries/Exceptions/ */