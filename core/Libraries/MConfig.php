<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MConfig
 *
 * @version     1.0.0
 */
class MConfig extends BConfig
{
    public function __construct()
    {
        parent::__construct();
    }
}

/* End of file MConfig.php */
/* Location: ./core/Libraries/ */