<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * Class MMail
 *
 * Mailer class to send emails with PHP by mail, sendmail, or smtp
 *
 * @version 1.0.0
 */
abstract class MMail extends BMail
{
    private $mail_type;
    private $date;
    private $mailer_name      = "M.Y.S.E mailer";
    private $mailer_version   = "1.0";

    private $priority         = 3;
    private $content_type     = "text/plain";
    protected $encoding       = "8bit";
    protected $charset        = "UTF-8";

    protected $message;
    protected $subject;

    protected $from_address;
    protected $from_name;

    protected $reply_address;
    protected $reply_name;

    private $read_notification= FALSE;

    private $headers          = array();
    protected $to_addresses   = array();
    protected $cc_addresses   = array();
    protected $bcc_addresses  = array();
    protected $attachments    = array();

    protected $new_line       = "\r\n";
    protected $email_headers  = "";
    protected $email_body     = "";

    public function __construct()
    {
        parent::__construct();

        $this->mail_type = "mail";
    }

    public function Set_mail_type( $mail_type )
    {
        $this->mail_type = $mail_type;
    }

    /**
     * @method	Set_message
     * @access	public
     * @desc	  This function set the message for email
     * @author	Cousin Bela
     *
     * @param	  string  	    $message          - subject for email
     *
     * @version	1.0
     */
    public function Set_message( $message )
    {
        $this->message = $message;
    }

    /**
     * @method	Set_from
     * @access	public
     * @desc	  This function set the subject for email
     * @author	Cousin Bela
     *
     * @param	  string  	    $subject          - subject for email
     *
     * @version	1.0
     */
    public function Set_subject( $subject )
    {
        $this->subject = $subject;
    }

    /**
     * @method	Set_from
     * @access	public
     * @desc	  This function set the email address from and name for it
     * @author	Cousin Bela
     *
     * @param	  string  	    $from             - email address from
     * @param	  string  	    $from_name        - name for it
     *
     * @version	1.0
     */
    public function Set_from( $from, $from_name = "" )
    {
        $this->from_address = $from;

        if( ! empty( $from_name ) )
        {
            $this->from_name = $from_name;
        }
    }

    /**
     * @method	Add_cc_address
     * @access	public
     * @desc	  This function add an email address to TO
     * @author	Cousin Bela
     *
     * @param	  string  	    $email_address    - email address
     *
     * @version	1.0
     */
    public function Add_to_address( $email_address )
    {
        $this->to_addresses []= $email_address;
    }

    /**
     * @method	Add_cc_address
     * @access	public
     * @desc	  This function add an email address to CC
     * @author	Cousin Bela
     *
     * @param	  string  	    $email_address    - email address
     *
     * @version	1.0
     */
    public function Add_cc_address( $email_address )
    {
        $this->cc_addresses []= $email_address;
    }

    /**
     * @method	Add_bcc_address
     * @access	public
     * @desc	  This function add an email address to BCC
     * @author	Cousin Bela
     *
     * @param	  string  	    $email_address    - email address
     *
     * @version	1.0
     */
    public function Add_bcc_address( $email_address )
    {
        $this->bcc_addresses []= $email_address;
    }

    /**
     * @method	Set_reply_address
     * @access	public
     * @desc	  This function set the reply address
     * @author	Cousin Bela
     *
     * @param	  string  	    $reply_address    - email address to reply
     * @param	  string  	    $name             - name for it
     *
     * @version	1.0
     */
    public function Set_reply_address( $reply_address, $name = "" )
    {
        $this->reply_address = $reply_address;

        if( ! empty( $name ) )
        {
            $this->reply_name = $name;
        }
    }

    /**
     * @method	Set_reply_address_name
     * @access	public
     * @desc	  This function set the name for reply address
     * @author	Cousin Bela
     *
     * @param	  string  	    $name             - name
     *
     * @version	1.0
     */
    public function Set_reply_address_name( $name )
    {
        $this->reply_name = $name;
    }

    /**
     * @method	Set_content_type
     * @access	public
     * @desc	  This function set the content type
     * @author	Cousin Bela
     *
     * @param	  string  	    $content_type     - new content type, ex: text/html
     *
     * @version	1.0
     */
    public function Set_content_type( $content_type )
    {
        $this->content_type = $content_type;
    }

    /**
     * @method	Set_content_type_to_html
     * @access	public
     * @desc	  This function set the content type to text/html
     * @author	Cousin Bela
     *
     * @version	1.0
     */
    public function Set_content_type_to_html()
    {
        $this->content_type = "text/html";
    }

    /**
     * @method	Set_content_type_to_text
     * @access	public
     * @desc	  This function set the content type to text/plain
     * @author	Cousin Bela
     *
     * @version	1.0
     */
    public function Set_content_type_to_text()
    {
        $this->content_type = "text/plain";
    }

    /**
     * @method	Set_charset
     * @access	public
     * @desc	  This function set the charset
     * @author	Cousin Bela
     *
     * @param	  string  	    $charset      - new charset, ex: UTF-8
     *
     * @version	1.0
     */
    public function Set_charset( $charset )
    {
        $this->charset = $charset;
    }

    /**
     * @method	Set_encoding
     * @access	public
     * @desc	  This function set the encoding
     * @author	Cousin Bela
     *
     * @param	  string  	    $encoding     - new encoding, ex: 8bit
     *
     * @version	1.0
     */
    public function Set_encoding( $encoding )
    {
        $this->encoding = $encoding;
    }

    /**
     * @method	Add_headers
     * @access	public
     * @desc	  This function add a header information to email
     * @author	Cousin Bela
     *
     * @param	  string  	    $header		    - new header information
     *
     * @version	1.0
     */
    public function Add_headers( $header )
    {
        $this->headers []= $header.$this->new_line;
    }

    /**
     * @method	Set_read_notification
     * @access	public
     * @desc	This function set we need read notification or not
     * @author	Cousin Bela
     *
     * @param	bool			    $path		      - want a read notification or not
     *
     * @version	1.0
     */
    public function Set_read_notification( $bool )
    {
        $this->read_notification = $bool;
    }

    /**
     * @method	Add_attachment
     * @access	public
     * @desc	This function add an attachment to email
     * @author	Cousin Bela
     *
     * @param	string			    $path		      - the path to the attachment to send
     * @param	string			    $file_name		  - the file name to send with
     *
     * @version	1.0
     */
    public function Add_attachment( $path, $file_name = "" )
    {
        if( ! empty( $file_name ) )
        {
            $this->attachments []= array(
                "path"  => $path,
                "name"  => $file_name
            );

            return;
        }

        $this->attachments []= $path;
    }

    /**
     * @method	Set_email_attachments
     * @access	protected
     * @desc	  This function set header of the email
     * @author	Cousin Bela
     *
     * @param	  string			  $boundary		  - the boundary string for separating the variables, attachments and others
     *
     * @version	1.0
     */
    protected function Set_email_headers( $boundary )
    {
        App()->logger->Debug( "Email - creating the mail - add headers" );

        $this->email_headers = "MIME-Version: 1.0".$this->new_line;
        $this->Set_content_type_in_email( $boundary );
        $this->Set_from_address_in_email();
        $this->Set_to_addresses_in_email();
        $this->Set_cc_addresses_in_email();
        $this->Set_bcc_addresses_in_email();
        $this->Set_reply_to_address_in_email();
        $this->Set_read_notification_in_email();
        $this->Set_mailer_and_date_in_email();

        App()->logger->Debug( "Email - creating the mail - headers added" );
    }

    private function Set_content_type_in_email( $boundary )
    {
        $content_type = 'Content-type: '.$this->content_type.'; charset='.$this->charset.$this->new_line;
        if( sizeof( $this->attachments ) )
        {
            $content_type = 'Content-type: multipart/mixed; boundary="PHP-mixed-'.$boundary.'"'.$this->new_line;
        }

        $this->email_headers .= $content_type;
    }

    private function Set_from_address_in_email()
    {
        $this->email_headers .= 'From: '.$this->from_name.' <'.$this->from_address.'>'.$this->new_line;
    }

    private function Set_to_addresses_in_email()
    {
        foreach( $this->to_addresses as $email_address )
        {
            $this->email_headers .= 'To: '.$email_address.$this->new_line;
        }
    }

    private function Set_cc_addresses_in_email()
    {
        if( empty( $this->cc_addresses ) )
        {
            return;
        }

        foreach( $this->cc_addresses as $email_address )
        {
            $this->email_headers .= 'Cc: '.$email_address.$this->new_line;
        }
    }

    private function Set_bcc_addresses_in_email()
    {
        if( empty( $this->bcc_addresses ) )
        {
            return;
        }

        foreach( $this->bcc_addresses as $email_address )
        {
            $this->email_headers .= 'Bcc: '.$email_address.$this->new_line;
        }
    }

    private function Set_reply_to_address_in_email()
    {
        if( ! empty( $this->reply_address ) )
        {
            $this->email_headers .= "Reply-To: ".$this->reply_name.' <'.$this->reply_address.'>'.$this->new_line;
        }
    }

    private function Set_read_notification_in_email()
    {
        if( $this->read_notification )
        {
            $this->email_headers .= 'Disposition-Notification-To: '
              .( $this->reply_address ? $this->reply_address : $this->from_address )
              .$this->new_line;
        }
    }

    private function Set_mailer_and_date_in_email()
    {
        $this->email_headers .= 'Date: '.date( "D, d M Y H:i:s -0000" ).$this->new_line;
        $this->email_headers .= 'X-Sender: '.$this->mailer_name.$this->new_line;
        $this->email_headers .= 'X-Mailer: '.$this->mailer_name.', v.'.$this->mailer_version.$this->new_line;
        $this->email_headers .= 'Subject: '.$this->subject.$this->new_line;
    }

    /**
     * @method	Set_email_body
     * @access	protected
     * @desc	This function set body message of the email
     * @author	Cousin Bela
     *
     * @version	1.0
     */
    protected function Set_email_body()
    {
        if( $this->content_type = "text/html" )
        {
            $this->Create_html_email_body();
            return;
        }

        $this->Create_text_email_body();
    }

    private function Create_html_email_body()
    {
        $this->email_body =
          '<html>
              <head>
                  <meta http-equiv="Content-Type" content="'.$this->content_type.'; charset='.$this->charset.'" />
                  <title>'.$this->subject.'</title>';

        $this->email_body .=
            '</head>
                <body>'
                    .$this->message.
                '</body>
            </html>';

        $this->email_body .= $this->new_line;
    }

    private function Create_text_email_body()
    {
        $this->email_body = $this->message;
    }

    /**
     * @method	Set_email_attachments
     * @access	protected
     * @desc	  This function set attachments to the email
     * @author	Cousin Bela
     *
     * @param	  string			  $boundary		  - the boundary string for separating the variables, attachments and others
     *
     * @version	1.0
     * @throws  MMail_exception
     */
    protected function Set_email_attachments( $boundary )
    {
        if( ! sizeof( $this->attachments ) )
        {
            return;
        }

        App()->logger->Debug( "Email - creating the mail - add attachments" );

        // Add email body as PHP-alt tag
        $this->email_body =
                '--PHP-mixed-'.$boundary.$this->new_line
                .'Content-Type: '.$this->content_type.'; charset="'.$this->charset.'"'.$this->new_line
                .'Content-Transfer-Encoding: '.$this->encoding.$this->new_line.$this->new_line
                .$this->email_body.$this->new_line.$this->new_line;

        // Add every attachment as PHP-mixed tag
        foreach( $this->attachments as $attachment )
        {
            $attachment_file= ( is_array( $attachment ) && isset( $attachment["path"] ) ? $attachment["path"] : $attachment );
            $attachment_name= basename( is_array( $attachment ) && isset( $attachment["name"] ) ? $attachment["name"] : $attachment );

            if( ! realpath( $attachment_file ) )
            {
                throw new MMail_exception( "Attachment not found at: ".$attachment );
            }

            $file = fopen( $attachment_file, "rb" );
            $data = fread( $file, filesize( $attachment_file ) );
            fclose( $file );
            $data = chunk_split( base64_encode( $data ) );

            $this->email_body .=
                '--PHP-mixed-'.$boundary.$this->new_line
                .'Content-Type: '.$this->Get_attachment_content_type( $attachment_file ).'; name='.$attachment_name.$this->new_line
                .'Content-Transfer-Encoding: base64'.$this->new_line
                .'Content-Disposition: attachment'.$this->new_line.$this->new_line
                .$data.$this->new_line.$this->new_line;
        }

        // Close the PHP-mixed tag
        $this->email_body .= '--PHP-mixed-'.$boundary.'--'.$this->new_line;

        App()->logger->Debug( "Email - creating the mail - attachments added" );
    }

    private function Get_attachment_content_type( $attachment )
    {
        $attachment_name = basename( $attachment );

        $ext = substr( $attachment_name, strrpos( $attachment_name, '.' ) );

        switch( $ext ) {
            // office and pdf and text
            case 'doc': 	return 'application/msword';
            case 'docx': 	return 'application/msword';
            case 'rtf': 	return 'application/rtf';
            case 'xls': 	return 'application/ms-excel';
            case 'pdf': 	return 'application/pdf';
            case 'txt': 	return 'application/txt';
            case 'ppt': 	return 'application/vnd.ms-powerpoint';
            case 'pps': 	return 'application/vnd.ms-powerpoint';
            // scripts
            case 'src': 	return 'application/x-wais-source';
            case 'js': 		return 'text/javascript';
            case 'sh': 		return 'application/x-sh';
            case 'csh': 	return 'application/x-csh';
            case 'pl': 		return 'application/x-perl';
            case 'tcl': 	return 'application/x-tcl';
            case 'css': 	return 'application/x-pointplus';
            case 'asp': 	return 'application/x-asap';
            case 'asn': 	return 'application/astound';
            case 'axs': 	return 'application/x-olescript';
            case 'ods': 	return 'application/x-oleobject';
            case 'opp': 	return 'x-form/x-openscape';
            case 'wba': 	return 'application/x-webbasic';
            case 'cpp': 	return 'text/plain';
            case 'c': 		return 'text/plain';
            case 'pl': 		return 'text/plain';
            case 'h': 		return 'text/plain';
            case 'html': 	return 'text/html';
            // pictures
            case 'gif': 	return 'image/gif';
            case 'xbm': 	return 'image/x-xbitmap';
            case 'xpm': 	return 'image/x-xpixmap';
            case 'png': 	return 'image/x-png';
            case 'jpg': 	return 'image/jpeg';
            case 'jpeg': 	return 'image/jpeg';
            case 'jpe': 	return 'image/jpeg';
            case 'tiff': 	return 'image/tiff';
            case 'rgb': 	return 'image/rgb';
            case 'gmp': 	return 'image/x-ms-bmp';
            // videos
            case 'mpg': 	return 'video/mpeg';
            case 'mpeg': 	return 'video/mpeg';
            case 'mpe': 	return 'video/mpeg';
            case 'mpv2': 	return 'video/mpeg-2';
            case 'mp2v': 	return 'video/mpeg-2';
            case 'qt': 		return 'video/quicktime';
            case 'mov': 	return 'video/quicktime';
            case 'avi': 	return 'video/x-msvideo';
            case 'movie': 	return 'video/x-sgi-movie';
            case 'vdo': 	return 'video/vdo';
            case 'ai': 		return 'application/postscript';
            case 'eps': 	return 'application/postscript';
            case 'ps': 		return 'application/postscript';
            // compressed
            case 'gtar': 	return 'application/x-gtar';
            case 'tar': 	return 'application/x-tar';
            case 'ustar': 	return 'application/x-ustar';
            case 'zip': 	return 'application/zip';
            case 'rar': 	return 'application/x-rar-compressed';
            // executable
            case 'exe': 	return 'application/octet-stream';

            default: 		return 'application/octet-stream';
        }
    }

    /**
     * @method	Create_boundary
     * @access	protected
     * @desc	  This function creates a boundary for email
     * @author	Cousin Bela
     *
     * @version	1.0
     */
    protected function Create_boundary()
    {
        $str = md5( time() );

        return substr( $str, 0, 15 );
    }

    public function Get_from()
    {
        return $this->from_address;
    }

    public function Get_to_addresses()
    {
        return $this->to_addresses;
    }

    public function Get_bcc_addresses()
    {
        return $this->bcc_addresses;
    }

    public function Get_cc_addresses()
    {
        return $this->cc_addresses;
    }

    public function Get_subject()
    {
        return $this->subject;
    }

    public function Get_message()
    {
        return $this->message;
    }

    public function Get_email_body()
    {
        return $this->email_body;
    }

    public function Get_attachments()
    {
        return $this->attachments;
    }

    /**
     * @method	Send
     * @access	abstract
     * @desc	  We need to implement this function to send the email
     * @author	Cousin Bela
     */
    abstract function Send();
}