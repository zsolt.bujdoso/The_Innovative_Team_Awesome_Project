<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Mail_Mail
 *
 * @version     1.0.0
 */
class Mail_Mail extends MMail
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @method	Send
     * @access	public
     * @desc	  This function tries to send the message
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    public function Send()
    {
        App()->logger->Debug( "Sending email with PHP mail" );

        // Creating boundary
        $boundary = $this->Create_boundary();

        $this->Set_email_headers( $boundary );
        $this->Set_email_body( $boundary );
        $this->Set_email_attachments( $boundary );

        $this->Send_to_addresses();
        $this->Send_to_cc_addresses();
        $this->Send_to_bcc_addresses();

        return TRUE;
    }

    /**
     * @method	Send_to_addresses
     * @access	public
     * @desc	  This function tries to send the email to addresses
     * @author	Cousin Bela
     *
     * @version	1.0
     */
    private function Send_to_addresses()
    {
        foreach( $this->to_addresses as $email_address )
        {
            $this->Send_email_to( $email_address );
        }
    }

    /**
     * @method	Send_email_to
     * @access	public
     * @desc	  This function tries to send the email message to address
     * @author	Cousin Bela
     *
     * @param   string                  $email_address              - email address where to send the email
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_email_to( $email_address )
    {
        App()->logger->Debug( "Sending email to: ".$email_address." with PHP mail" );

        if( @mail( $email_address, $this->subject, $this->email_body, $this->email_headers ) )
        {
            App()->logger->Debug( "Email sent with PHP mail to: ".$email_address );
            return TRUE;
        }

        throw new MMail_exception( "Error while sending email with PHP mail" );
        return FALSE;
    }

    /**
     * @method	Send_to_cc_addresses
     * @access	public
     * @desc	  This function tries to send the email to cc addresses
     * @author	Cousin Bela
     *
     * @version	1.0
     */
    private function Send_to_cc_addresses()
    {
        foreach( $this->cc_addresses as $email_address )
        {
            $this->Send_email_to( $email_address );
        }
    }

    /**
     * @method	Send_to_bcc_addresses
     * @access	public
     * @desc	  This function tries to send the email to bcc addresses
     * @author	Cousin Bela
     *
     * @version	1.0
     */
    private function Send_to_bcc_addresses()
    {
        foreach( $this->bcc_addresses as $email_address )
        {
            $this->Send_email_to( $email_address );
        }
    }
}

/* End of file Mail_Mail.php */
/* Location: ./core/Libraries/Mail/ */