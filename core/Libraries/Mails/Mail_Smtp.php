<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Mail_Smtp
 *
 * @version     1.0.0
 */
class Mail_Smtp extends MMail
{
    private $smtp_host      = "";
    private $smtp_port      = "465";
    private $smtp_user      = "";
    private $smtp_pass      = "";
    private $smtp_need_auth      = TRUE;
    private $smtp_timeout   = "1";

    private $smtp_connect;

    public function __construct( $host = "", $user = "", $pass = "", $port = "465" )
    {
        parent::__construct();

        if( empty( $host ) && empty( $user ) && empty( $pass ) )
        {
            $this->Load_default_config();
        }
        else
        {
            $this->smtp_host = $host;
            $this->smtp_user = $user;
            $this->smtp_pass = $pass;
            $this->smtp_port = $port;
        }

        // log class initialized
        App()->logger->Debug( "Mail_smtp class initialized" );
    }

    public function Load_default_config()
    {
        $this->smtp_host    = App()->config->email["smtp_host"];
        $this->smtp_port    = App()->config->email["smtp_port"];
        $this->smtp_user    = App()->config->email["smtp_user"];
        $this->smtp_pass    = App()->config->email["smtp_pass"];
        $this->smtp_timeout = 3;//App()->config->email["smtp_timeout"];
    }

    /**
     * @method	Set_smtp_host
     * @access	public
     * @desc	  This function sets the smtp host to try to connect
     * @author	Cousin Bela
     *
     * @param   string                  $host                       - the smtp host
     *
     * @version	1.0
     */
    public function Set_smtp_host( $host )
    {
        $this->smtp_host = $host;
    }

    /**
     * @method	Set_smtp_port
     * @access	public
     * @desc	  This function sets the smtp port to try to connect
     * @author	Cousin Bela
     *
     * @param   string                  $port                       - the smtp port
     *
     * @version	1.0
     */
    public function Set_smtp_port( $port )
    {
        $this->smtp_port = $port;
    }

    /**
     * @method	Set_smtp_user
     * @access	public
     * @desc	  This function sets the smtp user to try to connect
     * @author	Cousin Bela
     *
     * @param   string                  $user                       - the smtp user
     *
     * @version	1.0
     */
    public function Set_smtp_user( $user )
    {
        $this->smtp_user = $user;
    }

    /**
     * @method	Get_smtp_host
     * @access	public
     * @desc	This function return the smtp host name
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  string
     */
    public function Get_smtp_host()
    {
        return $this->smtp_host;
    }

    /**
     * @method	Get_smtp_port
     * @access	public
     * @desc	This function returns the port for smtp host
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  int
     */
    public function Get_smtp_port()
    {
        return $this->smtp_port;
    }

    /**
     * @method	Get_smtp_user
     * @access	public
     * @desc	This function returns the username for smtp host
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  string
     */
    public function Get_smtp_user()
    {
        return $this->smtp_user;
    }

    /**
     * @method	Set_smtp_pass
     * @access	public
     * @desc	  This function sets the smtp password to try to connect
     * @author	Cousin Bela
     *
     * @param   string                  $password                   - the smtp password
     *
     * @version	1.0
     */
    public function Set_smtp_pass( $password )
    {
        $this->smtp_pass = $password;
    }

    /**
     * @method	Set_smtp_timeout
     * @access	public
     * @desc	This function sets the timeout for smtp connection
     * @author	Cousin Bela
     *
     * @param   int                     $seconds                    - how many seconds to wait before will be timeout
     *
     * @version	1.0
     */
    public function Set_smtp_timeout( $seconds )
    {
        $this->smtp_timeout = $seconds;
    }

    /**
     * @method	Send
     * @access	public
     * @desc	  This function tries to send the message
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    public function Send()
    {
        if( empty( $this->smtp_host ) )
        {
            throw new MMail_exception( "Please set the HOST for sending email by SMTP" );
        }

        App()->logger->Debug( "Sending email with smtp on host: ".$this->smtp_host );

        // Creating boundary
        $boundary = $this->Create_boundary();

        $this->Set_email_headers( $boundary );
        $this->Set_email_body();
        $this->Set_email_attachments( $boundary );

        // Connect and authenticate
        $this->Smtp_connect();
        $this->smtp_need_authenticate();
        // From and To addresses
        $this->Send_command( 'from', $this->from_address );
        $this->Send_to_addresses();
        $this->Send_cc_addresses();
        $this->Send_bcc_addresses();
        //Send data
        $this->Send_email_message();

        $this->Send_data( 'quit' );
        App()->logger->Debug( "Email was sent successfully" );

        return TRUE;
    }

    /**
     * @method	Smtp_connect
     * @access	private
     * @desc	  This function tries to connect to the smtp server
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Smtp_connect()
    {
        // connect to smtp server
        $this->smtp_connect = fsockopen( $this->smtp_host, $this->smtp_port, $errno, $errstr, ( float ) $this->smtp_timeout );

        if( ! is_resource( $this->smtp_connect ) )
        {
            throw new MMail_exception( 'Error while connecting to smtp server: '.$errno." ".$errstr );
            return FALSE;
        }

        $reply = $this->Get_smtp_data();

        // log message
        App()->logger->Debug( 'Email - sending the hello command', 'debug' );

        // sending the hello command
        return $this->Send_command( 'hello' );
    }

    /**
     * @method	Get_smtp_data
     * @access	private
     * @desc	  This function tries to get the reply from smtp server
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Get_smtp_data()
    {
        $data = '';

        if( $this->smtp_connect )
        {
            while ( $str = fgets( $this->smtp_connect, 512 ) )
            {
                $data .= $str;

                if ( substr( $str, 3, 1 ) == " " )
                    break;
            }
        }

        return $data;
    }

    /**
     * @method	Send_command
     * @access	private
     * @desc	  This function tries to Send the command with data to smtp server
     * @author	Cousin Bela
     *
     * @param   string                  $command                    - the command to send
     * @param   mixed                   $data                       - data to send to smtp server
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_command( $command, $data = "" )
    {
        switch( $command )
        {
            case 'hello' :
                if( $this->smtp_need_auth OR $this->encoding == '8bit' )
                    $this->Send_data( 'EHLO '.$this->Get_hostname() );
                else
                    $this->Send_data( 'HELO '.$this->Get_hostname() );

                $response = 250;
                break;

            case 'start':
                $this->Send_data( 'STARTTLS' );

                $response = 250;
                break;

            case 'subject':
                $this->Send_data( 'SUBJECT:<'.$data.'>' );

                $response = 250;
                break;

            case 'from' :
                $this->Send_data( 'MAIL FROM:<'.$data.'>' );

                $response = 250;
                break;

            case 'to'	:
                $this->Send_data( 'RCPT TO:<'.$data.'>' );

                $response = 250;
                break;

            case 'data'	:
                $this->Send_data( 'DATA' );

                $response = 354;
                break;

            case 'quit'	:
                $this->Send_data( 'QUIT' );

                $response = 221;
                break;
        }

        // get response from smtp
        $reply = $this->Get_smtp_data();

        if ( substr( $reply, 0, 3 ) != $response )
        {
            throw new MMail_exception( 'Error while sending the command: '.$command.' '.$reply );
            return FALSE;
        }

        if ( $command == 'quit' )
        {
            fclose( $this->smtp_connect );
        }

        return TRUE;
    }

    /**
     * @method	smtp_need_authenticate
     * @access	private
     * @desc	This function tries to authenticate to the smtp server
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function smtp_need_authenticate()
    {
        if( ! $this->smtp_need_auth )
        {
            return TRUE;
        }

        // if user and password was not set, the authentificate not required
        if ( $this->smtp_user == '' && $this->smtp_pass == '' )
        {
            $this->smtp_need_auth = FALSE;
            return TRUE;
        }

        // if user or password not set, the authentificate can not be executed
        if( ( empty( $this->smtp_user ) || empty( $this->smtp_pass ) ) )
        {
            throw new MMail_exception( 'The username or password was not set for smtp server' );
            return FALSE;
        }

        $this->Send_login();
        $this->Send_username();
        $this->Send_password();

        return TRUE;
    }

    /**
     * @method	Send_login
     * @access	private
     * @desc	  This function tries to Send the the login details to smtp server
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_login()
    {
        $this->Send_data( 'AUTH LOGIN' );

        $reply = $this->Get_smtp_data();

        if ( strncmp( $reply, '334', 3 ) != 0 )
        {
            throw new MMail_exception( 'Error while logging in to smtp server: '.$reply );
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Send_data
     * @access	private
     * @desc	  This function tries to Send the data to smtp server
     * @author	Cousin Bela
     *
     * @param   mixed                   $data                       - data to send to smtp server
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_data( $data )
    {
        if ( ! fwrite( $this->smtp_connect, $data . $this->new_line ) )
        {
            throw new MMail_exception( 'Error while sending data to smpt server: '.$data );
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Send_username
     * @access	private
     * @desc	  This function tries to Send the the username to smtp server
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_username()
    {
        $this->Send_data( base64_encode( $this->smtp_user ) );

        $reply = $this->Get_smtp_data();

        if ( strncmp( $reply, '334', 3 ) != 0 )
        {
            throw new MMail_exception( 'Error with smtp username: '.$reply );
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Send_password
     * @access	private
     * @desc	  This function tries to Send the the password to smtp server
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_password()
    {
        $this->Send_data( base64_encode( $this->smtp_pass ) );

        $reply = $this->Get_smtp_data();

        if ( strncmp($reply, '235', 3 ) != 0 )
        {
            throw new MMail_exception( 'Error with smtp password: '.$reply );
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Send_to_addresses
     * @access	private
     * @desc	  This function tries to Send the addresses where to send the email
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_to_addresses()
    {
        foreach( $this->to_addresses as $email_address )
        {
            $this->Send_command( "to", $email_address );
        }
    }

    /**
     * @method	Send_cc_addresses
     * @access	private
     * @desc	  This function tries to Send the addresses where to send a copy of the email
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_cc_addresses()
    {
        if( empty( $this->cc_addresses ) )
        {
            return;
        }

        foreach( $this->cc_addresses as $email_address )
        {
            $this->Send_command( "to", $email_address );
        }
    }

    /**
     * @method	Send_bcc_addresses
     * @access	private
     * @desc	  This function tries to Send the addresses where to send a secure copy of the email
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_bcc_addresses()
    {
        if( empty( $this->bcc_addresses ) )
        {
            return;
        }

        foreach( $this->bcc_addresses as $email_address )
        {
            $this->Send_command( "to", $email_address );
        }
    }

    /**
     * @method	Send_email_message
     * @access	private
     * @desc	  This function tries to Send the email message with headers and attachments
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_email_message()
    {
        $this->Send_command( 'data' );
        $this->Send_data( $this->email_headers . preg_replace( '/^\./m', '..$1', $this->email_body ) );
        $this->Send_data( '.' );

        // Get reply to know if it was success
        $reply = $this->Get_smtp_data();

        if ( strncmp( $reply, '250', 3 ) != 0 )
        {
            throw new MMail_exception( 'An smtp error occured: '. $reply );
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Get_hostname
     * @access	private
     * @desc	  This function returns the hostname
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  string
     */
    private function Get_hostname()
    {
        if( ! empty( $_SERVER['SERVER_NAME'] ) )
        {
            return $_SERVER['SERVER_NAME'];
        }

        return "localhost.localdomain";
    }
}

/* End of file Mail_Smtp.php */
/* Location: ./core/Libraries/Mail/ */