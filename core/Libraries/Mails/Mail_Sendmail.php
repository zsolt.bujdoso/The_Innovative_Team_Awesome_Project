<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Mail_Sendmail
 *
 * @version     1.0.0
 */
class Mail_Sendmail extends MMail
{
    private $sendmail = "";

    public function __construct( $path = "" )
    {
        $this->sendmail = ( ! empty( $path ) ? $path : App()->config->email["sendmail"] );

        if( empty( $this->sendmail ) )
        {
            $this->sendmail = "/usr/sbin/sendmail";
        }
    }

    /**
     * @method	Set_sendmail_path
     * @access	public
     * @desc	  This function sets the path to the sendmail
     * @author	Cousin Bela
     *
     * @param   string                  $path                       - path to the sendmail
     *
     * @version	1.0
     */
    public function Set_sendmail_path( $path )
    {
        $this->sendmail = $path;
    }

    /**
     * @method	Send
     * @access	public
     * @desc	  This function tries to send the message
     * @author	Cousin Bela
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    public function Send()
    {
        App()->logger->Debug( "Sending email with sendmail" );

        // Creating boundary
        $boundary = $this->Create_boundary();

        $this->Set_email_headers( $boundary );
        $this->Set_email_body( $boundary );
        $this->Set_email_attachments( $boundary );

        $this->Send_to_addresses();
        $this->Send_to_cc_addresses();
        $this->Send_to_bcc_addresses();

        return TRUE;
    }

    /**
     * @method	Send_to_addresses
     * @access	public
     * @desc	  This function tries to send the email to addresses
     * @author	Cousin Bela
     *
     * @version	1.0
     */
    private function Send_to_addresses()
    {
        foreach( $this->to_addresses as $email_address )
        {
            $this->Send_email_to( $email_address );
        }
    }

    /**
     * @method	Send_email_to
     * @access	public
     * @desc	  This function tries to send the email message to address
     * @author	Cousin Bela
     *
     * @param   string                  $email_address              - email address where to send the email
     *
     * @version	1.0
     * @return  bool
     * @throws  MMail_exception
     */
    private function Send_email_to( $email_address )
    {
        App()->logger->Debug( "Sending email to: ".$email_address." with PHP mail" );

        // open a socket
        $fp = @popen( $this->sendmail . " -oi -f ".$this->from_address." -t", 'w');

        if( $fp === FALSE || $fp === NULL )
        {
            throw new MMail_exception( "Error while sending mail with sendmail, maybe it is not enabled on server config" );
            return FALSE;
        }

        fputs( $fp, $this->email_headers );
        fputs( $fp, $this->email_body );

        $status = pclose( $fp );

        if( version_compare( PHP_VERSION, '4.2.3' ) == -1 )
        {
            $status = $status >> 8 & 0xFF;
        }

        if( $status != 0 )
        {
            throw new MMail_exception(
              'An error occured with no. '.$status.' while sending the email. The socket can not be opened to sendmail'
            );
            return FALSE;
        }

        App()->logger->Debug( "Email sent with sendmail to: ".$email_address );

        return TRUE;
    }

    /**
     * @method	Send_to_cc_addresses
     * @access	public
     * @desc	  This function tries to send the email to cc addresses
     * @author	Cousin Bela
     *
     * @version	1.0
     */
    private function Send_to_cc_addresses()
    {
        foreach( $this->cc_addresses as $email_address )
        {
            $this->Send_email_to( $email_address );
        }
    }

    /**
     * @method	Send_to_bcc_addresses
     * @access	public
     * @desc	  This function tries to send the email to bcc addresses
     * @author	Cousin Bela
     *
     * @version	1.0
     */
    private function Send_to_bcc_addresses()
    {
        foreach( $this->bcc_addresses as $email_address )
        {
            $this->Send_email_to( $email_address );
        }
    }
}

/* End of file Mail_Sendmail.php */
/* Location: ./core/Libraries/Mail/ */