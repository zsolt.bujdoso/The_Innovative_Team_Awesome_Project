<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MLang
 *
 * @version     1.0.0
 */
class MLang extends BLang
{
    public function __construct()
    {
        $this->languages      = App()->config->language["languages"];
        $this->language       = App()->config->language["default"];

        $this->Set_language();
        $this->Set_into_session();

        $this->Load_language_variables();
        $this->Load_language_variables_from_module();
    }

    /**
     * @method	Get
     * @access	public
     * @desc    this method checks if the translation exist for name and returns it
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name			    - the name for getting translation
     *
     * @version	1.0
     * @return 	string
     * @throws  MLang_exception
     */
    public function Get( $name )
    {
        if( ! isset( $this->variables[$name] ) )
        {
            throw new MLang_exception( "The language array does not contain translation for '".$name."'" );
        }

        return $this->variables[$name];
    }

    /**
     * @method	Get_all_variables
     * @access	public
     * @desc    Returns all loaded language variables
     * @author	Zoltan Jozsa
     *
     * @version	1.0
     * @return 	string
     * @throws  MLang_exception
     */
    public function Get_all_variables()
    {
        return $this->variables;
    }
}

/* End of file MLang.php */
/* Location: ./core/Libraries/ */