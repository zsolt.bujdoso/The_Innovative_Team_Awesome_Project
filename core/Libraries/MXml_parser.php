<?php if( ! defined( "BASEPATH" ) )
    die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MXml_parser
 *
 * @version     1.0.0
 */
class MXml_parser extends BXml_parser
{
    /**
     * xml document
     * @var string
     */
    var $xml_document = "";

    /**
     * xml document
     * @var string
     */
    var $xml_parsed = null;


    /**
     * exchange date
     * BNR date format is Y-m-d
     * @var string
     */
    var $date       = "";


    /**
     * cursBnrXML class constructor
     * @access  public
     *
     * @param   string                      $file_path                  - file path or URL to selected XML document
     *
     * @version 1.0
     */
    public function __construct( $file_path )
    {
        $this->xml_document = file_get_contents( $file_path );
        $this->Parse_xml_document();
    }

    /**
     * @method  parse_xml_document
     * @desc    Try to parse the XML document
     * @access  private
     *
     * @version 1.0
     */
    private function Parse_xml_document()
    {
        $this->xml_parsed = new SimpleXMLElement( $this->xml_document );

        try
        {
            $this->date = $this->xml_parsed->Header->PublishingDate;
        }
        catch( MException $e )
        {
            $this->date = null;
        }
    }

    /**
     * @method  Get_value
     * @desc    Get value from xml
     * @access  public
     *
     * @param   string                      $index                      - the index of value to get
     *
     * @version 1.0
     * @return  mixed
     */
    public function Get_value( $index )
    {
        $value = FALSE;

        if( empty( $this->xml_parsed ) || empty( $this->xml_parsed->$index ) )
        {
            return $value;
        }

        try
        {
            $value = $this->xml_parsed->$index;
        }
        catch( MException $e ){}

        return $value;
    }

    /**
     * @method  Get_values
     * @desc    Get all values
     * @access  public
     *
     * @version 1.0
     * @return  mixed
     */
    function Get_values()
    {
        return $this->xml_parsed;
    }
}

/* End of file MXml_parser.php */
/* Location: ./core/Libraries/ */