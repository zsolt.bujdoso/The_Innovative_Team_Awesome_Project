<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Minifier_JS
 *
 * This JS minifier are going to remove all of whitespaces, unnecessary comments and
 * tokens.
 *
 */
class Minifier_JS
{
    const ORD_LF            = 10;
    const ORD_SPACE         = 32;
    const ACTION_KEEP_A     = 1;
    const ACTION_DELETE_A   = 2;
    const ACTION_DELETE_A_B = 3;

    protected $a                = "\n";
    protected $b                = '';
    protected $input            = '';
    protected $input_index      = 0;
    protected $input_length     = 0;
    protected $look_ahead       = null;
    protected $content          = '';
    protected $last_byte_out    = '';
    protected $kept_comment     = '';

    /**
     * @param string $input
     */
    public function __construct( $script )
    {
        $this->input = $script;
    }

    /**
     * @method  Minify
     * @access  public
     * @desc    Minify the string given in the constructor
     * @author  Cousin Bela
     *
     * @return string
     */
    public function Minify()
    {
        $mb_internal_enccoder = null;
        if( function_exists( 'mb_strlen' ) && ( ( int ) ini_get( 'mbstring.func_overload' ) & 2 ) )
        {
            $mb_internal_enccoder = mb_internal_encoding();
            mb_internal_encoding( '8bit' );
        }
        $this->input        = str_replace( "\r\n", "\n", $this->input );
        $this->input_length = strlen( $this->input );

        $this->action( self::ACTION_DELETE_A_B );

        while( $this->a !== null )
        {
            // determine next command
            $command = self::ACTION_KEEP_A; // default

            if( $this->a === ' ' )
            {
                if( ( $this->last_byte_out === '+' || $this->last_byte_out === '-' ) && ( $this->b === $this->last_byte_out ) )
                {
                    // Don't delete this space. If we do, the addition/subtraction
                    // could be parsed as a post-increment
                }
                elseif( ! $this->isAlphaNum( $this->b ) )
                {
                    $command = self::ACTION_DELETE_A;
                }
            }
            elseif( $this->a === "\n" )
            {
                if( $this->b === ' ' )
                {
                    $command = self::ACTION_DELETE_A_B;

                    // in case of mbstring.func_overload & 2, must check for null b,
                    // otherwise mb_strpos will give WARNING
                }
                elseif( $this->b === null || ( strpos( '{[(+-!~', $this->b ) === FALSE && ! $this->isAlphaNum( $this->b ) ) )
                {
                    $command = self::ACTION_DELETE_A;
                }
            }
            elseif( ! $this->isAlphaNum( $this->a ) )
            {
                if( $this->b === ' ' || ( $this->b === "\n" && ( strpos( '}])+-"\'', $this->a ) === FALSE ) ) )
                {
                    $command = self::ACTION_DELETE_A_B;
                }
            }

            $this->action( $command );
        }

        $this->content = trim( $this->content );

        if( $mb_internal_enccoder !== null )
        {
            mb_internal_encoding( $mb_internal_enccoder );
        }

        return $this->content;
    }

    /**
     * ACTION_KEEP_A = content A. Copy B to A. Get the next B.
     * ACTION_DELETE_A = Copy B to A. Get the next B.
     * ACTION_DELETE_A_B = Get the next B.
     *
     * @param int $command
     * @throws JSMin_UnterminatedRegExpException|JSMin_UnterminatedStringException
     */
    protected function action($command)
    {
        // make sure we don't compress "a + ++b" to "a+++b", etc.
        if ($command === self::ACTION_DELETE_A_B
            && $this->b === ' '
            && ($this->a === '+' || $this->a === '-'))
        {
            // Note: we're at an addition/substraction operator; the input_index
            // will certainly be a valid index
            if ($this->input[$this->input_index] === $this->a) {
                // This is "+ +" or "- -". Don't delete the space.
                $command = self::ACTION_KEEP_A;
            }
        }

        switch ($command) {
            case self::ACTION_KEEP_A: // 1
                $this->content .= $this->a;

                if( $this->kept_comment ) {
                    $this->content = rtrim($this->content, "\n");
                    $this->content .= $this->kept_comment;
                    $this->kept_comment = '';
                }

                $this->last_byte_out = $this->a;

            // fallthrough intentional
            case self::ACTION_DELETE_A: // 2
                $this->a = $this->b;
                if ($this->a === "'" || $this->a === '"') { // string literal
                    $str = $this->a; // in case needed for exception
                    for(;;) {
                        $this->content .= $this->a;
                        $this->last_byte_out = $this->a;

                        $this->a = $this->get();
                        if ($this->a === $this->b) { // end quote
                            break;
                        }
                        if ($this->isEOF($this->a)) {
                            $this->a = null;
                        }
                        $str .= $this->a;
                        if ($this->a === '\\') {
                            $this->content .= $this->a;
                            $this->last_byte_out = $this->a;

                            $this->a = $this->get();
                            $str    .= $this->a;
                        }
                    }
                }

            // fallthrough intentional
            case self::ACTION_DELETE_A_B: // 3
                $this->b = $this->next();
                if ($this->b === '/' && $this->isRegexpLiteral()) {
                    $this->content .= $this->a . $this->b;
                    $pattern = '/'; // keep entire pattern in case we need to report it in the exception
                    for(;;) {
                        $this->a = $this->get();
                        $pattern .= $this->a;
                        if ($this->a === '[') {
                            for(;;) {
                                $this->content .= $this->a;
                                $this->a = $this->get();
                                $pattern .= $this->a;
                                if ($this->a === ']') {
                                    break;
                                }
                                if ($this->a === '\\') {
                                    $this->content .= $this->a;
                                    $this->a = $this->get();
                                    $pattern .= $this->a;
                                }
                                if ($this->isEOF($this->a)) {
                                    $this->a = null;
                                }
                            }
                        }

                        if ($this->a === '/') { // end pattern
                            break; // while (true)
                        } elseif ($this->a === '\\') {
                            $this->content .= $this->a;
                            $this->a = $this->get();
                            $pattern .= $this->a;
                        } elseif ($this->isEOF($this->a)) {
                            $this->a = null;
                        }
                        $this->content .= $this->a;
                        $this->last_byte_out = $this->a;
                    }
                    $this->b = $this->next();
                }
            // end case ACTION_DELETE_A_B
        }
    }

    /**
     * @return bool
     */
    protected function isRegexpLiteral()
    {
        if (false !== strpos("(,=:[!&|?+-~*{;", $this->a)) {
            // we obviously aren't dividing
            return true;
        }
        if ($this->a === ' ' || $this->a === "\n") {
            $length = strlen($this->content);
            if ($length < 2) { // weird edge case
                return true;
            }
            // you can't divide a keyword
            if (preg_match('/(?:case|else|in|return|typeof)$/', $this->content, $m)) {
                if ($this->content === $m[0]) { // odd but could happen
                    return true;
                }
                // make sure it's a keyword, not end of an identifier
                $charBeforeKeyword = substr($this->content, $length - strlen($m[0]) - 1, 1);
                if (! $this->isAlphaNum($charBeforeKeyword)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return the next character from stdin. Watch out for look_ahead. If the character is a control character,
     * translate it to a space or linefeed.
     *
     * @return string
     */
    protected function get()
    {
        $c = $this->look_ahead;
        $this->look_ahead = null;
        if ($c === null) {
            // getc(stdin)
            if ($this->input_index < $this->input_length) {
                $c = $this->input[$this->input_index];
                $this->input_index += 1;
            } else {
                $c = null;
            }
        }
        if (ord($c) >= self::ORD_SPACE || $c === "\n" || $c === null) {
            return $c;
        }
        if ($c === "\r") {
            return "\n";
        }
        return ' ';
    }

    /**
     * Does $a indicate end of input?
     *
     * @param string $a
     * @return bool
     */
    protected function isEOF($a)
    {
        return ord($a) <= self::ORD_LF;
    }

    /**
     * Get next char (without getting it). If is ctrl character, translate to a space or newline.
     *
     * @return string
     */
    protected function peek()
    {
        $this->look_ahead = $this->get();
        return $this->look_ahead;
    }

    /**
     * Return true if the character is a letter, digit, underscore, dollar sign, or non-ASCII character.
     *
     * @param string $c
     *
     * @return bool
     */
    protected function isAlphaNum( $c )
    {
        return (preg_match('/^[a-z0-9A-Z_\\$\\\\]$/', $c) || ord($c) > 126);
    }

    /**
     * Consume a single line comment from input (possibly retaining it)
     */
    protected function consumeSingleLineComment()
    {
        $comment = '';
        while (true) {
            $get = $this->get();
            $comment .= $get;
            if (ord($get) <= self::ORD_LF) { // end of line reached
                // if IE conditional comment
                if (preg_match('/^\\/@(?:cc_on|if|elif|else|end)\\b/', $comment)) {
                    $this->kept_comment .= "/{$comment}";
                }
                return;
            }
        }
    }

    /**
     * Consume a multiple line comment from input (possibly retaining it)
     *
     * @throws JSMin_UnterminatedCommentException
     */
    protected function consumeMultipleLineComment()
    {
        $this->get();
        $comment = '';
        for(;;) {
            $get = $this->get();
            if ($get === '*') {
                if ($this->peek() === '/') { // end of comment reached
                    $this->get();
                    if (0 === strpos($comment, '!')) {
                        // preserved by YUI Compressor
                        if (!$this->kept_comment) {
                            // don't prepend a newline if two comments right after one another
                            $this->kept_comment = "\n";
                        }
                        // Delete the comment
                        $this->kept_comment .= "";// "/*!" . substr($comment, 1) . "*/\n";
                    } else if (preg_match('/^@(?:cc_on|if|elif|else|end)\\b/', $comment)) {
                        // IE conditional, Delete the comment
                        $this->kept_comment .= "";// "/*{$comment}*/";
                    }
                    return;
                }
            } elseif ($get === null) {
                throw new JSMin_UnterminatedCommentException(
                    "JSMin: Unterminated comment at byte {$this->input_index}: /*{$comment}");
            }
            $comment .= $get;
        }
    }

    /**
     * Get the next character, skipping over comments. Some comments may be preserved.
     *
     * @return string
     */
    protected function next()
    {
        $get = $this->get();
        if ($get === '/') {
            switch ($this->peek()) {
                case '/':
                    $this->consumeSingleLineComment();
                    $get = "\n";
                    break;
                case '*':
                    $this->consumeMultipleLineComment();
                    $get = ' ';
                    break;
            }
        }
        return $get;
    }
}

/* End of file Minifier_HTML.php */
/* Location: ./core/Libraries/Minifiers/ */