<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Compress Minifier_HTML
 *
 * This HTML minifier are going to remove all of whitespaces, unnecessary comments and
 * tokens.
 *
 */
class Minifier_HTML
{
    private $content;
    private $placeholders;
    private $replacement_hash;

    public function __construct( $html )
    {
        $this->content          = str_replace( "\r\n", "\n", trim( $html ) );
        $this->placeholders     = array();
        $this->replacement_hash = 'MINIFYHTML' . md5($_SERVER['REQUEST_TIME']);
    }

    /**
     * @method  Minify
     * @access  public
     * @desc    Minify the string given in the constructor
     * @author  Cousin Bela
     *
     * @return string
     */
    public function Minify()
    {
        // replace SCRIPTs (and minify) with placeholders
        $this->content = preg_replace_callback(
            '/(\\s*)<script(\\b[^>]*?>)([\\s\\S]*?)<\\/script>(\\s*)/i',
            array( $this, 'Remove_script_CB' ),
            $this->content
        );

        // replace STYLEs (and minify) with placeholders
        $this->content = preg_replace_callback(
            '/\\s*<style(\\b[^>]*>)([\\s\\S]*?)<\\/style>\\s*/i',
            array( $this, 'Remove_style_CB' ),
            $this->content
        );

        // remove HTML comments (not containing IE conditional comments).
        $this->content = preg_replace_callback(
            '/<!--([\\s\\S]*?)-->/',
            array( $this, 'Comment_CB' ),
            $this->content
        );

        // replace PREs with placeholders
        $this->content = preg_replace_callback(
            '/\\s*<pre(\\b[^>]*?>[\\s\\S]*?<\\/pre>)\\s*/i',
            array( $this, 'Remove_pre_CB' ),
            $this->content
        );

        // trim each line.
        $this->content = preg_replace( '/^\\s+|\\s+$/m', '', $this->content );

        // remove whitespace around block/undisplayed elements
        $this->content = preg_replace(
            '/\\s+(<\\/?(?:area|base(?:font)?|blockquote|body'
                .'|caption|center|cite|col(?:group)?|dd|dir|div|dl|dt|fieldset|form'
                .'|frame(?:set)?|h[1-6]|head|hr|html|legend|li|link|map|menu|meta'
                .'|ol|opt(?:group|ion)|p|param|t(?:able|body|head|d|h||r|foot|itle)'
                .'|ul)\\b[^>]*>)/i',
            '$1',
            $this->content
        );

        // remove ws outside of all elements
        $this->content = preg_replace(
            '/>(\\s(?:\\s*))?([^<]+)(\\s(?:\s*))?</',
            '>$1$2$3<',
            $this->content
        );

        // use newlines before 1st attribute in open tags (to limit line lengths)
        $this->content = preg_replace( '/(<[a-z\\-]+)\\s+([^>]+>)/i', "$1 $2\n", $this->content );

        // fill placeholders
        $this->content = str_replace(
            array_keys( $this->placeholders ),
            array_values( $this->placeholders ),
            $this->content
        );

        return $this->content;
    }

    private function Comment_CB( $string )
    {
        return ( strpos( $string[1], '[' ) === 0 || strpos( $string[1], '<![' ) !== FALSE ) ? $string[0] : '';
    }

    private function Reserve_place( $content )
    {
        $placeholder = '%' . $this->replacement_hash . count( $this->placeholders ) . '%';
        $this->placeholders[$placeholder] = $content;
        return $placeholder;
    }

    private function Remove_pre_CB( $string )
    {
        return $this->Reserve_Place( "<pre".$string[1] );
    }

    private function Remove_style_CB( $string )
    {
        $css   = $string[2];
        $css   = preg_replace( '/(?:^\\s*<!--|-->\\s*$)/', '', $css );
        $css   = $this->Remove_cdata( $css );
        $css   = trim( $css );

        return $this->Reserve_place( "<style".$string[1].$css."</style>" );
    }

    private function Remove_script_CB( $string )
    {
        $openScript = "<script".$string[2];
        $js = $string[3];

        $wihtespace1 = ( $string[1] === '' ) ? '' : ' ';
        $wihtespace2 = ( $string[4] === '' ) ? '' : ' ';

        $js = preg_replace( '/(?:^\\s*<!--\\s*|\\s*(?:\\/\\/)?\\s*-->\\s*$)/', '', $js );

        $js = $this->Remove_cdata( $js );
        $js = trim( $js );

        return $this->Reserve_place( $wihtespace1.$openScript.$js."</script>".$wihtespace2 );
    }

    private function Remove_cdata( $string )
    {
        return ( strpos( $string, '<![CDATA[' ) != FALSE )
            ? str_replace( array('<![CDATA[', ']]>' ), '', $string )
            : $string ;
    }
}

/* End of file Minifier_HTML.php */
/* Location: ./core/Libraries/Minifiers/ */