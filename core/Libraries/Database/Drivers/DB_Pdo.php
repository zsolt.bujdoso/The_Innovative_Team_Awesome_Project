<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class DB_Pdo
 *
 * Mysql adapter
 *
 * @property    MDB_Filter  $filters
 *
 * @version     1.0.0
 */
class DB_Pdo extends MDatabase
{
    public function __construct()
    {
        parent::__construct();

        $this->str_to_protect_column = '"';
        $this->str_to_protect_value  = "'";
    }

    //<editor-fold desc="Connection and configures">
    /**
     * @method  Connect
     * @access  public
     * @desc    This method will try to connect to the database and select the database
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Connect()
    {
        $this->identifier = pg_connect(
            "host=".$this->hostname.":".$this->port." dbname=".$this->database." user=".$this->username." password=".$this->password
        );

        if( pg_last_error( $this->identifier ) > 0 )
        {
            throw new MDatabase_exception(
                "Error encountered while connecting to database on host: ".$this->hostname.", port: ".$this->port." or database can be selected"
            );
        }

        return TRUE;
    }

    /**
     * @method  Select_database
     * @desc    This method selects the database we want
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @throws  MException, MDatabase_exception
     */
    public function Select_database()
    {
    }

    /**
     * @method  Set_Collate
     * @desc    This method runs an SQL to set collation for queries
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    public function Set_Collate()
    {
        /*$query = "";

        if( ! empty( $this->charset ) )
        {
            $query .= "SET NAMES '".$this->charset."' ";
        }

        if( ! empty( $this->collate ) )
        {
            $query .= "COLLATE '".$this->collate."'";
        }

        if( ! empty( $query ) )
        {
            $this->Query( $query );
        }*/
    }

    /**
     * @method  Close_connection
     * @desc    This method close the database connection
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    public function Close_connection()
    {
        return pg_close( $this->identifier );
    }
    //</editor-fold>

    /******************************************
     * TABLE MODIFICATIONS
     ******************************************/
    //<editor-fold desc="Table modifiers">
    /**
     * @method  Create_table
     * @access  public
     * @desc    Create new table
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $columns                    - an array with columns where index is name of the column
     *                                                                and value is type, length, and other options
     * @param   string                  $primary_key                - the primary key column name
     * @param   bool                    $auto_increment             - add auto increment for primary key
     * @param   string                  $engine                     - engine use for this table
     *
     * @version 1.0.0
     * @throws  MException
     * @return  bool
     */
    public function Create_table( $table_name, $columns, $primary_key = "", $auto_increment = TRUE, $engine = "InnoDB" )
    {
        if( $auto_increment )
        {
            $query = "CREATE SEQUENCE " . $table_name . "_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;";

            return $this->Query( $query );
        }

        $query          = "CREATE TABLE ".$this->Check_table_name( $table_name )
                            ." WITH OWNER = ".$this->username
                            ." ENCODING = 'UTF-8'
                            TABLESPACE = pg_default
                            CONNECTION LIMIT = -1";
        $table_columns  = array();
        $primary_keys   = explode( ",", $primary_key );
        $primary_keys   = array_filter( $primary_keys );

        if( ! empty( $columns ) && is_array( $columns ) )
        {
            foreach( $columns as $column => $type )
            {
                $sql = $this->filters->Protect( $column )." ".$type;

                if( $auto_increment && $column == $primary_key )
                {
                    $sql .= " NOT NULL DEFAULT nextval('".$table_name."_id_seq'::regclass)";
                }

                $table_columns []= $sql;
            }
        }

        if( empty( $table_columns ) )
        {
            throw new MDatabase_exception( "Columns for table ".$table_name." are not set" );
        }

        $query .= "( ".implode( ",", $table_columns ).",
                    PRIMARY KEY ( ".implode( ",", $this->filters->Protect( $primary_keys ) )." )
                  ) WITH ( OIDS = false ) TABLESPACE ( pg_default );";

        // Execute the query
        return $this->Query( $query );
    }

    /**
     * @method  Create_indexes
     * @access  public
     * @desc    Create new indexes for selected table
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $indexes                    - an array with columns where index is name of the column
     *                                                                and values are arrays with column names for selected index
     *
     * @version 1.0.0
     * @throws  MException
     * @return  bool
     */
    public function Create_indexes( $table_name, $indexes )
    {
        // If we set more indexes
        if( ! is_array( $indexes ) )
        {
            return FALSE;
        }

        foreach( $indexes as $name => $columns )
        {
            $this->Create_index( $table_name, $name, $columns );
        }
    }

    /**
     * @method  Create_index
     * @access  public
     * @desc    Create new index for selected table
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $index_name                 - new index name or an array with columns where index is name of the column
     *                                                                and values are arrays with column names for selected index
     * @param   array                   $columns                    - an array with columns for new index
     *
     * @version 1.0.0
     * @throws  MException
     * @return  bool
     */
    public function Create_index( $table_name, $index_name, $columns = array() )
    {
        $table_name = $this->Check_table_name( $table_name );
        $query      = "CREATE INDEX ".$index_name." ON public.".$table_name." USING btree (";
        $indexes    = array();

        foreach( $columns as $key => $column )
        {
            $indexes []= $this->filters->Protect( $column );
        }

        $query .= implode( ",", $indexes ).") TABLESPACE pg_default";

        // Execute the query
        return $this->Query( $query );
    }

    /**
     * @method  Check_table_name
     * @access  private
     * @desc    This method checks and add the prefix for selected table if it is not added
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table
     *
     * @version 1.0.0
     * @return string
     */
    public function Check_table_name( $table_name )
    {
        if( ! empty( $this->prefix ) )
        {
            $prefix_position = strpos( $table_name, $this->prefix );

            if( $prefix_position !== 0 )
            {
                $table_name = $this->prefix . $table_name;
            }
        }

        return $table_name;
    }

    /**
     * @method  Delete_table
     * @desc    This method try to delete a table by table name
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     *
     * @version 1.0
     * @return  bool
     */
    public function Delete_table( $table_name )
    {
        $query = "DROP TABLE IF EXISTS ".$this->filters->Protect( $this->Check_table_name( $table_name ) );

        return $this->Query( $query );
    }

    /**
     * @method  Truncate
     * @desc    Try to truncate a table by name
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     *
     * @version 1.0
     * @return  bool
     */
    public function Truncate( $table_name )
    {
        $query = "TRUNCATE TABLE ".$this->filters->Protect( $this->Check_table_name( $table_name ) );

        return $this->Query( $query );
    }

    /**
     * @method  Alter_table
     * @desc    Try to alter table by name and modifications string
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $query                      - modifications string
     *
     * @version 1.0
     * @return  bool
     */
    public function Alter_table( $table_name, $query )
    {
        $query = "ALTER TABLE ".$this->filters->Protect( $this->Check_table_name( $table_name ) )
            ." ".$query;

        return $this->Query( $query );
    }

    /**
     * @method  Add_column
     * @desc    Try to add a column into specified table after a column
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $column                     - the column name
     * @param   string                  $type                       - type of the column, length, and other modifiers
     * @param   string                  $after                      - column name to insert after
     *
     * @version 1.0
     * @return  bool
     */
    public function Add_column( $table_name, $column, $type, $after )
    {
        $query = "ALTER TABLE "
            .$this->filters->Protect( $this->Check_table_name( $table_name ) )
            ." ADD COLUMN ".$this->filters->Protect( $column )." ".$type." "
            .( ! empty( $after ) ? "AFTER ".$this->filters->Protect( $after ) : "" );

        return $this->Query( $query );
    }

    /**
     * @method  Delete_column
     * @desc    Try to delete a column from specified table
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $column                     - the column name
     *
     * @version 1.0
     * @return  bool
     */
    public function Delete_column( $table_name, $column )
    {
        $query = "ALTER TABLE ".$this->filters->Protect( $this->Check_table_name( $table_name ) )
            ." DELETE COLUMN ".$this->filters->Protect( $column );

        return $this->Query( $query );
    }

    /**
     * @method  Rename
     * @desc    Try to rename a table
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $new_table_name             - new name for table
     *
     * @version 1.0
     * @return  bool
     */
    public function Rename( $table_name, $new_table_name )
    {
        $query = "RENAME TABLE ".$this->filters->Protect( $this->Check_table_name( $table_name ) )
            ." TO ".$this->filters->Protect( $this->Check_table_name( $new_table_name ) );

        return $this->Query( $query );
    }
    //</editor-fold>

    /******************************************
     * TRANSACTIONS
     ******************************************/
    //<editor-fold desc="Transactions">
    /**
     * @method  Transaction_start
     * @desc    Try to start a transaction
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_start()
    {
        $query = "START TRANSACTION;";
        $this->Query( $query );

        $query = "SET autocommit=0;";
        return $this->Query( $query );
    }

    /**
     * @method  Transaction_end
     * @desc    Try to end transaction started before
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_end()
    {
        $query = "COMMIT;";
        $this->Query( $query );

        $query = "SET autocommit=1;";
        return $this->Query( $query );
    }

    /**
     * @method  Transaction_status
     * @desc    Returns the transaction status
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_status()
    {
        if( ! mysqli_errno( $this->identifier ) )
            return TRUE;

        return FALSE;
    }

    /**
     * @method  Transaction_status
     * @desc    Try to rollback started transaction before
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_rollback()
    {
        $query = "ROLLBACK;";
        $this->Query( $query );

        $query = "SET autocommit=1;";
        return $this->Query( $query );
    }
    //</editor-fold>

    /******************************************
     * GETTERS
     ******************************************/
    //<editor-fold desc="Getters">
    /**
     * @method  Get_list
     * @access  public
     * @desc    Returns an array with list of items from specified table or FALSE
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list( $table_name, $forced_index = null, $force_from_db = FALSE )
    {
        $this->filters->From( $table_name, "", $forced_index );

        return $this->Execute( "SELECT", $force_from_db );
    }

    /**
     * @method  Get_by_primary_key
     * @access  public
     * @desc    Returns one item from specified table by primary key
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $primary_key_column         - name of the column of primary key
     * @param   string                  $value                      - primary key value
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_primary_key( $table_name, $primary_key_column, $value, $force_from_db = FALSE )
    {
        return $this->Get_by_column( $table_name, $primary_key_column, $value, $force_from_db );
    }

    /**
     * @method  Get_by_column
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $column                     - name of the column
     * @param   string                  $value                      - value of column
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_column( $table_name, $column, $value, $force_from_db = FALSE )
    {
        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Where_equal( $column, $value );
        $this->filters->Limit( 1 );

        $result = $this->Execute( "SELECT", $force_from_db );

        if( $result && ( $first_row = $result->Get_first_row() ) )
        {
            return $first_row;
        }

        return $result;
    }

    /**
     * @method  Get_by_attributes
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_attributes( $table_name, $attributes = array(), $force_from_db = FALSE )
    {
        return $this->Get_by_column( $table_name, $attributes, null, $force_from_db );
    }

    /**
     * @method  Get_by_filters
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $filters                    - an object with specified filters
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_filters( $table_name, $filters = null, $force_from_db = FALSE )
    {
        $this->filters = $filters;

        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Limit( 1 );

        $result = $this->Execute( "SELECT", $force_from_db );

        if( $result && ( $first_row = $result->Get_first_row() ) )
        {
            return $first_row;
        }

        return $result;
    }

    /**
     * @method  Get_list_by_column
     * @access  public
     * @desc    Returns an array with list of items from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $column                     - name of the column
     * @param   string                  $value                      - value of column
     * @param   int                     $limit                      - number of items to return
     * @param   int                     $limit_from                 - number where to start from returning
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list_by_column( $table_name, $column, $value, $limit = null, $limit_from = null, $force_from_db = FALSE )
    {
        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Where_equal( $column, $value );
        $this->filters->Limit( $limit, $limit_from );

        return $this->Execute( "SELECT", $force_from_db );
    }

    /**
     * @method  Get_list_by_attributes
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   int                     $limit                      - number of items to return
     * @param   int                     $limit_from                 - number where to start from returning
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list_by_attributes( $table_name, $attributes = array(), $limit = null, $limit_from = null, $force_from_db = FALSE )
    {
        return $this->Get_list_by_column( $table_name, $attributes, null, $limit, $limit_from, $force_from_db );
    }

    /**
     * @method  Get_list_by_filters
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $filters                    - an object with specified filters
     * @param   int                     $limit                      - number of items to return
     * @param   int                     $limit_from                 - number where to start from returning
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list_by_filters( $table_name, $filters = null, $limit = null, $limit_from = null, $force_from_db = FALSE )
    {
        $this->filters = $filters;

        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Limit( $limit, $limit_from );

        return $this->Execute( "SELECT", $force_from_db );
    }
    //</editor-fold>

    /******************************************
     * CRUD FUNCTIONS
     ******************************************/
    //<editor-fold desc="Crud functions">
    /**
     * @method  Insert
     * @access  public
     * @desc    Insert a new row into specified table
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     *
     * @version 1.0.0
     * @return  int
     */
    public function Insert( $table_name, $data_set = array() )
    {
        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Set( $data_set );

        // Execute the insert. We don't have to check if error occurs, because in execute is checked
        $this->Execute( "INSERT" );

        // If error not throwed just return the last insert id
        return $this->Get_last_insert_id( $table_name );
    }

    /**
     * @method  Insert_by_sql
     * @access  public
     * @desc    Insert a new row into specified table
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $query_extension            - an sql command for selecting rows from other table
     *
     * @version 1.0.0
     * @return  int
     */
    public function Insert_by_sql( $table_name, $data_set = array(), $query_extension = "" )
    {
        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Set_for_sql( $data_set );

        $this->query_extension = $query_extension;

        // Execute the insert. We don't have to check if error occurs, because in execute is checked
        $this->Execute( "INSERT BY SQL" );

        // If error not throwed just return the last insert id
        return TRUE;
    }

    /**
     * @method  Update_by_column
     * @access  public
     * @desc    Update one or more row in specified table by column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $column                     - name of specified column
     * @param   string                  $value                      - value of column
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_column( $table_name, $data_set = array(), $column, $value )
    {
        if( is_array( $value ) )
        {
            return $this->Update_by_column_value_array( $table_name, $data_set, $column, $value );
        }

        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Set( $data_set );
        $this->filters->Where_equal( $column, $value );

        return $this->Execute( "UPDATE" );
    }

    /**
     * @method  Update_by_column_value_array
     * @access  private
     * @desc    Update more row in specified table by column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $column                     - name of specified column
     * @param   array                   $values                     - values of column
     *
     * @version 1.0.0
     * @return  bool
     */
    private function Update_by_column_value_array( $table_name, $data_set = array(), $column, $values )
    {
        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Set( $data_set );

        foreach( $values as $value )
        {
            $this->filters->Or_where_equal( $column, $value );
        }

        return $this->Execute( "UPDATE" );
    }

    /**
     * @method  Update
     * @access  public
     * @desc    Update one or more row in specified table by primary key
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $primary_key                - name of primary key column
     * @param   string                  $value                      - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_primary_key( $table_name, $data_set = array(), $primary_key, $value )
    {
        return $this->Update_by_column( $table_name, $data_set, $primary_key, $value );
    }

    /**
     * @method  Update
     * @access  public
     * @desc    Update one or more row in specified table by attributes
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_attributes( $table_name, $data_set = array(), $attributes )
    {
        // Update by columns, here the attributes has more column, and when will be checked by filters class
        // will be put into where equal one by one
        return $this->Update_by_column( $table_name, $data_set, $attributes, "" );
    }


    /**
     * @method  Update_by_filters
     * @access  public
     * @desc    Update one or more row in specified table by filters
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $filters                    - an object with specified filters
     * @param   int                     $limit                      - number of items to return
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_filters( $table_name, $filters = null, $limit = null )
    {
        $this->filters = $filters;

        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        return $this->Execute( "UPDATE" );
    }

    /**
     * @method  Delete_by_primary_key
     * @access  public
     * @desc    Update one row from specified table by primary key
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $primary_key                - name of primary key column
     * @param   string                  $value                      - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_primary_key( $table_name, $primary_key, $value )
    {
        return $this->Delete_by_column( $table_name, $primary_key, $value );
    }

    /**
     * @method  Delete_by_column
     * @access  public
     * @desc    Update one row from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $column                     - name of column
     * @param   string                  $value                      - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_column( $table_name, $column, $value )
    {
        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Where_equal( $column, $value );

        return $this->Execute( "DELETE" );
    }

    /**
     * @method  Delete_by_attributes
     * @access  public
     * @desc    Delete one or more rows from specified table by attributes
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_attributes( $table_name, $attributes )
    {
        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Where_equal( $attributes );

        return $this->Execute( "DELETE" );
    }

    /**
     * @method  Delete_by_filters
     * @access  public
     * @desc    Delete one or more row from specified table by filters
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   object                  $filters                    - object with filters
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_filters( $table_name, $filters = null )
    {
        $this->filters = $filters;

        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        return $this->Execute( "DELETE" );
    }

    /**
     * @method  Count_by_column
     * @access  public
     * @desc    Count rows from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $column                     - name of column
     * @param   string                  $value                      - value of primary key
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Count_by_column( $table_name, $column, $value, $force_from_db = FALSE )
    {
        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Where_equal( $column, $value );

        return $this->Execute( "COUNT", $force_from_db );
    }

    /**
     * @method  Count_by_attributes
     * @access  public
     * @desc    Count rows from specified table by attributes
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Count_by_attributes( $table_name, $attributes, $force_from_db = FALSE )
    {
        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        $this->filters->Where_equal( $attributes );

        return $this->Execute( "COUNT", $force_from_db );
    }

    /**
     * @method  Count_by_filters
     * @access  public
     * @desc    Count rows from specified table by filters
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   object                  $filters                    - object with filters
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Count_by_filters( $table_name, $filters = null, $force_from_db = FALSE )
    {
        $this->filters = $filters;

        if( $this->filters->Is_empty_from() )
        {
            $this->filters->From( $table_name );
        }

        return $this->Execute( "COUNT", $force_from_db );
    }
    //</editor-fold>

    /******************************************
     * INFORMATIONS
     ******************************************/
    //<editor-fold desc="Information getters">
    /**
     * @method  Get_table_names
     * @desc    This method return all table name from selected database
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  array|bool
     */
    public function Get_table_names()
    {
        $query = "SELECT * FROM pg_catalog.tables
                    WHERE table_type = 'BASE TABLE'
                    AND table_schema NOT IN ( 'pg_catalog', 'information_schema' )";

        return $this->Query( $query );
    }

    /**
     * @method  Get_primary_key
     * @access  public
     * @desc    Returns the primary key from specified table or FALSE
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     *
     * @version 1.0.0
     * @return  string|bool
     */
    public function Get_primary_key( $table_name )
    {
        if( empty( $table_name ) )
            return "";

        $table_name = $this->Get_table_name( $table_name );
        // create query for getting primary key
        $query 		= "SELECT  t.table_catalog,
                                 t.table_schema,
                                 t.table_name,
                                 kcu.constraint_name,
                                 kcu.column_name,
                                 kcu.ordinal_position
                          FROM INFORMATION_SCHEMA.TABLES t
                          LEFT JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
                              ON tc.table_catalog = t.table_catalog
                              AND tc.table_schema = t.table_schema
                              AND tc.table_name = t.table_name
                              AND tc.constraint_type = 'PRIMARY KEY'
                          LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
                              ON kcu.table_catalog = tc.table_catalog
                              AND kcu.table_schema = tc.table_schema
                              AND kcu.table_name = tc.table_name
                              AND kcu.constraint_name = tc.constraint_name
                          WHERE t.table_schema NOT IN ( 'pg_catalog', 'information_schema' )
                              AND t.table_name = '".$table_name."' 
                          ORDER BY kcu.constraint_name, kcu.ordinal_position";

        // get the result for this
        $result		= $this->Query( $query );

        if( $result )
        {
            // fetch the row
            $row = ( object ) mysqli_fetch_assoc( $result );

            // return the primary key value
            return $row->column_name;
        }

        return FALSE;
    }

    /**
     * @method  Get_process_list
     * @desc    Returns the list of processes from selected database
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  array|bool
     */
    public function Get_process_list()
    {
        return $this->Query( "SELECT * from pg_stat_activity" );
    }

    /**
     * @method  Get_executed_queries
     * @desc    This method returns all executed queries as array
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  array
     */
    public function Get_executed_queries()
    {
        return $this->executed_queries;
    }

    /**
     * @method  Get_last_query
     * @desc    This method returns the last executed query as array
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  array
     */
    public function Get_last_query()
    {
        return array_pop( $this->executed_queries );
    }

    /**
     * @method  Get_last_insert_id
     * @desc    This method returns the last inserted id or FALSE
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table you want
     *
     * @version 1.0
     * @return  int|bool
     */
    public function Get_last_insert_id( $table_name = "" )
    {
        $primary_key    = $this->Get_primary_key( $table_name );
        $table_name     = $this->Get_table_name( $table_name );

        return $this->Query( "SELECT currval( pg_get_serial_sequence( '".$table_name."', '".$primary_key."' ) )" );
    }
    //</editor-fold>

    /******************************************
     * QUERIES
     ******************************************/
    //<editor-fold desc="Query executions">
    /**
     * @method  Query
     * @desc    This method executes the query and return the result as object
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $query                      - query to execute
     *
     * @version 1.0
     * @throws  MDatabase_exception
     * @return  object|bool
     */
    public function Query( $query )
    {
        App()->profiler->Start( "SQL" );
        $result = pg_query( $this->identifier, $query );
        //echo $query." <br />";
        App()->profiler->Stop( "SQL" );

        // Save into executed queries
        $this->executed_queries []= array(
            "sql"   => $query,
            "time"  => App()->profiler->Get( "SQL" ),
            "rows"  => pg_affected_rows( $this->identifier )
        );

        if( pg_last_error( $this->identifier ) > 0 )
        {
            $this->Reset();

            throw new MDatabase_exception(
                $this->Get_error_message()
            );
        }

        return $result;
    }

    /******************************************
     * STATUS AND ERROR MESSAGE
     ******************************************/
    //<editor-fold desc="Status and Error message">
    /**
     * @method  Get_status
     * @desc    Return the status of last query
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return bool
     */
    public function Get_status()
    {
        if( pg_last_error( $this->identifier ) > 0 )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Get_error_message
     * @desc    Return the error message of last query
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return bool
     */
    public function Get_error_message()
    {
        $last_sql = array_pop( $this->executed_queries );

        return "Error code: ".pg_last_error( $this->identifier )."<br />Error: ".pg_errormessage( $this->identifier ).
            "<br />SQL: ".$last_sql["sql"];
    }
    //</editor-fold>

    /**
     * @method  Escape
     * @desc    Escape the string parameter and returns it
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $string                     - string to escape
     *
     * @version 1.0
     * @return bool
     */
    public function Escape( $string )
    {
        $string = str_replace( "\r\n", "[[new_line]]", $string );
        $string = pg_escape_string( $this->identifier, trim( $string ) );
        $string = str_replace( "[[new_line]]", "\r\n", $string );

        return $string;
    }

    /**
     * @method  Get_table_name
     * @desc    Check and return the table name with prefix
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to check
     *
     * @version 1.0
     * @return bool
     */
    public function Get_table_name( $table_name )
    {
        if( empty( $this->prefix ) || strpos( $table_name, $this->prefix ) === 0 )
        {
            return $table_name;
        }

        return $this->prefix.$table_name;
    }

    /**
     * @method  Get_database_name
     * @desc    Check and return the database name
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return bool
     */
    public function Get_database_name()
    {
        return $this->database;
    }
}