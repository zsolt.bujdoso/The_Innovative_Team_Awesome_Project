<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MDB_Result
 *
 * @version     1.0.0
 */
class MDB_Result extends stdClass implements IMDB_Result
{
    private $result = array();

    public function __construct()
    {

    }

    public function Add_db_result( $db_result )
    {
        $this->result []= $db_result;
    }

    public function & Get_result()
    {
        return $this->result;
    }

    public function Set_row( $index, $value )
    {
        return $this->result[$index] = $value;
    }

    public function Get_first_row()
    {
        if( empty( $this->result[0] ) )
        {
            return FALSE;
        }

        return $this->result[0];
    }

    public function Get_last_row()
    {
        $nr_items = $this->Get_nr_rows();
        if( empty( $this->result[$nr_items - 1] ) )
        {
            return FALSE;
        }

        return $this->result[$nr_items - 1];
    }

    public function Get_nr_rows()
    {
        return sizeof( $this->result );
    }
}