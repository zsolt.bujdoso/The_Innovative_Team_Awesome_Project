<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MDB_File_cache
 *
 * @version     1.0.0
 */
class MDB_File_cache extends BDB_Cache
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @method  Cache_exists
     * @access  public
     * @desc    This method checks if a cache file exists, and it is not older than cache time
     * @author  Cousin Bela
     *
     * @param   string                      $sql                        - sql query for cache name
     * @param   int                         $cache_time                 - how old can be the cache file in seconds
     *
     * @version 1.0
     * @return  bool
     */
    public function Cache_exists( $sql, $cache_time = null )
    {
        if( ! $this->Is_turned_on() )
        {
            return FALSE;
        }

        if( ! isset( $cache_time ) )
        {
            $cache_time = $this->cache_time;
        }

        $cache_file_path = $this->Get_cache_file_path( $sql );

        if( ! file_exists( $cache_file_path ) || filemtime( $cache_file_path ) < ( time() - $cache_time ) )
        {
            // Delete old file
            if( file_exists( $cache_file_path ) )
            {
                @unlink( $cache_file_path );
            }

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Get_content
     * @access  public
     * @desc    This method returns the content from a cache file
     * @author  Cousin Bela
     *
     * @param   string                      $sql                        - sql query for cache name
     *
     * @version 1.0
     * @return  string
     */
    public function Get_content( $sql )
    {
        if( ! $this->Is_turned_on() )
        {
            return FALSE;
        }

        $cache_file_path = $this->Get_cache_file_path( $sql );

        $content = unserialize( base64_decode( file_get_contents( $cache_file_path ) ) );

        return $content;
    }

    /**
     * @method  Create_cache
     * @access  public
     * @desc    This method create a new cache file with specified content
     * @author  Cousin Bela
     *
     * @param   string                      $sql                        - sql query for cache name
     * @param   int                         $result                     - result to be cached
     *
     * @version 1.0
     * @return  bool
     */
    public function Create_cache( $sql, $result )
    {
        if( ! $this->Is_turned_on() )
        {
            return FALSE;
        }

        $cache_file_path = $this->Get_cache_file_path( $sql );

        if( ! file_exists( $cache_file_path ) || filemtime( $cache_file_path ) < ( time() - $this->cache_time ) )
        {
            if( file_exists( $cache_file_path ) )
            {
                @unlink( $cache_file_path );
            }
        }

        return file_put_contents( $cache_file_path, base64_encode( serialize( $result ) ) );
    }

    /**
     * @method  Get_cache_file_path
     * @access  public
     * @desc    This method return the name of the cache file for specified path
     * @author  Cousin Bela
     *
     * @param   string                      $path                       - the type to send the classname
     *
     * @version 1.0
     * @return  string
     */
    private function Get_cache_file_path( $sql )
    {
        $cache_name = md5( $sql );

        return $this->cache_path.$cache_name.".cache";
    }
}

/* End of file MDB_File_cache.php */
/* Location: ./core/Libaries/Database/Cache/ */