<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MDB_Memory_cache
 *
 * @version     1.0.0
 */
class MDB_Memory_cache extends BDB_Cache
{
    private $temp_data = array();

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @method  Cache_exists
     * @access  public
     * @desc    This method checks if a cache file exists, and it is not older than cache time
     * @author  Cousin Bela
     *
     * @param   string                      $sql                        - sql query to get from cache
     * @param   int                         $cache_time                 - how old can be the cache file in seconds
     *
     * @version 1.0
     * @return  bool
     */
    public function Cache_exists( $sql, $cache_time = null )
    {
        if( ! $this->Is_turned_on() )
        {
            return FALSE;
        }

        $cache_name = md5( $sql );

        if( ! isset( $this->temp_data[$cache_name] ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Get_content
     * @access  public
     * @desc    This method returns the content from a cache file
     * @author  Cousin Bela
     *
     * @param   string                      $sql                        - sql query to get from cache
     *
     * @version 1.0
     * @return  string
     */
    public function Get_content( $sql )
    {
        if( ! $this->Is_turned_on() )
        {
            return FALSE;
        }

        $cache_name = md5( $sql );

        return $this->temp_data[$cache_name];
    }

    /**
     * @method  Create_cache
     * @access  public
     * @desc    This method create a new cache file with specified content
     * @author  Cousin Bela
     *
     * @param   string                      $sql                        - sql query for cache name
     * @param   int                         $result                     - result to be cached
     *
     * @version 1.0
     * @return  bool
     */
    public function Create_cache( $sql, & $result )
    {
        if( ! $this->Is_turned_on() )
        {
            return FALSE;
        }

        $cache_name = md5( $sql );
        $this->temp_data[$cache_name] =& $result;

        return TRUE;
    }
}

/* End of file MDB_Memory_cache.php */
/* Location: ./core/Libraries/Database/Cache/ */