<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MDatabase
 *
 * @version     1.0.0
 */
abstract class MDatabase extends BDatabase
{
    public function __construct()
    {
        parent::__construct();

        $this->Reset();
    }

    //<editor-fold desc="Connection and configures">
    /**
     * @method  Set_config
     * @desc    This method set the configuration into class variables
     * @access  public
     * @author  Cousin Bela
     *
     * @param   array                   $database_config            - an array with database configuration
     *
     * @version 1.0
     */
    public function Set_config( array $database_config )
    {
        $this->hostname = $database_config["hostname"];
        $this->port     = $database_config["port"];
        $this->username = $database_config["username"];
        $this->password = $database_config["password"];
        $this->database = $database_config["database"];
        $this->driver   = $database_config["driver"];
        $this->prefix   = $database_config["prefix"];
        $this->charset  = $database_config["charset"];
        $this->collate  = $database_config["collate"];
    }
    //</editor-fold>

    /**
     * @method  Reset
     * @desc    Reset the filters and arrays for next query
     * @access  private
     * @author  Cousin Bela
     *
     * @version 1.0
     * @throws  MPHP_exception
     * @return  bool
     */
    public function Reset()
    {
        $class_name = App()->Get_classname_for( "mdb_filter" );

        if( empty( $class_name ) )
        {
            throw new MPHP_exception( E_ERROR, "Database filter not set" );
        }

        $this->filters          = new $class_name( $this );
        $this->query_extension  = "";
    }

    /******************************************
     * QUERIES
     ******************************************/
    //<editor-fold desc="Query executions">
    /**
     * @method  Query
     * @desc    This method executes the query and return the result as object
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $query                      - name of the table you want
     *
     * @version 1.0
     * @throws  MDatabase_exception
     * @return  object|bool
     */
    /*public function Query( $query )
    {
        App()->profiler->Start( "SQL" );
        $result = mysql_query( $query, $this->identifier );
        //echo $query." <br />";
        App()->profiler->Stop( "SQL" );

        // Save into executed queries
        $this->executed_queries []= array(
            "sql"   => $query,
            "time"  => App()->profiler->Get( "SQL" ),
            "rows"  => mysql_affected_rows( $this->identifier )
        );

        if( mysql_errno( $this->identifier ) > 0 )
        {
            $this->Reset();

            throw new MDatabase_exception(
                $this->Get_error_message()
            );
        }

        return $result;
    }*/

    /**
     * @method  Create_result
     * @desc    This method create a result object from database result
     * @access  protected
     * @author  Cousin Bela
     *
     * @param   mixed                   $db_result                  - Result from query
     * @param   string                  $db_type                    - Type of DB, mysql, pg, mysqli
     *
     * @version 1.0
     * @return  MDB_Result
     */
    protected function Create_result( & $db_result, $db_type = "" )
    {
        if( empty( $db_type ) )
        {
            $db_type = $this->driver;
        }

        $result             = FALSE;
        $function_num_rows  = $db_type."_num_rows";

        if( $function_num_rows( $db_result ) <= 0 )
        {
            $this->Reset();

            return $result;
        }

        $result         = General_functions::Instantiate_class( "mdb_result", TRUE, TRUE, TRUE );
        $function_fetch = $db_type."_fetch_assoc";

        while( ( $row = $function_fetch( $db_result ) ) )
        {
            $item = new stdClass();

            foreach( $row as $variable => $value )
            {
                //$value          = str_replace( array( "\r", "\n", '\r', '\n' ), "", $value );
                $item->$variable= stripslashes( $value );
            }

            $result->Add_db_result( $item );
        }

        $this->Reset();

        return $result;
    }

    private function get_new_object()
    {
        if( empty( $class_vars ) )
        {
            return (object) array();
        }

        return new stdClass();
    }

    /**
     * @method  Execute
     * @desc    This method executes a mysql query by type
     * @access  protected
     * @author  Cousin Blea
     *
     * @param   string                  $type                       - type of query
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0
     * @return  object|bool
     */
    protected function Execute( $type, $force_from_db = FALSE )
    {
        switch( $type )
        {
            case "SELECT":
                return $this->Execute_select( $force_from_db );
            case "INSERT":
                return $this->Execute_insert();
            case "INSERT BY SQL":
                return $this->Execute_insert_by_sql();
            case "UPDATE":
                return $this->Execute_update();
            case "DELETE":
                return $this->Execute_delete();
            case "COUNT":
                return $this->Execute_count( $force_from_db );
        }

        return FALSE;
    }

    /**
     * @method  Execute_select
     * @desc    Create and execute a select query
     * @access  protected
     * @author  Cousin Bela
     *
     * @param   bool                    $force_from_db              - get data from DB forced, because of cache
     *
     * @version 1.0
     * @return bool
     */
    protected function Execute_select( $force_from_db = FALSE )
    {
        $query = $this->Get_query_for_select();

        if( ! $force_from_db && $this->Cache_exists( $query ) )
        {
            $this->Reset();

            return $this->cache->Get_content( $query );
        }

        $result = $this->Query( $query );
        $this->Reset();

        $result = $this->Create_result( $result );

        if( $this->Is_cache_on() )
        {
            $this->cache->Create_cache( $query, $result );
        }

        return $result;
    }

    /**
     * @method  Execute_insert
     * @desc    Create and execute an insert query and returns the inserted ID
     * @access  protected
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool|int
     */
    protected function Execute_insert()
    {
        $query = $this->Get_query_for_insert();

        $this->Query( $query );
        $this->Reset();

        return $this->Get_last_insert_id();
    }

    /**
     * @method  Execute_insert_by_sql
     * @desc    Create and execute an insert by other SQL query
     * @access  protected
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool|int
     */
    protected function Execute_insert_by_sql()
    {
        $query = $this->Get_query_for_insert_by_sql();
        $query .= $this->query_extension;

        $this->Query( $query );
        $this->Reset();

        return $this->Get_last_insert_id();
    }

    /**
     * @method  Execute_update
     * @desc    Create and execute an update query
     * @access  protected
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    protected function Execute_update()
    {
        $query = $this->Get_query_for_update();

        $result = $this->Query( $query );
        $this->Reset();

        return $result;
    }

    /**
     * @method  Execute_delete
     * @desc    Create and execute a delete query
     * @access  protected
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    protected function Execute_delete()
    {
        $query = $this->Get_query_for_delete();

        $result = $this->Query( $query );
        $this->Reset();

        return $result;
    }

    /**
     * @method  Execute_count
     * @desc    Create and execute a count query and return the number of rows
     * @access  protected
     * @author  Cousin Bela
     *
     * @param   bool                    $force_from_db              - get data from BD forced, because of cache
     *
     * @version 1.0
     * @return  bool
     */
    protected function Execute_count( $force_from_db = FALSE )
    {
        $query = $this->Get_query_for_count();

        if( ! $force_from_db && $this->Cache_exists( $query ) )
        {
            $this->Reset();

            return $this->cache->Get_content( $query );
        }

        $result = $this->Query( $query );
        $this->Reset();

        /* TODO: Group by esetén csak az elso sor erteket adja vissza, talan ossze kene szamolni az osszes talalatot */
        //$counter = mysql_fetch_assoc( $result );
        $db_result = $this->Create_result( $result );

        $result = 0;
        if( $db_result )
        {
            if( $db_result->Get_nr_rows() > 1 )
            {
                $result     = $db_result->Get_nr_rows();
            }
            else
            {
                $first_row  = $db_result->Get_first_row();
                $result     = $first_row->nr_rows;
            }
        }

        if( $this->Is_cache_on() )
        {
            $this->cache->Create_cache( $query, $result );
        }

        return $result;
    }
    //</editor-fold>

    /******************************************
     * QUERY GETTERS
     ******************************************/
    //<editor-fold desc="Query getters">

    /**
     * @method  Get_query
     * @desc    This method get a mysql query by type
     * @access  public
     * @author  Cousin Blea
     *
     * @param   object                  $filters                    - filters for query
     * @param   string                  $type                       - type of query
     *
     * @version 1.0
     * @return  string|bool
     */
    public function Get_query( $filters = null, $type = "SELECT" )
    {
        $this->filters = $filters;

        switch( strtoupper( $type ) )
        {
            case "SELECT":
                return $this->Get_query_for_select();
            case "INSERT":
                return $this->Get_query_for_insert();
            case "UPDATE":
                return $this->Get_query_for_update();
            case "DELETE":
                return $this->Get_query_for_delete();
            case "COUNT":
                return $this->Get_query_for_count();
        }

        return FALSE;
    }

    /**
     * @method  Get_query_for_select
     * @desc    Create and return a select query
     * @access  protected
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return bool
     */
    protected function Get_query_for_select()
    {
        $query = $this->filters->Get_select_query( "SELECT" );
        $query .= $this->filters->Get_from_query( "SELECT" )." ";
        $query .= $this->filters->Get_join_query();
        $query .= $this->filters->Get_where_query();
        $query .= $this->filters->Get_like_query();
        $query .= $this->filters->Get_group_by_query();
        $query .= $this->filters->Get_having_query();
        $query .= $this->filters->Get_order_by_query();
        $query .= $this->filters->Get_limit_query();

        return $query;
    }

    /**
     * @method  Get_query_for_insert
     * @desc    Create and return an insert query and returns the inserted ID
     * @access  protected
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool|int
     */
    protected function Get_query_for_insert()
    {
        $query = "INSERT INTO ";
        $query .= $this->filters->Get_from_query( "INSERT" )." ";
        $query .= $this->filters->Get_set_query();

        return $query;
    }

    /**
     * @method  Get_query_for_insert_by_sql
     * @desc    Create and return an insert query and returns the inserted ID
     * @access  protected
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool|int
     */
    protected function Get_query_for_insert_by_sql()
    {
        $query = "INSERT INTO ";
        $query .= $this->filters->Get_from_query( "INSERT" )." ";
        $query .= "( ".$this->filters->Get_set_query_by_sql()." ) ";

        return $query;
    }

    /**
     * @method  Get_query_for_update
     * @desc    Create and return an update query
     * @access  protected
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    protected function Get_query_for_update()
    {
        $query = "UPDATE ";
        $query .= $this->filters->Get_from_query( "UPDATE" )." ";
        $query .= $this->filters->Get_set_query();
        $query .= $this->filters->Get_where_query();
        $query .= $this->filters->Get_like_query();
        $query .= $this->filters->Get_limit_query();

        return $query;
    }

    /**
     * @method  Get_query_for_delete
     * @desc    Create and return a delete query
     * @access  protected
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    protected function Get_query_for_delete()
    {
        $query = "DELETE ";
        $query .= $this->filters->Get_from_query( "DELETE" );
        $query .= $this->filters->Get_where_query();
        $query .= $this->filters->Get_like_query();
        $query .= $this->filters->Get_limit_query();

        return $query;
    }

    /**
     * @method  Get_query_for_count
     * @desc    Create and return a count query and return the number of rows
     * @access  protected
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    protected function Get_query_for_count()
    {
        $query = $this->filters->Get_select_query( "COUNT" );
        $query .= $this->filters->Get_from_query( "COUNT" )." ";
        $query .= $this->filters->Get_join_query();
        $query .= $this->filters->Get_where_query();
        $query .= $this->filters->Get_like_query();
        $query .= $this->filters->Get_group_by_query();
        $query .= $this->filters->Get_having_query();
        $query .= $this->filters->Get_order_by_query();
        $query .= $this->filters->Get_limit_query();

        return $query;
    }
    //</editor-fold>

    /******************************************
     * CACHE MODIFIERS
     ******************************************/
    //<editor-fold desc="Cache modifiers">
    /**
     * @method  Turn_cache_on
     * @desc    Turn cache on if it is set
     * @access  public
     * @author  Cousin Blea
     *
     * @version 1.0
     */
    public function Turn_cache_on()
    {
        if( empty( $this->cache ) )
        {
            return FALSE;
        }

        $this->is_cache_on = TRUE;
        $this->cache->Turn_on();

        return TRUE;
    }

    /**
     * @method  Turn_cache_off
     * @desc    Turn cache off if it is set
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    public function Turn_cache_off()
    {
        if( empty( $this->cache ) )
        {
            return FALSE;
        }

        $this->is_cache_on = FALSE;
        $this->cache->Turn_off();

        return TRUE;
    }
    //</editor-fold>
}

/* End of file MDatabase.php */
/* Location: ./core/Libraries/Database/ */