<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MDB_Filter
 *
 * @property    DB_Mysql    $db
 *
 * @version     1.0.0
 */
class MDB_Filter implements IMDB_Filter
{
    private $select_arr     = array();
    private $from_arr       = array();
    private $join_arr       = array();
    private $set_arr        = array();
    private $where_arr      = array();
    private $or_where_arr   = array();
    private $where_in_arr   = array();
    private $or_where_in_arr= array();
    private $like_arr       = array();
    private $or_like_arr    = array();
    private $not_like_arr   = array();
    private $or_not_like_arr= array();
    private $null_arr       = array();
    private $or_null_arr    = array();
    private $not_null_arr   = array();
    private $or_not_null_arr= array();
    private $group_by_arr   = array();
    private $having_arr     = array();
    private $order_by_arr   = array();
    private $index          = "";
    private $alias          = "";
    private $distinct       = FALSE;

    private $limit          = "";
    private $limit_from     = 0;

    private $like_types_array= array( "BOTH", "LEFT", "RIGHT" );

    private $db                     = null;
    private $str_to_protect_column  = "`";
    private $str_to_protect_value   = "'";

    public function __construct( & $db = null )
    {
        $this->db = $db;
        if( empty( $db ) )
        {
            $this->db =& App()->db;
        }

        $this->str_to_protect_column= $this->db->str_to_protect_column;
        $this->str_to_protect_value = $this->db->str_to_protect_value;
    }

    /******************************************
     * SELECT
     ******************************************/
    //<editor-fold desc="Selectors">
    /**
     * @method  Select
     * @access  public
     * @desc    Select one or more columns
     * @author  Cousin Béla
     *
     * @param   array                   $column                     - column name to select or an array with columns
     * @param   string                  $column_alias               - alias for selected column
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    public function Select( $column, $column_alias = null, $protect = TRUE )
    {
        if( $protect )
        {
            $column = $this->Protect( $column );
        }
/*
        if( $this->distinct )
        {
            $column         = "DISTINCT ".$column;
            $this->distinct = FALSE;
        }
*/
        if( ! empty( $column_alias ) )
        {
            if( $protect )
            {
                $column_alias = $this->Protect( $column_alias, "VALUE" );
            }
            $column .= ' AS '.$column_alias;
        }

        $this->select_arr []= $column;
    }

    /**
     * @method  Select_all_columns
     * @access  public
     * @desc    Select all columns
     * @author  Cousin Béla
     *
     * @version 1.0.0
     */
    public function Select_all_columns()
    {
        $this->select_arr []= "*";
    }

    /**
     * @method  Select_column
     * @access  public
     * @desc    Select one column
     * @author  Cousin Béla
     *
     * @param   array                   $column                     - column name to select
     * @param   string                  $column_alias               - alias for selected column
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    public function Select_column( $column, $column_alias = "", $protect = TRUE )
    {
        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        if( ! empty( $column_alias ) )
        {
            if( $protect )
            {
                $column_alias = $this->Protect( $column_alias, "VALUE" );
            }
            $column .= ' AS '.$column_alias;
        }

        $this->select_arr []= $column;
    }

    /**
     * @method  Select_min
     * @access  public
     * @desc    Select minimum of column
     * @author  Cousin Béla
     *
     * @param   array                   $column                     - column name to select
     * @param   string                  $column_alias               - alias for selected column
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    public function Select_min( $column, $column_alias = "", $protect = TRUE )
    {
        if( $protect )
        {
            $column = $this->Protect( $column );

            if( ! empty( $column_alias ) )
            {
                $column_alias = $this->Protect( $column_alias, "VALUE" );
            }
        }

        $this->select_arr []= "MIN(".$column.")".( ! empty( $column_alias ) ? " AS ".$column_alias : "" );
    }

    /**
     * @method  Select_max
     * @access  public
     * @desc    Select maximum of column
     * @author  Cousin Béla
     *
     * @param   array                   $column                     - column name to select
     * @param   string                  $column_alias               - alias for selected column
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    public function Select_max( $column, $column_alias = "", $protect = TRUE )
    {
        if( $protect )
        {
            $column = $this->Protect( $column );

            if( ! empty( $column_alias ) )
            {
                $column_alias = $this->Protect( $column_alias, "VALUE" );
            }
        }

        $this->select_arr []= "MAX(".$column.")".( ! empty( $column_alias ) ? " AS ".$column_alias : "" );
    }

    /**
     * @method  Select_count
     * @access  public
     * @desc    Select count of column
     * @author  Cousin Béla
     *
     * @param   array                   $column                     - column name to select
     * @param   string                  $column_alias               - alias for selected column
     * @param   bool                    $distinct                   - count distinct or all
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    public function Select_count( $column, $column_alias = "", $distinct = FALSE, $protect = TRUE )
    {
        if( $protect )
        {
            $column = $this->Protect( $column );

            if( ! empty( $column_alias ) )
            {
                $column_alias = $this->Protect( $column_alias, "VALUE" );
            }
        }

        $this->select_arr []= "COUNT(".( $distinct ? "DISTINCT " : "" ).$column.")"
                                .( ! empty( $column_alias ) ? " AS ".$column_alias : "" );
    }

    /**
     * @method  Select_sum
     * @access  public
     * @desc    Select sum of column
     * @author  Cousin Béla
     *
     * @param   array                   $column                     - column name to select
     * @param   string                  $column_alias               - alias for selected column
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    public function Select_sum( $column, $column_alias = "", $protect = TRUE )
    {
        if( $protect )
        {
            $column = $this->Protect( $column );

            if( ! empty( $column_alias ) )
            {
                $column_alias = $this->Protect( $column_alias, "VALUE" );
            }
        }

        $this->select_arr []= "SUM(".$column.")".( ! empty( $column_alias ) ? " AS ".$column_alias : "" );
    }

    /**
     * @method  Distinct
     * @access  public
     * @desc    Set the distinct value to TRUE or FALSE
     * @author  Cousin Béla
     *
     * @version 1.0.0
     */
    public function Distinct( $value )
    {
        $this->distinct = $value;
    }
    //</editor-fold>

    /******************************************
     * SET
     ******************************************/
    //<editor-fold desc="Setters">
    /**
     * @method  Set
     * @access  public
     * @desc    Set value for column
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    public function Set( $column, $value = "", $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_from_array( $column, $protect );
        }

        $this->Set_from_string( $column, $value, $protect );
    }

    /**
     * @method  Set_from_array
     * @access  public
     * @desc    Set value for column
     * @author  Cousin Béla
     *
     * @param   mixed                   $columns                    - column name to select
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    private function Set_from_array( array $columns, $protect = TRUE )
    {
        foreach( $columns as $column => $value )
        {
            $this->Set_from_string( $column, $value, $protect );
        }
    }

    /**
     * @method  Set_from_string
     * @access  public
     * @desc    Set value for column
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    private function Set_from_string( $column, $value, $protect = TRUE )
    {
        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        if( isset( $value ) )
        {
            $value  = $this->db->Escape( $value );

            if( $protect )
            {
                $value = $this->Protect( $value, "VALUE" );
            }
        }

        $this->set_arr []= $column." = ".$value;
    }

    /**
     * @method  Set_for_sql
     * @access  public
     * @desc    Set column names for insert by sql
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    public function Set_for_sql( $column, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_from_array_for_sql( $column, $protect );
        }

        $this->Set_from_string_for_sql( $column, $protect );
    }

    /**
     * @method  Set_from_array_for_sql
     * @access  public
     * @desc    Set column names for insert by sql
     * @author  Cousin Béla
     *
     * @param   mixed                   $columns                    - column name to select
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    private function Set_from_array_for_sql( array $columns, $protect = TRUE )
    {
        foreach( $columns as $column )
        {
            $this->Set_from_string_for_sql( $column, $protect );
        }
    }

    /**
     * @method  Set_from_string_for_sql
     * @access  public
     * @desc    Set value for column
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     */
    private function Set_from_string_for_sql( $column, $protect = TRUE )
    {
        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        $this->set_arr []= $column;
    }
    //</editor-fold>

    /******************************************
     * FROM
     ******************************************/
    //<editor-fold desc="From">
    /**
     * @method  From
     * @access  public
     * @desc    Set From for sql query
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table from
     * @param   string                  $table_alias                - alias for selected table
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the table names or not
     *
     * @version 1.0.0
     */
    public function From( $table_name, $table_alias = "", $forced_index = null, $protect = TRUE )
    {
        $table_name = $this->db->Check_table_name( $table_name );

        // If table alias is not set here, but it was set in main alias, set it for this and reset
        if( empty( $table_alias ) && ! empty( $this->alias ) )
        {
            $table_alias = $this->alias;
            $this->Reset_alias();
        }

        if( $protect )
        {
            $table_name     = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias= $this->Protect( $table_alias );
            }
        }

        $this->from_arr []= $table_name.( ! empty( $table_alias ) ? " AS ".$table_alias : "" ).
                                ( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" );
    }

    /**
     * @method  Alias
     * @access  public
     * @desc    Set the alias for table to select from
     * @author  Cousin Béla
     *
     * @version 1.0.0
     */
    public function Alias( $alias )
    {
        $this->alias = $alias;
    }
    //</editor-fold>
    /******************************************
     * JOIN
     ******************************************/
    //<editor-fold desc="Joins">
    /**
     * @method  Join
     * @access  public
     * @desc    Set the join parameter for your query
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - column name to select
     * @param   string                  $condition                  - condition(s) for join
     * @param   string                  $table_alias                - alias for table in join
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Join( $table_name, $condition = "", $table_alias = "", $forced_index = null, $protect = TRUE )
    {
        if( empty( $table_name ) || empty( $condition ) )
        {
            throw new MDatabase_exception( "Please check your join parameters correctly" );
        }

        $table_name = $this->db->Get_table_name( $table_name );

        if( $protect )
        {
            $table_name = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias = $this->Protect( $table_alias );
            }
        }

        $this->join_arr []= "JOIN ".$table_name
                            .( ! empty( $table_alias ) ? " AS ".$table_alias : "" )
                            .( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" )
                            ." ON ".$condition;
    }

    /**
     * @method  Join_from_other_db
     * @access  public
     * @desc    Set the join parameter for your query from other database
     * @author  Cousin Béla
     *
     * @param   string                  $database                   - other database name to get from
     * @param   string                  $table_name                 - column name to select
     * @param   string                  $condition                  - condition(s) for join
     * @param   string                  $table_alias                - alias for table in join
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Join_from_other_db( $database, $table_name, $condition = "", $table_alias = "", $forced_index = null, $protect = TRUE )
    {
        if( empty( $database ) || empty( $table_name ) || empty( $condition ) )
        {
            throw new MDatabase_exception( "Please check your join parameters correctly" );
        }

        $table_name = $this->db->Get_table_name( $table_name );

        if( $protect )
        {
            $database   = $this->Protect( $database );
            $table_name = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias = $this->Protect( $table_alias );
            }
        }

        $this->join_arr []= "JOIN ".$database.".".$table_name
                            .( ! empty( $table_alias ) ? " AS ".$table_alias : "" )
                            .( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" )
                            ." ON ".$condition;
    }

    /**
     * @method  Join_left
     * @access  public
     * @desc    Set the join parameter for your query as left join
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - column name to select
     * @param   string                  $condition                  - condition(s) for join
     * @param   string                  $table_alias                - alias for table in join
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Join_left( $table_name, $condition = "", $table_alias = "", $forced_index = null, $protect = TRUE )
    {
        if( empty( $table_name ) || empty( $condition ) )
        {
            throw new MDatabase_exception( "Please check your join parameters correctly" );
        }

        $table_name = $this->db->Get_table_name( $table_name );

        if( $protect )
        {
            $table_name = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias = $this->Protect( $table_alias );
            }
        }

        $this->join_arr []= "LEFT JOIN ".$table_name
                            .( ! empty( $table_alias ) ? " AS ".$table_alias : "" )
                            .( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" )
                            ." ON ".$condition;
    }

    /**
     * @method  Join_left_from_other_db
     * @access  public
     * @desc    Set the join parameter for your query as left join from other database
     * @author  Cousin Béla
     *
     * @param   string                  $database                   - other database name to get from
     * @param   string                  $table_name                 - column name to select
     * @param   string                  $condition                  - condition(s) for join
     * @param   string                  $table_alias                - alias for table in join
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Join_left_from_other_db( $database, $table_name, $condition = "", $table_alias = "", $forced_index = null, $protect = TRUE )
    {
        if( empty( $database ) || empty( $table_name ) || empty( $condition ) )
        {
            throw new MDatabase_exception( "Please check your join parameters correctly" );
        }

        $table_name = $this->db->Get_table_name( $table_name );

        if( $protect )
        {
            $database   = $this->Protect( $database );
            $table_name = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias = $this->Protect( $table_alias );
            }
        }

        $this->join_arr []= "LEFT JOIN ".$database.".".$table_name
                            .( ! empty( $table_alias ) ? " AS ".$table_alias : "" )
                            .( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" )
                            ." ON ".$condition;
    }

    /**
     * @method  Join_right
     * @access  public
     * @desc    Set the join parameter for your query as right join
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - column name to select
     * @param   string                  $condition                  - condition(s) for join
     * @param   string                  $table_alias                - alis for table in join
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Join_right( $table_name, $condition = "", $table_alias = "", $forced_index = null,  $protect = TRUE )
    {
        if( empty( $table_name ) || empty( $condition ) )
        {
            throw new MDatabase_exception( "Please check your join parameters correctly" );
        }

        $table_name = $this->db->Get_table_name( $table_name );

        if( $protect )
        {
            $table_name = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias = $this->Protect( $table_alias );
            }
        }

        $this->join_arr []= "RIGHT JOIN ".$table_name
                            .( ! empty( $table_alias ) ? " AS ".$table_alias : "" )
                            .( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" )
                            ." ON ".$condition;
    }

    /**
     * @method  Join_right_from_other_db
     * @access  public
     * @desc    Set the join parameter for your query as right join from other database
     * @author  Cousin Béla
     *
     * @param   string                  $database                   - other database name to get from
     * @param   string                  $table_name                 - column name to select
     * @param   string                  $condition                  - condition(s) for join
     * @param   string                  $table_alias                - alis for table in join
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Join_right_from_other_db( $database, $table_name, $condition = "", $table_alias = "", $forced_index = null,  $protect = TRUE )
    {
        if( empty( $database ) || empty( $table_name ) || empty( $condition ) )
        {
            throw new MDatabase_exception( "Please check your join parameters correctly" );
        }

        $table_name = $this->db->Get_table_name( $table_name );

        if( $protect )
        {
            $database   = $this->Protect( $database );
            $table_name = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias = $this->Protect( $table_alias );
            }
        }

        $this->join_arr []= "RIGHT JOIN ".$database.".".$table_name
                            .( ! empty( $table_alias ) ? " AS ".$table_alias : "" )
                            .( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" )
                            ." ON ".$condition;
    }

    /**
     * @method  Join_inner
     * @access  public
     * @desc    Set the join parameter for your query as inner join
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - column name to select
     * @param   string                  $condition                  - condition(s) for join
     * @param   string                  $table_alias                - alis for table in join
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Join_inner( $table_name, $condition = "", $table_alias = "", $forced_index = null, $protect = TRUE )
    {
        if( empty( $table_name ) || empty( $condition ) )
        {
            throw new MDatabase_exception( "Please check your join parameters correctly" );
        }

        $table_name = $this->db->Get_table_name( $table_name );

        if( $protect )
        {
            $table_name = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias = $this->Protect( $table_alias );
            }
        }

        $this->join_arr []= "INNER JOIN ".$table_name
                            .( ! empty( $table_alias ) ? " AS ".$table_alias : "" )
                            .( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" )
                            ." ON ".$condition;
    }

    /**
     * @method  Join_inner_from_other_db
     * @access  public
     * @desc    Set the join parameter for your query as inner join from other database
     * @author  Cousin Béla
     *
     * @param   string                  $database                   - other database name to get from
     * @param   string                  $table_name                 - column name to select
     * @param   string                  $condition                  - condition(s) for join
     * @param   string                  $table_alias                - alis for table in join
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Join_inner_from_other_db( $database, $table_name, $condition = "", $table_alias = "", $forced_index = null, $protect = TRUE )
    {
        if( empty( $database ) || empty( $table_name ) || empty( $condition ) )
        {
            throw new MDatabase_exception( "Please check your join parameters correctly" );
        }

        $table_name = $this->db->Get_table_name( $table_name );

        if( $protect )
        {
            $database   = $this->Protect( $database );
            $table_name = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias = $this->Protect( $table_alias );
            }
        }

        $this->join_arr []= "INNER JOIN ".$database.".".$table_name
                            .( ! empty( $table_alias ) ? " AS ".$table_alias : "" )
                            .( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" )
                            ." ON ".$condition;
    }

    /**
     * @method  Join_outer
     * @access  public
     * @desc    Set the join parameter for your query as outer join
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - column name to select
     * @param   string                  $condition                  - condition(s) for join
     * @param   string                  $table_alias                - alis for table in join
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Join_outer( $table_name, $condition = "", $table_alias = "", $forced_index = null, $protect = TRUE )
    {
        if( empty( $table_name ) or empty( $condition ) )
        {
            throw new MDatabase_exception( "Please check your join parameters correctly" );
        }

        $table_name = $this->db->Get_table_name( $table_name );

        if( $protect )
        {
            $table_name = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias = $this->Protect( $table_alias );
            }
        }

        $this->join_arr []= "OUTER JOIN ".$table_name
                            .( ! empty( $table_alias ) ? " AS ".$table_alias : "" )
                            .( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" )
                            ." ON ".$condition;
    }

    /**
     * @method  Join_outer_from_other_db
     * @access  public
     * @desc    Set the join parameter for your query as outer join from other database
     * @author  Cousin Béla
     *
     * @param   string                  $database                   - other database name to get from
     * @param   string                  $table_name                 - column name to select
     * @param   string                  $condition                  - condition(s) for join
     * @param   string                  $table_alias                - alis for table in join
     * @param   string                  $forced_index               - an index to used as forced
     * @param   bool                    $protect                    - protect the column names or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Join_outer_from_other_db( $database, $table_name, $condition = "", $table_alias = "", $forced_index = null, $protect = TRUE )
    {
        if( empty( $database ) || empty( $table_name ) || empty( $condition ) )
        {
            throw new MDatabase_exception( "Please check your join parameters correctly" );
        }

        $table_name = $this->db->Get_table_name( $table_name );

        if( $protect )
        {
            $database   = $this->Protect( $database );
            $table_name = $this->Protect( $table_name );

            if( ! empty( $table_alias ) )
            {
                $table_alias = $this->Protect( $table_alias );
            }
        }

        $this->join_arr []= "OUTER JOIN ".$database.".".$table_name
                            .( ! empty( $table_alias ) ? " AS ".$table_alias : "" )
                            .( ! empty( $forced_index ) ? " USE INDEX (".$forced_index.") " : "" )
                            ." ON ".$condition;
    }
    //</editor-fold>

    /******************************************
     * WHERE
     ******************************************/
    //<editor-fold desc="Wheres">
    /**
     * @method  Where
     * @access  public
     * @desc    Add new filter to query
     * @author  Cousin Béla
     *
     * @param   mixed                   $where                      - filter string or array with filters
     *
     * @version 1.0.0
     */
    public function Where( $where )
    {
        if( is_array( $where ) )
        {
            return $this->Add_where_array_into_filters( $where );
        }

        $this->where_arr []= "( ".$where." )";
    }

    /**
     * @method  Add_where_array_into_filters
     * @access  private
     * @desc    Add filters from array into filters
     * @author  Cousin Béla
     *
     * @param   mixed                   $where                      - array with filters
     *
     * @version 1.0.0
     */
    private function Add_where_array_into_filters( $where )
    {
        if( ! empty( $where ) )
        {
            foreach( $where as $filter )
            {
                $this->Where( $filter );
            }
        }
    }

    /**
     * @method  Where_equal
     * @access  public
     * @desc    Set where a column is equal a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Where_equal( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_where_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->where_arr []= $column." = ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->where_arr []= $column;
    }

    /**
     * @method  Set_where_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_where_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Where_equal( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Where_not_equal
     * @access  public
     * @desc    Set where a column not equal a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Where_not_equal( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_where_not_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->where_arr []= $column." != ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->where_arr []= $column;
    }

    /**
     * @method  Set_where_not_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_where_not_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Where_not_equal( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Where_is_greater_than
     * @access  public
     * @desc    Set where a column is greater than a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Where_is_greater_than( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_where_greater_than_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->where_arr []= $column." > ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->where_arr []= $column;
    }

    /**
     * @method  Set_where_greater_than_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_where_greater_than_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Where_is_greater_than( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Where_is_greater_or_equal_than
     * @access  public
     * @desc    Set where a column is greater or equal than a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Where_is_greater_or_equal_than( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_where_greater_or_equal_than_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->where_arr []= $column." >= ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->where_arr []= $column;
    }

    /**
     * @method  Set_where_greater_or_equal_than_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_where_greater_or_equal_than_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Where_is_greater_or_equal_than( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Where_is_lesser_than
     * @access  public
     * @desc    Set where a column is lesser than a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Where_is_lesser_than( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_where_lesser_than_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->where_arr []= $column." < ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->where_arr []= $column;
    }

    /**
     * @method  Set_where_lesser_than_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   mixed                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_where_lesser_than_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Where_is_lesser_than( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Where_is_lesser_or_equal_than
     * @access  public
     * @desc    Set where a column is lesser or equal than a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Where_is_lesser_or_equal_than( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_where_lesser_or_equal_than_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->where_arr []= $column." <= ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->where_arr []= $column;
    }

    /**
     * @method  Set_where_lesser_or_equal_than_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_where_lesser_or_equal_than_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Where_is_lesser_or_equal_than( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Where_in
     * @access  public
     * @desc    Set where a column is in values array
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   array                   $values                     - values where the column is in
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Where_in( $column, $values = null, $protect = TRUE )
    {
        if( empty( $values ) )
        {
            throw new MDatabase_exception( "Invalid values for 'where in' parameter in your query" );
        }

        if( ! is_array( $values ) && is_string( $values ) )
        {
            if( $protect )
            {
                $values = $this->Protect( $values, "VALUE" );
            }

            $this->where_in_arr []= $column." IN ( ".$values." )";

            return;
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $values ) )
            {
                foreach( $values as $key => $value )
                {
                    $value = $this->Protect( $value, "VALUE" );
                }
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $values ) )
        {
            $this->where_in_arr []= $column." IN ( ".$this->str_to_protect_value.implode(
                $this->str_to_protect_value.",".$this->str_to_protect_value,
                $values
            ).$this->str_to_protect_value." )";

            return;
        }

        // value is empty, that means the column is a where string
        $this->where_in_arr []= $column;
    }

    /**
     * @method  Where_not_in
     * @access  public
     * @desc    Set where a column is not in values array
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   array                   $values                     - values where the column is in
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Where_not_in( $column, array $values = null, $protect = TRUE )
    {
        if( empty( $values ) || ! is_array( $values ) )
        {
            throw new MDatabase_exception( "Invalid values for 'where not in' parameter in your query" );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $values ) )
            {
                foreach( $values as $key => $value )
                {
                    $value = $this->Protect( $value, "VALUE" );
                }
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $values ) )
        {
            $this->where_in_arr []= $column." NOT IN ( ".$this->str_to_protect_value.implode(
                $this->str_to_protect_value.",".$this->str_to_protect_value,
                $values
            ).$this->str_to_protect_value." )";

            return;
        }

        // value is empty, that means the column is a where string
        $this->where_in_arr []= $column;
    }
    //</editor-fold>

    /******************************************
     * Or where
     ******************************************/
    //<editor-fold desc="Or Wheres">
    /**
     * @method  Or_where
     * @access  public
     * @desc    Add new filter to query
     * @author  Cousin Béla
     *
     * @param   mixed                   $where                      - filter string or array with filters
     *
     * @version 1.0.0
     */
    public function Or_where( $where )
    {
        if( is_array( $where ) )
        {
            return $this->Add_or_where_array_into_filters( $where );
        }

        $this->or_where_arr []= $where;
    }

    /**
     * @method  Add_or_where_array_into_filters
     * @access  private
     * @desc    Add filters from array into filters
     * @author  Cousin Béla
     *
     * @param   mixed                   $where                      - array with filters
     *
     * @version 1.0.0
     */
    private function Add_or_where_array_into_filters( $where )
    {
        if( ! empty( $where ) )
        {
            foreach( $where as $filter )
            {
                $this->Or_where( $filter );
            }
        }
    }
    /**
     * @method  Or_where_equal
     * @access  public
     * @desc    Set OR where a column is equal a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Or_where_equal( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_or_where_equal_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->or_where_arr []= $column." = ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->or_where_arr []= $column;
    }

    /**
     * @method  Set_or_where_equal_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_or_where_equal_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Or_where_equal( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Or_where_not_equal
     * @access  public
     * @desc    Set OR where a column not equal a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Or_where_not_equal( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_or_where_not_equal_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->or_where_arr []= $column." != ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->or_where_arr []= $column;
    }

    /**
     * @method  Set_or_where_not_equal_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_or_where_not_equal_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Or_where_not_equal( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Or_where_is_greater_than
     * @access  public
     * @desc    Set OR where a column is greater than a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Or_where_is_greater_than( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_or_where_greater_than_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->or_where_arr []= $column." > ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->or_where_arr []= $column;
    }

    /**
     * @method  Set_or_where_greater_than_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_or_where_greater_than_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Or_where_is_greater_than( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Or_where_is_greater_or_equal_than
     * @access  public
     * @desc    Set OR where a column is greater or equal than a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Or_where_is_greater_or_equal_than( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_or_where_greater_or_equal_than_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->or_where_arr []= $column." >= ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->or_where_arr []= $column;
    }

    /**
     * @method  Set_or_where_greater_or_equal_than_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_or_where_greater_or_equal_than_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Or_where_is_greater_or_equal_than( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Or_where_is_lesser_than
     * @access  public
     * @desc    Set OR where a column is lesser than a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Or_where_is_lesser_than( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_or_where_lesser_than_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->or_where_arr []= $column." < ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->or_where_arr []= $column;
    }

    /**
     * @method  Set_or_where_lesser_than_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_or_where_lesser_than_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Or_where_is_lesser_than( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Or_where_is_lesser_or_equal_than
     * @access  public
     * @desc    Set OR where a column is lesser or equal than a value
     * @author  Cousin Béla
     *
     * @param   mixed                   $column                     - column name to select
     * @param   string                  $value                      - value of the column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    public function Or_where_is_lesser_or_equal_than( $column, $value = null, $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_or_where_lesser_or_equal_than_array_into_filters( $column, $protect );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $value ) )
            {
                $value  = $this->Protect( $value, "VALUE" );
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $this->or_where_arr []= $column." <= ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->or_where_arr []= $column;
    }

    /**
     * @method  Set_or_where_lesser_or_equal_than_array_into_filters
     * @access  private
     * @desc    Sets the where array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_or_where_lesser_or_equal_than_array_into_filters( array $columns, $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Or_where_is_lesser_or_equal_than( $column, $value, $protect );
            }
        }
    }

    /**
     * @method  Or_where_in
     * @access  public
     * @desc    Set OR where a column is in values array
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   array                   $values                     - values where the column is in
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Or_where_in( $column, array $values = null, $protect = TRUE )
    {
        if( empty( $values ) || ! is_array( $values ) )
        {
            throw new MDatabase_exception( "Invalid values for 'or where in' parameter in your query" );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $values ) )
            {
                foreach( $values as $key => $value )
                {
                    $value = $this->Protect( $value, "VALUE" );
                }
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $values ) )
        {
            $this->or_where_arr []= $column." IN ( ".$this->str_to_protect_value.implode(
                $this->str_to_protect_value.",".$this->str_to_protect_value,
                $values
            ).$this->str_to_protect_value." )";

            return;
        }

        // value is empty, that means the column is a where string
        $this->or_where_arr []= $column;
    }

    /**
     * @method  Or_where_not_in
     * @access  public
     * @desc    Set OR where a column is not in values array
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   array                   $values                     - values where the column is in
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Or_where_not_in( $column, array $values = null, $protect = TRUE )
    {
        if( empty( $values ) || ! is_array( $values ) )
        {
            throw new MDatabase_exception( "Invalid values for 'or where not in' parameter in your query" );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );

            if( isset( $values ) )
            {
                foreach( $values as $key => $value )
                {
                    $value = $this->Protect( $value, "VALUE" );
                }
            }
        }

        // If not empty the value, that means column equal value
        if( isset( $values ) )
        {
            $this->or_where_arr []= $column." NOT IN ( ".$this->str_to_protect_value.implode(
                $this->str_to_protect_value.",".$this->str_to_protect_value,
                $values
            ).$this->str_to_protect_value." )";

            return;
        }

        // value is empty, that means the column is a where string
        $this->or_where_arr []= $column;
    }
    //</editor-fold>

    /******************************************
     * LIKE
     ******************************************/
    //<editor-fold desc="Likes">
    /**
     * @method  Like
     * @access  public
     * @desc    Set where a column like a value
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   string                  $value                      - values where the column like
     * @param   string                  $type                       - type for this like, BOTH( %...% ),
     *                                                                  LEFT ( %... ), RIGHT ( ...% )
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Like( $column, $value = null, $type = "BOTH", $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_like_array_into_filters( $column, $type, $protect );
        }

        if( ! isset( $value ) || empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column/value for 'like' parameter in your query" );
        }

        $type = $this->Get_like_type( $type );

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $value = ( $type == "BOTH" || $type == "LEFT" ? "%" : "" )
                .$value
                .( $type == "BOTH" || $type == "RIGHT" ? "%" : "" );

            if( $protect )
            {
                if( isset( $value ) )
                {
                    $value = $this->Protect( $value, "VALUE" );
                }
            }

            $this->like_arr []= $column." LIKE ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->like_arr []= $column;
    }

    /**
     * @method  Set_like_array_into_filters
     * @access  private
     * @desc    Sets the like array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $type                       - type for this like, BOTH( %...% ),
     *                                                                  LEFT ( %... ), RIGHT ( ...% )
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_like_array_into_filters( array $columns, $type = "BOTH", $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Like( $column, $value, $type, $protect );
            }
        }
    }

    /**
     * @method  Get_like_type
     * @access  public
     * @desc    Set the type of like, says where to add '%' character, left, right or both
     * @author  Cousin Béla
     *
     * @param   string                  $type                       - type for this like, BOTH( %...% ),
     *                                                                  LEFT ( %... ), RIGHT ( ...% )
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     * @return  string
     */
    private function Get_like_type( $type = "BOTH" )
    {
        $type = strtoupper( $type );

        if( ! in_array( $type, $this->like_types_array ) )
        {
            throw new MDatabase_exception(
                "Invalid type for like, must be one of ".implode( ",", $this->like_types_array )
            );
        }

        if( empty( $type ) )
        {
            $type = "BOTH";
        }

        return $type;
    }

    /**
     * @method  Or_like
     * @access  public
     * @desc    Set where a column like or like a value
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   string                  $value                      - values where the column like
     * @param   string                  $type                       - type for this like, BOTH( %...% ),
     *                                                                  LEFT ( %... ), RIGHT ( ...% )
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Or_like( $column, $value = null, $type = "BOTH", $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_or_like_array_into_filters( $column, $type, $protect );
        }

        if( ! isset( $value ) || empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column/value for 'or like' parameter in your query" );
        }

        $type = $this->Get_like_type( $type );

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $value = ( $type == "BOTH" || $type == "LEFT" ? "%" : "" )
                .$value
                .( $type == "BOTH" || $type == "RIGHT" ? "%" : "" );

            if( $protect )
            {
                if( isset( $value ) )
                {
                    $value = $this->Protect( $value, "VALUE" );
                }
            }

            $this->or_like_arr []= $column." LIKE ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->or_like_arr []= $column;
    }

    /**
     * @method  Set_or_like_array_into_filters
     * @access  private
     * @desc    Sets the or like array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $type                       - type for this like, BOTH( %...% ),
     *                                                                  LEFT ( %... ), RIGHT ( ...% )
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_or_like_array_into_filters( array $columns, $type = "BOTH", $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Or_like( $column, $value, $type, $protect );
            }
        }
    }

    /**
     * @method  Not_like
     * @access  public
     * @desc    Set where a column like not a value
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   string                  $value                      - values where the column like
     * @param   string                  $type                       - type for this like, BOTH( %...% ),
     *                                                                  LEFT ( %... ), RIGHT ( ...% )
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Not_like( $column, $value = null, $type = "BOTH", $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            return $this->Set_not_like_array_into_filters( $column, $type, $protect );
        }

        if( ! isset( $value ) || empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column/value for 'not like' parameter in your query" );
        }

        $type = $this->Get_like_type( $type );

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $value = ( $type == "BOTH" || $type == "LEFT" ? "%" : "" )
                .$value
                .( $type == "BOTH" || $type == "RIGHT" ? "%" : "" );

            if( $protect )
            {
                if( isset( $value ) )
                {
                    $value = $this->Protect( $value, "VALUE" );
                }
            }

            $this->not_like_arr []= $column." NOT LIKE ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->not_like_arr []= $column;
    }

    /**
     * @method  Set_not_like_array_into_filters
     * @access  private
     * @desc    Sets the not like array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $type                       - type for this like, BOTH( %...% ),
     *                                                                  LEFT ( %... ), RIGHT ( ...% )
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_not_like_array_into_filters( array $columns, $type = "BOTH", $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Not_like( $column, $value, $type, $protect );
            }
        }
    }

    /**
     * @method  Or_not_like
     * @access  public
     * @desc    Set where a column like not or like not a value
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   string                  $value                      - values where the column like
     * @param   string                  $type                       - type for this like, BOTH( %...% ),
     *                                                                  LEFT ( %... ), RIGHT ( ...% )
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Or_not_like( $column, $value = null, $type = "BOTH", $protect = TRUE )
    {
        if( is_array( $column ) )
        {
            $this->Set_or_not_like_array_into_filters( $column, $type, $protect );
        }

        if( ! isset( $value ) || empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column/value for 'not like' parameter in your query" );
        }

        $type = $this->Get_like_type( $type );

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        // If not empty the value, that means column equal value
        if( isset( $value ) )
        {
            $value = ( $type == "BOTH" || $type == "LEFT" ? "%" : "" )
                .$value
                .( $type == "BOTH" || $type == "RIGHT" ? "%" : "" );

            if( $protect )
            {
                if( isset( $value ) )
                {
                    $value = $this->Protect( $value, "VALUE" );
                }
            }

            $this->or_not_like_arr []= $column." NOT LIKE ".$value;

            return;
        }

        // value is empty, that means the column is a where string
        $this->or_not_like_arr []= $column;
    }

    /**
     * @method  Set_or_not_like_array_into_filters
     * @access  private
     * @desc    Sets the or not like array into filters
     * @author  Cousin Béla
     *
     * @param   array                   $columns                    - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $type                       - type for this like, BOTH( %...% ),
     *                                                                  LEFT ( %... ), RIGHT ( ...% )
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     */
    private function Set_or_not_like_array_into_filters( array $columns, $type = "BOTH", $protect = TRUE )
    {
        if( ! empty( $columns ) )
        {
            foreach( $columns as $column => $value )
            {
                $this->Or_not_like( $column, $value, $type, $protect );
            }
        }
    }
    //</editor-fold>

    /******************************************
     * NULL
     ******************************************/
    //<editor-fold desc="Nulls">
    /**
     * @method  Is_null
     * @access  public
     * @desc    Set where a column is null
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Is_null( $column, $protect = TRUE )
    {
        if( empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column for 'is null' parameter in your query" );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        $this->null_arr []= $column." IS NULL";
    }

    /**
     * @method  Or_is_null
     * @access  public
     * @desc    Set where a column is null or column is null ...
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Or_is_null( $column, $protect = TRUE )
    {
        if( empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column for 'or is null' parameter in your query" );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        $this->or_null_arr []= $column." IS NULL";
    }

    /**
     * @method  Is_not_null
     * @access  public
     * @desc    Set where a column is not null
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Is_not_null( $column, $protect = TRUE )
    {
        if( empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column for 'not null' parameter in your query" );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        $this->not_null_arr []= $column." IS NOT NULL";
    }

    /**
     * @method  Is_not_null
     * @access  public
     * @desc    Set where a column is not null or column is not null ...
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Or_is_not_null( $column, $protect = TRUE )
    {
        if( empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column for 'or not null' parameter in your query" );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        $this->or_not_null_arr []= $column." IS NOT NULL";
    }
    //</editor-fold>

    /******************************************
     * GROUP
     ******************************************/
    /**
     * @method  Group_by
     * @access  public
     * @desc    Set group by parameter
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Group_by( $column, $protect = TRUE )
    {
        if( empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column for 'group by' parameter in your query" );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        $this->group_by_arr []= $column;
    }

    /******************************************
     * HAVING
     ******************************************/
    /**
     * @method  Having
     * @access  public
     * @desc    Set having parameter
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   string                  $condition                  - condition for column
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Having( $column, $condition = "", $protect = TRUE )
    {
        if( empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column for 'having' parameter in your query" );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        $this->having_arr []= $column." ".$condition;
    }

    /******************************************
     * ORDER BY
     ******************************************/
    /**
     * @method  Order_by
     * @access  public
     * @desc    Set order by parameter
     * @author  Cousin Béla
     *
     * @param   string                  $column                     - column name to select
     * @param   string                  $direction                  - direction for order, can be ASC or DESC
     * @param   bool                    $protect                    - protect variables or not
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Order_by( $column, $direction = "ASC", $protect = TRUE )
    {
        if( empty( $column ) )
        {
            throw new MDatabase_exception( "Invalid column for 'order by' parameter in your query" );
        }

        $direction = strtoupper( $direction );
        if( ! in_array( $direction, array( "", "ASC", "DESC" ) ) )
        {
            throw new MDatabase_exception( "Invalid direction '".$direction."' for 'order by' parameter in your query" );
        }

        if( $protect )
        {
            $column = $this->Protect( $column );
        }

        $this->order_by_arr []= $column." ".( ! empty( $direction ) ? $direction : "ASC" );
    }

    /**
     * @method  Order_by_random
     * @access  public
     * @desc    Set order by parameter to random
     * @author  Cousin Béla
     *
     * @version 1.0.0
     */
    public function Order_by_random()
    {
        $this->order_by_arr []= "rand()";
    }

    /******************************************
     * LIMIT
     ******************************************/
    /**
     * @method  Limit
     * @access  public
     * @desc    Set limit parameter
     * @author  Cousin Béla
     *
     * @param   int                     $limit                      - how many items will be returned
     * @param   int                     $limit_from                 - offset to return from
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Limit( $limit = 0, $limit_from = "" )
    {
        if( empty( $limit ) )
        {
            return;
        }

        $this->limit        = $limit;
        $this->limit_from   = $limit_from;
    }

    /******************************************
     * USE INDEX
     ******************************************/
    /**
     * @method  Limit
     * @access  public
     * @desc    Set the index name to use in query
     * @author  Cousin Béla
     *
     * @param   string                  $index                      - the name of index to use in query
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     */
    public function Use_index( $index )
    {
        $this->index = $index;
    }

    /******************************************
     * Query getters
     ******************************************/
    /**
     * @method  Get_select_query
     * @access  public
     * @desc    Returns the select part of query
     * @author  Cousin Béla
     *
     * @param   string                  $type                       - type of the select, can be SELECT, COUNT
     *
     * @version 1.0.0
     * @return  string
     */
    public function Get_select_query( $type )
    {
        $query = "SELECT ";

        if( $type == "COUNT" )
        {
            $query  .= "COUNT( "
                        .( $this->distinct ? "DISTINCT " : "" )
                        .( empty( $this->select_arr ) ? "*" : $this->select_arr[0] )
                    ." ) AS nr_rows";
        }
        // add selects to query
        else
        {
            $query .= ( $this->distinct ? "DISTINCT " : "" )
                        .( empty( $this->select_arr ) ? "*" : $this->Add_to_query( $this->select_arr ) );
        }

        return $query;
    }

    /**
     * @method  Add_to_query
     * @access  public
     * @desc    Returns the select part of query
     * @author  Cousin Béla
     *
     * @param   array                   $filters                    - an array with filters to implode into query
     *
     * @version 1.0.0
     * @return  string
     */
    private function Add_to_query( array & $filters )
    {
        if( empty( $filters ) )
        {
            return "";
        }

        return implode( ", ", $filters );
    }

    /**
     * @method  Add_to_query_with
     * @access  public
     * @desc    Returns the select part of query
     * @author  Cousin Béla
     *
     * @param   array                   $filters                    - an array with filters to implode into query
     *
     * @version 1.0.0
     * @return  string
     */
    private function Add_to_query_with( array & $filters, $with_string = "AND" )
    {
        if( empty( $filters ) )
        {
            return "";
        }

        return implode( " ".$with_string." ", $filters );
    }

    /**
     * @method  Get_set_query
     * @access  public
     * @desc    Returns the set part of query
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     * @return  string
     */
    public function Get_set_query()
    {
        $query = "SET ";

        if( empty( $this->set_arr ) )
        {
            throw new MDatabase_exception( "Please set the columns and values to insert/update" );
        }

        $query .= $this->Add_to_query( $this->set_arr );

        return $query;
    }

    /**
     * @method  Get_set_query_by_sql
     * @access  public
     * @desc    Returns the set part of query for an other SQL
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     * @return  string
     */
    public function Get_set_query_by_sql()
    {
        $query = "";

        if( empty( $this->set_arr ) )
        {
            throw new MDatabase_exception( "Please set the columns and values to insert/update" );
        }

        $query .= $this->Add_to_query( $this->set_arr );

        return $query;
    }

    /**
     * @method  Is_empty_from
     * @access  public
     * @desc    Checks if from parameter for query is empty or not
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    public function Is_empty_from()
    {
        return ( empty( $this->from_arr ) );
    }

    /**
     * @method  Get_from_query
     * @access  public
     * @desc    Returns the from part of query
     * @author  Cousin Béla
     *
     * @param   string                  $type                       - type of the select, can be SELECT, COUNT, DELETE or empty
     *
     * @version 1.0.0
     * @throws  MDatabase_exception
     * @return  string
     */
    public function Get_from_query( $type )
    {
        $query = "";

        if( empty( $this->from_arr ) )
        {
            throw new MDatabase_exception( "Please set the from values for your query" );
        }

        switch( $type )
        {
            case "SELECT":
            case "DELETE":
            case "COUNT":
                $query .= " FROM ";
                break;

            default:
                break;
        }

        $query .= $this->Add_to_query( $this->from_arr );

        return $query;
    }

    /**
     * @method  Get_join_query
     * @access  public
     * @desc    Returns the join part of query
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    public function Get_join_query()
    {
        if( empty( $this->join_arr ) )
        {
            return "";
        }

        $query = implode( " ", $this->join_arr );

        return $query;
    }

    /**
     * @method  Get_where_query
     * @access  public
     * @desc    Returns the where part of query
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    public function Get_where_query()
    {
        if( $this->Is_empty_where() )
        {
            return "";
        }

        $query = " WHERE ";

        // Get filters as query
        $where_query = $this->Add_to_query_with( $this->where_arr, "AND" );
        $where_query .= ( ! empty( $where_query ) && ! empty( $this->where_in_arr ) ? " AND " : "" )
                                .$this->Add_to_query_with( $this->where_in_arr, "AND" );
        // Get filters for or_where
        $or_where_query = $this->Add_to_query_with( $this->or_where_arr, "OR" );
        $or_where_query .= ( ! empty( $or_where_query ) && ! empty( $this->or_where_in_arr ) ? " OR " : "" )
                                .$this->Add_to_query_with( $this->or_where_in_arr, "OR" );

        $is_null_query  = ( ! empty( $where_query ) ? " AND " : "" ).$this->Add_to_query_with( $this->null_arr, "AND" );
        $is_null_query  .= ( ! empty( $is_null_query ) && ! empty( $this->not_null_arr ) ? " AND " : "" )
                                .$this->Add_to_query_with( $this->not_null_arr, "AND" );

        $or_is_null_query= ( ! empty( $or_where_query ) ? " OR " : "" ).$this->Add_to_query_with( $this->or_null_arr, "OR" );
        $or_is_null_query.= ( ! empty( $or_is_null_query ) && ! empty( $this->or_not_null_arr ) ? " OR " : "" )
                                .$this->Add_to_query_with( $this->or_not_null_arr, "OR" );

        // Add filters to query
        $query .= ( ! empty( $where_query ) ? "( ".$where_query." )" : "" );
        $query .= ( ! empty( $or_where_query ) ?
                    ( ! empty( $where_query ) ? " OR " : "" ) . "( ".$or_where_query." )"
                    :
                    ""
                );

        return $query;
    }

    /**
     * @method  Is_empty_where
     * @access  public
     * @desc    Checks if where parameter for query is empty or not
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    private function Is_empty_where()
    {
        if( ! empty( $this->where_arr ) )
        {
            return FALSE;
        }

        if( ! empty( $this->where_in_arr ) )
        {
            return FALSE;
        }

        if( ! empty( $this->or_where_arr ) )
        {
            return FALSE;
        }

        if( ! empty( $this->or_where_in_arr ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Get_like_query
     * @access  public
     * @desc    Returns the like part of query
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    public function Get_like_query()
    {
        if( $this->Is_empty_like() )
        {
            return "";
        }

        $query = ( $this->Is_empty_where() ? " WHERE " : " AND " );

        // Get filters as query
        $like_query = $this->Add_to_query_with( $this->like_arr, "AND" );

        if( ! empty( $this->not_like_arr ) )
        {
            $like_query .= ( ! empty( $like_query ) ? " AND " : "" ).$this->Add_to_query_with( $this->not_like_arr, "AND" );
        }

        // Get filters for or_like
        $or_like_query = $this->Add_to_query_with( $this->or_like_arr, "OR" );

        if( ! empty( $this->or_not_like_arr ) )
        {
            $or_like_query .= ( ! empty( $or_like_query ) ? " OR " : "" ).$this->Add_to_query_with( $this->or_not_like_arr, "OR" );
        }

        // Add filters to query
        $query .= ( ! empty( $like_query ) ? "( ".$like_query." )" : "" );
        $query .= ( ! empty( $or_like_query ) ?
                    ( ! empty( $like_query ) ? " AND " : "" ) . "( ".$or_like_query." )"
                    :
                    ""
                );

        return $query;
    }

    /**
     * @method  Is_empty_like
     * @access  public
     * @desc    Checks if like parameter for query is empty or not
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    private function Is_empty_like()
    {
        if( ! empty( $this->like_arr ) )
        {
            return FALSE;
        }

        if( ! empty( $this->not_like_arr ) )
        {
            return FALSE;
        }

        if( ! empty( $this->or_like_arr ) )
        {
            return FALSE;
        }

        if( ! empty( $this->or_not_like_arr ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Get_null_query
     * @access  public
     * @desc    Returns the null part of query
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    public function Get_null_query()
    {
        if( $this->Is_empty_null() )
        {
            return "";
        }

        $query = ( $this->Is_empty_where() && $this->Is_empty_like() ? " WHERE " : " AND " );

        // Get filters as query
        $null_query = $this->Add_to_query_with( $this->null_arr, "AND" );
        if( ! empty( $this->not_null_arr ) )
        {
            $null_query .= ( ! empty( $null_query ) ? " AND " : "" ).$this->Add_to_query_with( $this->not_null_arr, "AND" );
        }

        // Get filters for or_like
        $or_null_query = $this->Add_to_query_with( $this->or_null_arr, "OR" );
        if( ! empty( $this->or_not_null_arr ) )
        {
            $or_null_query .= ( ! empty( $or_null_query ) ? " OR " : "" ).$this->Add_to_query_with( $this->or_not_null_arr, "OR" );
        }

        // Add filters to query
        $query .= ( ! empty( $null_query ) ? "( ".$null_query." )" : "" );
        $query .= ( ! empty( $or_null_query ) ?
                      ( ! empty( $or_null_query ) ? " AND " : "" ) . "( ".$or_null_query." )"
                      :
                      ""
                );

        return $query;
    }

    /**
     * @method  Is_empty_null
     * @access  public
     * @desc    Checks if null parameter for query is empty or not
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    private function Is_empty_null()
    {
        if( ! empty( $this->null_arr ) )
        {
            return FALSE;
        }

        if( ! empty( $this->not_null_arr ) )
        {
            return FALSE;
        }

        if( ! empty( $this->or_null_arr ) )
        {
            return FALSE;
        }

        if( ! empty( $this->or_not_null_arr ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Get_group_by_query
     * @access  public
     * @desc    Returns the group by part of query
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    public function Get_group_by_query()
    {
        if( empty( $this->group_by_arr ) )
        {
            return "";
        }

        return " GROUP BY ".$this->Add_to_query( $this->group_by_arr );
    }

    /**
     * @method  Get_having_query
     * @access  public
     * @desc    Returns the having part of query
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    public function Get_having_query()
    {
        if( empty( $this->having_arr ) )
        {
            return "";
        }

        return " HAVING ". implode( " AND ", $this->having_arr );
    }

    /**
     * @method  Get_order_by_query
     * @access  public
     * @desc    Returns the order by part of query
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    public function Get_order_by_query()
    {
        if( empty( $this->order_by_arr ) )
        {
            return "";
        }

        return " ORDER BY ".$this->Add_to_query( $this->order_by_arr );
    }

    /**
     * @method  Get_limit_query
     * @access  public
     * @desc    Returns the limit part of query
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    public function Get_limit_query()
    {
        if( empty( $this->limit ) )
        {
            return "";
        }

        $query = " LIMIT ";
        $query .= ( ! empty( $this->limit_from ) ? $this->limit_from.", " : "" );
        $query .= $this->limit." ";

        return $query;
    }

    /**
     * @method  Get_alias
     * @access  public
     * @desc    Returns the alias for from part of query
     * @author  Cousin Béla
     *
     * @version 1.0.0
     * @return  string
     */
    public function Get_alias()
    {
        return $this->alias;
    }

    public function Reset_select()
    {
        $this->select_arr = array();
    }

    public function Reset_set()
    {
        $this->set_arr = array();
    }

    public function Reset_from()
    {
        $this->from_arr = array();
    }

    public function Reset_alias()
    {
        $this->alias = "";
    }

    public function Reset_where()
    {
        $this->where_arr = array();
    }

    public function Reset_where_in()
    {
        $this->where_in_arr = array();
    }

    public function Reset_or_where()
    {
        $this->or_where_arr = array();
    }

    public function Reset_or_where_in()
    {
        $this->or_where_in_arr = array();
    }

    public function Reset_like()
    {
        $this->like_arr = array();
    }

    public function Reset_or_like()
    {
        $this->or_like_arr = array();
    }

    public function Reset_not_like()
    {
        $this->not_like_arr = array();
    }

    public function Reset_or_not_like()
    {
        $this->or_not_like_arr = array();
    }

    public function Reset_null()
    {
        $this->null_arr = array();
    }

    public function Reset_or_null()
    {
        $this->or_null_arr = array();
    }

    public function Reset_not_null()
    {
        $this->not_null_arr = array();
    }

    public function Reset_or_not_null()
    {
        $this->or_not_null_arr = array();
    }

    public function Reset_group_by()
    {
        $this->group_by_arr = array();
    }

    public function Reset_order_by()
    {
        $this->order_by_arr = array();
    }

    public function Reset_limit()
    {
        $this->limit        = "";
        $this->limit_from   = 0;
    }

    /**
     * @method  Protect
     * @access  public
     * @desc    Add the quotes into variables or names in query
     * @author  Cousin Béla
     *
     * @param   string                  $value                      - the value to protect
     * @param   string                  $type                       - type of item to protect, COLUMN or VALUE
     *
     * @version 1.0.0
     * @return  string
     */
    public function Protect( $value, $type = "COLUMN" )
    {
        $type = strtoupper( $type ) ;

        if( ! isset( $value ) )
        {
            return "";
        }

        // IF this is a value, return with ` characters
        if( $type  == "COLUMN" )
        {
            return $this->Protect_column( $value );
        }
        // else return the value with ' characters

        return $this->Protect_value( $value );
    }

    private function Protect_column( $column )
    {
        $columns_array = array();

        $columns = $column;
        // Explode the string with ',' to know if has more columns
        if( is_string( $column ) )
        {
            $columns = explode( ",", $column );
        }

        foreach( $columns as $column )
        {
            if( trim( $column ) == "" )
            {
                continue;
            }

            $column = trim( $column );

            // Explode column with '.' to get the alias
            $column_with_alias = explode( ".", $column );
            // If has an alias
            if( sizeof( $column_with_alias ) > 1 )
            {
                // If we want everything from table, does not need the protectors
                if( $column_with_alias[1] == "*" )
                {
                    $columns_array []= $this->str_to_protect_column.$column_with_alias[0].$this->str_to_protect_column.".*";
                    continue;
                }

                $columns_array []= $this->str_to_protect_column.
                    implode(
                        $this->str_to_protect_column.".".$this->str_to_protect_column,
                        $column_with_alias
                    ).
                    $this->str_to_protect_column;
                continue;
            }

            // add the column with protectors
            $columns_array []= $this->str_to_protect_column.$column.$this->str_to_protect_column;
        }

        return implode( ", ", $columns_array );
    }

    /**
     * @method  Protect_value
     * @access  public
     * @desc    Add the quotes into variables in query
     * @author  Cousin Béla
     *
     * @param   string                  $value                      - the value to protect
     *
     * @version 1.0.0
     * @return  string
     */
    public function Protect_value( $value )
    {
        if( ! isset( $value ) )
        {
            return "";
        }

        return $this->str_to_protect_value.$this->db->Escape( $value ).$this->str_to_protect_value;
    }
}