<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MMinifier
 *
 */
class MMinifier
{
    private static $available_types = array( "HTML", "JS", "CSS" );

    public static function Minify( $content, $type = "html" )
    {
        $type = strtoupper( $type );

        if( ! in_array( $type, self::$available_types ) )
        {
            $type = "HTML";
        }

        $class = "Minifier_".$type;

        $minifier = new $class( $content );

        return $minifier->Minify();
    }
}

/* End of file MMinifier.php */
/* Location: ./core/Libraries/ */