<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MValidation
 *
 * @version     1.0.0
 */
class MValidation extends BValidation
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @method	Required
     * @access	protected
     * @desc    this method checks if the value is empty or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name			    - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Required( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_required" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Required_numeric
     * @access	protected
     * @desc    this method checks if the value is empty or not, but can be 0
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name			    - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Required_numeric( $name = "", $value = "" )
    {
        if( is_null( $value ) || ! is_numeric( $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_required" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	email
     * @access	protected
     * @desc    this method checks if the value is a valid email or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name			    - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Email( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        preg_match( "/^([a-z0-9])([a-z0-9\._-])*\@([a-z0-9])*([a-z0-9\._-])*\.([a-z]){2,4}$/i", $value, $valid_email );
        if( empty( $valid_email ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_email" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Length
     * @access	protected
     * @desc    this method checks if the length value equal with length or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name			    - the name for error string
     * @param 	string						$value			    - the post value
     * @param	int							$length			    - the length
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Length( $name = "", $value = "", $length = 0 )
    {
        if( empty( $value ) || strlen( $value ) != $length )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_length" ), $name, $length ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Min_length
     * @access	protected
     * @desc    this method checks if the value has a minimal character number or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   int                         $length			    - the minimum length
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Min_length( $name = "", $value = "", $length = 0 )
    {
        if( ! empty( $value ) && strlen( $value ) < $length )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_min_length" ), $name, $length ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Max_length
     * @access	protected
     * @desc    this method checks if the value has more than a maximal character number or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   int                         $length			    - the minimum length
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Max_length( $name = "", $value = "", $length = 0 )
    {
        if( ! empty( $value ) && strlen( $value ) > $length )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_max_length" ), $name, $length ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Match
     * @access	protected
     * @desc    this method checks if the value is equal with an another field value or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   string						$key                - the key for new value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Match( $name = "", $value = "", $key = "" )
    {
        $match_value = ( isset( $_POST[$key] ) ? $_POST[$key] : "" );

        if( $value != $match_value )
        {
            $this->Add_error_message(
                sprintf(
                    App()->lang->Get( "validation_match" ),
                    $name,
                    App()->lang->Get( ucfirst( $key ) )
                )
            );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Numeric
     * @access	public
     * @desc    this method checks if the value is a numeric value or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    function Numeric( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^\-?[0-9]+$/", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_numeric" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Number
     * @access	protected
     * @desc    this method checks if the value is a number value or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	int						    $value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Number( $name = "", $value = null )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^\-?([0-9]+)\.?([0-9]+)?$/", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_number" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Negative
     * @access  protected
     * @desc    This method checks if the value is strictly negative or not
     * @author  Csenteri Attila
     *
     * @param 	string						$name               - the name for error string
     * @param 	int						    $value			    - the post value
     *
     * @version 1.0
     * @return  boolean
     */
    protected function Negative( $name = "", $value = null )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! $this->Number( $name, $value ) )
        {
            return FALSE;
        }

        if( $value > 0 );
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_negative" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Negative
     * @access  protected
     * @desc    This method checks if the value is strictly positive or not
     * @author  Csenteri Attila
     *
     * @param 	string						$name               - the name for error string
     * @param 	int						    $value			    - the post value
     *
     * @version 1.0
     * @return  boolean
     */
    protected function Positive( $name = "", $value = null )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! $this->Number( $name, $value ) )
        {
            return FALSE;
        }

        return $this->Min_value( $name, $value, 0 );
    }

    /**
     * @method	Price
     * @access	protected
     * @desc    this method checks if the value is a price value or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Price( $name = "", $value = "" )
    {
        return $this->Number( $name, $value );
    }

    /**
     * @method	Numeric_phone
     * @access	protected
     * @desc    this method checks if the value is a numeric, and he lets some chars for phone numbers
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Numeric_phone( $name = "", $value = "" )
    {
        $error = FALSE;

        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[0-9\-\+\ ]+$/", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_numeric" ), $name ) );

            $error = TRUE;
        }

        $result_numbers = "";

        for( $i = 0; $i < strlen( $value ); ++$i )
        {
            if( is_numeric( $value[$i] ) )
            {
                $result_numbers .= $value[$i];
            }
        }

        if( strlen( $result_numbers ) < 10 || strlen( $result_numbers ) > 16 )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_phone_length" ), $name ) );
            $error = TRUE;
        }

        return ( ! $error );
    }

    /**
     * @method	Integer
     * @access	protected
     * @desc    this method checks if the value is a numeric value or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Integer( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^\-?[0-9]+$/", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_integer" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Max_value
     * @access  protected
     * @desc    This method checks if the value has a maximal given number or not (default Max_val is 0)
     * @author  Csenteri Attila
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   int						    $max_value          - the key for new value
     *
     * @version 1.0
     * @return  boolean
     */
    protected function Max_value( $name = "", $value = "", $max_value = 0 )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! $this->Number( $name, $value ) )
        {
            return FALSE;
        }

        if( $value > $max_value )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_max_value" ), $name, $max_value ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Min_value
     * @access  protected
     * @desc    This method checks if the value has a minimal given number or not (default Min_val is 0)
     * @author  Csenteri Attila
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   int						    $min_value          - the key for new value
     *
     * @version 1.0
     * @return  boolean
     */
    protected function Min_value( $name = "", $value = "", $min_value = 0 )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        /*if( $this->Number( $name, $value ) )
        {
            return FALSE;
        }*/

        if( ! is_numeric( $value ) || $value < $min_value )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_min_value" ), $name, $min_value ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Lesser
     * @access  protected
     * @desc    This method checks if the value is lesser than a field from post
     * @author  Cousin Bela
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   string						$key                - the key for new value
     *
     * @version 1.0
     * @return  boolean
     */
    protected function Lesser( $name = "", $value = "", $key = "" )
    {
        if( ( empty( $value ) && ! is_numeric( $value ) ) )
        {
            return FALSE;
        }

        if( empty( $key ) || ( ! Post_helper::Get( $key ) && ! Post_helper::Get( $key ) != 0 ) )
        {
            return FALSE;
        }

        $compare_value = Post_helper::Get_integer( $key );

        if( $value >= $compare_value )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_lesser" ), $name, $compare_value ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Lesser_or_equal
     * @access  protected
     * @desc    This method checks if the value is lesser or equal to a field from post
     * @author  Cousin Bela
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   string						$key                - the key for new value
     *
     * @version 1.0
     * @return  boolean
     */
    protected function Lesser_or_equal( $name = "", $value = "", $key = "" )
    {
        if( ( empty( $value ) && ! is_numeric( $value ) ) )
        {
            return FALSE;
        }

        if( empty( $key ) || ( ! Post_helper::Get( $key ) && ! Post_helper::Get( $key ) != 0 ) )
        {
            return FALSE;
        }

        $compare_value = Post_helper::Get_integer( $key );

        if( $value > $compare_value )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_lesser_or_equal" ), $name, $compare_value ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Bigger
     * @access  protected
     * @desc    This method checks if the value is bigger than a field from post
     * @author  Cousin Bela
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   string						$key                - the key for new value
     *
     * @version 1.0
     * @return  boolean
     */
    protected function Bigger( $name = "", $value = "", $key = "" )
    {
        if( ( empty( $value ) && ! is_numeric( $value ) ) )
        {
            return FALSE;
        }

        if( empty( $key ) || ( ! Post_helper::Get( $key ) && ! Post_helper::Get( $key ) != 0 ) )
        {
            return FALSE;
        }

        $compare_value = Post_helper::Get_integer( $key );

        if( $value <= $compare_value )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_bigger" ), $name, $compare_value ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Bigger_or_equal
     * @access  protected
     * @desc    This method checks if the value is bigger or equal to a field from post
     * @author  Cousin Bela
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   string						$key                - the key for new value
     *
     * @version 1.0
     * @return  boolean
     */
    protected function Bigger_or_equal( $name = "", $value = "", $key = "" )
    {
        if( ( empty( $value ) && ! is_numeric( $value ) ) )
        {
            return FALSE;
        }

        if( empty( $key ) || ( ! Post_helper::Get( $key ) && ! Post_helper::Get( $key ) != 0 ) )
        {
            return FALSE;
        }

        $compare_value = Post_helper::Get_integer( $key );

        if( $value < $compare_value )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_bigger_or_equal" ), $name, $compare_value ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Between
     * @access  protected
     * @desc    This method checks if the value is between two values
     * @author  Cousin Bela
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   int						    $min_value          - the key for min value
     * @param   int						    $max_value          - the key for max value
     *
     * @version 1.0
     * @return  boolean
     */
    protected function Between( $name = "", $value = "", $min_value = 0, $max_value = 0 )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( empty( $max_value ) )
        {
            $min_value_array = explode( ",", $min_value );
            if( count( $min_value_array ) == 2 )
            {
                $min_value = $min_value_array[0];
                $max_value = $min_value_array[1];
            }
        }

        if( $value < $min_value || $value > $max_value )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_between" ), $name, $min_value, $max_value ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Link
     * @access	protected
     * @desc    this method lets the value has characters for a link
     * @author	Cousin Bela
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Link( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\p{L}\.\_\-\/\#\?\:\=]+$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_link" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Path
     * @access	protected
     * @desc    this method lets the value has characters for a path
     * @author	Cousin Bela
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Path( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\.\_\-\/]+$/i", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_path" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Alpha
     * @access	protected
     * @desc    this method checks if the value just has alphabetical values or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Alpha( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z\p{L}]+$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_alpha" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Name
     * @access	protected
     * @desc    this method checks if the value has a name or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Name( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\p{L}\-\ \']*$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_name" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Title
     * @access	protected
     * @desc    this method checks if the value has a title or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Title( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\p{L}\p{M}\-\_\ \'\"\:\!\%\+\?\,\.\@\&]*$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_title_name" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Alpha_numeric
     * @access	protected
     * @desc    this method checks if the value just has alphabetical values and numeric values
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Alpha_numeric( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\p{L}]+$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_alpha_numeric" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Alpha_dash
     * @access	protected
     * @desc    this method checks if the value just has alphabetical values and punctual characters
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Alpha_dash( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\p{L}\.\_\-]+$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_alpha_dash" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Alpha_dash_slash
     * @access	protected
     * @desc    this method checks if the value just has alphabetical values and punctual characters and slash
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Alpha_dash_slash( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\p{L}\.\_\-\/]+$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_alpha_dash_slash" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Alpha_dash_slash_space
     * @access	protected
     * @desc    this method checks if the value just has alphabetical values and punctual characters and slash
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Alpha_dash_slash_space( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\p{L}\.\_\-\/ ]+$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_alpha_dash_slash_space" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Alpha_dash_space
     * @access	protected
     * @desc    this method checks if the value just has alphabetical values and punctual characters and space
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Alpha_dash_space( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\p{L}\.\_\- ]+$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_alpha_dash_space" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Module_name
     * @access	protected
     * @desc    this method checks if the value has a module name
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Module_name( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z\_\-]+$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_module_name" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Address
     * @access	protected
     * @desc    this method checks if the value has a valid address
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Address( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\p{L}\.\_\- \/\,\(\)]+$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_address" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Normal
     * @access	protected
     * @desc    this method checks if the value just has alphabetical values or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Normal( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-zA-Z0-9\pL\pM\pS\pP\.\#\/\\\_\- \!\?\:\)\(\=\*\°\²\"\'\@\{\}\[\]\%\|\,\;\<\>\t\r\n\&\+\$\^]*$/miu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_normal" ), $name ) );

            return FALSE;
        }

        return $this->xss_clean( $name, $value );
    }

    /**
     * @method	Date
     * @access	protected
     * @desc    this method checks if the value is date or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Date( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^([0-9]{4}\-[0-9]{2}\-[0-9]{2})$/s", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_date" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Datetime
     * @access	protected
     * @desc    this method checks if the value is date or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Datetime( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^([0-9]{4}\-[0-9]{2}\-[0-9]{2})(\ [0-9]{2}\:[0-9]{2}\:[0-9]{2})?$/s", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_datetime" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Min_date
     * @access	protected
     * @desc    this method checks if the value is bigger date or not than the desired
     * @example Can be "now" or a valid date like "2015-01-01"
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   string    				    $min_date           - the valid date from, ex: now or a valid date
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Min_date( $name = "", $value = "", $min_date = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! $this->Date( $name, $value ) )
        {
            return FALSE;
        }

        if( empty( $min_date ) || $min_date == "now" )
        {
            $min_date = date( "Y-m-d" );
        }

        if( strtotime( $value ) < strtotime( $min_date ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_min_date" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Min_datetime
     * @access	protected
     * @desc    this method checks if the value is bigger date and time or not than the desired
     * @example Can be "now" or a valid date like "2015-01-01 10:01:00"
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   string    				    $min_date           - the valid date from, ex: now or a valid date
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Min_datetime( $name = "", $value = "", $min_date = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! $this->Datetime( $name, $value ) )
        {
            return FALSE;
        }

        if( empty( $min_date ) || $min_date == "now" )
        {
            $min_date = date( "Y-m-d H:i:s" );
        }

        if( strtotime( $value ) < strtotime( $min_date ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_min_datetime" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Max_date
     * @access	protected
     * @desc    this method checks if the value is lower date and time or not than the desired
     * @example Can be "now" or a valid date like "2015-01-01 10:01:00"
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   string    				    $max_date           - the maximum valid date, ex: now or a valid date
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Max_date( $name = "", $value = "", $max_date = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! $this->Date( $name, $value ) )
        {
            return FALSE;
        }

        if( empty( $max_date ) || $max_date == "now" )
        {
            $max_date = date( "Y-m-d" );
        }

        if( strtotime( $value ) > strtotime( $max_date ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_max_date" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Max_datetime
     * @access	protected
     * @desc    this method checks if the value is lower date and time or not than the desired
     * @example Can be "now" or a valid date like "2015-01-01 10:01:00"
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     * @param   string    				    $max_date           - the maximum valid date, ex: now or a valid date
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Max_datetime( $name = "", $value = "", $max_date = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! $this->Datetime( $name, $value ) )
        {
            return FALSE;
        }

        if( empty( $max_date ) || $max_date == "now" )
        {
            $max_date = date( "Y-m-d H:i:s" );
        }

        if( strtotime( $value ) > strtotime( $max_date ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_max_datetime" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Filename
     * @access	protected
     * @desc    this method checks if the value has a filename or not
     * @author	Attila
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Filename( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[a-z0-9\p{L}\-\_\.\ \']*$/iu", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_filename" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Ip
     * @access	protected
     * @desc    this method checks if the value is a valid ip address
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Ip( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}+$/", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_ip" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Password
     * @access	protected
     * @desc    this method checks if the value is a correct password
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Password( $name = "", $value = "" )
    {
        $error = FALSE;
        /*if( $this->Required( $name, $value ) )
        {
            $error = TRUE;
        }*/
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/[a-z]/", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_password_a" ), $name ) );

            $error = TRUE;
        }

        if( ! preg_match( "/[A-Z]/", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_password_A" ), $name ) );

            $error = TRUE;
        }

        if( ! preg_match( "/[0-9]/", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_password_0" ), $name ) );

            $error = TRUE;
        }

        if( ! preg_match( "/[\.\-_,\s]/", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_password_white" ), $name ) );

            $error = TRUE;
        }

        return ( ! $error );
    }

    /**
     * @method	Color
     * @access	protected
     * @desc    this method checks if the value is a valid color
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Color( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^\#?([a-f0-9]{1,6})$/i", $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_color" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Email_domain_valid
     * @access	protected
     * @desc    this method checks if the domain of email address is valid
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Email_domain_valid( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        $value_arr = explode( "@", $value );

        if( empty( $value_arr[1] ) || ! $this->Is_domain_valid( $value_arr[1] ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_domain_valid" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Domain_valid
     * @access	protected
     * @desc    this method checks if the domain is valid
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Domain_valid( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! $this->Is_domain_valid( $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_domain_valid" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Is_domain_valid
     * @access	protected
     * @desc    this method checks if the selected domain is valid or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    private function Is_domain_valid( $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/([a-z0-9\-\_]*\.)?([a-z0-9\-\_]+)\.([a-z]{2,6})/i", $value ) )
        {
            return FALSE;
        }

        $ip_address = gethostbyname( $value );

        if( empty( $ip_address ) || $ip_address == $value )
        {
            ob_start();
            system( "host ".$value );
            $host = ob_get_clean();

            if( empty( $host ) || strpos( $host, "not found" ) !== FALSE )
            {
                return FALSE;
            }

            return TRUE;
        }

        return TRUE;
    }

    /**
     * @method	Xss_clean
     * @access	protected
     * @desc    this method checks if the value is xss clean or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$name               - the name for error string
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    protected function Xss_clean( $name = "", $value = "" )
    {
        if( empty( $value ) )
        {
            return TRUE;
        }

        if( ! $this->Xss_php_clean( $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_xss" ), $name ) );

            return FALSE;
        }

        if( ! $this->Xss_javascript_clean( $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_xss" ), $name ) );

            return FALSE;
        }

        if( ! $this->Xss_html_clean( $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_xss" ), $name ) );

            return FALSE;
        }

        if( ! $this->Xss_other_clean( $value ) )
        {
            $this->Add_error_message( sprintf( App()->lang->Get( "validation_xss" ), $name ) );

            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Xss_php_clean
     * @access	protected
     * @desc    this method checks if the value is php xss clean or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    private function Xss_php_clean( $value )
    {
        if( strpos( $value, "<?" ) !== FALSE )
        {
            return FALSE;
        }

        if( preg_match( "/(.*)\<\?(.*)/", $value ) )
        {
            return FALSE;
        }

        if( preg_match( "/(.*)\?\>(.*)/", $value ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Xss_javascript_clean
     * @access	protected
     * @desc    this method checks if the value is php xss clean or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    private function Xss_javascript_clean( $value )
    {
        if( strpos( $value, "<script" ) !== FALSE )
        {
            return FALSE;
        }

        if( preg_match( "/(.*)\<script(.*)\>(.*)/", $value ) )
        {
            return FALSE;
        }

        if( preg_match( "/(.*)\<\/script\>(.*)/", $value ) )
        {
            return FALSE;
        }

        if( preg_match( "/onclick|onmouse|onload\=/", $value ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Xss_html_clean
     * @access	protected
     * @desc    this method checks if the value is html xss clean or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    private function Xss_html_clean( $value )
    {
        if( preg_match( "/(.*)\<\!\-\-(.*)/", $value ) )
        {
            return FALSE;
        }

        if( preg_match( "/(.*)\-\-\>(.*)/", $value ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method	Xss_other_clean
     * @access	protected
     * @desc    this method checks if the value is any other xss clean or not
     * @author	Zoltan Jozsa
     *
     * @param 	string						$value			    - the post value
     *
     * @version	1.0
     * @return 	boolean
     */
    private function Xss_other_clean( $value )
    {
        if( preg_match( "/(.*)link\=>(.*)/", $value ) )
        {
            return FALSE;
        }

        return TRUE;
    }
}