<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MImage
 *
 * @version     1.0.0
 */
class MImage
{
    public $library     = "gd2";

    public function __construct()
    {

    }

    public function Create_thumbnail( $filename, $width, $height, $prefix = "" )
    {
        $found_path = Image_helper::Get_image_path( $filename );

        if( $found_path === FALSE )
        {
            return "";
        }
    }

    public function Create_thumbnail_from_template( $filename, $template_name )
    {
        Logger_helper::Debug( "started to create thumbnail for file ".$filename." and template: ".$template_name );

        $found_path = Image_helper::Get_image_path( $filename );

        if( $found_path === FALSE )
        {
            Logger_helper::Error( "path not founded for file: ".$filename );
            $found_path = Image_helper::Get_image_path( Image_helper::Get_default_image() );
        }

        Logger_helper::Debug( "path founded: ".$found_path." for file: ".$filename." and template: ".$template_name );

        return $this->Create_thumbnail_from_template_by_path( $found_path, $template_name );
    }

    public function Create_thumbnail_from_template_by_path( $file_path, $template_name )
    {
        if( ! $this->Is_good_image_file( $file_path ) )
        {
            Logger_helper::Error( "is not a good image file: ".$file_path );

            return "";
        }

        $template = $this->Get_template( $template_name );

        if( ! $template )
        {
            throw new MPHP_exception( E_ERROR, "Template_not_found" );
        }

        $new_image_path = dirname( $file_path )."/".$template["prefix"].basename( $file_path );

        if( file_exists( $new_image_path ) )
        {
            $new_image_path = str_replace( array( BASEPATH, "\\" ), array( "", "/" ), $new_image_path );

            return Url_helper::Base_url().$new_image_path;
        }

        // call library ex. Image_GD2();
        $class_name = "Image_".strtoupper( $this->library );
        $library    = new $class_name();

        if( $library->Create_thumbnail_from_template( $file_path, $new_image_path, $template ) )
        {
            /*try
            {
                //chmod( $new_image_path, 0777 );
            }
            catch( Exception $e ){}*/

            $new_image_path = str_replace( array( BASEPATH, "\\" ), array( "", "/" ), $new_image_path );

            Logger_helper::Debug( "image created: ".$new_image_path );

            return Url_helper::Base_url().$new_image_path;
        }

        return "";
    }

    protected function Is_good_image_file( $file_path )
    {
        if( empty( $file_path ) )
        {
            return FALSE;
        }

        if( ! file_exists( $file_path ) )
        {
            return FALSE;
        }

        $resource       = finfo_open( FILEINFO_MIME_TYPE );
        $content_type   = finfo_file( $resource, $file_path );

        // if content type isn't start with image/, that means is not an image/png, image/jpeg, etc.
        if( strpos( $content_type, "image/" ) !== 0 )
        {
            return FALSE;
        }

        return TRUE;
    }

    protected function Get_template( $template_name )
    {
        $image_config = App()->config->image;

        if( ! empty( $image_config["list"][$template_name] ) )
        {
            return $image_config["list"][$template_name];
        }

        return FALSE;
    }

}

/* End of file MImages.php */
/* Location: ./core/Libraries/ */