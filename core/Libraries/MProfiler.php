<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MProfiler
 *
 * @version     1.0.0
 */
class MProfiler extends BProfiler
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @method	Start
     * @access	public
     * @desc    Start a profiler for name
     * @author	Cousin Béla
     * 
     * @param   string        $name         - name of counter to start
     *  
     * @version	1.0.0
     */
    public function Start( $name )
    {
        if( ! empty( $this->profilers[$name] ) )
        {
            return;
        }
        
        $this->profilers[$name] = array(
            "start"       => microtime( TRUE ),
            "end"         => "",
            "difference"  => "",
        );
    }
    
    /**
     * @method	Stop
     * @access	public
     * @desc    Stop a profiler for name
     * @author	Cousin Béla
     * 
     * @param   string        $name         - name of counter to stop
     *  
     * @version	1.0.0
     */
    public function Stop( $name )
    {
        if( empty( $this->profilers[$name] ) )
        {
            return;
        }
        
        $this->profilers[$name]["end"]        = microtime( TRUE );
        $this->profilers[$name]["difference"] = ( $this->profilers[$name]["end"] - $this->profilers[$name]["start"] ) * 1000;
    }
    
    /**
     * @method	Stop_all
     * @access	public
     * @desc    Stops all profiler
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     */
    public function Stop_all()
    {
        foreach( $this->profilers as $profiler )
        {
            $profiler["end"]        = microtime( TRUE );
            $profiler["difference"] = ( $profiler["end"] - $profiler["start"] ) * 1000;
        }
    }
    
    /**
     * @method	Get
     * @access	public
     * @desc    Return execution time by name
     * @author	Cousin Béla
     * 
     * @param   string        $name         - name of counter to start
     *  
     * @version	1.0.0
     * @return  string
     */
    public function Get( $name )
    {
        if( empty( $this->profilers[$name] ) )
        {
            return FALSE;
        }
        
        return ( $this->profilers[$name]["difference"] )." ms";
    }

    /**
     * @method	Show_result
     * @access	public
     * @desc    Show the results
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    public function Show_result()
    {
        if( $this->Is_turned_off() )
        {
            return;
        }

        $data = array();

        if( App()->config->profile["show_time"] )
        {
            $data["pre_controller_time"]= $this->Get( "Pre_controller" );
            $data["controller_time"]    = $this->Get( "Controller_method" );
            $data["system_time"]        = $this->Get( "System" );

            if( $data["system_time"] == " ms" )
            {
                $this->Stop( "System" );
                $data["system_time"]    = $this->Get( "System" );
            }
        }

        if( $this->Show_queries() )
        {
            $data["sql_queries"]        = App()->db->Get_executed_queries();
        }

        if( App()->config->profile["show_memory"] )
        {
            $data["memory_size"]        = round( ( memory_get_usage( TRUE ) / 1048576 ), 2 )." MB";
        }

        // Show profiler status in a template
        if( ! empty( App()->config->profile["template"] ) )
        {
            echo App()->renderer->Load_partial_view(
                App()->config->profile["template"],
                $data
            );
            
            return;
        }
    }

    private function Show_queries()
    {
        if( App()->config->profile["show_queries"] )
        {
            return TRUE;
        }

        if( App()->config->profile["show_duplicated_queries"] )
        {
            return TRUE;
        }

        if( App()->config->profile["show_number_of_queries"] )
        {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * @method	Is_turned_off
     * @access	public
     * @desc    Check if all profiler are turned off
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  bool
     */
    protected function Is_turned_off()
    {
        if( App()->config->profile["show_time"] )
        {
            return FALSE;
        }

        if( App()->config->profile["show_queries"] )
        {
            return FALSE;
        }

        if( App()->config->profile["show_memory"] )
        {
            return FALSE;
        }

        return TRUE;
    }
}

/* End of file Profiler.php */
/* Location: ./core/Libraries/ */