<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MPayment
 *
 * @property    Payment_Paypal      $library
 *
 * @version 1.0.0
 */
class MPayment extends BPayment
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Set_checkout_details( & $order )
    {
        $this->Reset_result();

        $this->library->Set_express_checkout_details( $order );

        if( ! $this->library->Get_status() )
        {
            $this->error_message= $this->library->Get_error_message();
            $this->status       = FALSE;

            return FALSE;
        }

        $this->Load_result();

        $this->status           = TRUE;
        $this->token            = $this->library->Get_token();

        return TRUE;
    }

    public function Get_checkout_details( & $order )
    {
        $this->Reset_result();

        $this->library->Get_express_checkout_details( $order );

        $this->Load_result();

        if( ! $this->library->Get_status() )
        {
            $this->error_message= $this->library->Get_error_message();
            $this->status       = FALSE;

            return FALSE;
        }

        $this->status           = TRUE;
        $this->token            = $this->library->Get_token();

        return TRUE;
    }

    /*
     result has to contain an array like this
         array
          'TOKEN' => string 'EC-5K4873818N1000529' (length=20)
          'SUCCESSPAGEREDIRECTREQUESTED' => string 'false' (length=5)
          'TIMESTAMP' => string '2015-12-11T22:24:09Z' (length=20)
          'CORRELATIONID' => string '9d610313bbb61' (length=13)
          'ACK' => string 'Success' (length=7)
          'VERSION' => string '93.0' (length=4)
          'BUILD' => string '18308778' (length=8)
          'L_ERRORCODE0' => string '11607' (length=5)
          'L_SHORTMESSAGE0' => string 'Duplicate Request' (length=17)
          'L_LONGMESSAGE0' => string 'A successful transaction has already been completed for this token.' (length=67)
          'L_SEVERITYCODE0' => string 'Warning' (length=7)
          'INSURANCEOPTIONSELECTED' => string 'false' (length=5)
          'SHIPPINGOPTIONISDEFAULT' => string 'false' (length=5)
          'TRANSACTIONID' => string '4F580722T6179610D' (length=17)
          'TRANSACTIONTYPE' => string 'expresscheckout' (length=15)
          'PAYMENTTYPE' => string 'instant' (length=7)
          'PAYMENTMETHODTYPE' => string 'instant' (length=7)
          'ORDERTIME' => string '2015-12-11T21:56:42Z' (length=20)
          'AMT' => string '46.97' (length=5)
          'TAXAMT' => string '0.00' (length=4)
          'CURRENCYCODE' => string 'EUR' (length=3)
          'PAYMENTSTATUS' => string 'Pending' (length=7)
          'PENDINGREASON' => string 'multicurrency' (length=13)
          'REASONCODE' => string 'None' (length=4)
          'PROTECTIONELIGIBILITY' => string 'Ineligible' (length=10)
          'PROTECTIONELIGIBILITYTYPE' => string 'None' (length=4)
          'SECUREMERCHANTACCOUNTID' => string 'CZS5EX4QCAEPC' (length=13)
          'ERRORCODE' => string '0' (length=1)
     */
    public function Do_express_checkout( & $order )
    {
        $this->Reset_result();

        $this->library->Do_express_checkout( $order );
        $this->Load_result();

        if( ! $this->library->Get_status() )
        {
            $this->error_message= $this->library->Get_error_message();
            $this->status       = FALSE;

            return FALSE;
        }

        $this->status           = TRUE;
        $this->correlation_id   = $this->library->Get_correlation_id();
        $this->transaction_id   = $this->library->Get_transaction_id();

        return TRUE;
    }

    public function Do_direct_payment( & $order, $card_details )
    {
        $this->Reset_result();

        $this->library->Do_direct_payment( $order, $card_details );

        if( ! $this->library->Get_status() )
        {
            $this->error_message= $this->library->Get_error_message();
            $this->status       = FALSE;

            return FALSE;
        }

        $this->Load_result();

        $this->status           = TRUE;
        $this->correlation_id   = $this->library->Get_correlation_id();
        $this->transaction_id   = $this->library->Get_transaction_id();

        return TRUE;
    }
}

/* End of file MPayment.php */
/* Location: ./core/Libraries/ */