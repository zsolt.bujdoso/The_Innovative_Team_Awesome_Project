<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MRedirect
 *
 * @version     1.0.0
 */
class MRedirect extends BRedirect
{
    public function __construct()
    {
        parent::__construct();
    }
}

/* End of file MRenderer.php */
/* Location: ./core/Libraries/ */