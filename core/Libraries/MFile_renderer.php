<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MFile_renderer
 *
 * @version     1.0.0
 */
class MFile_renderer extends BRenderer
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function Render( $template, $params = array() )
    {
        if( ! empty( $this->layout ) )
        {
            $this->layout_content = $this->Render_partial( $this->layout, $params, TRUE );
        }
        
        
    }
    
    public function Render_partial( $template, $params = array(), $return = FALSE )
    {
        $content = $this->Render_template( $template, $params, $return );
        
        if( $return )
        {
            return $content;
        }
        
        echo $content;
    }
    
    private function Render_template( $template, $params = array(), $return = FALSE )
    {
        $template_path = $this->Find_template( $template );
        
        if( ! $template_path )
        {
            throw new MPHP_exception( E_ERROR, "Template not found" );
        }
    }
}

/* End of file File_renderer.php */
/* Location: ./core/Libraries/ */