<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MForm_validation
 *
 * @property    array   $attributes
 *
 * @version     1.0.0
 */
class MForm_validation extends MValidation
{
    /**
     * @var     $variables
     */
    protected $variables;

    /**
     * @var     $labels
     */
    protected $labels;

    /**
     * @var     $rules
     */
    protected $rules;

    /**
     * @var     $attributes
     */
    protected $attributes;

    public function __construct()
    {
        parent::__construct();

        $this->variables    = $this->Get_variables();
        $this->labels       = $this->Get_labels();
        $this->rules        = $this->Get_rules();
    }

    protected function Get_variables()
    {
        return array();
    }

    protected function Get_labels()
    {
        return array();
    }

    protected function Get_rules()
    {
        return array();
    }

    /**
     * @method	Add_rule
     * @access	public
     * @desc    this method add a new rule to a field
     * @author	Zoltan Jozsa
     *
     * @param   string                      $code                       - code of validation
     * @param   string                      $rules                      - rule for this validation
     *
     * @version	1.0
     */
    public function Add_rule( $code, $rules )
    {
        if( empty( $this->rules[$code] ) )
        {
            $this->rules[$code] = $rules;
            return;
        }

        $this->rules[$code] .= "|".$rules;
    }

    /**
     * @method	Set_label
     * @access	public
     * @desc    this method add a new rule to a field
     * @author	Zoltan Jozsa
     *
     * @param   string                      $code                       - code of validation
     * @param   string                      $label                      - label for this code
     *
     * @version	1.0
     */
    public function Set_label( $code, $label )
    {
        $this->labels[$code] = $label;
    }

    /**
     * @method	Set_rules
     * @access	public
     * @desc    this method add a new rule to a field
     * @author	Zoltan Jozsa
     *
     * @param   string                      $code                       - code of validation
     * @param   string                      $rules                      - rule for this validation
     *
     * @version	1.0
     */
    public function Set_rules( $code, $rules )
    {
        if( empty( $this->variables[$code] ) )
        {
            $this->variables[$code] = $code;
        }

        $this->rules[$code] = $rules;
    }

    /**
     * @method	Validate
     * @access	public
     * @desc    this method checks the variables will make the rules or not
     * @author	Zoltan Jozsa
     *
     * @version	1.0
     * @throws  MPHP_exception
     * @return 	boolean
     */
    public function Validate()
    {
        if( empty( $this->variables ) )
        {
            return TRUE;
        }

        if( ! is_array( $this->rules ) )
        {
            throw new MPHP_exception( E_ERROR, App()->lang->Get( "Error_with_rules" ) );
        }

        foreach( $this->variables as $field )
        {
            $rule = $this->rules[$field];

            if( empty( $rule ) )
            {
                continue;
            }

            // explode the rules with character | to get more rules for this field
            $field_rules = explode( "|", $rule );

            foreach( $field_rules as $field_rule )
            {
                $value = null; //( isset( $this->variables[$field] ) ? $this->variables[$field] : "" );

                if( isset( $this->attributes[$field] ) )
                {
                    $value = $this->attributes[$field];
                }

                // the field rule means the name of the function to call
                $function = ucfirst( strtolower( $field_rule ) );

                $function_parameters = array(
                    $this->labels[$field],
                    $value
                );

                // If this rule has parameter like between [0,99] or min_value[1]
                preg_match( "/([a-zA-Z\_]+)\[([a-zA-Z0-9\.\-\_]+)\,?([a-zA-Z0-9\.\-\_]+)?\]/", $field_rule, $matches );
                if( isset( $matches[2] ) )
                {
                    $function   = ucfirst( strtolower( $matches[1] ) );
                    $parameters = array_slice( $matches, 0 );

                    $function_parameters += $parameters;
                }

                // Call the function with parameters
                $result = call_user_func_array(
                    array(
                        $this,
                        $function,
                    ),
                    $function_parameters
                );

                if( ! $result )
                {
                    $this->has_error = TRUE;
                    break;
                }
            }
        }

        return $this->Get_status();
    }

    public function & __get( $name )
    {
        if( property_exists( $this, $name ) )
        {
            return $this->$name;
        }

        if( isset( $this->variables[$name] ) )
        {
            return $this->variables[$name];
        }

        // throw an exception, this property does not exists
        throw new MProperty_not_exists_exception( 'The class "'.get_class( $this ).'" does not have a property named '.$name );
    }

    public function __set( $name, $value )
    {
        if( property_exists( $this, $name ) )
        {
            return $this->$name = $value;
        }

        if( isset( $this->variables[$name] ) )
        {
            return $this->variables[$name] = $value;
        }

        // throw an exception, this property does not exists
        throw new MProperty_not_exists_exception( 'The class "'.get_class( $this ).'" does not have a property named '.$name );
    }
}

/* End of file MForm_validation.php */
/* Location: ./core/Libraries/ */