<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MTheme
 *
 * @version     1.0.0
 */
class MTheme extends BTheme
{
    public function __construct()
    {
        parent::__construct();

        $this->Set_theme( App()->config->theme );
    }

    public function Set_theme( $theme_name )
    {
        $this->name = $theme_name;
        $this->path = $this->default_path.( $theme_name ? $theme_name.DIRECTORY_SEPARATOR : "" );
    }

    public function Set_theme_id( $theme_id )
    {
        $this->theme_id = $theme_id;
    }

    public function Get_name()
    {
        return $this->name;
    }

    public function Get_path()
    {
        return $this->path;
    }

    public function Get_path_for_url()
    {
        return str_replace( BASEPATH, "", $this->path );
    }

    public function Get_theme_id()
    {
        return $this->theme_id;
    }
}

/* End of file MTheme.php */
/* Location: ./core/Libraries/ */