<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MSession
 *
 * @version     1.0.0
 */
class MSession extends BSession
{
    public function __construct()
    {
        parent::__construct();

        $this->Start_session();

        $this->session_id = session_id();

        $this->Set_data();
        
        if( ! $this->Check_if_valid() )
        {
            $this->Create();
        }
        
        $this->Refresh_data();
    }

    private function Start_session()
    {
        try{
            session_start();
        }
        catch( Exception $e )
        {
            session_regenerate_id();
            session_start();
        }
    }

    /**
     * @method  Set_data
     * @access  public
     * @desc    This method sets the data variable with old session data
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    private function Set_data()
    {
        $this->data =& $_SESSION;

        if( empty( $_SERVER["HTTP_USER_AGENT"] ) )
        {
            $_SERVER["HTTP_USER_AGENT"] = "Ismeretlen bot";
        }
    }

    /**
     * @method  Check_if_valid
     * @access  public
     * @desc    This method checks if we have a good session, that match ip, agent, and it is not expired
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    private function Check_if_valid()
    {
        if( $this->match_ip && 
            ( ! isset( $this->data["ip_address"] ) || $this->data["ip_address"] != @$_SERVER["REMOTE_ADDR"] ) )
        {
            return FALSE;
        }
        
        if( $this->match_agent && 
            ( ! isset( $this->data["agent"] ) || $this->data["agent"] != @$_SERVER["HTTP_USER_AGENT"] ) )
        {
            return FALSE;
        }
        
        if( ! isset( $this->data["session_time"] ) )
        {
            return FALSE;
        }

        if( $this->expiration > 0 && $this->data["session_time"] < ( time() - $this->expiration ) )
        {
            return FALSE;
        }
        
        // everything is ok
        return TRUE;
    }

    /**
     * @method  Create
     * @access  public
     * @desc    This method create a new session id and set new data
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    private function Create()
    {
        // Start session if it does not started
        if( ! session_id() )
        {
            $this->Start_session();

            $this->session_id = session_id();
        }
        
        $this->Set_data();
    }

    /**
     * @method  Refresh_data
     * @access  public
     * @desc    This method refresh the flashdata in new session, and refresh agent, ip address and time
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    private function Refresh_data()
    {
        $this->Increment_flash_data();
        
        $this->data["ip_address"]   = ( ! empty( $_SERVER["REMOTE_ADDR"] ) ? $_SERVER["REMOTE_ADDR"] : "" );
        $this->data["agent"]        = ( ! empty( $_SERVER["HTTP_USER_AGENT"] ) ? $_SERVER["HTTP_USER_AGENT"] : "Ismeretlen bot" );
        $this->data["session_time"] = time();
    }

    /**
     * @method  Increment_flash_data
     * @access  public
     * @desc    This method increments the level for old datas in flashdata, and if the level is greather than 1 than deletes them
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    private function Increment_flash_data()
    {
        if( ! isset( $this->data["flash"] ) )
        {
            return;
        }

        foreach( $this->data["flash"] as $key => $flash_data )
        {
            $this->data["flash"][$key]["level"] += 1;

            if( $this->data["flash"][$key]["level"] > 1 )
            {
                unset( $this->data["flash"][$key] );
            }
        }
    }

    /**
     * @method  Set
     * @access  public
     * @desc    Set new data into session
     * @author  Cousin Bela
     *
     * @param   string                      $key                        - name of the new variable
     * @param   string                      $value                      - value of new variable
     *
     * @version 1.0
     * @return  bool
     */
    public function Set( $key, $value )
    {
        if( $key == "flash" )
        {
            return FALSE;
        }

        $this->data[$key] = $value;

        return TRUE;
    }

    /**
     * @method  Remove
     * @access  public
     * @desc    Unset new data into session
     * @author  Cousin Bela
     *
     * @param   string                      $key                        - name of the old variable
     *
     * @version 1.0
     * @return  bool
     */
    public function Remove( $key )
    {
        if( $key == "flash" )
        {
            return FALSE;
        }

        unset( $this->data[$key] );
        unset( $_SESSION[$key] );

        return TRUE;
    }

    /**
     * @method  Get
     * @access  public
     * @desc    Returns tha value for a variable if exists with specified name
     * @author  Cousin Bela
     *
     * @param   string                      $key                        - the name for variable to return
     *
     * @version 1.0
     * @return  mixed
     * @throws  MException
     */
    public function Get( $key )
    {
        if( ! isset( $this->data[$key] ) )
        {
            throw new MProperty_not_exists_exception( "Variable ".$key." not found in session" );
        }
        
        return $this->data[$key];
    }

    /**
     * @method  Has
     * @access  public
     * @desc    Checks if a variable exists in session
     * @author  Cousin Bela
     *
     * @param   string                      $key                        - the name of variable to check
     *
     * @version 1.0
     * @return  bool
     */
    public function Has( $key )
    {
        if( ! isset( $this->data[$key] ) )
        {
            return FALSE;
        }
        
        return TRUE;
    }

    /**
     * @method  Set_flash
     * @access  public
     * @desc    Set new variable in flashdata that will expire after 2 refresh
     * @author  Cousin Bela
     *
     * @param   string                      $key                        - the type to send the classname
     * @param   string                      $value                      - content to be cached
     *
     * @version 1.0
     * @return  bool
     */
    public function Set_flash( $key, $value )
    {
        $_SESSION["flash"][$key] = $this->data["flash"][$key] = array(
            "level" => 0,
            "value" => $value
        );

        return TRUE;
    }

    /**
     * @method  Remove_flash
     * @access  public
     * @desc    Unset new data into session
     * @author  Cousin Bela
     *
     * @param   string                      $key                        - name of the old variable
     *
     * @version 1.0
     */
    public function Remove_flash( $key )
    {
        unset( $this->data["flash"][$key] );
        unset( $_SESSION["flash"][$key] );
    }

    /**
     * @method  Get_flash
     * @access  public
     * @desc    Returns value of variable from flashdata or throws an error if not exists
     * @author  Cousin Bela
     *
     * @param   string                      $key                        - name of the variable to return
     *
     * @version 1.0
     * @return  mixed
     * @throws  MException
     */
    public function Get_flash( $key )
    {
        if( ! isset( $this->data["flash"][$key] ) || $this->data["flash"][$key]["level"] != 1 )
        {
            throw new MProperty_not_exists_exception( "Variable ".$key." not found in session" );
        }
        
        return $this->data["flash"][$key]["value"];
    }

    /**
     * @method  Get_flash_without_checking_level
     * @access  public
     * @desc    Returns value of variable from flashdata or throws an error if not exists
     * @author  Cousin Bela
     *
     * @param   string                      $key                        - name of the variable to return
     *
     * @version 1.0
     * @return  mixed
     * @throws  MException
     */
    public function Get_flash_without_checking_level( $key )
    {
        if( ! isset( $this->data["flash"][$key] ) )
        {
            throw new MProperty_not_exists_exception( "Variable ".$key." not found in session" );
        }

        return $this->data["flash"][$key]["value"];
    }

    /**
     * @method  Has_flash
     * @access  public
     * @desc    Check if variable exists in flashdata or not
     * @author  Cousin Bela
     *
     * @param   string                      $key                        - The key to check if exists
     *
     * @version 1.0
     * @return  bool
     */
    public function Has_flash( $key )
    {
        if( ! isset( $this->data["flash"][$key] ) || $this->data["flash"][$key]["level"] != 1 )
        {
            return FALSE;
        }
        
        return TRUE;
    }

    /**
     * @method  Has_flash_without_checking_level
     * @access  public
     * @desc    Check if variable exists in flashdata or not
     * @author  Cousin Bela
     *
     * @param   string                      $key                        - The key to check if exists
     *
     * @version 1.0
     * @return  bool
     */
    public function Has_flash_without_checking_level( $key )
    {
        if( ! isset( $this->data["flash"][$key] ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Destroy
     * @access  public
     * @desc    Unset new data into session
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Destroy()
    {
        $_SESSION = array();

        return TRUE;
    }

    /**
     * @method  Get_id
     * @access  public
     * @desc    Returns the session id
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public function Get_id()
    {
        return $this->session_id;
    }
}

/* End of file Session.php */
/* Location: ./core/Libraries/ */