<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Image_GD2
 *
 * @version     1.0.0
 */
class Image_GD2
{
    private $width              = 0;
    private $height             = 0;
    private $crop               = FALSE;
    private $crop_from_x        = "auto";
    private $crop_from_y        = "auto";
    private $crop_original      = FALSE;
    private $watermark          = FALSE;
    private $watermark_file     = "";
    private $watermark_percent  = "original";
    private $watermark_position = "center";

    private $transparent        = TRUE;

    private $scaled_width;
    private $scaled_height;
    private $destination_image;
    private $source_image;
    private $image_content_type;
    private $watermark_from_x   = 0;
    private $watermark_from_y   = 0;

    public function __construct()
    {
        $this->transparent = $this->Get_transparency_from_global_settings();
    }

    private function Get_transparency_from_global_settings()
    {
        $image_config = App()->config->image;

        if( ! empty( $image_config["transparent"] ) )
        {
            return $image_config["transparent"];
        }

        return FALSE;
    }

    public function Create_thumbnail_from_template( $source_filename, $destination_filename, & $template = null )
    {
        Logger_helper::Debug( "Create thumbnail from template" );
        Logger_helper::Debug( "source filename: ". $source_filename );
        Logger_helper::Debug( "destination filename: ". $destination_filename );

        $this->Set_template( $template );

        list( $width_orig, $height_orig ) = getimagesize( $source_filename );

        $do_bigger_than_original = $this->crop ? TRUE : FALSE;

        $this->Calculate_new_width_and_height( $width_orig, $height_orig, $do_bigger_than_original );

        Logger_helper::Debug( "Width and height for image calculated" );

        $this->destination_image    = imagecreatetruecolor( $this->scaled_width, $this->scaled_height );
        try
        {
            $this->source_image = $this->Image_create_by_extension($source_filename);
        }
        catch( Exception $e )
        {
            Logger_helper::Error( $e->getMessage() );
            return "";
        }

        $this->Create_transparent_image( $this->destination_image );

        if( ! imagecopyresampled(
                $this->destination_image, $this->source_image,
                0, 0,
                0, 0,
                $this->scaled_width, $this->scaled_height,
                $width_orig, $height_orig
            ) 
        )
        {
            throw new MPHP_exception( E_ERROR, "Error while creating thumbnail" );
        }

        $this->Crop();
        $this->Watermark();

        return $this->Image_output( $this->destination_image, $destination_filename );
    }

    private function Set_template( & $template = null )
    {
        if( empty( $template ) )
        {
            Logger_helper::Debug( "Template empty" );
            return;
        }

        $this->width                = $this->Get_width_from_template( Variable_helper::Has_value( $template, "width" ) );
        $this->height               = $this->Get_height_from_template( Variable_helper::Has_value( $template, "height" ) );
        $this->crop                 = Variable_helper::Has_value( $template, "crop" );
        $this->crop_from_x          = Variable_helper::Has_value( $template, "crop_from_x" );
        $this->crop_from_y          = Variable_helper::Has_value( $template, "crop_from_y" );
        $this->crop_original        = Variable_helper::Has_value( $template, "crop_original" );
        $this->watermark            = Variable_helper::Has_value( $template, "watermark" );
        $this->watermark_percent    = Variable_helper::Has_value( $template, "watermark_percent" );
        $this->watermark_file       = Image_helper::Get_image_path( Variable_helper::Has_value( $template, "watermark_file" ) );
        $this->watermark_position   = Variable_helper::Has_value( $template, "watermark_position" );

        $this->scaled_height        = $this->height;
        $this->scaled_width         = $this->width;

        Logger_helper::Debug( "Template set" );
    }

    private function Calculate_new_width_and_height( $width_orig, $height_orig, $do_bigger_than_original = TRUE )
    {
        $scale = $this->Get_scale( $width_orig, $height_orig );

        if( $scale === FALSE )
        {
            return;
        }

        $this->Check_widths_and_heights( $scale );

        if( $do_bigger_than_original )
        {
            if( $this->scaled_width / $this->scaled_height > $scale )
            {
                $this->scaled_height = round( $this->scaled_width / $scale );
            }
            else
            {
                $this->scaled_width = round( $this->scaled_height * $scale );
            }
            Logger_helper::Debug( "Scaled width and height set: ".$this->scaled_width." x ".$this->scaled_height );
        }
        else
        {
            // recalculate scale
            $scale      = $this->width / $this->height;
            $scale_orig = $width_orig / $height_orig;

            if( $scale_orig > $scale )
            {
                $this->scaled_width = $this->width;
                $this->scaled_height = round( ( $this->width * $height_orig ) / $width_orig );
            }

            elseif( $scale_orig < $scale )
            {
                $this->scaled_width = round( ( $this->height * $width_orig ) / $height_orig );
                $this->scaled_height = $this->height;
            }

            elseif( $scale_orig == $scale )
            {
                $this->scaled_width = $this->width;
                $this->scaled_height = $this->height;
            }
            Logger_helper::Debug( "Scaled width and height recalculated: ".$this->scaled_width." x ".$this->scaled_height );
        }
    }

    protected function Get_scale( $width_orig, $height_orig )
    {
        if( $this->crop_original )
        {
            $this->scaled_width   = $width_orig;
            $this->scaled_height  = $height_orig;

            return FALSE;
        }

        if( ( $this->scaled_width == 0 && $this->scaled_height == 0 ) )
        {
            $this->scaled_width   = $width_orig;
            $this->scaled_height  = $height_orig;
        }

        $scale = $width_orig / $height_orig;

        Logger_helper::Debug( "Scale calculated: ".$scale );

        return $scale;
    }

    protected function Check_widths_and_heights( $scale )
    {
        // If calculated scaled width is 0 than recalculate it by scaled height and new scale
        if( $this->scaled_width == 0 )
        {
            $this->scaled_width = round( $this->scaled_height * $scale );
        }

        // If calculated scaled height is 0 than recalculate it by scaled width and new scale
        if( $this->scaled_height == 0 )
        {
            $this->scaled_height = round( $this->scaled_width / $scale );
        }

        // If the height in template is set to 0, we have to calculate it automatically
        if( $this->height == 0 )
        {
            $this->height = $this->scaled_height;
        }

        // If the width in template is set to 0, we have to calculate it automatically
        if( $this->width == 0 )
        {
            $this->width = $this->scaled_width;
        }

        Logger_helper::Debug( "Width and height checked" );
    }
    
    private function Create_transparent_image( $image )
    {
        if( ! $this->transparent )
        {
            Logger_helper::Debug( "Transparency not set" );

            return;
        }

        Logger_helper::Debug( "Set transparency" );

        imagealphablending( $image, TRUE );

        Logger_helper::Debug( "Image alpha blending OK" );

        // Allocate a transparent color and fill the new image with it.
        // Without this the image will have a black background instead of being transparent.
        $transparent = imagecolorallocatealpha( $image, 0, 0, 0, 127 );
        imagefill( $image, 0, 0, $transparent );

        Logger_helper::Debug( "Image fill OK" );

        // save the alpha (transparency) if exists
        imagesavealpha( $image, TRUE );

        Logger_helper::Debug( "Image save alpha OK" );
    }

    private function Image_create_by_extension( $filename )
    {
        $this->image_content_type = pathinfo( $filename, PATHINFO_EXTENSION );

        Logger_helper::Debug( "Image create by extension. Type: ".$this->image_content_type." and file name: ".$filename );

        switch( $this->image_content_type ){
            case "jpg"  :
            case "jpeg" :
                /*todo: van néhány kép amire kiakad a függvény.. meg kell nézni, hogy van rá esetleg megoldás*/
                return imagecreatefromjpeg( $filename );

            case "gif" :
                return imagecreatefromgif( $filename );

            case "png" :
                return imagecreatefrompng( $filename );

            case "bmp" :
                return imagecreatefromwbmp( $filename );

            default:
                return imagecreatefromjpeg( $filename );
        }
    }

    private function Crop()
    {
        if( ! $this->crop )
        {
            return;
        }

        $crop_destination_image = imagecreatetruecolor( $this->width, $this->height );

        if( $this->crop_from_x == "auto" )
        {
            $this->crop_from_x = ( $this->scaled_width - $this->width ) / 2;
        }

        if( $this->crop_from_y == "auto" )
        {
            $this->crop_from_y = ( $this->scaled_height - $this->height ) / 2;
        }

        Logger_helper::Debug( "Crop image: from x: ". $this->crop_from_x." and from y: ".$this->crop_from_y );

        imagecopy(
            $crop_destination_image, $this->destination_image, // source image will be the new resized image
            0, 0,
            $this->crop_from_x, $this->crop_from_y,
            $this->scaled_width, $this->scaled_height
        );

        $this->destination_image = $crop_destination_image;
    }

    private function Watermark()
    {
        if( empty( $this->watermark ) || empty( $this->watermark_file ) )
        {
            return;
        }

        Logger_helper::Debug( "Adding watermark: ". $this->watermark_file );

        $this->Set_watermark_size_by_percent();

        $new_watermark_image = $this->Get_watermark_image();

        $this->Position_watermark();

        imagecopy(
            $this->destination_image, $new_watermark_image,
            $this->watermark_from_x, $this->watermark_from_y,
            0, 0,
            $this->scaled_width, $this->scaled_height
        );
    }

    private function Position_watermark()
    {
        switch( $this->watermark_position )
        {
            case "top-left":
                break;

            case "top-center":
                $this->watermark_from_x = ( $this->width - $this->scaled_width ) / 2;
                break;

            case "top-right":
                $this->watermark_from_x = $this->width - $this->scaled_width;
                break;

            case "left-center":
                $this->watermark_from_y = ( $this->height - $this->scaled_height ) / 2;
                break;

            case "right-center":
                $this->watermark_from_x = $this->width - $this->scaled_width;
                $this->watermark_from_y = ( $this->height - $this->scaled_height ) / 2;
                break;

            case "bottom-left":
                $this->watermark_from_y = $this->height - $this->scaled_height;
                break;

            case "bottom-center":
                $this->watermark_from_x = ( $this->width - $this->scaled_width ) / 2;
                $this->watermark_from_y = $this->height - $this->scaled_height;
                break;

            case "bottom-right":
                $this->watermark_from_x = $this->width - $this->scaled_width;
                $this->watermark_from_y = $this->height - $this->scaled_height;
                break;

            default: // absolute centered watermark
                $this->watermark_from_x = ( $this->width - $this->scaled_width ) / 2;
                $this->watermark_from_y = ( $this->height - $this->scaled_height ) / 2;
        }

        Logger_helper::Debug( "Watermark position: x: ". $this->watermark_from_x." and y: ".$this->watermark_from_y );
    }

    private function Set_watermark_size_by_percent()
    {
        if( empty( $this->watermark_percent ) )
        {
            return;
        }

        list( $width_orig, $height_orig ) = getimagesize( $this->watermark_file );

        $this->Calculate_new_width_and_height( $width_orig, $height_orig, FALSE );

        switch( $this->watermark_percent )
        {
            case "original":
                $this->scaled_width     = $width_orig;
                $this->scaled_height    = $height_orig;
                break;

            case "fit": // automatically calculated in function Calculate_new_width_and_height
                break;

            case "auto":
                if( $width_orig <= $this->width && $height_orig <= $this->height )
                {
                    $this->scaled_width  = $width_orig;
                    $this->scaled_height = $height_orig;
                }
                break;

            default:
                $this->scaled_width     *= $this->watermark_percent / 100;
                $this->scaled_height    *= $this->watermark_percent / 100;
        }

        Logger_helper::Debug( "Watermark size: ". $this->scaled_width." x ".$this->scaled_height );
    }

    private function Get_watermark_image()
    {
        list( $width_orig, $height_orig ) = getimagesize( $this->watermark_file );

        $watermark_file_name = dirname( $this->watermark_file )."/";
        $watermark_file_name .= $this->scaled_width."z".$this->scaled_height."_";
        $watermark_file_name .= basename( $this->watermark_file );

        Logger_helper::Debug( "Watermark file name: ". $watermark_file_name );

        if( ! file_exists( $watermark_file_name ) )
        {
            Logger_helper::Debug( "Watermark file not exists, create it" );

            $new_watermark_image    = imagecreatetruecolor( $this->scaled_width, $this->scaled_height );
            $source_watermark_image = $this->Image_create_by_extension( $this->watermark_file );

            $this->Create_transparent_image( $new_watermark_image );

            if( ! imagecopyresampled(
                    $new_watermark_image, $source_watermark_image,
                    0, 0,
                    0, 0,
                    $this->scaled_width, $this->scaled_height,
                    $width_orig, $height_orig
                )
            )
            {
                throw new MPHP_exception( E_ERROR, "Error while creating thumbnail for watermark" );
            };

            $this->Image_output( $new_watermark_image, $watermark_file_name );
        }
        else
        {
            $new_watermark_image = $this->Image_create_by_extension( $watermark_file_name );
        }

        return $new_watermark_image;
    }

    private function Image_output( & $source, $filename )
    {
        $function = "image";

        switch( $this->image_content_type ){
            case "jpeg" :
            case "jpg"  :
                $function .= "jpeg";
                break;

            case "gif" :
                $function .= "gif";
                break;

            case "png" :
                $function .= "png";
                break;

            case "bmp" :
                $function .= "wbmp";
                break;

            default: $function .= "jpeg";
        }

        if( ! function_exists( $function ) )
        {
            return FALSE;
        }

        return $function( $source, $filename );
    }

    public function Set_transparency( $transparent )
    {
        $this->transparent = $transparent;
    }

    public function Set_width( $width )
    {
        $this->width = $width;
    }

    public function Set_height( $height )
    {
        $this->height = $height;
    }

    public function Set_watermark_file( $file_path )
    {
        $this->watermark_file = $file_path;
    }

    public function Set_destination_image( $destination_image_path )
    {
        $this->destination_image = $destination_image_path;
    }

    public function Set_source_image( $source_image_path )
    {
        $this->source_image = $source_image_path;
    }

    public function Set_crop_from_x( $crop_from_x )
    {
        $this->crop_from_x = $crop_from_x;
    }

    public function Set_crop_from_y( $crop_from_y )
    {
        $this->crop_from_y = $crop_from_y;
    }

    private function Get_width_from_template( $width )
    {
        if( empty( $width ) )
        {
            return 0;
        }

        if( $width == "auto" )
        {
            return 0;
        }

        return $width;
    }

    private function Get_height_from_template( $height )
    {
        if( empty( $height ) )
        {
            return 0;
        }

        if( $height == "auto" )
        {
            return 0;
        }

        return $height;
    }
}

/* End of file Image_GD2.php */
/* Location: ./core/Libraries/Images/ */