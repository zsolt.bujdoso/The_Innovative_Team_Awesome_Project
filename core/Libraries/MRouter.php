<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MRouter
 *
 * @version     1.0.0
 */
class MRouter extends BRouter
{
    protected $parameters;
    protected $url;
  
    public function __construct()
    {
        parent::__construct();
        
        $this->module = "";
        $this->controller = App()->config->url["default_controller"];
        $this->function = App()->config->url["default_function"];
        
        $this->Process();
    }

    private function Process()
    {
        $this->url = App()->request->Get_url();

        $this->Check_routes();
        $this->Calculate_parameters();
        $this->Calculate_module();

        $this->Calculate( $this->controller );
        if( empty( $this->controller ) )
        {
            if( ! empty( App()->config->url["default_module"] ) )
            {
                $this->module = App()->config->url["default_module"];
            }
            $this->controller = App()->config->url["default_controller"];
        }

        $this->Calculate( $this->function );
        if( empty( $this->function ) )
        {
            $this->function = App()->config->url["default_function"];
        }
    }
    
    private function Check_routes()
    {
        if( empty( App()->config->url["routes"] ) || ! is_array( App()->config->url["routes"] ) )
        {
            return;
        }

        $routes = App()->config->url["routes"];
        if( $this->Check_routes_from_config( $routes ) )
        {
            return;
        }

        $routes = App()->config->url["default_routes"];
        $this->Check_routes_from_config( $routes );
    }

    private function Check_routes_from_config( & $routes )
    {
        foreach( $routes as $expression => $route )
        {
            if( preg_match( "/^".$expression."$/", $this->url ) )
            {
                $this->url = preg_replace( "/".$expression."/", $route, $this->url );
                return TRUE;
            }
        }

        return FALSE;
    }

    private function Calculate_parameters()
    {
        $this->parameters = explode( "/", $this->url );
    }
    
    private function Calculate_module()
    {
        if( isset( $this->parameters[0] ) && in_array( $this->parameters[0], App()->config->module ) )
        {
            $this->module = $this->parameters[0];
            
            $this->Slice_first_parameter_from_url();
        }
    }
    
    private function Calculate( & $element )
    {
        if( isset( $this->parameters[0] ) )
        {
            $element = $this->parameters[0];
            
            $this->Slice_first_parameter_from_url();
        }
    }
    
    private function Slice_first_parameter_from_url()
    {
        if( sizeof( $this->parameters ) > 1 )
        {
            $this->parameters = array_splice( 
                                  $this->parameters, 
                                  1, 
                                  sizeof( $this->parameters ) - 1 
                              );
        }
        else
        {
            $this->parameters = array();
        }
    }

    /**
     * @method  Get_module
     * @access  public
     * @desc    This method returns the name of the module, if it is a module
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public function Get_module()
    {
        return $this->module;
    }

    /**
     * @method  Get_controller
     * @access  public
     * @desc    This method returns the name of the controller
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public function Get_controller()
    {
        return $this->controller;
    }

    /**
     * @method  Get_function
     * @access  public
     * @desc    This method returns the name of the function to executein controller
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public function Get_function()
    {
        return $this->function;
    }

    /**
     * @method  Get_parameters
     * @access  public
     * @desc    This method returns all parameters after removed the module, controller and action
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  array
     */
    public function Get_parameters()
    {
        return $this->parameters;
    }
}