<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MModule
 *
 * @version 1.0.0
 */
class MModule extends Base implements IModule
{
    protected $id;
    protected $path;

    public function __construct()
    {
        parent::__construct();

        $this->Calculate_id();
        $this->Calculate_path();
    }

    public function Set_import()
    {
        return array();
    }

    public function Init()
    {
    }

    /**
     * @method  Calculate_id
     * @access  private
     * @desc    Returns the name of selected module
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    private function Calculate_id()
    {
        $class_name = get_class( $this );

        $this->id = strtolower( $class_name );
    }

    /**
     * @method  Get_id
     * @access  public
     * @desc    Returns the name of selected module
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public function Get_id()
    {
        return $this->id;
    }

    /**
     * @method  Calculate_path
     * @access  public
     * @desc    Returns the path for selected module
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public function Calculate_path()
    {
        $class_name = $this->Get_id();
        $class_name = ucfirst( $class_name );

        if( ! isset( App()->classmap[$class_name] ) )
        {
            return "";
        }

        $path = App()->classmap[$class_name];

        $this->path = dirname( $path ).DIRECTORY_SEPARATOR;
    }

    /**
     * @method  Get_path
     * @access  public
     * @desc    Returns the path for selected module
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public function Get_path()
    {
        return $this->path;
    }
}

/* End of file Web_application.php */
/* Location: ./core/Base/ */