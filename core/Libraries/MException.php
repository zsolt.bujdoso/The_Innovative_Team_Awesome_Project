<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @propery Logger $logger
 *
 * @version     1.0.0
 */
class MException extends BException
{
    const ERROR   = "error";
    const WARNING = "warning";

    protected $exception_type           = "";
    protected $exception_type_message   = "Exception encountered";

    public function __construct( $message = "", $exception_type = "error" )
    {
        parent::__construct();

        $this->message          = $message;
        $this->exception_type   = $exception_type;
    }
    
    public function Handle( $exception_type = "error" )
    {
        if( empty( $exception_type ) )
        {
            $exception_type = $this->exception_type;
        }

        switch( $exception_type )
        {
            case MException::ERROR:
                $this->Handle_error();
                break;

            case MException::WARNING:
                $this->Handle_warning();
                break;

            default:
                break;
        }
    }

    /**
     * @method	Handle_error
     * @access	protected
     * @desc    this method shows an error when a mail error occured
     * @author	Cousin Béla
     *
     * @version	1.0.1
     */
    protected function Handle_error()
    {
        $APP = App();

        if( is_object( $APP ) )
        {
            $renderer       = $APP->Get_instantiated_object( "renderer" );
            $this->backtrace= print_r( debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS ), TRUE );

            if( ! empty( $renderer ) && is_object( $renderer ) )
            {
                if( ! $this->Handle_debugged() )
                {
                    $this->Handle_by_code( $this->error_code );
                }
            }
            else
            {
                $this->message_with_template = $this->exception_type_message.":<br /><br />"
                    . "<pre>"
                        .$this->message
                    ."</pre>"
                    . "<br /><br />".
                    ( $this->Is_on_debug() ? "<pre>".$this->backtrace."</pre>" : "" );
            }
        }

        // log the error message
        $logger = App()->Get_instantiated_object( "logger" );
        if( is_object( $logger ) )
        {
            $logger->Error( $this->message );
        }
    }

    /**
     * @method	Handle_debugged
     * @access	protected
     * @desc    Handle the error message and create a message to show as debugged
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    protected function Handle_debugged()
    {
        if( ! $this->Is_on_debug() )
        {
            return FALSE;
        }

        $this->message_with_template = App()->renderer->Load_partial_view(
            App()->config->error["debug"],
            array( "error_message" => $this->message )
        );

        return TRUE;
    }

    /**
     * @method	Handle_by_code
     * @access	protected
     * @desc    Handle the error message and create a message to show as handled
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    protected function Handle_by_code( $error_code )
    {
        $this->message_with_template = App()->renderer->Load_partial_view(
            App()->config->error[$error_code],
            array( "error_message" => $this->message )
        );
    }

    /**
     * @method	Handle_warning
     * @access	protected
     * @desc    this method shows a warning
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    protected function Handle_warning()
    {
        $logger = App()->Get_instantiated_object( "logger" );
        if( is_object( $logger ) )
        {
            $logger->Warning( $this->message );
        }
    }
}

/* End of file Exception.php */
/* Location: ./core/Libraries/ */