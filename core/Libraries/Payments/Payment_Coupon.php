<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Payment_Coupon
 *
 * @version 1.0.0
 */
class Payment_Coupon extends BPayment_type implements IPayment
{
    private $coupon_details;

    public function __construct()
    {
        $this->type         = "coupon";

        parent::__construct();

        Logger_helper::Debug( "Coupon payment class initialized" );
    }

    public function Set_express_checkout_details( & $order )
    {
        Logger_helper::Debug( "Coupon payment - setting express checkout, getting the token" );

        $this->Set_details_into_data( $order );

        $this->status           = TRUE;
        $this->correlation_id	= "Success";
        $this->token	        = "Success";
        return $this->Get_status();
    }

    private function Set_details_into_data( & $order )
    {
        App()->controller->data["payment_url"] = MY_Url_helper::Site_url()
                                                    .Variable_helper::Has_value( $this->all_config, "express_checkout_success_url" )
                                                    ."?order_id=".$order->id."&payment_system=".$this->type;
        App()->controller->data["payment_data"]= '
            <h2>'.sprintf( App()->lang->Get( "Pay_with_coupon" ), $order->id ).'</h2>
            <p>'.App()->lang->Get( "Enter_coupon_for_payment" ).'</p>
            <div class="write-coupon-code">
                <div class="input-text">
                    <div>
                        <input id="coupon_code" type="text" name="coupon_code" value="" />
                    </div>
                </div>
            </div>
        ';
    }

    public function Get_express_checkout_details( & $order )
    {
        Logger_helper::Debug( "Coupon payment - getting express checkout" );

        $coupon_code            = ( ! empty( $order->coupon_code ) ? $order->coupon_code : "" );
        if( Post_helper::Get( "coupon_code" ) )
        {
            if( ! $this->Validate_coupon_code() )
            {
                App()->redirect->To_url(
                    MY_Url_helper::Site_url()."orders/pay/checkout/"
                    ."?order_id=".$order->id."&payment_system=".$this->type
                );
            }
            $coupon_code        = Post_helper::Get( "coupon_code" );
        }
        $coupon_details         = $this->Load_coupon_details( $coupon_code );

        if( ! $this->Is_coupon_valid( $order, $coupon_details ) )
        {
            Message_helper::Add_error_flash_message( App()->lang->Get( "Coupon_not_found" ) );
            App()->redirect->To_url(
                MY_Url_helper::Site_url()."orders/pay/checkout/"
                ."?order_id=".$order->id."&payment_system=".$this->type
            );
        }

        $this->coupon_details   = $coupon_details;
        $this->status           = TRUE;
        $this->correlation_id	= "CouponSuccess";
        return $this->Get_status();
    }

    private function Validate_coupon_code()
    {
        $coupon_code = Post_helper::Get( "coupon_code" );
        $validation_details = Validation_helper::Validate_code( $coupon_code );

        if( $validation_details !== TRUE )
        {
            Message_helper::Add_error_flash_message( $validation_details );
            return FALSE;
        }

        return TRUE;
    }

    private function Load_coupon_details( $coupon_code )
    {
        if( empty( $coupon_code ) )
        {
            return FALSE;
        }

        return Coupons_model::Get_model()->Get_by_item_code_and_domain_for_payment(
            $coupon_code,
            App()->controller->domain_id
        );
    }

    private function Is_coupon_valid( & $order, & $coupon_details )
    {
        if( empty( $coupon_details ) )
        {
            Logger_helper::Error( "Coupon payment - coupon not found" );
            return FALSE;
        }

        if( ! $coupon_details->is_enabled && ! User_helper::Is_admin_logged_in() )
        {
            Logger_helper::Error( "Coupon payment - coupon not enabled" );
            return FALSE;
        }

        if( strtotime( $coupon_details->date_start ) > time() || strtotime( $coupon_details->date_finish ) < time() )
        {
            Logger_helper::Error( "Coupon payment - date is not between coupon date start and finish" );
            return FALSE;
        }

        if( $coupon_details->cash <= 0 )
        {
            Logger_helper::Error( "Coupon payment - cash not set for coupon" );
            return FALSE;
        }

        if( $coupon_details->cash < $order->total_price )
        {
            Logger_helper::Error( "Coupon payment - total price is bigger than coupon gives" );
            return FALSE;
        }

        if( $coupon_details->usable == 0 || ( $coupon_details->used >= $coupon_details->usable && $coupon_details->usable != -1 ) )
        {
            Logger_helper::Error( "Coupon payment - coupon has used as max times is set" );
            return FALSE;
        }

        return TRUE;
    }

    public function Do_express_checkout( & $order )
    {
        Logger_helper::Debug( "Coupon payment - do express checkout" );
        Logger_helper::Info( "Coupon payment - express checkout success" );

        if( ! User_helper::Is_admin_logged_in() && $this->is_live )
        {
            $data = array(
                "used"        => $this->coupon_details->used + 1,
                "modify_date" => Date_helper::Get_date_time()
            );
            Coupons_model::Get_model()->Update_by_primary_key( $data, $this->coupon_details->id );
        }

        $this->status           = TRUE;
        $this->correlation_id	= "CouponSuccess";
        $this->transaction_id	= Post_helper::Get( "coupon_code" );
        $this->Create_success_result();

        return $this->Get_status();
    }

    private function Create_success_result( $transaction_type = "express" )
    {
        $this->result = array(
            "CORRELATIONID"     => $this->correlation_id,
            "TRANSACTIONID"     => $this->transaction_id,
            "TRANSACTIONTYPE"   => $transaction_type,
            "PAYMENTTYPE"       => $this->type,
            "PAYMENTMETHODTYPE" => "",
            "PAYMENTSTATUS"     => "CouponSuccess",
        );
    }

    public function Do_direct_payment( & $order, $card_details )
    {
        Logger_helper::Debug( "Coupon payment - do direct payment started" );

        $this->status           = FALSE;
        $this->correlation_id	= "";
        $this->transaction_id	= "";

        return $this->Get_status();
    }
}

/* End of file Payment_Coupon.php */
/* Location: ./core/Libraries/Payments/ */