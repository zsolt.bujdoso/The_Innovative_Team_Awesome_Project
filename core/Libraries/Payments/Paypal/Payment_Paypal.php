<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Payment_Paypal
 *
 * @version 1.0.0
 */
class Payment_Paypal extends BPayment_type
{
    protected $payer_id;

    public function __construct()
    {
        $this->type         = "paypal";

        parent::__construct();

        Logger_helper::Debug( "PayPal payment class initialized" );
    }

    public function Set_express_checkout_details( & $order )
    {
        Logger_helper::Info( "PayPal payment - setting express checkout started" );

        if( ! $this->is_on )
        {
            Logger_helper::Info( "PayU payment - setting express checkout error because is turned off" );

            return FALSE;
        }

        Logger_helper::Debug( "PayPal payment - setting express checkout, getting the token" );

        $express_checkout_success_url   = Variable_helper::Has_value( $this->all_config, "express_checkout_success_url" );
        $express_checkout_success_url   .= "?order_id=".$order->id."&payment_system=".$this->type;
        $express_checkout_cancel_url    = Variable_helper::Has_value( $this->all_config, "express_checkout_cancel_url" );
        $express_checkout_cancel_url    .= "?order_id=".$order->id."&payment_system=".$this->type;
        $base_url                       = MY_Url_helper::Site_url();

        // creating string for request
        $nvp_string	= "&PAYMENTREQUEST_0_AMT=".urlencode( $order->total_price )
            ."&PAYMENTREQUEST_0_PAYMENTACTION=Sale"
            ."&PAYMENTREQUEST_0_CURRENCYCODE=".urlencode( $order->currency )
            ."&RETURNURL=".urlencode( $base_url.$express_checkout_success_url )
            ."&CANCELURL=".urlencode( $base_url.$express_checkout_cancel_url )
            ."&PAYMENTACTION=Sale"
            ."&AMT=".urlencode( $order->total_price )
            ."&CURRENCYCODE=".urlencode( $order->currency );

        Logger_helper::Debug( "PayPal payment - created nvp string without user details:<br />".$nvp_string );

        // call the method on paypal
        $this->Send_data_and_get_result( "SetExpressCheckout", $nvp_string );

        if( ! $this->Is_success_result() )
        {
            $this->Set_error_message();
            return FALSE;
        }

        $this->status           = TRUE;
        $this->correlation_id	= Variable_helper::Has_value( $this->result, "CORRELATIONID" );
        $this->token	        = Variable_helper::Has_value( $this->result, "TOKEN" );

        return $this->Redirect_to_checkout_url();
    }

    private function Send_data_and_get_result( $method_name, $nvp_string )
    {
        Logger_helper::Debug( "PayPal payment - sending data to method: ".$method_name );

        // get the details for connecting to paypal servers
        $API_UserName	= Variable_helper::Has_value( $this->config, "paypal_api_username" );
        $API_Password	= Variable_helper::Has_value( $this->config, "paypal_api_password" );
        $API_Signature	= Variable_helper::Has_value( $this->config, "paypal_api_signature" );
        $API_Endpoint 	= Variable_helper::Has_value( $this->config, "endpoint" );
        $API_Version    = Variable_helper::Has_value( $this->payment_config, "version" );

        if( empty( $API_UserName ) || empty( $API_Password ) || empty( $API_Signature ) )
        {
            Logger_helper::Error( "PayPal payment - user, pass or signature missing" );
            return FALSE;
        }

        // NVPRequest for submitting to server
        $nvp_request = "METHOD=".urlencode( $method_name )
            ."&VERSION=".urlencode( $API_Version )
            ."&PWD=".urlencode( $API_Password )
            ."&USER=".urlencode( $API_UserName )
            ."&SIGNATURE=".urlencode( $API_Signature ).$nvp_string;

        // Create curl with post parameters
        $ch = $this->Create_curl( $API_Endpoint, $nvp_request );

        // setting the nvpreq as POST FIELD to curl
        /*curl_setopt( $ch, CURLOPT_POSTFIELDS, $nvp_request );*/

        // getting response from server
        $response       = curl_exec( $ch );

        Logger_helper::Debug( "PayPal payment - response: ".urldecode( $response ) );

        // converting NVPResponse to an Associative Array
        $result         = $this->Deformat_NVP( $response );

        Logger_helper::Debug( "PayPal payment - result array: <br />".print_r( $result, TRUE ) );

        if ( curl_errno( $ch ) )
            $result = $this->Add_error( curl_errno( $ch ), curl_error( $ch ), $result );
        else
            curl_close($ch);

        $this->result = $result;

        $this->Normalize_result();
    }

    private function Create_curl( $API_Endpoint, $nvp_request )
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $API_Endpoint );
        curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );

        // turning off the server and peer verification( TrustManager Concept ).
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER,1 );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        // if USE_PROXY constant set to TRUE then only proxy will be enabled.
        // Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php

        if( $this->config["use_proxy"] )
        {
            curl_setopt( $ch, CURLOPT_PROXY, $this->config["proxy_host"] . ":" . $this->config["proxy_port"] );
        }

        // setting the nvpreq as POST FIELD to curl
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $nvp_request );

        return $ch;
    }

    /**
     * @method	Deformat_NVP
     * @access	private
     * @desc	this method creates an array from an nvp string
     * @author
     *
     * @param 	string						$nvp_string					- the nvp string
     *
     * @version	1.0
     * @return 	array
     */
    private function Deformat_NVP( $nvp_string )
    {
        $initial	= 0;
        $nvp_array 	= array();

        while( strlen( $nvp_string ) )
        {
            // postion of Key
            $key_pos    = strpos( $nvp_string, "=" );
            // position of value
            $value_pos  = strpos( $nvp_string, "&" ) ? strpos( $nvp_string, "&" ) : strlen( $nvp_string );

            /* getting the Key and Value values and storing in a Associative Array */
            $key        = substr( $nvp_string, $initial, $key_pos );
            $value      = substr( $nvp_string, $key_pos + 1, $value_pos - $key_pos - 1 );

            // decoding the respose
            $nvp_array[urldecode( $key )] = urldecode( $value );
            $nvp_string	= substr( $nvp_string, $value_pos + 1, strlen( $nvp_string ) );
        }

        return $nvp_array;
    }

    /**
     * @method	Add_error
     * @access	private
     * @desc	this method creates an array for error
     * @author
     *
     * @param 	int							$error_number				- the error number
     * @param 	string						$error_message				- the error message
     * @param 	array						$nvp_array				    - the result array
     *
     * @version	1.0
     * @return 	array
     */
    private function Add_error( $error_number, $error_message, $nvp_array )
    {
        $nvp_array["Error"]["Number"]	= $error_number;
        $nvp_array["Error"]["Number"]	= $error_message;

        return $nvp_array;
    }

    private function Is_success_result()
    {
        // check if it was success
        if( ! array_key_exists( "ACK", $this->result ) )
        {
            return FALSE;
        }

        if( ! empty( $this->result["L_ERRORCODE0"] ) )
        {
            return FALSE;
        }

        if( $this->result["ACK"] == "Success" )
        {
            return TRUE;
        }

        if( $this->result["ACK"] == "SuccessWithWarning" )
        {
            return TRUE;
        }

        return FALSE;
    }

    private function Set_error_message()
    {
        if( ! empty( $this->result["L_ERRORCODE0"] ) )
        {
            $this->error_message .= $this->result["L_ERRORCODE0"]." - ";
        }

        if( ! empty( $this->result["L_SHORTMESSAGE0"] ) )
        {
            $this->error_message .= $this->result["L_SHORTMESSAGE0"]." - ";
        }

        if( ! empty( $this->result["L_LONGMESSAGE0"] ) )
        {
            $this->error_message .= $this->result["L_LONGMESSAGE0"];
        }
    }

    private function Redirect_to_checkout_url()
    {
        $checkout_url = Variable_helper::Has_value( $this->config, "checkout_url" );

        if( empty( $checkout_url ) )
        {
            Logger_helper::Error( "PayPal payment - unable to redirect to checkout url, is missing" );
            $this->status = FALSE;
            return FALSE;
        }

        if( empty( $this->token ) )
        {
            Logger_helper::Error( "PayPal payment - unable to redirect to checkout url, token is missing" );
            $this->status = FALSE;
            return FALSE;
        }

        App()->redirect->To_url( $checkout_url.$this->token );
        App()->Finish();
    }

    public function Get_express_checkout_details( & $order )
    {
        Logger_helper::Debug( "PayPal payment - getting express checkout" );

        if( empty( $this->token ) )
        {
            $this->token = Get_helper::Get( "token" );

            if( empty( $this->token ) )
            {
                Logger_helper::Debug( "PayPal payment - token missing for get express checkout" );
                $this->status = FALSE;

                return FALSE;
            }
        }

        $this->payer_id = Get_helper::Get( "PayerID" );
        $nvp_string     = '&TOKEN='.$this->token;
        $this->Send_data_and_get_result( "GetExpressCheckoutDetails", $nvp_string );

        if( ! $this->Is_success_result() )
        {
            $this->Set_error_message();
            $this->status = FALSE;
            return FALSE;
        }

        $this->status           = TRUE;
        $this->correlation_id	= Variable_helper::Has_value( $this->result, "CORRELATIONID" );

        return TRUE;
    }

    public function Do_express_checkout( & $order )
    {
        Logger_helper::Debug( "PayPal payment - do express checkout" );

        $server_name 	= $_SERVER['SERVER_NAME'];

        // creating string for request
        $nvp_string	    = '&TOKEN='.urlencode( $this->token )
            .'&PAYERID='.urlencode( $this->payer_id )
            .'&PAYMENTREQUEST_0_PAYMENTACTION=Sale'
            .'&PAYMENTREQUEST_0_AMT='.urlencode( $order->total_price )
            .'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode( $order->currency )
            .'&IPADDRESS='.urlencode( $server_name )
            .'&PAYMENTACTION=Sale'
            .'&AMT='.urlencode( $order->total_price )
            .'&CURRENCYCODE='.urlencode( $order->currency );

        $this->Send_data_and_get_result( "DoExpressCheckoutPayment", $nvp_string );

        if( ! $this->Is_success_result() )
        {
            Logger_helper::Error( "PayPal payment - do express checkout result error" );

            $this->Set_error_message();
            $this->status = FALSE;

            return FALSE;
        }

        Logger_helper::Info( "PayPal payment - express checkout success" );

        $this->status                       = TRUE;
        $this->correlation_id	            = Variable_helper::Has_value( $this->result, "CORRELATIONID" );
        $this->transaction_id	            = Variable_helper::Has_value( $this->result, "TRANSACTIONID" );
        $this->result["TRANSACTIONTYPE"]    = "express";
        $this->result["PAYMENTMETHODTYPE"]  = "card";

        return TRUE;
    }

    private function Normalize_result()
    {
        $result = array();

        foreach( $this->result as $key => $value )
        {
            $key = str_ireplace( "PAYMENTINFO_0_", "", $key );

            $result[$key] = $value;
        }

        $this->result = $result;
    }

    public function Do_direct_payment( & $order, $card_details )
    {
        Logger_helper::Debug( "PayPal payment - do direct payment started" );

        /**
         * Get required parameters from the web form for the request
         */
        $expiration_month   = urlencode( $card_details["expiration_month"] );
        $expiration_month   = str_pad( $expiration_month, 2, "0", STR_PAD_LEFT );
        $expiration_year    = urlencode( $card_details["expiration_year"] );

        $user_details       = Variable_helper::Has_value( $order, "user_details" );
        $country_details    = Variable_helper::Has_value( $user_details, "country" );

        /* Construct the request string that will be sent to PayPal.
           The variable $nvpstr contains all the variables and is a
           name value pair string with & as a delimiter */
        $nvp_string	="&PAYMENTACTION=Sale"
            ."&IPADDRESS="		.App()->request->Get_ip_address()
            ."&CREDITCARDTYPE="	.urlencode( $card_details["card_type"] )
            ."&ACCT="			.urlencode( $card_details["card_number"] )
            ."&CVV2="			.urlencode( $card_details["cvv2_number"] )
            ."&EXPDATE="		.$expiration_month.$expiration_year
            ."&FIRSTNAME="		.urlencode( Variable_helper::Has_value( $user_details, "first_name" ) )
            ."&LASTNAME="		.urlencode( Variable_helper::Has_value( $user_details, "last_name" ) )
            ."&STREET="			.urlencode( Variable_helper::Has_value( $user_details, "address1" ) )
            ."&STREET2="		.urlencode( Variable_helper::Has_value( $user_details, "address2" ) )
            ."&CITY="			.urlencode( Variable_helper::Has_value( $user_details, "city" ) )
            ."&STATE="			.urlencode( Variable_helper::Has_value( $user_details, "state" ) )
            ."&ZIP="			.urlencode( Variable_helper::Has_value( $user_details, "postcode" ) )
            ."&COUNTRYCODE="	.urlencode( Variable_helper::Has_value( $country_details, "code" ) )
            ."&AMT="			.urlencode( $order->total_price )
            ."&TAXAMT="			.urlencode( $order->tax )
            ."&CURRENCYCODE="	.urlencode( $order->currency )
            ."&ORDERID="		.urlencode( $order->id )
            ."&RETURNFMFDETAILS=1";

        $this->result   = $this->Send_data_and_get_result( "DoDirectPayment", $nvp_string );

        if( ! $this->Is_success_result() )
        {
            $this->Set_error_message();
            $this->status = FALSE;
            return FALSE;
        }

        $this->status           = TRUE;
        $this->correlation_id	= Variable_helper::Has_value( $this->result, "CORRELATIONID" );
        $this->transaction_id	= Variable_helper::Has_value( $this->result, "TRANSACTIONID" );
        return TRUE;
    }
}

/* End of file Payment_Paypal.php */
/* Location: ./core/Libraries/Payments/ */