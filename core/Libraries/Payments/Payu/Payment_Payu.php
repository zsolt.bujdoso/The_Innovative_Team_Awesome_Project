<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Payment_Payu
 *
 * @version 1.0.0
 */
class Payment_Payu extends BPayment_type
{
    public function __construct()
    {
        $this->type         = "payu";

        parent::__construct();

        Logger_helper::Debug( "PayU payment class initialized" );
    }

    public function Set_express_checkout_details( & $order )
    {
        if( ! $this->is_on )
        {
            Logger_helper::Info( "PayU payment - setting express checkout error because is turned off" );
            $this->error_message = "PayU payment - setting express checkout error because is turned off";

            return FALSE;
        }

        Logger_helper::Debug( "PayU payment - setting express checkout, getting the token" );

        $user_details       = Variable_helper::Has_value( $order, "user_details" );
        if( $order->is_company )
        {
            $user_details   = Variable_helper::Has_value( $order, "company_details" );
        }
        $parameters = array(
            "MERCHANT"      => Variable_helper::Has_value( $this->config, "merchant" ),
            "ORDER_REF"     => $order->id,
            "ORDER_DATE"    => Date_helper::Get_date_time(),
            "ORDER_PNAME"   => array(
                "Produse din comanda cu ID: ".$order->id
            ),
            "ORDER_PCODE"   => array(
                "PRODUSE".$order->id
            ),
            "ORDER_PINFO"   => array(
                ""
            ),
            "ORDER_PRICE"   => array(
                $order->total_price
            ),
            "ORDER_QTY"     => array(
                "1"
            ),
            "ORDER_VAT"     => array(
                $order->tax
            ),
            "PRICES_CURRENCY"       => $order->currency,
            "DISCOUNT"              => "0",
            "DESTINATION_CITY"      => Variable_helper::Has_value( $order, "city" ),
            "DESTINATION_STATE"     => Variable_helper::Has_value( $order, "county" ),
            "DESTINATION_COUNTRY"   => Variable_helper::Has_value( $order, "country" ),
            "PAY_METHOD"            => $this->config['pay_method'],
            "ORDER_PRICE_TYPE"      => array(
                "GROSS"
            ),
        );

        $parameters["ORDER_HASH"]   = $this->Create_hmac_md5_hash( $parameters );
        $parameters["BILL_EMAIL"]   = Variable_helper::Has_value( $user_details, "email" );
        $parameters["BILL_PHONE"]   = Variable_helper::Has_value( $user_details, "phone" );
        $parameters["BILL_FNAME"]   = Variable_helper::Has_value( $user_details, "first_name" );
        $parameters["BILL_LNAME"]   = Variable_helper::Has_value( $user_details, "last_name" );
        $parameters["TESTORDER"]    = ( $this->is_live ? "FALSE" : "TRUE" );
        $parameters["AUTOMODE"]     = 1;
        $parameters["LANGUAGE"]     = App()->lang->Get_language();
        $parameters["TIMEOUT_URL"]  = MY_Url_helper::Site_url().Variable_helper::Has_value(
                                        $this->all_config,
                                        "express_checkout_cancel_url"
                                    )
                                    ."?order_id=".$order->id."&payment_system=".$this->type;
        $parameters["BACK_REF"]     = MY_Url_helper::Site_url().Variable_helper::Has_value(
                                            $this->all_config,
                                            "express_checkout_success_url"
                                        )
                                        ."?order_id=".$order->id."&payment_system=".$this->type;

        $this->Set_details_into_data( $parameters );

        $this->status           = TRUE;
        $this->correlation_id	= "Success";
        $this->token	        = "Success";
        return $this->Get_status();
    }

    private function Create_hmac_md5_hash( & $parameters = array() )
    {
        $secret_key = Variable_helper::Has_value( $this->config, "secret_key" );

        if( empty( $secret_key ) )
        {
            Logger_helper::Error( "PayU payment - unable to set secret key in parameters, is missing" );
            $this->error_message = "PayU payment - unable to set secret key in parameters, is missing";
            $this->status = FALSE;

            return FALSE;
        }

        $result = "";

        if( empty( $parameters ) || ! is_array( $parameters ) )
        {
            return $result;
        }

        foreach( $parameters as $value )
        {
            if( is_array( $value ) )
            {
                foreach( $value as $sub_value )
                {
                    $result .= strlen( $sub_value ).$sub_value;
                }

                continue;
            }

            $result .= strlen( $value ).$value;
        }

        $result = hash_hmac(
            "md5",
            $result,
            $secret_key
        );

        return $result;
    }

    private function Set_details_into_data( $parameters )
    {
        App()->controller->data["payu_parameters"]      = $parameters;
        App()->controller->data["payment_url"]          = Variable_helper::Has_value( $this->config, "checkout_url" );
        App()->controller->data["payment_data"]         = "";
        App()->controller->data["is_redirection_needed"]= 1;

        foreach( $parameters as $key => $parameter )
        {
            if( is_array( $parameter ) )
            {
                foreach( $parameter as $value )
                {
                    App()->controller->data["payment_data"] .= '<input type="hidden" name="'.$key.'[]" value="'.$value.'"/>';
                }
                continue;
            }

            App()->controller->data["payment_data"] .= '<input type="hidden" name="'.$key.'" value="'.$parameter.'"/>';
        }

        if( ! empty( App()->controller->data["payment_url"] ) )
        {
            Logger_helper::Error( "PayU payment - unable to set checkout url, is missing" );
            $this->error_message = "PayU payment - unable to set checkout url, is missing";
            $this->status = FALSE;

            return FALSE;
        }
    }

    public function Get_express_checkout_details( & $order )
    {
        Logger_helper::Debug( "PayU payment - getting express checkout" );

        $ctrl = Get_helper::Get( "ctrl" );

        Logger_helper::Debug( "PayU payment - ctrl received: ".$ctrl );

        $parameters = array(
            MY_Url_helper::Site_url().Variable_helper::Has_value( $this->all_config, "express_checkout_success_url" )
                ."?order_id=".$order->id."&payment_system=payu"
        );

        $test_ctrl = $this->create_hmac_md5_hash( $parameters );

        Logger_helper::Debug( "PayU payment - ctrl created to test: ".$test_ctrl );

        if( $test_ctrl != $ctrl )
        {
            $this->status           = FALSE;
            $this->error_message    = "Invalid signature";

            return FALSE;
        }

        $this->status           = TRUE;
        $this->correlation_id	= "PayUSuccess";
        return $this->Get_status();
    }

    public function Do_express_checkout( & $order )
    {
        Logger_helper::Debug( "PayU payment - do express checkout" );
        Logger_helper::Info( "PayU payment - express checkout success" );

        $this->status           = TRUE;
        $this->correlation_id	= "PayUSuccess";
        $this->transaction_id	= "";
        $this->Create_success_result();

        return $this->Get_status();
    }

    private function Create_success_result( $transaction_type = "express" )
    {
        $this->result = array(
            "CORRELATIONID"     => $this->correlation_id,
            "TRANSACTIONID"     => $this->transaction_id,
            "TRANSACTIONTYPE"   => $transaction_type,
            "PAYMENTTYPE"       => $this->type,
            "PAYMENTMETHODTYPE" => "",
            "PAYMENTSTATUS"     => "PayUSuccess",
        );
    }

    public function Do_direct_payment( & $order, $card_details )
    {
        Logger_helper::Debug( "PayU payment - do direct payment started" );

        $this->status           = FALSE;
        $this->correlation_id	= "";
        $this->transaction_id	= "";

        return $this->Get_status();
    }
}

/* End of file Payment_Payu.php */
/* Location: ./core/Libraries/Payments/ */