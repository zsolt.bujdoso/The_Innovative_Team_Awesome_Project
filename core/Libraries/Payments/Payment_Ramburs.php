<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Payment_Ramburs
 *
 * @version 1.0.0
 */
class Payment_Ramburs extends BPayment_type implements IPayment
{
    public function __construct()
    {
        $this->type         = "ramburs";

        parent::__construct();

        Logger_helper::Debug( "Ramburs payment class initialized" );
    }

    public function Set_express_checkout_details( & $order )
    {
        Logger_helper::Debug( "Ramburs payment - setting express checkout, getting the token" );

        $this->status           = TRUE;
        $this->correlation_id	= "Success";
        $this->token	        = "Success";
        return $this->Get_status();
    }

    public function Get_express_checkout_details( & $order )
    {
        Logger_helper::Debug( "Ramburs payment - getting express checkout" );

        $this->status = TRUE;
        $this->correlation_id	= "RambursSuccess";
        return $this->Get_status();
    }

    public function Do_express_checkout(  & $order )
    {
        Logger_helper::Debug( "Ramburs payment - do express checkout" );

        Logger_helper::Info( "Ramburs payment - express checkout success" );

        $this->status           = TRUE;
        $this->correlation_id	= "RambursSuccess";
        $this->transaction_id	= "";
        return $this->Get_status();
    }

    public function Do_direct_payment( & $order, $card_details )
    {
        Logger_helper::Debug( "Ramburs payment - do direct payment started" );

        $this->status           = FALSE;
        $this->correlation_id	= "";
        $this->transaction_id	= "";
        return $this->Get_status();
    }
}

/* End of file Payment_Ramburs.php */
/* Location: ./core/Libraries/Payments/ */