<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Payment_Euplatesc
 *
 * @version 1.0.0
 */
class Payment_Euplatesc extends BPayment_type
{
    public function __construct()
    {
        $this->type = "euplatesc";

        parent::__construct();

        Logger_helper::Debug( "EuPlatesc payment class initialized" );
    }

    public function Set_express_checkout_details( & $order )
    {
        if( ! $this->is_on )
        {
            Logger_helper::Info( "EuPlatesc payment - setting express checkout error because is turned off" );
            $this->error_message = "EuPlatesc payment - setting express checkout error because is turned off";

            return $this->Get_status();
        }

        Logger_helper::Debug( "EuPlatesc payment - setting express checkout, getting the token" );

        $user_details       = Variable_helper::Has_value( $order, "user_details" );
        if( $order->is_company )
        {
            $user_details   = Variable_helper::Has_value( $order, "company_details" );
        }
        $parameters = array(
            "amount"        => $order->total_price,
            "curr"          => $order->currency,
            "invoice_id"    => $order->id,
            "order_desc"    => "Produse din comanda cu ID: ".$order->id,
            "merch_id"      => Variable_helper::Has_value( $this->config, "merchant" ),
            "timestamp"     => gmdate( "YmdHis" ),
            "nonce"         => md5( time()."-".$order->id."-".$order->currency."-".$order->total_price ),
        );

        $p_sign                 = $this->Create_hmac_md5_hash( $parameters );
        $parameters["fp_hash"]  = $p_sign;
        $parameters["ExtraData"]= array(
            "order_id"  => $order->id,
            "token"     => md5( $order->id ),
            "payer_id"  => User_helper::Get_user_id(),
        );

        $this->Set_details_into_data( $parameters );

        $this->status           = TRUE;
        $this->correlation_id	= "Success";
        $this->token	        = "Success";

        return $this->Get_status();
    }

    private function Create_hmac_md5_hash( & $parameters = array() )
    {
        $secret_key = Variable_helper::Has_value( $this->config, "secret_key" );

        if( empty( $secret_key ) )
        {
            Logger_helper::Error( "EuPlatesc payment - unable to set secret key in parameters, is missing" );
            $this->error_message = "EuPlatesc payment - unable to set secret key in parameters, is missing";
            $this->status = FALSE;

            return FALSE;
        }

        $result = "";

        if( empty( $parameters ) || ! is_array( $parameters ) )
        {
            return $result;
        }

        foreach( $parameters as $value )
        {
            if( is_array( $value ) )
            {
                foreach( $value as $sub_value )
                {
                    $result .= strlen( $sub_value ).$sub_value;
                }

                continue;
            }
            elseif( is_null( $value ) || strlen( $value ) == 0 )
            {
                $result .= "-";
                continue;
            }

            $result .= strlen( $value ).$value;
        }

        $secret_key     = pack( 'H*', $secret_key );
        $block_size     = 64;
        $hash_function  = "md5";

        if( strlen( $secret_key ) > $block_size )
        {
            $secret_key = pack( 'H*', $hash_function( $secret_key ) );
        }

        $secret_key = str_pad( $secret_key, $block_size, chr( 0x00 ) );
        $ipad       = str_repeat( chr( 0x36 ), $block_size );
        $opad       = str_repeat( chr( 0x5c ), $block_size );
        $hmac       = pack( 'H*', $hash_function( ( $secret_key ^ $opad ) . pack( 'H*', $hash_function( ( $secret_key ^ $ipad ) . $result ) ) ) );

        return bin2hex( $hmac );
    }

    private function Set_details_into_data( $parameters )
    {
        App()->controller->data["euplatesc_parameters"] = $parameters;
        App()->controller->data["payment_url"]          = Variable_helper::Has_value( $this->config, "checkout_url" );
        App()->controller->data["payment_data"]         = "";
        App()->controller->data["is_redirection_needed"]= 1;

        foreach( $parameters as $key => $parameter )
        {
            if( is_array( $parameter ) )
            {
                foreach( $parameter as $sub_key => $value )
                {
                    App()->controller->data["payment_data"] .= '<input type="hidden" name="'.$key.'['.$sub_key.']" value="'.$value.'"/>';
                }
                continue;
            }

            App()->controller->data["payment_data"] .= '<input type="hidden" name="'.$key.'" value="'.$parameter.'"/>';
        }

        if( ! empty( App()->controller->data["payment_url"] ) )
        {
            Logger_helper::Error( "EuPlatesc payment - unable to set checkout url, is missing" );
            $this->error_message = "EuPlatesc payment - unable to set checkout url, is missing";
            $this->status = FALSE;

            return FALSE;
        }

        return TRUE;
    }

    public function Get_express_checkout_details( & $order )
    {
        Logger_helper::Debug( "EuPlatesc payment - getting express checkout" );

        $p_sign     = addslashes( trim( Post_helper::Get( "fp_hash" ) ) );
        $action     = Post_helper::Get( "action" );
        $message    = Post_helper::Get( "message" );
        $approved   = Post_helper::Get( "approval" );
        $ep_id      = Post_helper::Get( "ep_id" );

        Logger_helper::Debug( "EuPlatesc payment - fp_hash received: ".$p_sign );

        if( $action != 0 || empty( $approved ) )
        {
            $this->status = FALSE;
            $this->error_message = "Payment not approved: ".$message;

            return $this->Get_status();
        }

        $parameters = array(
            "amount"        => addslashes( trim( Post_helper::Get( "amount" ) ) ),
            "curr"          => addslashes( trim( Post_helper::Get( "curr" ) ) ),
            "invoice_id"    => addslashes( trim( Post_helper::Get( "invoice_id" ) ) ),
            "ep_id"         => addslashes( trim( $ep_id ) ),
            "merch_id"      => addslashes( trim( Post_helper::Get( "merch_id" ) ) ),
            "action"        => addslashes( trim( Post_helper::Get( "action" ) ) ),
            "message"       => addslashes( trim( Post_helper::Get( "message" ) ) ),
            "approval"      => addslashes( trim( Post_helper::Get( "approval" ) ) ),
            "timestamp"     => addslashes( trim( Post_helper::Get( "timestamp" ) ) ),
            "nonce"         => addslashes( trim( Post_helper::Get( "nonce" ) ) ),
        );

        $test_p_sign = strtoupper( $this->Create_hmac_md5_hash( $parameters ) );

        if( $test_p_sign != $p_sign )
        {
            $this->status = FALSE;
            $this->error_message = "Invalid signature";

            return $this->Get_status();
        }

        $this->status           = TRUE;
        $this->correlation_id   = $approved;
        $this->transaction_id   = $ep_id;

        return $this->Get_status();
    }

    public function Do_express_checkout( & $order )
    {
        Logger_helper::Debug( "EuPlatesc payment - do express checkout" );
        Logger_helper::Info( "EuPlatesc payment - express checkout success" );

        $this->status           = TRUE;
        $this->Create_success_result();

        return $this->Get_status();
    }

    private function Create_success_result( $transaction_type = "express" )
    {
        $this->result = array(
            "CORRELATIONID"     => $this->correlation_id,
            "TRANSACTIONID"     => $this->transaction_id,
            "TRANSACTIONTYPE"   => $transaction_type,
            "PAYMENTTYPE"       => $this->type,
            "PAYMENTMETHODTYPE" => "",
            "PAYMENTSTATUS"     => "EuPlatescSuccess",
        );
    }

    public function Do_direct_payment( & $order, $card_details )
    {
        Logger_helper::Debug( "EuPlatesc payment - do direct payment started" );

        $this->status           = FALSE;
        $this->correlation_id	= "";
        $this->transaction_id	= "";

        return $this->Get_status();
    }
}

/* End of file Payment_Euplatesc.php */
/* Location: ./core/Libraries/Payments/ */