<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MVariable_validation
 *
 * @version     1.0.0
 */
class MVariable_validation extends MValidation
{
    /**
     * @var     $variable
     */
    protected $variable;

    /**
     * @var     $name
     */
    protected $name;

    /**
     * @var     $rules
     */
    protected $rules;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @method	Validate
     * @access	public
     * @desc    this method checks the variables will make the rules or not
     * @author	Zoltan Jozsa
     *
     * @version	1.0.1
     * @throws  MPHP_exception
     * @return 	boolean
     */
    public function Validate()
    {
        // todo: Ezt még nem tudni miért volt betéve
        /*if( empty( $this->variable ) )
        {
            return TRUE;
        }*/

        // If the rules are not set correctly, throw an exception
        if( empty( $this->rules ) )
        {
            throw new MPHP_exception(
                E_ERROR,
                App()->lang->Get( "Error_with_rules" )
            );
        }

        // explode the rules with character | to get more rules for this field
        $field_rules = explode( "|", $this->rules );

        // get all rules and run them
        foreach( $field_rules as $field_rule )
        {
            // Get the value or check in attributes
            $value = $this->variable;

            // If this rule has parameter between []
            preg_match( "/(.*)\[(.*)\]/", $field_rule, $parameters );
            if( ! empty( $parameters ) )
            {
                $parameter = $parameters[2];
                $function = ucfirst( strtolower( $parameters[1] ) );

                $this->$function(
                    ( empty( $this->name ) ? "variable" : $this->name ),
                    $value,
                    $parameter
                );
                continue;
            }

            //else there is no parameter, just rule the function
            $function = ucfirst( strtolower( $field_rule ) );
            $this->$function(
                ( empty( $this->name ) ? "variable" : $this->name ),
                $value
            );
        }

        return empty( $this->errors );
    }

    public function Set_value( $value )
    {
        $this->variable = $value;
    }

    public function Set_name( $name )
    {
        $this->name = $name;
    }

    public function Set_rules( $rules )
    {
        $this->rules = $rules;
    }

    public function & __get( $name )
    {
        if( property_exists( $this, $name ) )
        {
            return $this->$name;
        }

        // throw an exception, this property does not exists
        throw new MProperty_not_exists_exception( 'The class "'.get_class( $this ).'" does not have a property named '.$name );
    }

    public function __set( $name, $value )
    {
        if( property_exists( $this, $name ) )
        {
            $this->$name = $value;
            return;
        }

        // throw an exception, this property does not exists
        throw new MProperty_not_exists_exception( 'The class "'.get_class( $this ).'" does not have a property named '.$name );
    }
}