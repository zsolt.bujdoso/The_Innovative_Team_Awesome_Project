<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MModel
 *
 * @property    DB_Mysql    $db
 *
 * @version     1.0.0
 */
class MModel extends BModel
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @method  Close_connection
     * @desc    This method close the database connection
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    public function Close_connection()
    {
        $this->db->Close_connection();
    }
    //</editor-fold>

    /******************************************
     * TABLE MODIFICATIONS
     ******************************************/
    //<editor-fold desc="Table modifiers">
    /**
     * @method  Create_table
     * @access  public
     * @desc    Create new table
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $columns                    - an array with columns where index is name of the column
     *                                                                and value is type, length, and other options
     * @param   string                  $primary_key                - the primary key column name
     * @param   bool                    $auto_increment             - add auto increment for primary key
     * @param   string                  $engine                     - engine use for this table
     *
     * @version 1.0.0
     * @throws  MException
     * @return  bool
     */
    public function Create_table( $table_name, array $columns, $primary_key = "", $auto_increment = TRUE, $engine = "InnoDB" )
    {
        return $this->db->Create_table( $table_name, $columns, $primary_key, $auto_increment, $engine );
    }

    /**
     * @method  Check_table_name
     * @access  private
     * @desc    This method checks and add the prefix for selected table if it is not added
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table
     *
     * @version 1.0.0
     * @return string
     */
    public function Check_table_name( $table_name )
    {
        return $this->db->Check_table_name( $table_name );
    }

    /**
     * @method  Delete_table
     * @desc    This method try to delete a table by table name
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     *
     * @version 1.0
     * @return  bool
     */
    public function Delete_table( $table_name )
    {
        return $this->db->Delete_table( $table_name );
    }

    /**
     * @method  Truncate
     * @desc    Try to truncate a table by name
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     *
     * @version 1.0
     * @return  bool
     */
    public function Truncate( $table_name )
    {
        return $this->db->Truncate( $table_name );
    }

    /**
     * @method  Alter_table
     * @desc    Try to alter table by name and modifications string
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $query                      - modifications string
     *
     * @version 1.0
     * @return  bool
     */
    public function Alter_table( $table_name, $query )
    {
        return $this->db->Alter_table( $table_name, $query );
    }

    /**
     * @method  Add_column
     * @desc    Try to add a column into specified table after a column
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $column                     - the column name
     * @param   string                  $type                       - type of the column, length, and other modifiers
     * @param   string                  $after                      - column name to insert after
     *
     * @version 1.0
     * @return  bool
     */
    public function Add_column( $table_name, $column, $type, $after )
    {
        return $this->db->Add_column( $table_name, $column, $type, $after );
    }

    /**
     * @method  Delete_column
     * @desc    Try to delete a column from specified table
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $column                     - the column name
     *
     * @version 1.0
     * @return  bool
     */
    public function Delete_column( $table_name, $column )
    {
        return $this->db->Delete_column( $table_name, $column );
    }

    /**
     * @method  Rename
     * @desc    Try to rename a table
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $table_name                 - name of the table to delete
     * @param   string                  $new_table_name             - new name for table
     *
     * @version 1.0
     * @return  bool
     */
    public function Rename( $table_name, $new_table_name )
    {
        return $this->db->Rename( $table_name, $new_table_name );
    }
    //</editor-fold>

    /******************************************
     * TRANSACTIONS
     ******************************************/
    //<editor-fold desc="Transactions">
    /**
     * @method  Transaction_start
     * @desc    Try to start a transaction
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_start()
    {
        return $this->db->Transaction_start();
    }

    /**
     * @method  Transaction_end
     * @desc    Try to end transaction started before
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_end()
    {
        return $this->db->Transaction_end();
    }

    /**
     * @method  Transaction_status
     * @desc    Returns the transaction status
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_status()
    {
        return $this->db->Transaction_status();
    }

    /**
     * @method  Transaction_rollback
     * @desc    Try to rollback started transaction before
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Transaction_rollback()
    {
        return $this->db->Transaction_rollback();
    }
    //</editor-fold>

    /******************************************
     * GETTERS
     ******************************************/
    //<editor-fold desc="Getters">
    /**
     * @method  Get_list
     * @access  public
     * @desc    Returns an array with list of items from specified table or FALSE
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list( $table_name )
    {
        return $this->db->Get_list( $table_name );
    }

    /**
     * @method  Get_by_primary_key
     * @access  public
     * @desc    Returns one item from specified table by primary key
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $primary_key_column         - name of the column of primary key
     * @param   string                  $value                      - primary key value
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_primary_key( $table_name, $primary_key_column, $value )
    {
        return $this->db->Get_by_primary_key( $table_name, $primary_key_column, $value );
    }

    /**
     * @method  Get_by_column
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $column                     - name of the column
     * @param   string                  $value                      - value of column
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_column( $table_name, $column, $value )
    {
        return $this->db->Get_by_column( $table_name, $column, $value );
    }

    /**
     * @method  Get_by_attributes
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $attributes                 - an array with attributes where index will be
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_attributes( $table_name, array $attributes = array() )
    {
        return $this->db->Get_by_attributes( $table_name, $attributes );
    }

    /**
     * @method  Get_by_filters
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $filters                    - an object with specified filters
     *
     * @version 1.0.0
     * @return  object|bool
     */
    public function Get_by_filters( $table_name, $filters = null )
    {
        return $this->db->Get_by_filters( $table_name, $filters );
    }

    /**
     * @method  Get_list_by_column
     * @access  public
     * @desc    Returns an array with list of items from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $column                     - name of the column
     * @param   string                  $value                      - value of column
     * @param   int                     $limit                      - number of items to return
     * @param   int                     $limit_from                 - number where to start from returning
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list_by_column( $table_name, $column, $value, $limit = null, $limit_from = null )
    {
        return $this->db->Get_list_by_column( $table_name, $column, $value, $limit, $limit_from );
    }

    /**
     * @method  Get_list_by_attributes
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     * @param   int                     $limit                      - number of items to return
     * @param   int                     $limit_from                 - number where to start from returning
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list_by_attributes( $table_name, array $attributes = array(), $limit = null, $limit_from = null )
    {
        return $this->db->Get_list_by_attributes( $table_name, $attributes, $limit, $limit_from );
    }

    /**
     * @method  Get_list_by_filters
     * @access  public
     * @desc    Returns one item from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $filters                    - an object with specified filters
     * @param   int                     $limit                      - number of items to return
     * @param   int                     $limit_from                 - number where to start from returning
     *
     * @version 1.0.0
     * @return  array|bool
     */
    public function Get_list_by_filters( $table_name, $filters = null, $limit = null, $limit_from = null )
    {
        return $this->db->Get_list_by_filters( $table_name, $filters, $limit, $limit_from );
    }
    //</editor-fold>

    /******************************************
     * CRUD FUNCTIONS
     ******************************************/
    //<editor-fold desc="Crud functions">
    /**
     * @method  Get_list_by_filters
     * @access  public
     * @desc    Insert a new row into specified table
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     *
     * @version 1.0.0
     * @return  int
     */
    public function Insert( $table_name, array $data_set = array() )
    {
        // If error not throwed just return the last insert id
        return $this->db->Insert( $table_name, $data_set );
    }

    /**
     * @method  Update
     * @access  public
     * @desc    Update one or more row in specified table by column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $column                     - name of specified column
     * @param   string                  $value                      - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_column( $table_name, array $data_set = array(), $column, $value )
    {
        return $this->db->Update_by_column( $table_name, $data_set, $column, $value );
    }

    /**
     * @method  Update
     * @access  public
     * @desc    Update one or more row in specified table by primary key
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   string                  $primary_key                - name of primary key column
     * @param   string                  $value                      - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_primary_key( $table_name, array $data_set = array(), $primary_key, $value )
    {
        return $this->db->Update_by_primary_key( $table_name, $data_set, $primary_key, $value );
    }

    /**
     * @method  Update
     * @access  public
     * @desc    Update one or more row in specified table by attributes
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   mixed                   $data_set                   - an array with set of columns where index will be
     *                                                                the name of the column and value the value
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Update_by_attributes( $table_name, array $data_set = array(), array $attributes )
    {
        // Update by columns, here the attributes has more column, and when will be checked by filters class
        // will be put into where equal one by one
        return $this->db->Update_by_attributes( $table_name, $data_set, $attributes );
    }

    /**
     * @method  Delete_by_primary_key
     * @access  public
     * @desc    Update one row from specified table by primary key
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $primary_key                - name of primary key column
     * @param   string                  $value                      - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_primary_key( $table_name, $primary_key, $value )
    {
        return $this->db->Delete_by_primary_key( $table_name, $primary_key, $value );
    }

    /**
     * @method  Delete_by_column
     * @access  public
     * @desc    Update one row from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $column                     - name of column
     * @param   string                  $value                      - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_column( $table_name, $column, $value )
    {
        return $this->db->Delete_by_column( $table_name, $column, $value );
    }

    /**
     * @method  Delete_by_attributes
     * @access  public
     * @desc    Delete one or more rows from specified table by attributes
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_attributes( $table_name, array $attributes )
    {
        return $this->db->Delete_by_attributes( $table_name, $attributes );
    }

    /**
     * @method  Delete_by_filters
     * @access  public
     * @desc    Delete one or more row from specified table by filters
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   object                  $filters                    - object with filters
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Delete_by_filters( $table_name, $filters = null )
    {
        return $this->Delete_by_filters( $table_name, $filters );
    }

    /**
     * @method  Count_by_column
     * @access  public
     * @desc    Count rows from specified table by specified column
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   string                  $column                     - name of column
     * @param   string                  $value                      - value of primary key
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Count_by_column( $table_name, $column, $value )
    {
        return $this->db->Count_by_column( $table_name, $column, $value );
    }

    /**
     * @method  Count_by_attributes
     * @access  public
     * @desc    Count rows from specified table by attributes
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   array                   $attributes                 - an array with attributes where index will be
     *                                                                the name of the column and value the value
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Count_by_attributes( $table_name, array $attributes )
    {
        return $this->db->Count_by_attributes( $table_name, $attributes );
    }

    /**
     * @method  Count_by_filters
     * @access  public
     * @desc    Count rows from specified table by filters
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     * @param   object                  $filters                    - object with filters
     *
     * @version 1.0.0
     * @return  bool
     */
    public function Count_by_filters( $table_name, $filters = null )
    {
        return $this->db->Count_by_filters( $table_name, $filters );
    }
    //</editor-fold>

    /******************************************
     * INFORMATIONS
     ******************************************/
    //<editor-fold desc="Information getters">
    /**
     * @method  Get_primary_key
     * @access  public
     * @desc    Returns the primary key from specified table or FALSE
     * @author  Cousin Béla
     *
     * @param   string                  $table_name                 - name of the table you want
     *
     * @version 1.0.0
     * @return  string|bool
     */
    public function Get_primary_key( $table_name )
    {
        return $this->db->Get_primary_key( $table_name );
    }
    //</editor-fold>

    /******************************************
     * QUERY GETTERS
     ******************************************/
    //<editor-fold desc="Query getters">
    /**
     * @method  Get_query
     * @desc    This method get a mysql query by type
     * @access  public
     * @author  Cousin Blea
     *
     * @param   string                  $type                       - type of query
     *
     * @version 1.0
     * @return  object|bool
     */
    public function Get_query( $type = "SELECT" )
    {
        return $this->db->Get_query( $type );
    }
    //</editor-fold>

    /******************************************
     * CACHE MODIFIERS
     ******************************************/
    //<editor-fold desc="Cache modifiers">
    /**
     * @method  Turn_cache_on
     * @desc    Turn cache on if it is set
     * @access  public
     * @author  Cousin Blea
     *
     * @version 1.0
     */
    public function Turn_cache_on()
    {
        return $this->db->Turn_cache_on();
    }

    /**
     * @method  Turn_cache_off
     * @desc    Turn cache off if it is set
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     */
    public function Turn_cache_off()
    {
        return $this->db->Turn_cache_off();
    }
    //</editor-fold>
}