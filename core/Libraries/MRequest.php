<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MRequest
 *
 * @property    string      $script_path
 * @property    string      $viewer_browser
 * @property    string      $viewer_platform
 *
 * @version     1.0.0
 */
class MRequest extends BRequest
{
    protected $script_path;
    protected $viewer_browser;
    protected $viewer_platform;

    public function __construct()
    {
        parent::__construct();

        $this->ip_address = Server_helper::Get( "REMOTE_ADDR" );
        
        $this->Calculate_path();
        $this->Load_and_process_url();
        $this->Get_platform();
    }

    protected function Calculate_path()
    {
        $script_file_path   = @dirname( Server_helper::Get( "SCRIPT_FILENAME" ) )."/";
        $this->script_path  = @str_replace( Server_helper::Get( "DOCUMENT_ROOT" ), "", $script_file_path );
    }

    protected function Load_and_process_url()
    {
        $this->Load_url();
        $this->Remove_script_path();
        
        $this->url        = trim( $this->url, "/" );
        $this->url        = preg_replace( "|([\/]+)|", "/", $this->url );

        $this->Process_url();
    }

    protected function Load_url()
    {
        $type       = ( isset( App()->config->url["type"] ) ? App()->config->url["type"] : "PATH" );
        $parameter  = ( isset( App()->config->url["parameter"] ) ? App()->config->url["parameter"] : "r" );
      
        if( strtoupper( $type ) == "GET" )
        {
            $this->url = "";

            if( isset( $_GET[$parameter] ) )
            {
                $this->url = @urldecode( $_GET[$parameter] );
            }

            return;
        }
         
        $url        = @urldecode( Server_helper::Get( "REQUEST_URI" ) );
        $parse_url  = parse_url( $url );
        $this->url  = $parse_url["path"];

        if( ! empty( $parse_url["query"] ) )
        {
            $this->query_parameters = $parse_url["query"];
        }
    }

    protected function Remove_script_path()
    {
        if( empty( $this->script_path ) )
        {
            return;
        }

        // If script file name is set in url
        if( @preg_match( "|^".$_SERVER["SCRIPT_NAME"]."(.*)|", $this->url ) )
        {
            $script_name  = @$_SERVER["SCRIPT_NAME"];
            $this->url    = preg_replace( "|^".$script_name."(.*)|", "$1", $this->url );
        }

        if( $this->script_path != "/" )
        {
            $this->url = str_replace( $this->script_path, "", $this->url );
        }
    }

    protected function Process_url()
    {
        $host               = Server_helper::Get( "HTTP_HOST" );
        $host_parsed        = parse_url( $host );
        $this->host         = ( ! empty( $host_parsed["host"] ) ? $host_parsed["host"] : $host_parsed["path"] );
        $protocol           = @explode( "/", $_SERVER["SERVER_PROTOCOL"] );
        $this->protocol     = ( isset( $protocol[0] ) ? $protocol[0] : "http" );
        $this->port         = @$_SERVER["SERVER_PORT"];
        $this->method       = @$_SERVER["REQUEST_METHOD"];
        $this->parameters   = ( ! empty( $this->url ) ? explode( "/", $this->url ) : array() );
    }

    /**
     * @method	Get_platform
     * @access	public
     * @desc    this method get the platform which is used for viewing this page
     * @author	Zoltan Jozsa
     *
     * @version	1.1
     * @return  string
     */
    public function Get_platform()
    {
        // if the user from mobile platform, wants to show the main site
        /* todo: Ezt megnézni, hogy hogy is van itt */
        if( App()->session->Has( "show_main_site" ) )
        {
            // set the platform to mobile
            $this->viewer_platform = "windows_or_linux";

            return;
        }

        // get the agent
        $user_agent = Server_helper::Get( "HTTP_USER_AGENT" );

        // set the platform to mobile
        $this->viewer_platform = "mobile";

        // check if it is a mobile
        if( stripos( $user_agent, "symbian" ) === FALSE
            && stripos( $user_agent, "android" ) === FALSE
            && stripos( $user_agent, "iphone" ) === FALSE
            && stripos( $user_agent, "symbos" ) === FALSE
            && stripos( $user_agent, "mobil" ) === FALSE
            && stripos( $user_agent, "mobi" ) === FALSE
            && stripos( $user_agent, "phone" ) === FALSE
        )
        {
            // set the platform to mobile
            $this->viewer_platform = "windows_or_linux";
        }

        $this->Calculate_browser();
    }

    /**
     * @method	Calculate_browser
     * @access	private
     * @desc    this method get the browser paltform which is used for viewing this page
     * @author	Zoltan Jozsa
     *
     * @version	1.0
     * @return
     */
    private function Calculate_browser()
    {
        // get the agent
        $user_agent = Server_helper::Get( "HTTP_USER_AGENT" );

        foreach( Array_helper::Get_browser_platforms_array() as $platform => $platform_checks )
        {
            if( is_array( $platform_checks ) )
            {
                foreach( $platform_checks as $platform_check )
                {
                    if( stripos( $user_agent, $platform_check ) !== FALSE )
                    {
                        $this->viewer_browser = $platform;

                        return;
                    }
                }
            }
            elseif( stripos( $user_agent, $platform_checks ) !== FALSE )
            {
                $this->viewer_browser = $platform;

                return;
            }
        }

        $this->viewer_browser = "other";
    }

    /**
     * @method	Is_mobile
     * @access	public
     * @desc    Return boolean value for visitor come from mobile or not
     * @author	Zoltan Jozsa
     *
     * @version	1.1
     * @return  bool
     */
    public function Is_mobile()
    {
        return ( $this->viewer_platform == "mobile" );
    }

    /**
     * @method	Is_robot
     * @access	public
     * @desc    Return boolean value for visitor is robot or not
     * @author	Zoltan Jozsa
     *
     * @version	1.1
     * @return  bool
     */
    public function Is_robot()
    {
        $agent = Server_helper::Get( "HTTP_USER_AGENT" );

        // baidu
        if( stripos( $agent, "spider" ) !== FALSE )
        {
            return TRUE;
        }

        // google and others
        if( stripos( $agent, "bot" ) !== FALSE )
        {
            return TRUE;
        }

        // yahoo and others
        if( stripos( $agent, "search" ) !== FALSE )
        {
            return TRUE;
        }

        // facebook
        if( stripos( $agent, "externalhit" ) !== FALSE )
        {
            return TRUE;
        }

        // google and others
        if( stripos( $agent, "Google" ) !== FALSE )
        {
            return TRUE;
        }

        // google and others
        if( stripos( $agent, "Facebook" ) !== FALSE )
        {
            return TRUE;
        }

        return FALSE;
    }
}