<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MRenderer
 *
 * @version     1.0.0
 */
class MRenderer extends BRenderer
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @method  Load_view
     * @access  public
     * @desc    Load a view by name when rendering the page with layout
     * @author  Cousin Bela
     *
     * @param   string                  $view_name              - the name of template
     * @param   array                   $params                 - parameters array with tokens to replace
     * @param   int                     $cache_time             - if we want with cache, set the time for cache available
     *
     * @version 1.0.0
     * @return  string
     */
    public function Load_view( $view_name = "", $params = array(), $cache_time = 0 )
    {
        return $this->Load_view_by_layout( $view_name, $params, $cache_time );
    }

    /**
     * @method  Load_partial_view
     * @access  public
     * @desc    Load a view by name to rendering
     * @author  Cousin Bela
     *
     * @param   string                  $view_name              - the name of template
     * @param   array                   $params                 - parameters array with tokens to replace
     * @param   int                     $cache_time             - if we want with cache, set the time for cache available
     *
     * @version 1.0.0
     * @return  string
     */
    public function Load_partial_view( $view_name = "", $params = array(), $cache_time = 0 )
    {
        $content = $this->Load_template( $view_name, $params, $cache_time );

        return $content;
    }

    /**
     * @method  Load_view_by_layout
     * @access  private
     * @desc    Load a view by name when rendering the page with layout
     * @author  Cousin Bela
     *
     * @param   string                  $view_name              - the name of template
     * @param   array                   $params                 - parameters array with tokens to replace
     * @param   int                     $cache_time             - if we want with cache, set the time for cache available
     *
     * @version 1.0.0
     * @return  string
     */
    private function Load_view_by_layout( $view_name, $params = array(), $cache_time = 0 )
    {
        if( $this->Is_cache_on() && App()->cache->Cache_exists( $view_name, $cache_time ) )
        {
            $this->layout_content = App()->cache->Get_content( $view_name );
            return;
        }

        /*
        if( substr_count( $_SERVER["HTTP_ACCEPT_ENCODING"], "gzip" ) )
        {
            //header( "Content-Encoding: gzip" );
            ob_start( "ob_gzhandler" );
        }
        else
        {*/
            ob_start();
        //}

        $this->Load_layout( $view_name, $params, $cache_time );
        $this->layout_content = ob_get_clean();

        // Minify content
        if( empty( $_POST["with_ajax"] ) )
        {
            $this->layout_content = MMinifier::Minify( $this->layout_content );
        }

        if( $this->Is_cache_on() && empty( $_POST["with_ajax"] ) && ! empty( $cache_time ) )
        {
            App()->cache->Create_cache( $view_name, $this->layout_content );
        }
    }

    /**
     * @method  Load_layout
     * @access  private
     * @desc    Load a layout by name for rendering the page
     * @author  Cousin Bela
     *
     * @param   string                  $view_name              - the name of template
     * @param   array                   $params                 - parameters array with tokens to replace
     * @param   int                     $cache_time             - if we want with cache, set the time for cache available
     *
     * @version 1.0.0
     * @return  string
     * @throws  MException
     */
    private function Load_layout( $view_name, $params = array(), $cache_time = 0 )
    {
        // calculate new path where to load the view from
        $this->Calculate_template_path( "/layouts/".$view_name );

        // Calculate layout path
        $layout_path = $this->template_path."layouts".DIRECTORY_SEPARATOR.$this->layout.Settings::EXT;
        if( ! file_exists( $layout_path ) )
        {
            throw new MPHP_exception(
                E_ERROR,
                "Layout with name ".$this->layout." not found, path where tried to search is: ".dirname( $layout_path )
            );
        }

        // Generate content for layout
        $content = $this->Load_template( $view_name, $params, $cache_time );

        // Set variables for we can use in templates, layouts
        foreach( $this->params as $key => $value )
        {
            $$key = $value;
        }

        try
        {
            include $layout_path;
        }
        catch( MPHP_exception $e )
        {
            ob_end_clean();
            $e->Show_error_and_stop();
        }
    }

    /**
     * @method  Load_template
     * @access  private
     * @desc    Load a view by name when rendering the page with layout
     * @author  Cousin Bela
     *
     * @param   string                  $view_name              - the name of template
     * @param   array                   $params                 - parameters array with tokens to replace
     * @param   int                     $cache_time             - if we want with cache, set the time for cache available
     *
     * @version 1.0.0
     * @return  string
     * @throws  MException
     */
    private function Load_template( $view_name, $params = array(), $cache_time = 0 )
    {
        if( $this->Is_cache_on() && App()->cache->Cache_exists( $view_name, $cache_time ) )
        {
            return $this->Get_template_from_cache( $view_name );
        }

        if( ! empty( $params ) && is_array( $params ) )
        {
            $this->params = array_merge( $this->params, $params );
        }

        // calculate new path where to load the view from
        $this->Calculate_template_path( $view_name );

        $this->template = preg_replace( "/^([\/]+)(.*)/", "$2", $view_name );

        // Calculate template path
        $template_path  = $this->template_path;
        $template_path .= $this->template.Settings::EXT;

        // if template not exists
        if( ! file_exists( $template_path ) )
        {
            throw new MPHP_exception(
                E_ERROR,
                "View with name ".basename( $this->template )." not found, last path where tried to search is: ".dirname( $template_path )
            );
        }

        // Set param for we can use in template
        foreach( $this->params as $key => $value )
        {
            $$key = $value;
        }

        try
        {
            ob_start();
            include $template_path;
            $content = ob_get_clean();
        }
        catch( MPHP_exception $e )
        {
            ob_end_clean();
            $e->Show_error_and_stop();
        }

        // Create cache file
        if( $this->Is_cache_on() && $cache_time )
        {
            App()->cache->Create_cache( $view_name, $content );
        }

        return $content;
    }

    /**
     * @method  Is_cache_on
     * @access  private
     * @desc    Checks and returns TRUE if cache is on
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  string
     */
    private function Is_cache_on()
    {
        if( ! isset( App()->config->cache["is_on"] ) || ! App()->config->cache["is_on"] )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Get_template_from_cache
     * @access  private
     * @desc    Get content from cache by template name
     * @author  Cousin Bela
     *
     * @param   string                  $view_name              - the name of template
     *
     * @version 1.0.0
     * @return  string
     */
    private function Get_template_from_cache( $view_name )
    {
        $content = App()->cache->Get_content( $view_name );

        return $content;
    }

    /**
     * @method  Calculate_template_name_for_cache
     * @access  private
     * @desc    Calculate template name for cache file
     * @author  Cousin Bela
     *
     * @param   string                  $view_name              - the name of template
     *
     * @version 1.0.0
     * @return  string
     */
    private function Calculate_template_name_for_cache( $view_name )
    {
        return str_replace( array( "\\", "/" ), ".", $view_name );
    }


    /**
     * @method  Show_content
     * @access  public
     * @desc    Shows the rendered content
     * @author  Cousin Bela
     * 
     * @version 1.0.0
     */
    public function Show_content()
    {
        echo $this->layout_content;
    }
}

/* End of file MRenderer.php */
/* Location: ./core/Libraries/ */