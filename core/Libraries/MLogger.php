<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class MLogger
 *
 * @version     1.0.0
 */
class MLogger extends BLogger
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @method	Debug
     * @access	public
     * @desc    Add a debug message to logs
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     */
    public function Debug( $message )
    {
        $this->Add( $message, 2 );
    }
    
    /**
     * @method	Add
     * @access	private
     * @desc    Add a message to logs by type
     * @author	Cousin Béla
     * 
     * @param   string        $message      - message to log
     * @param   string        $type         - type if the message: 
     * 
     * @version	1.0.0
     */
    private function Add( $message, $type )
    {
        $type               = strtoupper( $type );
        $enabled_log_type   = $this->available_types[$this->log_types];
        
        if( ! $this->log_types || ( $enabled_log_type != "ALL" && $this->log_types != $type ) )
        {
            return;
        }
          
        if( $this->add_date )
        {
            $message = date( $this->date_format )." - ".$message;
        }
        
        $this->logs[] = $this->available_types[$type]." - ".$message;
    }

    /**
     * @method	Error
     * @access	public
     * @desc    Add an error message to logs
     * @author	Cousin Béla
     *
     * @param   string        $message      - message to log
     *
     * @version	1.0.0
     */
    public function Error( $message )
    {
        $this->Add( $message, 3 );
    }
    
    /**
     * @method	Info
     * @access	public
     * @desc    Add an info message to logs
     * @author	Cousin Béla
     * 
     * @param   string        $message      - message to log
     * 
     * @version	1.0.0
     */
    public function Info( $message )
    {
        $this->Add( $message, 4 );
    }

    /**
     * @method	Warning
     * @access	public
     * @desc    Add a warning message to logs
     * @author	Cousin Béla
     *
     * @param   string        $message      - message to log
     *
     * @version	1.0.0
     */
    public function Warning( $message )
    {
        $this->Add( $message, 5 );
    }
    
    /**
     * @method	GetLogs
     * @access	public
     * @desc    Return all logs
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     */
    public function Write_logs()
    {
        $log_data = $this->Get_log_data();

        if( empty( $log_data ) )
        {
            return;
        }

        $log_data .= "<br /><br />Link: ".$_SERVER["REQUEST_URI"];

        //return $this->logs;
    }

    protected function Get_log_data()
    {
        if( empty( $this->logs ) )
        {
            return "";
        }

        $logs_data = "";
        foreach( $this->logs as $log )
        {
            $logs_data .= $log."\r\n";
        }

        return $logs_data;
    }
}

/* End of file Logger.php */
/* Location: ./core/Libraries/ */