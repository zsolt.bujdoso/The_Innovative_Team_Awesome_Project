<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*
 * APPLICATION, this will be the instance
 */
$GLOBALS["APPLICATION"] = $_APPLICATION = "";

/**
 * class Application
 *
 * @version     1.0.0
 */
class Application extends App_Base
{
    // Public variables
    protected $instantiated_objects = array();
    public $classmap;
    public $classnames_map;
    public $import_paths;
    public $db;
    public $data = array();

    // Private variables
    private $databases;

    public function __construct()
    {
        try
        {
            parent::__construct();

            global $_APPLICATION;
            $_APPLICATION = $this;

            $this->Set_paths_and_maps();
            $this->Init();
        }
        catch( MException $e )
        {
            $e->Show_error_and_stop();
        }
    }
    
    private function Set_paths_and_maps()
    {
        $this->classmap         = include_once COREPATH."Base/Config/Class_map".Settings::EXT;
        $this->classnames_map   = include_once COREPATH."Base/Config/Classnames_map".Settings::EXT;
        $this->import_paths     = include_once COREPATH."Base/Config/Autoload_map".Settings::EXT;
    }
    
    /**
     * @method	Load_classes_for_classmap
     * @access	public
     * @desc    Load class files to create a classmap for autoload
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     */
    private function Load_classes_for_classmap()
    {
        // Load other classes into classmap
        if( ! empty( $this->import_paths ) && is_array( $this->import_paths ) )
        {
            foreach( $this->import_paths as $include_path )
            {
                General_functions::Load_class_files_from_folder_and_subfolders( $include_path );
            }
        }
    }
    
    protected function Init()
    {
        $this->Load_classes_for_classmap();

        // Create and load default variables
        $this->Create_and_load_default_variables();

        // Execute controller and action
        $this->Create_and_execute_controller_and_action();
    }
    
    /**
     * @method	Create_and_load_default_variables
     * @access	public
     * @desc    Create variables and Load classes into them
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     */
    protected function Create_and_load_default_variables()
    {
        $config     = General_functions::Instantiate_class( "mconfig", TRUE, TRUE );
        $this->Set_instantiated_object( "config", $config );

        $profiler   = General_functions::Instantiate_class( "mprofiler", TRUE, TRUE );
        $this->Set_instantiated_object( "profiler", $profiler );

        $logger     = General_functions::Instantiate_class( "mlogger", TRUE, TRUE );

        $this->Set_instantiated_object( "logger", $logger );

        // Create database connection if is set
        $this->Init_database_and_connect();

        $session    = General_functions::Instantiate_class( "msession", TRUE, TRUE );
        $this->Set_instantiated_object( "session", $session );

        $validation = General_functions::Instantiate_class( "mvalidation", TRUE, TRUE );
        $this->Set_instantiated_object( "validation", $validation );

        $lang       = General_functions::Instantiate_class( "mlang", TRUE, TRUE );
        $this->Set_instantiated_object( "lang", $lang );

        $request    = General_functions::Instantiate_class( "mrequest", TRUE, TRUE );
        $this->Set_instantiated_object( "request", $request );

        $router     = General_functions::Instantiate_class( "mrouter", TRUE, TRUE );
        $this->Set_instantiated_object( "router", $router );

        if( ! empty( $this->config->cache["class"] ) )
        {
            $this->classnames_map["mcache"] = $this->config->configurations["cache"]["class"];
            $cache  = General_functions::Instantiate_class( "mcache", TRUE, TRUE );
            $this->Set_instantiated_object( "cache", $cache );
        }

        $renderer   = General_functions::Instantiate_class( "mrenderer", TRUE, TRUE );
        $this->Set_instantiated_object( "renderer", $renderer );

        $theme      = General_functions::Instantiate_class( "mtheme", TRUE, TRUE );
        $this->Set_instantiated_object( "theme", $theme );

        $redirect   = General_functions::Instantiate_class( "mredirect", TRUE, TRUE );
        $this->Set_instantiated_object( "redirect", $redirect );
    }

    /**
     * @method	Init_database_and_connect
     * @access	public
     * @desc    Init new database connection and return it or set into App
     * @author	Cousin Béla
     *
     * @param   string                      $database_config_name                   - config name in database configs
     * @param   bool                        $return                                 - return the new connection or not
     *
     * @version 1.0
     * @return  bool|object
     */
    public function Init_database_and_connect( $database_config_name = "", $return = FALSE )
    {
        $database =& $this->Init_database( $database_config_name );

        // if no database, return FALSE
        if( ! $database )
        {
            return FALSE;
        }

        if( $return )
        {
            return $database;
        }

        $this->db =& $database;
    }

    /**
     * @method	Init_database
     * @access	public
     * @desc    Init new database connection and return it
     * @author	Cousin Béla
     *
     * @param   string                      $database_config_name                   - config name in database configs
     *
     * @version 1.0
     * @return  bool|object
     * @throws  MDatabase_exception
     */
    private function & Init_database( $database_config_name = "" )
    {
        // if database set is empty, set it to default
        if( empty( $database_config_name ) )
        {
            $database_config_name = App()->config->database["default"];
        }

        // Get the database config
        $database_config = Database_helper::Load_database_config( $database_config_name );

        // Check this config is empty
        if( ! $database_config )
        {
            $database = FALSE;
            return $database;
        }

        $database = General_functions::Instantiate_class( "db_".$database_config["driver"], TRUE, TRUE, TRUE );
        $database->Set_config( $database_config );

        // try to connect to database
        if( ! $database->Connect() )
        {
            throw new MDatabase_exception(
                "Error encountered while connecting to database on host: ".$database_config["hostname"].", port: ".$database_config["port"].PHP_EOL
                .$database->Get_error_message()
            );
        }

        $database->Set_collate();
        $database->Select_database();

        $this->databases[$database_config_name] =& $database;

        return $database;
    }
    
    /**
     * @method	Create_and_execute_controller
     * @access	public
     * @desc    This method will try to instantiate and execute the controller
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    protected function Create_and_execute_controller_and_action()
    {
        $this->Include_module_and_module_paths( $this->router->Get_module() );
        $this->Include_controller();

        General_functions::Instantiate_class( "controller", TRUE );

        // $this->controller = new $this->controller();

        if( ! $this->controller instanceof MController )
        {
            throw new MPHP_exception(
                E_ERROR,
                "The controller '".$this->controller()."' must be an instance of 'MController'"
            );
        }

        $this->Execute_Action();
        $this->Render_and_stop();
    }       
    
    /**
     * @method	Include_module_and_module_paths
     * @access	public
     * @desc    Import module and instantiate it, after that import paths from it
     * @author	Cousin Béla
     *
     * @param   string                      $module                   - module name to import
     *
     * @version	1.0.0
     * @throws  MPHP_exception
     */
    private function Include_module_and_module_paths( $module )
    {
        if( empty( $module ) )
        {
            return;
        }
        
        General_functions::Include_module( $module, TRUE );
        General_functions::Instantiate_class( "module", TRUE );

        if( ! $this->module instanceof MModule )
        {
            throw new MPHP_exception(
                E_ERROR,
                "The module '".$module."' must be an instance of 'MModule'"
            );
        }

        $this->module->Init();
    }
    
    /**
     * @method	Include_controller
     * @access	public
     * @desc    Include the controller and instanciate it
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     * @throws  MException
     */
    private function Include_controller()
    {
        if( ! $this->router->Get_controller() )
        {
            throw new MPHP_exception( E_ERROR, "You have to set a default controller" );
        }

        General_functions::Include_controller( $this->router->Get_controller() );
        // General_functions::Instantiate_class( "controller", TRUE );
    }
    
    /**
     * @method	Execute_Action
     * @access	public
     * @desc    Execute the action method from controller
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     */
    protected function Execute_Action()
    {
        $this->controller->Init();

        // Execute before action
        $this->Execute_controller_before_action();

        $this->Execute_controller_method();
        
        // Execute after action
        $this->Execute_controller_after_action();
    }
    
    /**
     * @method	Execute_controller_before_action
     * @access	public
     * @desc    Execute the before action function if it is set 
     *          and starts the Pre_controller profiler
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     */
    private function Execute_controller_before_action()
    {
        $this->profiler->Start( "Pre_controller" );
        
        if( method_exists( $this->controller, "Before_action" ) )
        {
            $this->controller->Before_action();
        }
    }
    
    /**
     * @method	Execute_controller_method
     * @access	public
     * @desc    Execute the method from controller
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     */
    private function Execute_controller_method()
    {
        $this->profiler->Start( "Controller_method" );
        
        $function = strtolower( $this->router->Get_function() );
        
        if( ! method_exists( $this->controller, "Action_".$function ) )
        {
            throw new MPage_not_found_exception(
                "Controller '".get_class( $this->controller )."' does not have function "
                . "named 'Action_".$function."'" 
            );
        }
        
        call_user_func_array(
            array( 
                $this->controller, 
                "Action_".$function, 
            ),
            $this->router->Get_parameters() 
        );

        $this->profiler->Stop( "Controller_method" );
    }
    
    /**
     * @method	Execute_controller_after_action
     * @access	public
     * @desc    Execute the after action function if it is set 
     *          and stops the Pre_controller profiler
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     */
    private function Execute_controller_after_action()
    {
        if( method_exists( $this->controller, "After_action" ) )
        {
            $this->controller->After_action();
        }

        $this->profiler->Stop( "Pre_controller" );
    }

    /**
     * @method	Render_and_stop
     * @access	public
     * @desc    Render content and stop the system
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    public function Render_and_stop()
    {
        header( Array_helper::Get_status_by_code( Settings::STATUS_OK ) );

        if( is_object( $this->renderer ) )
        {
            // show the rendered content
            $this->renderer->Show_content();
        }

        $this->Finish();
    }

    /**
     * @method	Stop_all
     * @access	public
     * @desc    Stop all libraries, and close connections if exists
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    public function Finish( $write_logs = TRUE )
    {
        if( $write_logs )
        {
            $this->Write_logs();
        }
        $this->Close_database();
        $this->Show_profiler();

        die();
    }

    /**
     * @method	Show_profiler
     * @access	public
     * @desc    Show the profiler data, execution time the system were run
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    public function Show_profiler()
    {
        if( ! is_object( $this->profiler ) )
        {
            return;
        }

        $this->profiler->Stop( "System" );

        // If auto show is set to TRUE
        if( $this->config->profile["auto_show"] )
        {
            $this->profiler->Show_result();
        }
    }

    /**
     * @method	Stop_and_write_logs
     * @access	public
     * @desc    Stop and write out logs were it has to do, file or memory, or DB
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    private function Write_logs()
    {
        if( ! is_object( $this->logger ) )
        {
            return;
        }

        $this->logger->Write_logs();
    }

    /**
     * @method	Stop_and_close_database
     * @access	public
     * @desc    Stop and close database connection
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    public function Close_database()
    {
        if( empty( $this->databases ) )
        {
            return;
        }

        foreach( $this->databases as & $database )
        {
            $database->Close_connection();
        }
    }
    
    /**
     * @method	Set_instantiated_object
     * @access	public
     * @desc    Save instantiated object for we can use at other times too
     * @author	Cousin Béla
     *
     * @param   string                      $name                       - name of the object to save
     * @param   object                      $value                      - object
     *
     * @version	1.0.0
     */
    public function Set_instantiated_object( $name, & $value )
    {
        $this->instantiated_objects[$name] = $value;
    }

    /**
     * @method	Get_instantiated_object
     * @access	public
     * @desc    Return instantiated object if exists
     * @author	Cousin Béla
     *
     * @param   string                      $name                       - name of the object to return
     *
     * @version	1.0.0
     * @return  object|bool
     */
    public function & Get_instantiated_object( $name )
    {
        $return = FALSE;

        if( ! isset( $this->instantiated_objects[$name] ) )
        {
            return $return;
        }

        return $this->instantiated_objects[$name];
    }
    
    /**
     * @method  Get_classname_for
     * @access  public
     * @desc    This method returns the name of the class we have to instantiate, if not exists than return false
     * @author  Cousin Bela
     * 
     * @param   string                      $type                       - the type to send the classname
     *
     * @version 1.0.0
     * @return  boolean|string
     */
    public function Get_classname_for( $type )
    {
        if( empty( $this->classnames_map[$type] ) )
        {
            return FALSE;
        }

        return $this->classnames_map[$type];
    }
    
    /**
     * @method  Get_classname_for
     * @access  public
     * @desc    This method sets the name of the class we have to instanciate for selected type
     * @author  Cousin Bela
     *
     * @param   string                      $type                       - the type to send the classname
     * @param   string                      $classname                  - the name of the classname
     *
     * @version 1.0
     * @return  boolean or string
     */
    public function Set_classname_for( $type, $classname )
    {
        $this->classnames_map[$type] = $classname;
    }

    /**
     * @method  Has_class
     * @access  public
     * @desc    This method check class exist by name or not
     * @author  Cousin Bela
     *
     * @param   string                      $class_name                 - name of the class
     *
     * @version 1.0.0
     * @return  boolean|string
     */
    public function Has_class( $class_name )
    {
        if( ! isset( $this->classmap[$class_name] ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Has_module
     * @access  public
     * @desc    This method check module exist by name or not
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - name of the module
     *
     * @version 1.0.0
     * @return  boolean|string
     */
    public function Has_module( $module )
    {
        $module = strtolower( $module );

        if( empty( $this->instantiated_objects["config"] ) || ! in_array( $module, $this->config->module ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Get_module
     * @access  public
     * @desc    This method returns a module if exist by name or not
     * @author  Cousin Bela
     *
     * @param   string                      $module                     - code of the module
     *
     * @version 1.0.0
     * @return  boolean|string
     */
    public function Get_module( $module )
    {
        $module .= "_module";

        if( ! isset( $this->instantiated_objects[$module] ) )
        {
            return FALSE;
        }

        return $this->instantiated_objects[$module];
    }

    /**
     * @method  Get_modules
     * @access  public
     * @desc    This method returns all modules array
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  boolean|string
     */
    public function Get_modules()
    {
        return $this->config->module;
    }

    /**
     * @method  Get_module_details
     * @access  public
     * @desc    This method returns module details array
     * @author  Cousin Bela
     *
     * @param   string                      $code                       - code of the module
     *
     * @version 1.0.0
     * @return  boolean|string
     */
    public function Get_module_details( $code )
    {
        if( empty( $this->data["modules"] ) )
        {
            return FALSE;
        }

        foreach( $this->data["modules"]->Get_result() as $module )
        {
            if( $module->link == $code )
            {
                return $module;
            }
        }

        return FALSE;
    }

    /**
     * @method  Is_config_loaded
     * @access  public
     * @desc    This method checks if config was loaded or not
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  boolean|string
     */
    public function Is_config_loaded()
    {
        if( empty( $this->instantiated_objects["config"] ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * @method  Has_config
     * @access  public
     * @desc    This method checks if a config exists or not
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  boolean|string
     */
    public function Has_config( $config_name )
    {
        if( empty( $this->instantiated_objects["config"] ) && $this->config->Has( $config_name ) )
        {
            return FALSE;
        }

        return TRUE;
    }
}
    
/**
 * @method  Get_classname_for
 * @access  public
 * @desc    Returns the instance of created application
 * @author  Cousin Bela
 *
 * @global  object $_APPLICATION
 *
 * @version 1.0
 * @return  Application
 */
function & App()
{   
    global $_APPLICATION;

    return $_APPLICATION;
}

/* End of file Application.php */
/* Location: ./core/Base/ */