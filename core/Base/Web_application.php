<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Web_application
 *
 * @version     1.0.0
 */
class Web_application extends Application
{
  
    public function __construct()
    {
        parent::__construct();
    }
    
    protected function Init()
    {
        // Set the renderer for we can show file contents
        //$this->Set_classname_for( "renderer", "MFile_renderer" );
        
        parent::Init();
    }
}

/* End of file Web_application.php */
/* Location: ./core/Base/ */