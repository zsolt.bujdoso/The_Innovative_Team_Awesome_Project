<?php

/**
 * This class is needed for Profilers for we can know which functions we have to implement
 * Implements the IProfiler to know how they looks like
 * 
 * 
 */
abstract class AProfiler extends Base implements IProfiler
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function Start( $type )
    {
      
    }
    
    public function Stop( $type )
    {
      
    }
    
    public function Get( $type )
    {
      
    }
}

/* End of file AProfiler.php */
/* Location: ./core/Base/AClasses */
