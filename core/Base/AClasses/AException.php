<?php

abstract class AException extends Base implements IException
{
    public function __construct()
    {
        parent::__construct();
    }
}

/* End of file AException.php */
/* Location: ./core/Base/AClasses */