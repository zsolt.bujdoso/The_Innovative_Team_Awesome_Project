<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    COREPATH."Base/Interfaces/",
    COREPATH."Base/AClasses/",
    COREPATH."Base/BClasses/",
    COREPATH."Libraries/",
    COREPATH."Helpers/",
    COREPATH."Models/",
    BASEPATH.Settings::PROJECT_CORE_DIR."Libraries/",
    BASEPATH.Settings::PROJECT_CORE_DIR."Helpers/",
    BASEPATH.Settings::PROJECT_CORE_DIR."Models/",
    //BASEPATH.Settings::PROJECT_CORE_DIR."Plugins/",
);


/* End of file Aotoload_map.php */
/* Location: ./core/Base/Config */