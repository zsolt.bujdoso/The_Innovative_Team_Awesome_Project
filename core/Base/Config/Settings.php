<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


class Settings
{
    /*****************************************************
    *	SET THE VERSION NUMBER
    *****************************************************/
    // 
    //	This constants has the version number
    //
    const VERSION = "1.0.0";
    
    /*****************************************************
    *	DEFINE SOME OTHER CONSTANTS, NEEDED FOR FRAMEWORK
    *****************************************************/

    /** 
     *	This variable have the extension for files we will work
     */
    const EXT = ".php";
    
    /** 
     *	This variable have the prefix for classes that rewrites main classes
     */
    const SUBCLASS_PREFIX = "MY_";
    
    /** 
     *	This variable have the direcotry name for core folders parent
     */
    const PROJECT_CORE_DIR = "Core/";

    const TEMPLATE_ERROR_NO_DEBUG = "no_debug";
    const STATUS_OK = 200;
    const STATUS_REDIRECTED = 301;
    const STATUS_MOVED_PERMANENTLY = 302;
    const STATUS_NOT_FOUND = 404;
    const STATUS_INTERNAL_SERVER_ERROR  = 500;

    // 
    //	Create an array for exceptions of files or folders which
    //	will not be in the files list when we get the list of files
    //	for a folder
    //
    public static $FILE_NAMES_EXCEPTION_FOR_GET_FILES_LIST = array( ".", "..", ".svn" );
    
    public static $DEFAULT_CONFIGRATIONS = array(
        // main config
        "site_name"         => "Make Your Site Easy",
        "log_types"         => 0,
        "log_add_date"      => FALSE,
        "log_date_format"   => "Y-m-d H:i:s",
        "debug_mode"        => TRUE,
        "config_path"       => "Config/",
        "theme"             => "",

        "cache"             => array(
                                "path"  => "",
                                "is_on" => FALSE,
                                "class" => ""
                            ),

        "database"          => array(
                            "default"       => "local",
                            "is_cache_on"   => FALSE,
                            "cache_path"    => "Cache/Database/",
                            "cache_class"   => "",
                            "cache_time"    => 600,
                            "list"          => array(
                                                  "live" => array(
                                                      "hostname"  => "",
                                                      "port"      => "",
                                                      "username"  => "",
                                                      "password"  => "",
                                                      "database"  => "",
                                                      "driver"    => "",
                                                      "prefix"    => "",
                                                      "charset"   => "",
                                                      "collate"   => "",
                                                  ),
                                                  "local" => array(
                                                      "hostname"  => "",
                                                      "port"      => "",
                                                      "username"  => "",
                                                      "password"  => "",
                                                      "database"  => "",
                                                      "driver"    => "",
                                                      "prefix"    => "",
                                                      "charset"   => "",
                                                      "collate"   => "",
                                                  ),
                                              ),
                            ),
        "email"             => array(
                                // Set the mailer type: mail, smtp or sendmail
                                "mailer"        => "mail",

                                // if mailer is smtp, set the cinfog
                                "smtp_host"     => "",
                                "smtp_port"     => "465",
                                "smtp_user"     => "",
                                "smtp_pass"     => "",
                                "smtp_timeout"  => "30",
                                "sendmail"      => "",
                            ),
        "language"          => array(
                                "default"       => "hungarian",
                                "code"          => "hu",
                                "languages"     => array(
                                                    "hu" => array( "folder" => "hungarian", "name" => "Hungarian", "date_format" => "Y-m-d" ),
                                                    "en" => array( "folder" => "english",   "name" => "English", "date_format" => "d-m-Y" ),
                                                    "ro" => array( "folder" => "romanian",  "name" => "Romanian", "date_format" => "d-m-Y" )
                                                ),
                            ),
        "module"            => array(),
        "session"           => array(
                                // Set the session type: SESSION or DB
                                "type"          => "MSession",
                                // If type is DB
                                "table_name"    => "session",

                                // Common
                                "encryption_key"=> "",
                                "match_ip"      => TRUE,
                                "match_agent"   => TRUE,
                                "encrypt_cookie"=> FALSE,
                                "expiration"    => 7200,
                                "session_name"  => "PHPSESSID",
                            ),
        "url"               => array(
                                "type"              => "PATH",
                                "parameter"         => "r",
                                "default_module"    => "",
                                "default_controller"=> "default",
                                "default_function"  => "index",
                                "routes"            => array(),
                            ),
        "profile"           => array(
                                "auto_show"                 => FALSE,
                                "show_memory"               => FALSE,
                                "show_time"                 => FALSE,
                                "show_system_time"          => FALSE,
                                "show_queries"              => FALSE,
                                "show_number_of_queries"    => FALSE,
                                "show_duplicated_queries"   => FALSE,
                                "show_pre_controller_time"  => FALSE,
                                "show_controller_time"      => FALSE,
                                "template"                  => "",
                            ),
        "error"             => array(
                                "debug"     => null,
                                "301"       => null,
                                "302"       => null,
                                "404"       => null,
                                "500"       => null,
                            ),
    );
    
    public static $NOT_REWRITABLE_CLASSES = array( 
        "mconfig", "app_base", "application", "web_application"
    );
}

/* End of file Settings.php */
/* Location: ./core/Base/Config/ */