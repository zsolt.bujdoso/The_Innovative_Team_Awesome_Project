<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


return array(
    "mlogger"       => "MLogger",
    "mconfig"       => "MConfig",
    "mcache"        => "MFile_cache",
    "mexception"    => "MException",
    "mlang"         => "MLang",
    "mprofiler"     => "MProfiler",
    "mrequest"      => "MRequest",
    "mrouter"       => "MRouter",
    "mvalidation"   => "MValidation",
    "mrenderer"     => "MRenderer",
    "mdatabase"     => "MDatabase",
    "msession"      => "MSession",
    "mtheme"        => "MTheme",
    "mredirect"     => "MRedirect",
);


/* End of file Classnames_map.php */
/* Location: ./core/Base/Config */