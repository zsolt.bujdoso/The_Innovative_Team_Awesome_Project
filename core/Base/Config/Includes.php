<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


require_once "Settings.php";
require_once COREPATH."Base/Base".Settings::EXT;
require_once COREPATH."Base/Interfaces/IProfiler".Settings::EXT;
require_once COREPATH."Base/AClasses/AProfiler".Settings::EXT;
require_once COREPATH."Base/BClasses/BProfiler".Settings::EXT;
require_once COREPATH."Base/General_functions".Settings::EXT;
require_once COREPATH."Base/App_Base".Settings::EXT;
require_once COREPATH."Base/Application".Settings::EXT;
require_once COREPATH."Base/Web_application".Settings::EXT;
require_once COREPATH."Base/Api_application".Settings::EXT;


/* End of file Includes.php */
/* Location: ./core/Base/Config */