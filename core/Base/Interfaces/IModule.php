<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );

/**
 * This interface is needed for Profilers for we can know how the functions need to looks like
 * 
 * 
 */
interface IModule
{
    function Init();
    function Set_Import();
    function Get_id();
}

/* End of file IProfiler.php */
/* Location: ./core/Base/Interfaces */