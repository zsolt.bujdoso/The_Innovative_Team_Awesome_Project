<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );

interface IMDB_Result
{
    public function Add_db_result( $db_result );
    public function Get_first_row();
    public function Get_nr_rows();
}

/* End of file IMDB_Result.php */
/* Location: ./core/Base/Interfaces/ */