<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );

interface IController
{
    public function Init();
}

/* End of file IController.php */
/* Location: ./core/Base/Interfaces */