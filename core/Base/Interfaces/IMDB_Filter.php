<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );


interface IMDB_Filter
{
    public function Get_select_query( $type );
    public function Get_set_query();
    public function Get_from_query( $type );
    public function Get_join_query();
    public function Get_where_query();
    public function Get_like_query();
    public function Get_group_by_query();
    public function Get_having_query();
    public function Get_order_by_query();
    public function Get_limit_query();
}

/* End of file IMDB_Result.php */
/* Location: ./core/Base/Interfaces/ */