<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );

interface ILang
{
    function Set_language();
}

/* End of file ILang.php */
/* Location: ./core/Base/Interfaces */