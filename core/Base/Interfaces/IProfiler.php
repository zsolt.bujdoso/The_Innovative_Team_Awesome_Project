<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );

/**
 * This interface is needed for Profilers for we can know how the functions need to looks like
 * 
 * 
 */
interface IProfiler
{
    function Start( $type );
    function Stop( $type );
    function Stop_all();
    function Get( $type );
}

/* End of file IProfiler.php */
/* Location: ./core/Base/Interfaces */