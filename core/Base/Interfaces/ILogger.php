<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );

interface ILogger
{
    function Debug( $message );
    function Error( $message );
    function Info( $message );
    function Write_logs();
}

/* End of file ILogger.php */
/* Location: ./core/Base/Interfaces/ */