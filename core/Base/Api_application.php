<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Api_application
 *
 * @version     1.0.0
 */
class Api_application extends Application
{
  
    public function __construct()
    {
        parent::__construct();
    }
    
    protected function Init()
    {
        General_functions::Include_files_from_folder_and_subfolders( BASEPATH."Core/Autoloaders/" );

        parent::Init();
    }
}

/* End of file Api_application.php */
/* Location: ./core/Base/ */