<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BPayment
 *
 * @property    Payment_Ramburs     $library
 *
 * @version     1.0.0
 */
abstract class BPayment extends Base
{
    protected $type;
    protected $available_types  = array( "coupon", "paypal", "payu", "ramburs", "mobilpay", "euplatesc" );
    protected $library;

    protected $status           = FALSE;
    protected $error_message;

    protected $token;
    protected $transaction_id;
    protected $correlation_id;
    protected $result;

    public function __construct()
    {
        /*
         * todo: Cousin: Load available payment types
         */

        $this->type = "ramburs";

        $this->Load_payment_library();
    }

    public function Set_type_and_load_library( $payment_type )
    {
        if( ! in_array( $payment_type, $this->available_types ) )
        {
            return FALSE;
        }

        $this->type = $payment_type;

        $this->Load_payment_library();
    }

    public function Set_types( $payment_types )
    {
        if( ! is_array( $payment_types ) )
        {
            return FALSE;
        }

        $this->available_types = $payment_types;
    }

    protected function Load_payment_library()
    {
        $class_name     = "Payment_".ucfirst( $this->type );
        $this->library  = new $class_name();
    }

    public function Clear_types()
    {
        $this->available_types = array();
    }

    public function Add_type( $payment_type )
    {
        if( in_array( $payment_type, $this->available_types ) )
        {
            return FALSE;
        }

        $payment_name = strtolower( "m".$payment_type );

        if( ! App()->Get_classname_for( $payment_name ) )
        {
            return FALSE;
        }

        $this->available_types []= $payment_type;
    }

    protected function Reset_result()
    {
        $this->result = array();
    }

    protected function Load_result()
    {
        if( ! is_object( $this->library ) || ! method_exists( $this->library, "Get_result" ) )
        {
            return;
        }

        $this->result = $this->library->Get_result();
    }

    /**
     * @return  string
     */
    public function Get_error_message()
    {
        return $this->error_message;
    }

    /**
     * @return  bool
     */
    public function Get_status()
    {
        return $this->status;
    }

    /**
     * @return  string
     */
    public function Get_token()
    {
        return $this->token;
    }

    /**
     * @return  string
     */
    public function Get_transaction_id()
    {
        return $this->transaction_id;
    }

    /**
     * @return  string
     */
    public function Get_correlation_id()
    {
        return $this->correlation_id;
    }

    /**
     * @return  mixed
     */
    public function Get_result()
    {
        return $this->result;
    }

    public function Get_result_value( $key )
    {
        if( ! isset( $this->result[$key] ) )
        {
            return "";
        }

        return $this->result[$key];
    }
}

/* End of file BPayment.php */
/* Location: ./core/Base/BClasses */