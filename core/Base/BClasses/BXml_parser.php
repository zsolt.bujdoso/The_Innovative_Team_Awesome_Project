<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BXml_parser
 *
 * @version     1.0.0
 */
abstract class BXml_parser extends Base implements IXml_parser
{
    public function __construct()
    {
    }
}

/* End of file BXml_parser.php */
/* Location: ./core/Base/BClasses */