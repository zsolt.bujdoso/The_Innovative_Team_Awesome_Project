<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BValidation
 *
 * @version     1.0.0
 */
abstract class BValidation extends Base implements IValidation
{
    protected $errors           = array();
    protected $start_delimiter  = "";
    protected $end_delimiter    = "<br />";
    protected $has_error        = FALSE;
    
    public function __construct()
    {
    }

    /**
     * @method	Add_error_message
     * @access	public
     * @desc    this method add a new error message from callback functions
     * @author	Zoltan Jozsa
     * 
     * @param 	string						$message		        - the new message
     * 
     * @version	1.0
     */
    public function Add_error_message( $message = "" )
    {
        $this->errors []= $this->start_delimiter.$message.$this->end_delimiter;
    }

    /**
     * @method	Set_error_delimiter
     * @access	public
     * @desc    this method sets the delimiter for error messages
     * @author	Zoltan Jozsa
     * 
     * @param 	string						$start_delimiter	    - the start delimiter
     * @param 	string						$end_delimiter		    - the end delimiter
     * 
     * @version	1.0
     */
    public function Set_error_delimiter( $start_delimiter = "", $end_delimiter = "" )
    {
        $this->start_delimiter	= $start_delimiter;
        $this->end_delimiter	= $end_delimiter;
    }

    /**
     * @method	Get_error_messages
     * @access	public
     * @desc    this method returns the error messages as string
     * @author	Zoltan Jozsa
     *
     * @version	1.0
     * @return  string
     */
    public function Get_error_messages()
    {
        if( empty( $this->errors ) )
        {
            return "";
        }

        return implode( "", $this->errors );
    }

    public function Get_status()
    {
        return ( ! $this->has_error );
    }
}

/* End of file BValidation.php */
/* Location: ./core/Base/BClasses */