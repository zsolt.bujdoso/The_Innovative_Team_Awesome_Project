<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BCache
 *
 * @version     1.0.0
 */
abstract class BCache extends Base implements ICache
{
    protected $cache_path   = "";
    protected $is_on        = FALSE;
    protected $cache_time   = 600;

    public function __construct()
    {
        if( ! empty( App()->config->cache["path"] ) )
        {
            $this->cache_path   = BASEPATH."Runtime".DIRECTORY_SEPARATOR
                                    .App()->config->cache["path"]."Page".DIRECTORY_SEPARATOR;
        }

        if( empty( $this->cache_path ) )
        {
            $this->cache_path   = BASEPATH."Runtime".DIRECTORY_SEPARATOR
                                    ."Cache".DIRECTORY_SEPARATOR."Page".DIRECTORY_SEPARATOR;
        }

        if( isset( App()->config->cache["is_on"] ) )
        {
            $this->is_on        = App()->config->cache["is_on"];
        }
    }

    public function Set_path( $path )
    {
        $this->cache_path = trim( $path, "/" ).DIRECTORY_SEPARATOR;
    }

    public function Set_cache_time( $time )
    {
        $this->cache_time = $time;
    }

    public function Turn_on()
    {
        $this->is_on = TRUE;
    }

    public function Turn_off()
    {
        $this->is_on = FALSE;
    }

    public function Is_turned_on()
    {
        return $this->is_on;
    }

    public function Get_path()
    {
        return $this->cache_path;
    }
}

/* End of file BCache.php */
/* Location: ./core/Base/BClasses */