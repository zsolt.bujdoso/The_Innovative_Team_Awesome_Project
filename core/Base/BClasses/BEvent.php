<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BEvent
 *
 * @version     1.0.0
 */
abstract class BEvent extends Base implements IEvent
{
    public function __construct()
    {
    }
    
    
    
    public function raiseEvent()
    {
      
    }
    
    public function __get( $name )
    {
      
    }
    
    public function __set( $name, $value )
    {
      
    }
}

/* End of file BEvent.php */
/* Location: ./core/Base/BClasses */