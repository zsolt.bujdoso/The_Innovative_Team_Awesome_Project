<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BDatabase
 *
 * @property    MDB_Mem_cache    $cache
 *
 * @version     1.0.0
 */
abstract class BDatabase extends Base implements IDatabase
{
    protected $hostname   = "";
    protected $port       = "";
    protected $username   = "";
    protected $password   = "";
    protected $database   = "";
    protected $driver     = "";
    protected $prefix     = "";
    protected $charset    = "";
    protected $collate    = "";
    protected $cache      = null;
    protected $cache_class= "";
    protected $is_cache_on= FALSE;

    protected $last_cache_class = "";
    protected $last_is_cache_on = FALSE;

    public $str_to_protect_column = "`";
    public $str_to_protect_value  = "'";

    protected $filters    = null;

    protected $identifier = null;

    protected $executed_queries = array();
    protected $query_extension  = "";

    public function __construct()
    {
        parent::__construct();

        $this->Set_cache();
    }

    public function Set_cache( $cache_class = "" )
    {
        $database_config = App()->config->database;
        if( empty( $database_config["is_cache_on"] ) )
        {
            $this->Clear_cache();
            return;
        }

        $this->is_cache_on = TRUE;

        if( empty( $cache_class ) && ! empty( $database_config["cache_class"] ) )
        {
            $cache_class = $database_config["cache_class"];
        }

        if( empty( $cache_class ) )
        {
            throw new MPHP_exception( E_ERROR, "Class for Database cache not set" );
        }

        $this->last_cache_class = $this->cache_class;
        $this->cache_class      = $cache_class;
        $this->cache            = General_functions::Instantiate_class(
            strtolower( $cache_class ), TRUE, TRUE, TRUE
        );
    }

    private function Clear_cache()
    {
        $this->is_cache_on      = FALSE;
        $this->last_is_cache_on = FALSE;
        $this->cache            = null;
        $this->cache_class      = "";
        $this->last_cache_class = "";
    }

    public function Turn_cache_on_and_set_to_memory()
    {
        $this->last_is_cache_on = $this->is_cache_on;
        App()->config->database["is_cache_on"] = $this->is_cache_on = TRUE;

        $this->Set_cache( "MDB_Mem_cache" );
    }

    public function Turn_cache_on_and_set_to_file()
    {
        $this->last_is_cache_on = $this->is_cache_on;
        App()->config->database["is_cache_on"] = $this->is_cache_on = TRUE;

        $this->Set_cache( "MDB_File_cache" );
    }

    public function Turn_cache_back_to_last_state()
    {
        App()->config->database["is_cache_on"] = $this->is_cache_on = $this->last_is_cache_on;
        $this->cache_class = $this->last_cache_class;
        $this->Set_cache( $this->cache_class );
    }

    public function Set_cache_to_memory()
    {
        $this->Set_cache( "MDB_Mem_cache" );
    }

    public function Set_cache_to_file()
    {
        $this->Set_cache( "MDB_File_cache" );
    }

    public function Set_cache_back_to_last_state()
    {
        $this->cache_class = $this->last_cache_class;

        $this->Set_cache( $this->cache_class );
    }

    public function Is_cache_on()
    {
        if( empty( $this->is_cache_on ) )
        {
            return FALSE;
        }

        return TRUE;
    }

    protected function Cache_exists( $query )
    {
        if( empty( $this->cache ) )
        {
            return FALSE;
        }

        return $this->cache->Cache_exists( $query );
    }

    abstract function Connect();
    abstract function Select_database();
    abstract function Set_collate();
    abstract function Close_connection();
}

/* End of file BDatabase.php */
/* Location: ./core/Base/BClasses */