<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BException
 *
 * @version     1.0.0
 */
abstract class BException extends Exception implements IException
{
    protected $message_with_template    = FALSE;
    protected $backtrace                = "";
    protected $error_code               = "";

    public function __construct()
    {
        $this->Set_status_in_header();
    }

    private function Set_status_in_header()
    {
        if( empty( $this->error_code ) )
        {
			return;
        }
		
		if( function_exists( "http_status_code" ) )
		{
			http_status_code( $this->error_code );
			return;
		}

		header( Array_helper::Get_status_by_code( $this->error_code ) );
    }

    /**
     * @method	Show_error_and_stop
     * @access	public
     * @desc    this method shows the error message and will try to run the finish method for application
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    public function Show_error_and_stop()
    {
        try
        {
            $this->Delete_rendered_content();
        }
        catch( Exception $e ) {}

        //$this->Set_status_in_header();
        $this->Show_error();

        // If debug mode is turned off, send email
        if( ! $this->Is_on_debug() && $this->error_code != 404 )
        {
            // Send an email to developers to know something wrong happened
            $this->Send_error_by_email();
        }

        $this->Finish();
    }

    protected function Delete_rendered_content()
    {
        if( ob_get_length() )
        {
            ob_end_clean();
        }
    }

    /**
     * @method	Show_error
     * @access	protected
     * @desc    this method will show the error message if in the config is set
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    protected function Show_error()
    {
        // If message just a text message, show it
        if( $this->Is_on_debug() )
        {
            $this->Show_error_debugged();
            return;
        }

        $this->Show_error_normally();
    }

    protected function Show_error_debugged()
    {
        // If the message was set from template, ex page 404
        if( $this->message_with_template )
        {
            echo $this->message_with_template;
            return;
        }

        echo "Error: <br />"
          . "<pre>"
          .$this->message
          ."</pre>"
          ."<pre>"
          ."Trace: <br />"
          .$this->getFile()
          ."</pre>"
          ."Line number: ".$this->getLine()
          . "<br />";
    }

    protected function Show_error_normally()
    {
        $APP =& App();

        if( is_object( $APP ) && ! empty( $APP->config->error[$this->error_code] ) )
        {
            $renderer = $APP->Get_instantiated_object( "renderer" );

            if( is_object( $renderer ) )
            {
                echo $APP->renderer->Load_partial_view(
                    $APP->config->error[$this->error_code],
                    array( "error_message" => $this->message )
                );

                return;
            }
        }

        echo "We are sorry, the page isn't visible now, please try again later<br />";
        echo $this->message."<br />";
        echo $this->exception_type_message."<br />";
    }

    /**
     * @method	Finish
     * @access	protected
     * @desc    this method tries to finish the application
     * @author	Cousin Béla
     *
     * @version	1.0.0
     */
    protected function Finish()
    {
        // try to finish the application
        try
        {
            // Stop everything
            App()->Finish();
        }
        catch( Exception $e )
        {
            die( "exception end" );
        }
    }

    /**
     * @method	Get_message
     * @access	public
     * @desc    this method return the error message
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string - error message
     */
    public function Get_message()
    {
        return $this->message;
    }

    /**
     * @method	Get_message
     * @access	public
     * @desc    this method returns the file where error happened
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public function Get_file()
    {
        return $this->getFile();
    }

    /**
     * @method	Send_error_by_email
     * @access	public
     * @desc    this method send an email to specified email address to know something wrong happened
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    protected function Send_error_by_email()
    {
        $APP =& App();

		if( ! $APP->Has_config( "email" ) )
        {
            return;
        }
		
		$email_config = $APP->config->email;
        if( empty( $email_config["mailer"] ) )
        {
            return;
        }

        $mailer_class_name = "Mail_".ucfirst( strtolower( $email_config["mailer"] ) );
        if( ! App()->Get_classname_for( strtolower( $mailer_class_name ) ) )
        {
            throw new MMail_exception( $mailer_class_name." not exists" );
        }

        // Create new email with errors
        $mailer = new $mailer_class_name();
        $mailer->Add_to_address( "errors@sakersoft.ro" );
        $mailer->Set_subject( $_SERVER["HTTP_HOST"].": error code: ".$this->code );
        $mailer->Set_message(
            $this->message."<br />"
            ."error code: ".$this->error_code."<br />"
            ."error file: ".$this->file."<br />"
            ."error line: ".$this->line."<br /><br />"
            ."Data POSTed: <br /><pre>".print_r( $_POST, TRUE )."</pre><br /><br />"
            ."Data parameters sent by GET: <br /><pre>".print_r( $_GET, TRUE )."</pre><br /><br />"
            ."Server details: <br /><pre>".print_r( $_SERVER, TRUE )."</pre><br />"
            ."Back trace: <br /><pre>".$this->backtrace."</pre><br />"
        );
        $mailer->Set_content_type_to_html();
        $mailer->Set_charset( "utf-8" );

        // Set from and send the email message
        $mailer->Set_from( "contact@".str_replace( "www.", "", $_SERVER["HTTP_HOST"] ), $_SERVER["HTTP_HOST"] );
        $mailer->Send();
    }

    protected function Is_on_debug()
    {
        if( function_exists( "App" ) )
        {
            $app = App();

            if( method_exists( $app, "Get_instantiated_object" ) )
            {
                $config = App()->Get_instantiated_object( "config" );
            }
        }

        if( empty( $config ) )
        {
            return TRUE;
        }

        return ( App()->config->debug_mode === TRUE );
    }
}

/* End of file BException.php */
/* Location: ./core/Base/BClasses */