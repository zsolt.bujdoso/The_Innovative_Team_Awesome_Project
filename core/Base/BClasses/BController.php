<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BController
 *
 * @property    MConfig     $config
 * @property    MTheme      $theme
 * @property    MFile_cache $cache
 * @property    MLang       $lang
 * @property    MLogger     $logger
 * @property    MProfiler   $profiler
 * @property    MRenderer   $renderer
 * @property    MRouter     $router
 * @property    MRequest    $request
 * @property    MValidation $validation
 * @property    MSession    $session
 * @property    MRedirect   $redirect
 *
 * @version     1.0.0
 */
abstract class BController extends Base implements IController
{
    public $config;
    public $cache;
    public $lang;
    public $logger;
    public $profiler;
    public $renderer;
    public $router;
    public $request;
    public $validation;
    public $session;
    public $theme;
    public $redirect;

    public function __construct()
    {
        parent::__construct();
    }

    public function Init()
    {
        $this->config     =& App()->config;
        $this->theme      =& App()->theme;
        $this->cache      =& App()->cache;
        $this->lang       =& App()->lang;
        $this->logger     =& App()->logger;
        $this->profiler   =& App()->profiler;
        $this->renderer   =& App()->renderer;
        $this->router     =& App()->router;
        $this->request    =& App()->request;
        $this->validation =& App()->validation;
        $this->session    =& App()->session;
        $this->redirect   =& App()->redirect;
    }

    public function & __get( $name )
    {
        if( property_exists( $this, $name ) )
        {
            return $this->$name;
        }

        $property = App()->$name;

        return $property;
    }
}

/* End of file BController.php */
/* Location: ./core/Base/BClasses */