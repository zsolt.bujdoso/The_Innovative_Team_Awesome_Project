<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BRouter
 *
 * @version     1.0.0
 */
abstract class BRouter extends Base implements IRouter
{
    protected $module;
    protected $controller;
    protected $function;
  
    public function __construct()
    {
        parent::__construct();
        
        $this->module = "";
        $this->controller = "Default_controller";
        $this->function = "Index";
    }
    
    public function Check_if_route_exists()
    {
      
    }
}

/* End of file BRouter.php */
/* Location: ./core/Base/BClasses */