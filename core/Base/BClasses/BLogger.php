<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BLogger
 *
 * @version     1.0.0
 */
abstract class BLogger extends Base implements ILogger
{
    protected $logs             = array();
    protected $available_types  = array(
                                    0 => FALSE, 
                                    1 => "ALL", 
                                    2 => "DEBUG", 
                                    3 => "ERROR", 
                                    4 => "INFO",
                                    5 => "WARNING",
                                );
    protected $log_types        = 0;
    protected $add_date         = FALSE;
    protected $date_format      = "Y-m-d H:i:s";
  
    public function __construct()
    {
        $this->log_types      = App()->config->log_types;
        $this->add_date       = App()->config->log_add_date;
        $this->date_format    = App()->config->log_date_format;
    }
}

/* End of file BLogger.php */
/* Location: ./core/Base/BClasses/ */