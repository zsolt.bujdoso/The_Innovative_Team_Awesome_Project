<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BSession
 *
 * @version     1.0.0
 */
abstract class BSession extends Base implements ISession
{
    protected $data;
    protected $encrypt_cookie;
    protected $encryption_key;
    protected $match_ip;
    protected $match_agent;
    protected $expiration;
    protected $session_name;
    protected $session_id;
    
    public function __construct()
    {
        $this->encrypt_cookie = App()->config->session["encrypt_cookie"];
        $this->encryption_key = App()->config->session["encryption_key"];
        $this->match_ip       = App()->config->session["match_ip"];
        $this->match_agent    = App()->config->session["match_agent"];
        $this->expiration     = App()->config->session["expiration"];
        $this->session_name   = App()->config->session["session_name"];
    }
}

/* End of file BSession.php */
/* Location: ./core/Base/BClasses/ */