<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BPayment_type
 *
 * @version     1.0.0
 */
abstract class BPayment_type extends Base implements IPayment
{
    protected $type             = "";
    protected $status           = FALSE;
    protected $error_message;

    protected $token            = "";
    protected $transaction_id   = 0;
    protected $correlation_id   = 0;
    protected $result;

    protected $payment_mode     = "test";
    protected $payment_config;
    protected $all_config;
    protected $config;
    protected $is_on            = TRUE;
    protected $is_live          = FALSE;

    public function __construct()
    {
        $this->all_config       = App()->config->payment;
        $this->payment_config   = Variable_helper::Has_value( $this->all_config["list"], $this->type );

        $payment_mode           = Variable_helper::Has_value( $this->all_config, "payment_mode" );
        $payment_mode           = strtolower( $payment_mode );
        $this->payment_mode     = $payment_mode;

        if( $payment_mode == "live" )
        {
            $this->is_live = TRUE;
        }

        if( empty( $this->payment_config ) )
        {
            $this->is_on    = FALSE;
            Logger_helper::Error( ucfirst( $this->type )." payment - config missing, turn off" );
            return;
        }

        $this->config = $this->payment_config[$payment_mode];
    }

    /**
     * @return  string
     */
    public function Get_error_message()
    {
        return $this->error_message;
    }

    /**
     * @return  bool
     */
    public function Get_status()
    {
        return $this->status;
    }

    /**
     * @return  string
     */
    public function Get_token()
    {
        return $this->token;
    }

    /**
     * @return  string
     */
    public function Get_transaction_id()
    {
        return $this->transaction_id;
    }

    /**
     * @return  string
     */
    public function Get_correlation_id()
    {
        return $this->correlation_id;
    }

    /**
     * @return  mixed
     */
    public function Get_result()
    {
        return $this->result;
    }

    public abstract function Set_express_checkout_details( & $order );
    public abstract function Get_express_checkout_details( & $order );
    public abstract function Do_express_checkout( & $order );
    public abstract function Do_direct_payment( & $order, $card_details );
}

/* End of file BPayment_type.php */
/* Location: ./core/Base/BClasses */