<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BModule
 *
 * @version     1.0.0
 */
abstract class BModule extends Base implements IModule
{
    public function __construct()
    {
    }
}

/* End of file BModule.php */
/* Location: ./core/Base/BClasses */