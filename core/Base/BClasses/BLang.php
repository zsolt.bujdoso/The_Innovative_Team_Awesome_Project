<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BLang
 *
 * @version     1.0.0
 */
abstract class BLang extends Base implements ILang
{
    protected $language;
    protected $language_code;
    protected $languages;

    protected $language_details;

    protected $variables = array();
  
    public function __construct()
    {
        $this->languages      = App()->config->language["languages"];
        $this->language       = App()->config->language["default"];

        $this->Set_language();
        $this->Set_into_session();
        $this->Load_language_variables();
        $this->Load_language_variables_from_module();
    }

    public function Set_language()
    {
        // If language was set into session
        $this->language_code = ( App()->session->Has( "lang" ) ?
            App()->session->Get( "lang" )
            :
            App()->config->language["code"]
        );

        $this->Set_default_language();
        $this->Try_to_get_lang_from_url();
        $this->Try_to_get_lang_from_GET();
        $this->Verify_language_code();

        $this->language         = $this->languages[$this->language_code]["name"];
        $this->language_details = $this->languages[$this->language_code];
    }

    private function Try_to_get_lang_from_url()
    {
        $url = $this->Calculate_url();

        if( preg_match( "/^([a-zA-Z]{2})\/(.*)/", $url ) || preg_match( "/^([a-zA-Z]{2}\/?)$/", $url ) )
        {
            $this->language_code = preg_replace( "/^([a-zA-Z]{2})(.*)/", "$1", $url );
        }
    }

    private function Calculate_url()
    {
        $url                = $_SERVER["REQUEST_URI"];

        $script_file_path   = @dirname( $_SERVER["SCRIPT_FILENAME"] )."/";
        $script_path        = @str_replace( $_SERVER["DOCUMENT_ROOT"], "", $script_file_path );
        $url                = str_replace( $script_path, "", $url );

        return ltrim( $url, "/" );
    }

    private function Try_to_get_lang_from_GET()
    {
        if( isset( $_GET["lang"] ) )
        {
            // If language is set in link
            $this->language_code = $_GET["lang"];
        }
    }

    private function Set_default_language()
    {
        if( empty( $this->language_code ) )
        {
            $this->language_code  = App()->config->language["code"];
        }
    }

    private function Verify_language_code()
    {
        // Check if default language not exists
        if( ! array_key_exists( $this->language_code, $this->languages ) )
        {
            throw new MLang_exception( "Language with code '".$this->language_code."' is not enabled" );
        }
    }

    protected function Set_into_session()
    {
        App()->session->Set( "lang", $this->language_code );
    }

    protected function Load_language_variables()
    {
        if( empty( $this->language_code ) )
        {
            return;
        }

        $folder = COREPATH."Languages/".$this->language_code."/";
        $this->Load_language_files( $folder );

        $folder = BASEPATH."Core/Languages/".$this->language_code."/";
        $this->Load_language_files( $folder );
    }

    protected function Load_language_files( $path )
    {
        $language_files = General_functions::Get_file_list( $path );

        // If there is no language file just return
        if( empty( $language_files ) )
        {
            return;
        }

        foreach( $language_files as $language_file_name )
        {
            $this->Load_language_file( $path, $language_file_name );
        }
    }

    protected function Load_language_file( $path, $file )
    {
        if( ! file_exists( $path.$file ) || $file == "." || $file == ".." )
        {
            return;
        }

        $new_language = include $path.$file;

        if( ! is_array( $new_language ) )
        {
            throw new MLang_exception( "The languge file '".$file."' does not contains a valid language array" );
        }

        $this->variables = array_merge( $this->variables, $new_language );
    }

    protected function Load_language_variables_from_module()
    {

    }

    public function Get_language_details()
    {
        return $this->language_details;
    }

    public function Get_language_details_by_code( $language_code )
    {
        if( ! empty( $this->languages[$language_code] ) )
        {
            return $this->languages[$language_code];
        }

        return FALSE;
    }

    public function Get_language_code()
    {
        return $this->language_code;
    }

    public function Get_language()
    {
        return $this->language;
    }
}

/* End of file BLang.php */
/* Location: ./core/Base/BClasses */