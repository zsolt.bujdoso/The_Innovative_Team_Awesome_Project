<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BRequest
 *
 * @version     1.0.0
 */
abstract class BRedirect extends Base implements IRedirect
{
    var $base_url = "";

    public function __construct()
    {
        parent::__construct();

        $this->base_url = Url_helper::Base_url();
    }

    /**
     * @method  To_url
     * @access  public
     * @desc    This method redirect the page to a specified URL
     * @author  Zoltan Jozsa
     *
     * @param   string                      $url                        - the url to redirect to
     *
     * @version 1.0.0
     */
    public function To_url( $url )
    {
        $url = ltrim( $url, "/" );

        if( ! String_helper::Is_link( $url ) )
        {
            $url = Url_helper::Base_url().$url;
        }

        $this->Redirect_to_url( $url );
    }

    /**
     * @method  To_site_url
     * @access  public
     * @desc    This method redirect the page to a specified URL on this site
     * @author  Zoltan Jozsa
     *
     * @param   string                      $url                        - the url to redirect to
     *
     * @version 1.0.0
     */
    public function To_site_url( $url )
    {
        $url = ltrim( $url, "/" );

        if( ! String_helper::Is_link( $url ) )
        {
            $url = Url_helper::Site_url().$url;
        }

        $this->Redirect_to_url( $url );
    }

    protected function Redirect_to_url( $url )
    {
        header( "Location: ".$url );

        if( function_exists( "App" ) )
        {
            $APP = App();

            if( is_object( $APP ) )
            {
                App()->Finish();
            }
        }

        die();
    }

    /**
     * @method  To_last_url_or_home
     * @access  public
     * @desc    This method redirect the page to a specified URL on this site
     * @author  Zoltan Jozsa
     *
     * @version 1.0.0
     */
    public function To_last_url_or_home()
    {
        $url = Server_helper::Get_last_visited_url();

        if( ! String_helper::Is_link( $url ) )
        {
            $url = Url_helper::Site_url();
        }

        $this->Redirect_to_url( $url );
    }
}

/* End of file BRedirect.php */
/* Location: ./core/Base/BClasses */