<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BDB_Cache
 *
 * @version     1.0.0
 */
abstract class BDB_Cache extends Base implements ICache
{
    protected $cache_path   = "";
    protected $is_on        = FALSE;
    protected $cache_time   = 600;

    public function __construct()
    {
        if( ! empty( App()->config->database["is_cache_on"] ) )
        {
            $this->Turn_on();
        }

        if( ! empty( App()->config->database["cache_path"] ) )
        {
            $this->cache_path   = BASEPATH."Runtime".DIRECTORY_SEPARATOR
                .rtrim( App()->config->database["cache_path"], "/\\" ).DIRECTORY_SEPARATOR;
        }

        if( empty( $this->cache_path ) )
        {
            $this->cache_path   = BASEPATH."Runtime".DIRECTORY_SEPARATOR
                ."cache".DIRECTORY_SEPARATOR."database".DIRECTORY_SEPARATOR;
        }

        if( ! file_exists( $this->cache_path ) || ! is_dir( $this->cache_path ) )
        {
            mkdir( $this->cache_path, 0755, TRUE );
        }
    }

    public function Set_path( $path )
    {
        $this->cache_path = trim( $path, "/" ).DIRECTORY_SEPARATOR;
    }

    public function Set_cache_time( $time )
    {
        $this->cache_time = $time;
    }

    public function Turn_on()
    {
        $this->is_on = TRUE;
    }

    public function Turn_off()
    {
        $this->is_on = FALSE;
    }

    public function Is_turned_on()
    {
        return $this->is_on;
    }

    public function Get_path()
    {
        return $this->cache_path;
    }
}

/* End of file BDB_Cache.php */
/* Location: ./core/Base/BClasses */