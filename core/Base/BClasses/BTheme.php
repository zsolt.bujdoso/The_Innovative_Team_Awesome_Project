<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BTheme
 *
 * @version     1.0.0
 */
abstract class BTheme extends Base implements ITheme
{
    protected $name;
    protected $path;
    protected $default_path;
    protected $theme_id;

    public function __construct()
    {
        parent::__construct();

        $this->name         = "";
        $this->default_path = BASEPATH."Themes".DIRECTORY_SEPARATOR;
        $this->path         = $this->default_path;
    }
}

/* End of file BTheme.php */
/* Location: ./core/Base/BClasses */