<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BRenderer
 *
 * @property    MRouter     App()->router
 *
 * @version     1.0.0
 */
abstract class BRenderer extends Base implements IRenderer
{
    public $layout;
    protected $template;
    protected $layout_content;
    protected $content;
    protected $params;
    protected $template_path;
    
    public function __construct()
    {
        $this->layout        = "main";
        $this->template_path = "";
        $this->template      = App()->router->Get_function();
        $this->params        = array();
    }

    protected  function Calculate_template_path( $view_name = "" )
    {
        $this->template_path = "";

        $this->Calculate_actual_path( $view_name );

        // If view not found, try in module
        if( empty( $this->template_path ) )
        {
            // if we want to start from main directory in theme,
            // example: we are in a module controller ( path is: views/controller_name/ )
            // and we want to start from 'views/'
            $this->Calculate_template_path_to_module( $view_name );
        }

        // If view not found, try in start of theme
        if( empty( $this->template_path ) )
        {
            // if we want to start from project theme directory
            $this->Calculate_template_path_to_main( $view_name );
        }
    }

    private function Calculate_actual_path( $view_name )
    {
        if( preg_match( "/^\/{2,}(.*)/", $view_name ) )
        {
            return;
        }

        $this->template_path = App()->theme->Get_path();

        if( App()->router->Get_module() )
        {
            $this->template_path = MODULEPATH.ucfirst( App()->router->Get_module() ).DIRECTORY_SEPARATOR;
        }

        // Add the views directory
        $view_location = "Views".DIRECTORY_SEPARATOR;

        // Add the controller directory if not start with /
        if( ! preg_match( "/^\/+(.*)/", $view_name ) )
        {
            $view_location .= App()->router->Get_controller().DIRECTORY_SEPARATOR;
        }

        // replace the first slashes with nothing
        $view_name = preg_replace( "/^\/{1,}(.*)/", "$1", $view_name );

        if( file_exists( $this->template_path.$view_location.$view_name.Settings::EXT ) )
        {
            $this->template_path .= $view_location;
            return;
        }

        $this->template_path = "";
    }

    private function Calculate_template_path_to_module( $view_name )
    {
        if( preg_match( "/^\/{2,}(.*)/", $view_name ) )
        {
            return;
        }

        if( App()->router->Get_module() )
        {
            $this->template_path .= MODULEPATH.App()->router->Get_module().DIRECTORY_SEPARATOR;
            $view_location       = "Views".DIRECTORY_SEPARATOR;

            // Add the controller directory if not start with /
            if( ! preg_match( "/^\/+(.*)/", $view_name ) )
            {
                $view_location .= App()->router->Get_controller().DIRECTORY_SEPARATOR;
            }

            // replace the first slashes with nothing
            $view_name = preg_replace( "/^\/{1,}(.*)/", "$1", $view_name );

            if( file_exists( $this->template_path.$view_location.$view_name ) )
            {
                $this->template_path .= $view_location;
                return;
            }

            $this->template_path = "";
        }
    }

    private function Calculate_template_path_to_main( $view_name )
    {
        $this->template_path = App()->theme->Get_path()."Views".DIRECTORY_SEPARATOR;

        // Add the controller directory if not start with /
        if( ! preg_match( "/^\/+(.*)/", $view_name ) )
        {
            $this->template_path .= App()->router->Get_controller().DIRECTORY_SEPARATOR;
        }
    }
    
    public function Set_layout( $layout )
    {
        $this->layout = $layout;
    }
    
    public function Get_layout()
    {
        return $this->layout;
    }

    public function & __get( $name )
    {
        //throw new MException( "Variable with name '' not found in" );
    }
}

/* End of file BRenderer.php */
/* Location: ./core/Base/BClasses */