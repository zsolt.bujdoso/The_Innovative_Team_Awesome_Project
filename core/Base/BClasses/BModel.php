<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BModel
 *
 * @property        DB_Mysql       $db
 *
 * @version     1.0.0
 */
class BModel implements IModel
{
    protected $db;

    public function __construct()
    {
        $this->Set_database( App()->db );
    }

    public function Set_database( & $database )
    {
        $this->db = $database;
    }

    /******************************************
     * INFORMATIONS
     ******************************************/
    //<editor-fold desc="Information getters">
    /**
     * @method  Get_table_names
     * @desc    This method return all table name from selected database
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  array|bool
     */
    public function Get_table_names()
    {
        return $this->db->Get_table_names();
    }

    /**
     * @method  Get_process_list
     * @desc    Returns the list of processes from selected database
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  array|bool
     */
    public function Get_process_list()
    {
        return $this->db->Get_process_list();
    }

    /**
     * @method  Get_executed_queries
     * @desc    This method returns all executed queries as array
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  array
     */
    public function Get_executed_queries()
    {
        return $this->db->Get_executed_queries();
    }

    /**
     * @method  Get_last_query
     * @desc    This method returns the last executed query as array
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  array
     */
    public function Get_last_query()
    {
        return $this->db->Get_last_query();
    }

    /**
     * @method  Get_last_insert_id
     * @desc    This method returns the last inserted id or FALSE
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  int|bool
     */
    public function Get_last_insert_id()
    {
        return $this->db->Get_last_insert_id();
    }
    //</editor-fold>

    /******************************************
     * STATUS AND ERROR MESSAGE
     ******************************************/
    //<editor-fold desc="Status and Error message">
    /**
     * @method  Get_status
     * @desc    Return the status of last query
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return bool
     */
    public function Get_status()
    {
        return $this->db->Get_status();
    }

    /**
     * @method  Get_error_message
     * @desc    Return the error message of last query
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return bool
     */
    public function Get_error_message()
    {
        return $this->db->Get_error_message();
    }
    //</editor-fold>

    /**
     * @method  Escape
     * @desc    Escape the string parameter and returns it
     * @access  public
     * @author  Cousin Bela
     *
     * @param   string                  $string                     - string to escape
     *
     * @version 1.0
     * @return bool
     */
    public function Escape( $string )
    {
        return $this->db->Escape( $string );
    }

    /**
     * @method  Reset
     * @desc    Reset the filters and arrays for next query
     * @access  private
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  bool
     */
    public function Reset()
    {
        $this->db->Reset();
    }

    /**
     * @method  Get_model
     * @desc    If instance is empty then instantiate a new class and return it, otherwise return it
     * @access  public
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  object
     */
    public static function Get_model()
    {
        $classname  = strtolower( get_called_class() );
        $model      = App()->Get_instantiated_object( $classname );

        if( ! $model )
        {
            General_functions::Instantiate_class( $classname );
            return App()->Get_instantiated_object( $classname );
        }

        return $model;
    }
}