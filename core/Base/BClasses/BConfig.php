<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BConfig
 *
 * @version     1.0.0
 */
abstract class BConfig extends Base implements IConfig
{
    public $configurations;
    private $default_config_files = array(
        "autoimport.php",
        "classmaps.php",
        "language.php",
        "config.php",
        "constant.php",
        "module.php",
        "cache.php",
        "error.php",
        "email.php",
        "database.php",
        "image.php",
        "session.php",
        "theme.php",
        "url.php",
        "payment.php",
        "profile.php",
    );
    
    public function __construct()
    {
        $this->configurations = Settings::$DEFAULT_CONFIGRATIONS;

        $path = BASEPATH.Settings::PROJECT_CORE_DIR.$this->configurations["config_path"];

        $this->Load_config_files_from_path( $path );
    }
    
    /**
     * @method	Load_project_config
     * @access	public
     * @desc    This method loads the configuration files from selected project
     * @author	Cousin Béla
     * 
     * @version	1.0.0
     */
    public function Load_config_files_from_path( $path, $is_subdirectory = FALSE )
    {
        // Try to load default config files
        foreach( $this->default_config_files as $config_file )
        {
            $this->Load_config_file( $path, $config_file, $is_subdirectory );
        }
        
        // Load other config files
        $config_files = General_functions::Get_file_list( $path, TRUE );

        // If there is no config file just return
        if( empty( $config_files ) )
        {
            return;
        }

        foreach( $config_files as $config_file_name )
        {
            if( is_dir( $path.$config_file_name ) )
            {
                $this->Load_config_files_from_path( $path.$config_file_name.DIRECTORY_SEPARATOR, TRUE );
            }

            if( in_array( $config_file_name, $this->default_config_files ) )
            {
                continue;
            }
            
            $this->Load_config_file( $path, $config_file_name, $is_subdirectory );
        }
    }
    
    /**
     * @method	Load_config_file
     * @access	public
     * @desc    This method imports configuration from selected config file
     * @author	Cousin Béla
     * 
     * @param 	string              $path                           - the path where the file is
     * @param 	string              $config_file_name               - name of the config file, ex: database, cache
     * @param 	boolean             $is_subdirectory                - is a subdirectory in Config dir or not
     *
     * @version	1.0.0
     * @throws  Exception
     */
    private function Load_config_file( $path, $config_file_name, $is_subdirectory )
    {
        if( ! file_exists( $path.$config_file_name ) || is_dir( $path.$config_file_name ) )
        {
            return;
        }
        
        $config = include_once $path.$config_file_name;
        
        if( ! is_array( $config ) && in_array( $config_file_name, $this->default_config_files ) 
            && $config_file_name != "constant.php" )
        {
            // throw an error because this config file has errors
            throw new Exception( 
                "Config file named '".$config_file_name."' has invalid configuration, maybe it's not contains an array" 
            );
        }

        // Import settings from this configuration file
        $this->Import_config(
            $config,
            $config_file_name,
            ( $is_subdirectory ? basename( $path ) : null )
        );
    }
    
    /**
     * @method	Import_config
     * @access	public
     * @desc    This method imports configuration from selected config file if needed
     * @author	Cousin Béla
     * 
     * @param 	string              $config                     - an array with configuration
     * @param 	string              $file_name                  - name of the config file, ex: database, cache
     * @param 	string              $config_name                - name in the config file, ex: payment, database
     *
     * @version	1.0.0
     */
    private function Import_config( & $config, $file_name, $config_name = "" )
    {
        if( empty( $config ) )
        {
            return;
        }
        
        $type_to_check = $type = str_replace( Settings::EXT, "", $file_name );
        if( ! empty( $config_name ) )
        {
            $type_to_check = $config_name;
        }

        switch( $type_to_check )
        {
            case "config":
                //$this->Import_main_config( $config );
                $this->Import_config_default( $config );
                break;

            /*
            case "cache":
                //$this->Import_cache_config( $config );
                $this->Import_config_default( $config, "cache" );
                break;
            */

            case "classmaps":

                $this->Import_classmaps( $config );
                break;
              
            case "database":
                $this->Import_database( $config, $type, ! empty( $config_name ) );
                break;
              
            case "autoimport":
                $this->Import_autoimport( $config );
                break;
              
            case "constant":
                break;

            /*
            case "email":
                //$this->Import_email( $config );
                $this->Import_config_default( $config, "email" );
                break;
            */

            case "module":
                $this->Import_modules( $config );
                break;

            /*
            case "language":
                //$this->Import_language( $config );
                $this->Import_config_default( $config, "language" );
                break;
            */

            case "theme":
                $this->Import_theme( $config );
                break;

            case "session":
                $this->Import_session( $config );
                break;

            case "url":
                $this->Import_url( $config );
                break;
              
            default:
                $this->Import_config_default( $config, $type, $config_name );
                break;
        }
    }
    
    /**
     * @method	Import_main_config
     * @access	public
     * @desc    This method imports modules configuration from selected project
     * @author	Cousin Béla
     * 
     * @param 	array               $config             - an array with configuration
     * 
     * @version	1.0.0
     *//*
    private function Import_main_config( $config )
    {
        if( isset( $config["name"] ) )
        {
            $this->configurations["name"]           = $config["name"];
        }

        if( isset( $config["log_types"] ) )
        {
            $this->configurations["log_types"]      = $config["log_types"];
        }

        if( isset( $config["log_add_date"] ) )
        {
            $this->configurations["log_add_date"]   = $config["log_add_date"];
        }

        if( isset( $config["log_date_format"] ) )
        {
            $this->configurations["log_date_format"]= $config["log_date_format"];
        }

        if( isset( $config["debug_mode"] ) )
        {
            $this->configurations["debug_mode"]     = $config["debug_mode"];
        }

        if( isset( $config["theme"] ) )
        {
            $this->configurations["theme"] = $config["theme"];
        }
    }*/

    /**
     * @method	Import_cache_config
     * @access	public
     * @desc    This method imports cache configuration from selected project
     * @author	Cousin Béla
     *
     * @param 	array               $config             - an array with configuration
     *
     * @version	1.0.0
     *//*
    private function Import_cache_config( $config )
    {
        if( isset( $config["path"] ) )
        {
            $this->configurations["cache"]["path"] = $config["path"];
        }
        if( isset( $config["is_on"] ) )
        {
            $this->configurations["cache"]["is_on"] = $config["is_on"];
        }
        if( isset( $config["is_on"] ) )
        {
            $this->configurations["cache"]["class"] = $config["class"];
        }
    }*/
    
    /**
     * @method	Import_classmaps
     * @access	public
     * @desc    This method imports classmaps for replacing default classes
     * @author	Cousin Béla
     * 
     * @param 	array               $config             - an array with configuration
     * 
     * @version	1.0.0
     */
    private function Import_classmaps( & $config )
    {
        foreach( $config as $type => $class_name )
        {
            // If classname isn't set, go to the next
            if( ! isset( App()->classnames_map[$type] ) )
            {
                continue;
            }

            // If classname is not reqritebla, go to the next
            if( in_array( $type, Settings::$NOT_REWRITABLE_CLASSES ) )
            {
                continue;
            }

            // Rewrite existing class for the selected class file
            App()->classnames_map[$type] = $class_name;
        }
    }
    
    /**
     * @method	Import_database
     * @access	public
     * @desc    This method imports database configuration from selected project
     * @author	Cousin Béla
     * 
     * @param 	array               $config                     - an array with configuration
     * @param 	string              $config_name                - name in the config file, ex: payment, database
     * @param 	boolean             $is_in_subdirectory         - is file from a subdirectory
     *
     * @version	1.0.0
     */
    private function Import_database( & $config, $config_name = "", $is_in_subdirectory = FALSE )
    {
        if( empty( $is_in_subdirectory ) )
        {
            $config_name = "database";

            if( isset( $config["default"] ) )
            {
                $this->configurations[$config_name]["default"] = $config["default"];
            }

            if( isset( $config["is_cache_on"] ) )
            {
                $this->configurations[$config_name]["is_cache_on"] = $config["is_cache_on"];
            }

            if( isset( $config["cache_path"] ) )
            {
                $this->configurations[$config_name]["cache_path"] = $config["cache_path"];
            }

            if( isset( $config["cache_time"] ) )
            {
                $this->configurations[$config_name]["cache_time"] = $config["cache_time"];
            }

            if( isset( $config["cache_class"] ) )
            {
                $this->configurations[$config_name]["cache_class"] = $config["cache_class"];
            }

            return;
        }

        // save config from database file ex: Config/database/live
        $this->configurations["database"]["list"][$config_name] = array(
              "hostname"  => ( isset( $config["hostname"] ) ? $config["hostname"] : "" ),
              "port"      => ( isset( $config["port"] ) ? $config["port"] : 3306 ),
              "username"  => ( isset( $config["username"] ) ? $config["username"] : "" ),
              "password"  => ( isset( $config["password"] ) ? $config["password"] : "" ),
              "database"  => ( isset( $config["database"] ) ? $config["database"] : "" ),
              "driver"    => ( isset( $config["driver"] ) ? $config["driver"] : "" ),
              "prefix"    => ( isset( $config["prefix"] ) ? $config["prefix"] : "" ),
              "charset"   => ( isset( $config["charset"] ) ? $config["charset"] : "" ),
              "collate"   => ( isset( $config["collate"] ) ? $config["collate"] : "" ),
        );
    }
    
    /**
     * @method	Import_autoimport
     * @access	public
     * @desc    This method imports autoload class configuration from selected project
     * @author	Cousin Béla
     * 
     * @param 	array               $config             - an array with configuration
     * 
     * @version	1.0.0
     */
    private function Import_autoimport( & $config )
    {
        $import_paths =& App()->import_paths;
      
        foreach( $config as $path )
        {
            $import_paths []= $path;
            
            General_functions::Load_class_files_from_folder_and_subfolders( $path );
        }
    }
   
    /**
     * @method	Import_email
     * @access	public
     * @desc    This method imports email configuration from selected project
     * @author	Cousin Béla
     * 
     * @param 	array               $config             - an array with configuration
     * 
     * @version	1.0.0
     *//*
    private function Import_email( $config )
    {
        $this->configurations["email"]["mailer"]    = ( isset( $config["mailer"] ) ? $config["mailer"] : "" );
        $this->configurations["email"]["smtp_host"] = ( isset( $config["smtp_host"] ) ? $config["smtp_host"] : "" );
        $this->configurations["email"]["smtp_port"] = ( isset( $config["smtp_port"] ) ? $config["smtp_port"] : "" );
        $this->configurations["email"]["smtp_user"] = ( isset( $config["smtp_user"] ) ? $config["smtp_user"] : "" );
        $this->configurations["email"]["smtp_pass"] = ( isset( $config["smtp_pass"] ) ? $config["smtp_pass"] : "" );
        $this->configurations["email"]["sendmail"]  = ( isset( $config["sendmail"] ) ? $config["sendmail"] : "" );
    }*/
    
    /**
     * @method	Import_modules
     * @access	public
     * @desc    This method imports modules configuration from selected project
     * @author	Cousin Béla
     * 
     * @param 	array               $modules            - an array with configuration
     * 
     * @version	1.0.0
     */
    private function Import_modules( & $modules )
    {
        foreach( $modules as $module )
        {
            General_functions::Include_module( $module );
            
            $this->configurations["module"][] = $module;
        }
    }
    
    /**
     * @method	Import_image
     * @access	public
     * @desc    This method imports image configuration from selected project
     * @author	Cousin Béla
     * 
     * @param 	array               $config             - an array with configuration
     * 
     * @version	1.0.0
     *//*
    private function Import_image( $config )
    {
        
    }*/
    
    /**
     * @method	Import_language
     * @access	public
     * @desc    This method imports language configuration from selected project
     * @author	Cousin Béla
     * 
     * @param 	array               $config             - an array with configuration
     * 
     * @version	1.0.0
     *//*
    private function Import_language( $config )
    {
        $this->configurations["language"]["default"]  = ( isset( $config["default"] ) ? $config["default"] : "" );
        $this->configurations["language"]["code"]     = ( isset( $config["code"] ) ? $config["code"] : "" );
        
        if( isset( $config["languages"] ) && is_array( $config["languages"] ) )
        {
            foreach( $config["languages"] as $key => $language )
            {
                $this->configurations["language"]["languages"][$key] = array(
                    "name"    => ( isset( $language["name"] ) ? $language["name"] : "" ),
                    "folder"  => ( isset( $language["folder"] ) ? $language["folder"] : "" ),
                );
            }
        }
    }*/
    
    /**
     * @method	Import_url
     * @access	public
     * @desc    This method imports path configuration from selected project
     * @author	Cousin Béla
     * 
     * @param 	array               $config             - an array with configuration
     * 
     * @version	1.0.0
     */
    private function Import_url( $config )
    {
        if( isset( $config["type"] ) )
        {
            $this->configurations["url"]["type"]      =  $config["type"];
        }
        
        if( isset( $config["default_module"] ) )
        {
            $this->configurations["url"]["default_module"] = $config["default_module"];
        }

        if( isset( $config["default_controller"] ) )
        {
            $this->configurations["url"]["default_controller"] = $config["default_controller"];
        }

        if( isset( $config["default_function"] ) )
        {
            $this->configurations["url"]["default_function"] = $config["default_function"];
        }

        if( isset( $config["default_routes"] ) )
        {
            $this->configurations["url"]["default_routes"] = $config["default_routes"];
        }
        
        if( isset( $config["routes"] ) && is_array( $config["routes"] ) )
        {
            if( empty( $this->configurations["url"]["routes"] ) )
            {
                $this->configurations["url"]["routes"] = $config["routes"];
            }
            else
            {
                $routes = array_merge( $this->configurations["url"]["routes"], $config["routes"] );
                $this->configurations["url"]["routes"] = $routes;
            }
        }
    }
    
    /**
     * @method	Import_session
     * @access	public
     * @desc    This method session path configuration from selected project
     * @author	Cousin Béla
     * 
     * @param 	array               $config             - an array with configuration
     * 
     * @version	1.0.0
     */
    private function Import_session( & $config )
    {
        if( isset( $config["type"] ) )
        {
            $this->configurations["session"]["type"]        = $config["type"];
            if( ! empty( $config["type"] ) )
            {
                App()->Set_classname_for( "session", $this->configurations["session"]["type"] );
            }
            unset( $config["type"] );
        }

        $this->Import_config_default( $config, "session" );

        /*
        if( isset( $config["table_name"] ) )
        {
            $this->configurations["session"]["table_name"]  = $config["table_name"];
        }
        
        if( isset( $config["encryption_key"] ) )
        {
            $this->configurations["session"]["encryption_key"]  = $config["encryption_key"];
        }
        
        if( isset( $config["match_ip"] ) )
        {
            $this->configurations["session"]["match_ip"]    = $config["match_ip"];
        }
        
        if( isset( $config["match_agent"] ) )
        {
            $this->configurations["session"]["match_agent"] = $config["match_agent"];
        }
        
        if( isset( $config["encrypt_cookie"] ) )
        {
            $this->configurations["session"]["encrypt_cookie"]  = $config["encrypt_cookie"];
        }
        
        if( isset( $config["expiration"] ) )
        {
            $this->configurations["session"]["expiration"]  = $config["expiration"];
        }*/
    }
    
    /**
     * @method	Import_theme
     * @access	public
     * @desc    This method session configuration for themes
     * @author	Cousin Béla
     * 
     * @param 	array               $config                     - an array with configuration
     *
     * @version	1.0.0
     */
    private function Import_theme( & $config )
    {
        if( isset( $config["theme"] ) )
        {
            $this->configurations["theme"]       = $config["theme"];
        }

        if( isset( $config["admin_theme"] ) )
        {
            $this->configurations["admin_theme"] = $config["admin_theme"];
        }
    }

    /**
     * @method	Import_config_default
     * @access	public
     * @desc    This method session configuration as name that the file name is from selected project
     * @author	Cousin Béla
     *
     * @param 	array               $config                     - an array with configuration
     * @param 	string              $type                       - type of configuration
     * @param 	string              $config_name                - name in the config file, ex: payment, database
     *
     * @version	1.0.0
     */
    private function Import_config_default( & $config, $type = null, $config_name = "" )
    {
        foreach( $config as $key => $value )
        {
            if( ! empty( $config_name ) )
            {
                $this->configurations[$config_name]["list"][$type][$key] = $value;

                continue;
            }

            if( ! empty( $type ) )
            {
                $this->configurations[$type][$key] = $value;

                continue;
            }

            $this->configurations[$key] = $value;
        }
    }

    /**
     * @method	Has
     * @access	public
     * @desc    This method checks if a configuration exists by name or not
     * @author	Cousin Béla
     *
     * @param 	string              $name               - name of configuration
     *
     * @version	1.0.0
     * @return  bool
     */
    public function Has( $name )
    {
        if( empty( $name ) )
        {
            return FALSE;
        }
        
        if( array_key_exists( $name, $this->configurations ) )
        {
            return TRUE;
        }

        if( property_exists( $this, $name ) )
        {
            return TRUE;
        }

        return FALSE;
    }
    
    public function & __get( $name )
    {
        // If exists in the configuration array
        if( array_key_exists( $name, $this->configurations ) )
        {
            return $this->configurations[$name];
        }
        
        // if this class has this property
        if( property_exists( $this, $name ) )
        {
            return $this->$name;
        }
        
        // throw an error, the property does not exist
        throw new MProperty_not_exists_exception( "Config with name '".$name."' does not exists" );
    }
    
    public function __set( $name, $value )
    {
        if( $name == "configurations" )
        {
            throw new MConfig_exception( "You cant overwrite the full configurations array with an other" );
        }

        // if this class has this property
        if( property_exists( $this, $name ) )
        {
            $this->$name = $value;
            return;
        }
        
        // TODO throw an error, the property does not exist
        // throw new MException( "Config with name '".$name."' does not exists" );

        // If exists in the configuration array
        /*if( array_key_exists( $name, $this->configurations ) )
        {*/
            $this->configurations[$name] = $value;
        //}
    }
}

/* End of file BConfig.php */
/* Location: ./core/Base/BClasses */