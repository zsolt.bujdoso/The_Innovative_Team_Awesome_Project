<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *
 * class BProfiler
 *
 * This class is needed for catching execution time for several process
 * 
 * @version     1.0.0
 */
abstract class BProfiler extends Base implements IProfiler
{
    protected $profilers = array();
    
    public function __construct()
    {
        // Start a System profiler
        $this->Start( "System" );
    }

    /**
     * @method  Turn_off
     * @access  Public
     * @desc    This method turns off all profilers
     * @author  Cousin Bela
     *
     * @version 1.0.0
     */
    public function Turn_off()
    {
        App()->config->profile["show_time"]     = FALSE;
        App()->config->profile["show_queries"]  = FALSE;
        App()->config->profile["show_memory"]   = FALSE;
    }
}

/* End of file BProfiler.php */
/* Location: ./core/Base/BClasses */