<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class BRequest
 *
 * @version     1.0.0
 */
abstract class BRequest extends Base implements IRequest
{
    protected $ip_address;
    protected $url;
    protected $host;
    protected $protocol;
    protected $method;
    protected $port;
    protected $parameters;
    protected $query_parameters;

    public function __construct()
    {
        parent::__construct();
    }
    
    public function Get_url()
    {
        return $this->url;
    }
    
    public function Get_url_parameters()
    {
        return $this->parameters;
    }

    public function Get_query_parameters()
    {
        return $this->query_parameters;
    }
    
    public function Get_host()
    {
        return $this->host;
    }
    
    public function Get_protocol()
    {
        return $this->protocol;
    }
    
    public function Get_port()
    {
        return $this->port;
    }
    
    public function Get_method()
    {
        return $this->method;
    }

    public function Get_ip_address()
    {
        return $this->ip_address;
    }
}

/* End of file BRequest.php */
/* Location: ./core/Base/BClasses */