<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * class General_functions
 *
 * @version     1.0.0
 */
class General_functions
{
    private static $module_path = "Core/Modules/";

    /**
     * @method	Is_php
     * @access	public
     * @desc    This method checks if a php version is greather than the specified version
     * @author	Cousin Béla
     * 
     * @param 	string              $version            - the version for compare
     * 
     * @version	1.0.0
     * @return 	boolean
     */
    public static function Is_php( $version = "5.0.0" )
    {
        // creating a variable for returning
        $is_php           = FALSE;

        $php_version      = phpversion();
        // explode the versions for comparing
        $arr_version      = explode( ".", $version );
        $arr_php_version  = explode( ".", $php_version );

        $i = 0;
        // check if the php version is greather than the version
        foreach( $arr_php_version as $version )
        {
            if( array_key_exists( $i, $arr_version ) )
            {
                if( $arr_version[$i] < $version )
                {
                    $is_php = TRUE;
                    break;
                }

                if( $arr_version[$i] > $version )
                    break;
            }
            else
                break;

            ++$i;
        }

        // return the result
        return $is_php;
    }
    
    /**
     * @method	Load_class_files_from_folder_and_subfolders
     * @access	public
     * @desc    this method is for Autoload register, this will check if a class exists or not
     * @author	Cousin Béla
     * 
     * @param 	string              $path               - the name of the class to check if exists
     * @param 	bool                $has_prefix         - classes has Prefixes
     * @param 	string              $prefix             - prefix name for classes
     *
     * @version	1.0.0
     */
    public static function Load_class_files_from_folder_and_subfolders( $path, $has_prefix = FALSE, $prefix = "" )
    {
        $classmap   = & App()->classmap;
        $classes    = self::Get_file_list( $path, TRUE );

        foreach( $classes as $class_file )
        {
            $new_prefix         = "";
            $file_name_array    = explode( ".", $class_file );
            $extension          = end( $file_name_array );
            
            // IF this file has a good extensions and is a file, save into classmap
            if( is_file( $path.$class_file ) && ".".$extension == Settings::EXT )
            {
                $class_name     = str_replace( Settings::EXT, "", $class_file );

                if( $has_prefix )
                {
                    if( ! empty( $prefix ) )
                    {
                        $new_prefix = $prefix."_";
                    }

                    $new_prefix .= basename( $path )."_";
                    $class_name = $new_prefix.$class_name;
                }

                $classmap[$class_name]  = $path.$class_file;
                $type                   = strtolower( $class_name );

                // Set the new or rewrite existing class for the selected class file
                if( ! in_array( $type, Settings::$NOT_REWRITABLE_CLASSES ) )
                {
                    App()->Set_classname_for( $type, $class_name );
                }
            }
            // else if is a directory, load files from it
            elseif( is_dir( $path.$class_file ) )
            {
                $new_prefix = $prefix;

                if( ! empty( $prefix ) )
                {
                    $new_prefix .= "_";
                }

                $new_prefix .= basename( $path );

                self::Load_class_files_from_folder_and_subfolders( $path.$class_file."/", $has_prefix, $new_prefix );
            }
        }
    }
    
    /**
     * @method	Get_file_list
     * @access	public
     * @desc    this method returns an array with list of files
     * @author	Cousin Béla
     * 
     * @param 	string              $path                       - the path to directory for list of  files
     * @param   bool                $with_directories		    - if this is true, than returns a list with directories
     * 
     * @version	1.0.0
     * @return  array
     */
    public static function Get_file_list( $path = "", $with_directories = FALSE )
    {
        // create an array for files list
        $files_list = array();

        // check if the path exists and it is a directory
        if( file_exists( $path ) && is_dir( $path ) )
        {
            $dir_handle	= opendir( $path );
            while( $dir_handle && $file = readdir( $dir_handle ) )
            {
                // if the file is not in the exceptions array, and the name of it does not start with '.' (is not hidden)
                if( ! in_array( $file, Settings::$FILE_NAMES_EXCEPTION_FOR_GET_FILES_LIST ) && strpos( ".", $file ) == 0 )
                {
                    // if the file is a directory and we want a list with directories
                    // or if it is not a directory, add to the array
                    if( ! is_dir( $path.$file ) || $with_directories )
                    {
                        $files_list	[]= $file;
                    }
                }
            }
        }

        // else return an empty array
        return $files_list;
    }

    /**
     * @method	Include_files_from_folder_and_subfolders
     * @access	public
     * @desc    this method is for Autoload register, this will check if a class exists or not
     * @author	Cousin Béla
     *
     * @param 	string              $path                   - the name of the class to check if exists
     *
     * @version	1.0.0
     */
    public static function Include_files_from_folder_and_subfolders( $path )
    {
        $classes = self::Get_file_list( $path, TRUE );

        foreach( $classes as $class_file )
        {
            $file_name_array  = explode( ".", $class_file );
            $extension        = end( $file_name_array );

            // IF this file has a good extensions and is a file, save into classmap
            if( is_file( $path.$class_file ) && ".".$extension == Settings::EXT )
            {
                include $path.$class_file;
            }
            // else if is a directory, load files from it
            elseif( is_dir( $path.$class_file ) )
            {
                self::Include_files_from_folder_and_subfolders( $path.$class_file."/" );
            }
        }
    }
    
    /**
     * @method	Autoload_class
     * @access	public
     * @desc    This method is for Autoload register, this will check if a class exists or not
     * @author	Cousin Béla
     * 
     * @param 	string              $class_name         - the name of the class to check if exists
     * 
     * @version	1.0.0
     */
    public static function Autoload_class( $class_name = "" )
    {
        $class_name = basename( $class_name );

        // if class has already included
        if( class_exists( $class_name ) )
        {
            return;
        }
        
        $class_map = App()->classmap;
        
        // Try to get from classmap
        if( isset( $class_map[$class_name] ) )
        {
            include_once $class_map[$class_name];
            return;
        }

        if( self::Check_if_class_is_already_loaded( $class_name ) )
        {
            return;
        }
        
        // If class is missing and we have to throw an error
        self::Error_handler( E_ERROR, "Class {".$class_name."} not found" );
    }

    private static function Check_if_class_is_already_loaded( $class_name )
    {
        $loaded_classes = get_declared_classes();

        foreach( $loaded_classes as $loaded_class_name )
        {
            if( strpos( $loaded_class_name, "Facebook" ) !== FALSE )
            {
                var_dump( $loaded_class_name );
            }

            if( $loaded_class_name == $class_name )
            {
                return TRUE;
            }
        }

        return FALSE;
    }
    
    /**
     * @method	Include_module
     * @access	public
     * @desc    This method will include a module if not included and if exists, and will set to active
     *          if we have to do
     * @author	Cousin Béla
     * 
     * @param 	string              $module             - the name of the module
     * @param 	bool                $set_active         - set the module to activ
     * 
     * @version	1.0.0
     * @return  boolean
     * @throws  MPage_not_found_exception
     */
    public static function Include_module( $module = "", $set_active = FALSE )
    {
        $class_map  =& App()->classmap;
        
        // Check if exists in project modules
        $module_name  = strtolower( $module );
        $module_class = ucfirst( $module_name )."_module";

        if( ! isset( $class_map[$module_class] ) )
        {
            // Check and include module if found, else show an error
            if( ! self::Check_and_include_module( $module_name, $module_class ) )
            {
                throw new MPage_not_found_exception(
                    'Module "'.$module_name.'" or module class "'.$module_class.'" not found'
                );
            }

            // Instanciate the module to import paths
            self::Instantiate_class( $module_name."_module" );
            $module = App()->{$module_name."_module"};

            // Import module paths
            self::Import_module_paths( $module );

            if( function_exists( "App" ) && App()->Is_config_loaded() )
            {
                $module_config_path = BASEPATH.self::$module_path.ucfirst( $module_name )."/Config/";
                App()->config->Load_config_files_from_path( $module_config_path );
            }
        }

        // If we have to set this module to active
        if( $set_active )
        {
            App()->Set_classname_for( "module", $module_class );
        }
            
        return TRUE;
    }
    
    /**
     * @method	Check_and_include_module
     * @access	public
     * @desc    This method will include a module if not included and if exists
     * @author	Cousin Béla
     * 
     * @param 	string              $module             - the name of the module
     * @param 	string              $module_class       - the name of the class for module
     *
     * @version	1.0.0
     * @return  boolean
     */
    private static function Check_and_include_module( $module, $module_class )
    {
        $class_map          =& App()->classmap;

        $module             = ucfirst( $module );
        $path               = BASEPATH.self::$module_path.$module;
        $module_class_path  = $path."/".$module_class.Settings::EXT;

        if( file_exists( $module_class_path ) && is_file( $module_class_path ) )
        {
            $class_map[$module_class] = $module_class_path;
            App()->Set_classname_for( strtolower( $module_class ), $module_class );
            return TRUE;
        }
        else
        {
            // Check if exists in core modules
            $path               = COREPATH."Modules/".$module;
            $module_class_path  = $path."/".$module_class;

            if( file_exists( $module_class_path ) && is_file( $module_class_path ) )
            {
                $class_map[$module_class] = $module_class_path;
                App()->Set_classname_for( strtolower( $module_class ), $module_class );
                return TRUE;
            }
        }
        
        return FALSE;
    }

    /**
     * @method	Import_module_paths
     * @access	public
     * @desc    Add import paths to main import paths from module, and load them
     * @author	Cousin Béla
     *
     * @param   object                      $module                   - module object
     *
     * @version	1.0.0
     */
    private static function Import_module_paths( $module )
    {
        $import_paths = $module->Set_Import();

        if( ! is_array( $import_paths ) )
        {
            $import_paths = array();
        }

        // Include default paths
        //$import_paths []= trim( $module->Get_path(), "\\" ).DIRECTORY_SEPARATOR."Controllers";
        $import_paths []= $module->Get_path()."Models";
        $import_paths []= $module->Get_path()."Libraries";
        $import_paths []= $module->Get_path()."Helpers";

        foreach( $import_paths as $path )
        {
            $path = str_replace( array( "\\", "/" ), DIRECTORY_SEPARATOR, $path );
            $path = rtrim( $path, DIRECTORY_SEPARATOR ).DIRECTORY_SEPARATOR;

            App()->import_paths []= $path;

            General_functions::Load_class_files_from_folder_and_subfolders( $path );
        }
    }
    
    /**
     * @method	Include_controller
     * @access	public
     * @desc    This method will include a controller if not included and exists
     * @author	Cousin Béla
     * 
     * @param 	string              $controller             - the name of the module
     * 
     * @version	1.0.0
     * @return  boolean
     * @throws  MController_not_found_exception
     */
    public static function Include_controller( $controller )
    {
        $controller       = strtolower( $controller );
        $controller_class = ucfirst( $controller )."_controller";
        
        // Check and include controller if exists, otherwise show an error
        if( ! self::Check_and_include_controller( $controller_class ) )
        {
            throw new MController_not_found_exception(
                'Controller "'.$controller_class.'" not found' 
            );
        }

        App()->Set_classname_for( "controller", $controller_class );
        
        return TRUE;
    }
    
    /**
     * @method	Check_and_include_controller
     * @access	public
     * @desc    This method will include the controller from active module, or from controllers 
     *          directory in projects or system folder
     * @author	Cousin Béla
     * 
     * @param 	string              $controller_class       - the name of the controller class
     * 
     * @version	1.0.0
     * @return  boolean
     * @throws  MController_not_found_exception
     */
    private static function Check_and_include_controller( $controller_class )
    {
        $class_map      =& App()->classmap;
        $path           = BASEPATH."Core/";
        $module         = App()->router->Get_module();

        // Check if exists in project modules
        try
        {
            if( ! empty( $module ) && App()->Has_module( $module ) && is_object( App()->module ) )
            {
                $path   = App()->module->Get_path();
            }
        }
        catch( Exception $e )
        {
            $path = BASEPATH."Core/";
        }

        $class_path   = $path."Controllers/".$controller_class.Settings::EXT;
        if( file_exists( $class_path ) && is_file( $class_path ) )
        {
            $class_map[$controller_class] = $class_path;
            return TRUE;
        }

        // If module not empty, show an error because controller in module not found
        if( ! empty( $module ) )
        {
            throw new MController_not_found_exception(
                'Controller "'.$controller_class.'" not found in module "'.$module.'"'
            );
        }
        
        // Check if exists in core modules
        $class_path    = COREPATH."Controllers/".$controller_class.Settings::EXT;
        
        if( file_exists( $class_path ) && is_file( $class_path ) )
        {
            $class_map[$controller_class] = $class_path;
            App()->Set_classname_for( strtolower( $controller_class ), $controller_class );
            return TRUE;
        }
        
        return FALSE;
    }
    
    /**
     * @method	Instantiate_class
     * @access	public
     * @desc    This method will instantiate a class if exists and set the reference into
     *          variable in application
     * @author	Cousin Béla
     * 
     * @param 	string              $type                       - the type of the class to instanciate
     * @param 	bool                $error_if_missing           - throw an error if missing
     * @param 	bool                $return                     - return the instantiated class or set into main app
     * @param 	bool                $new                        - create new instance forced
     *
     * @version	1.0.0
     * @return  object
     */
    public static function Instantiate_class( $type, $error_if_missing = TRUE, $return = FALSE, $new = FALSE )
    {
        $class = App()->Get_classname_for( $type );
       
        // If class is missing and we have to throw an error
        if( ! $class && $error_if_missing )
        {
            self::Error_handler( E_ERROR, "Class for type {".$type."} not found" );
        }

        // Try to get with subclassprefix
        if( isset( App()->classmap[Settings::SUBCLASS_PREFIX.$class] ) && ! in_array( $type, Settings::$NOT_REWRITABLE_CLASSES ) )
        {
            $class = Settings::SUBCLASS_PREFIX.$class;
        }

        // try to get the already instantiated object
        if( ! ( $instantiated = App()->Get_instantiated_object( $type ) ) || $new )
        {
            // create new instance
            $instantiated = new $class();
        }

        if( $return )
        {
            return $instantiated;
        }

        App()->Set_instantiated_object( $type, $instantiated );
    }


    /**
     * @method	Error_handler
     * @access	public
     * @desc    This method will will called when a PHP error occured on page
     * @author	Cousin Béla
     *
     * @param 	int                 $errno                      - the number of error code
     * @param 	string              $errstr                     - error message
     * @param 	string              $errfile                    - trace for error file
     * @param 	int                 $errline                    - line where the error happened
     * @param 	array               $errcontext                 - error context
     *
     * @throws  MPHP_exception
     * @version	1.0.0
     */
    public static function Error_handler( $errno = 0, $errstr = "", $errfile = "", $errline = 0, $errcontext = array() )
    {
        /*$class_name = App()->Get_classname_for( "mphp_exception" );

        $class      = new $class_name( $errno, $errstr, $errfile, $errline, $errcontext );
        $class->Show_error_and_stop();*/

        /*
        echo $errstr."<br/ >";
        echo $errfile."<br/ >";
        echo $errline."<br/ >";
        die;
        */
        throw new MPHP_exception( $errno, $errstr, $errfile, $errline, $errcontext );
    }
}

/* End of file General_functions.php */
/* Location: ./core/Base/ */