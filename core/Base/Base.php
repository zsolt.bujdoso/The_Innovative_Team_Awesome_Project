<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Base
 *
 * @version     1.0.0
 */
class Base
{
    public function __construct()
    {
    }
    
    public function & __get( $name )
    {
        if( property_exists( $this, $name ) )
        {
            return $this->$name;
        }
        
        // TODO: throw an exception, this property does not exists
        throw new MProperty_not_exists_exception( 'The class "'.get_class( $this ).'" does not have a property named '.$name );
    }
    
    public function __set( $name, $value )
    {
        if( property_exists( $this, $name ) )
        {
            $this->$name = $value;
            return;
        }
        
        // TODO: throw an exception, this property does not exists
        throw new MProperty_not_exists_exception( 'The class "'.get_class( $this ).'" does not have a property named '.$name );
    }
}

/* End of file Base.php */
/* Location: ./core/Base/ */