<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Path_helper
 *
 * This class has functions to get the active path, or base path, or other path helper functions
 *
 * @author  Cousin Bela
 * @version 1.0
 */
class Path_helper
{
    /**
     * @method  Base_path
     * @access  public static
     * @desc    This method return the base path for the site where we are, ex: /var/www/domain/tizbani.no-ip.org/framework/project/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Base_path()
    {
        return BASEPATH;
    }

    /**
     * @method  Theme_path
     * @access  public static
     * @desc    This method return the active theme path for the site where we are,
     *          ex: /var/www/domain/themes/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Theme_path()
    {
        return App()->theme->Get_path();
    }

    /**
     * @method  Theme_view_path
     * @access  public static
     * @desc    This method return the view path from active theme path for the site where we are,
     *          ex: /var/www/domain/themes/test/Views/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Theme_view_path()
    {
        return self::Theme_path()."Views".DIRECTORY_SEPARATOR;
    }

    /**
     * @method  Theme_image_path
     * @access  public static
     * @desc    This method return the image path from active theme path for the site where we are,
     *          ex: /var/www/domain/themes/test/assets/images/static/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Theme_image_path()
    {
        return self::Theme_path()."assets".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."static".DIRECTORY_SEPARATOR;
    }

    /**
     * @method  Active_module_path
     * @access  public static
     * @desc    This method return the active module path
     *          ex: /var/www/domain/Core/Modules/Products/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Active_module_path()
    {
        return MODULEPATH.ucfirst( App()->router->Get_module() ).DIRECTORY_SEPARATOR;
    }
}

/* End of file Path_helper.php */
/* Location: ./core/Helpers/ */