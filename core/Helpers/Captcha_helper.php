<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Captcha_helper
 *
 * @version     1.0.0
 */
class Captcha_helper
{
    /**
     * @method  Get_text
     * @access  public static
     * @desc    This method returns the new value as a text
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  string
     */
    public static function Get_text()
    {
        $captcha_number1    = ( int ) Variable_helper::Has_value( App()->controller->data, "captcha_number1" );
        $captcha_number2    = ( int ) Variable_helper::Has_value( App()->controller->data, "captcha_number2" );
        $captcha_text       = App()->lang->Get( "Are_you_human" )." ".$captcha_number1 ." + ".$captcha_number2;

        return $captcha_text;
    }

    /**
     * @method  Get_value
     * @access  public static
     * @desc    This method returns the new value of captcha code
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  int
     */
    public static function Get_value()
    {
        $captcha_number1    = ( int ) Variable_helper::Has_value( App()->controller->data, "captcha_number1" );
        $captcha_number2    = ( int ) Variable_helper::Has_value( App()->controller->data, "captcha_number2" );
        $captcha_text       = $captcha_number1 + $captcha_number2;

        return $captcha_text;
    }

    /**
     * @method  Get_old_value
     * @access  public static
     * @desc    This method returns the old captcha value from session if exists
     * @author  Cousin Bela
     *
     * @version 1.0.0
     * @return  int
     */
    public static function Get_old_value()
    {
        $captcha_code = null;
        if( App()->session->Has( "old_captcha_code" ) )
        {
            $captcha_code = App()->session->Get( "old_captcha_code" );
        }

        return $captcha_code;
    }

    /**
     * @method  Is_captcha_good
     * @access  public static
     * @desc    This method create a new captcha code for validation and set it into data and session
     * @author  Cousin Bela
     *
     * @param   string                      $captcha_code                   - the value of captcha code to check
     *
     * @version 1.0.0
     * @return  bool
     */
    public static function Is_captcha_good( $captcha_code = "" )
    {
        $old_captcha_code = self::Get_old_value();

        if( empty( $captcha_code ) )
        {
            return FALSE;
        }

        return ( $captcha_code == $old_captcha_code );
    }

    /**
     * @method  Create_captcha
     * @access  public static
     * @desc    This method create a new captcha code for validation and set it into data and session
     * @author  Cousin Bela
     *
     * @version 1.0.0
     */
    public static function Create_captcha()
    {
        self::Load_old_captcha();
        
        $captcha_number1 = rand( 1, 10 );
        $captcha_number2 = rand( 1, 10 );

        App()->controller->data["captcha_number1"] = $captcha_number1;
        App()->controller->data["captcha_number2"] = $captcha_number2;
        App()->session->Set( "captcha_code", ( $captcha_number1 + $captcha_number2 ) );
    }

    /**
     * @method  Load_old_captcha
     * @access  public static
     * @desc    This method checks if old captcha code is set, and set it as an old captcha code
     * @author  Cousin Bela
     *
     * @version 1.0.0
     */
    public static function Load_old_captcha()
    {
        $old_captcha_code = null;
        if( App()->session->Has( "captcha_code" ) )
        {
            $old_captcha_code = App()->session->Get( "captcha_code" );
        }

        if( empty( $old_captcha_code ) )
        {
            return;
        }

        App()->session->Set( "old_captcha_code", $old_captcha_code );
    }
}

/* End of file Captcha_helper.php */
/* Location: ./core/Helpers/ */