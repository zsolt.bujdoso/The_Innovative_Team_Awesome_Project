<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class String_helper
 *
 * This class help us to set messages into session or session flash
 *
 * @version 1.0.0
 */
class String_helper
{
    /**
     * @method	Trim_slashes
     * @access	public
     * @desc    Trim /-s from left and right side of a string
     * @author	Cousin Béla
     *
     * @param   string                      $string                     - the string to trim from
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Trim_slashes( $string )
    {
        return trim( $string, "/" );
    }

    /**
     * @method  Remove_slashes
     * @access  public
     * @desc    Remove slashes from a string
     * @author  Cousin Bela
     *
     * @param   mixed                       $object                     - the item to remove slashes from
     *
     * @version 1.0.0
     * @return  object
     */
    public static function Remove_slashes( $object )
    {
        // if is array, then remove from each item
        if ( is_array( $object ) )
        {
            foreach ( $object as $key => $value )
            {
                $object[$key] = remove_slashes( $value );
            }
        }
        // remove from this string
        else
        {
            $object = stripslashes( $object );
        }

        return $object;
    }

    /**
     * @method  Remove_spaces
     * @access  public
     * @desc    Remove spaces from a string
     * @author  Cousin Bela
     *
     * @param   mixed                       $object                     - the item to remove slashes from
     *
     * @version 1.0.0
     * @return  object
     */
    public static function Remove_spaces( $object )
    {
        // if is array, then remove from each item
        if ( is_array( $object ) )
        {
            foreach ( $object as $key => $value )
            {
                $object[$key] = str_replace( " ", "", $value );
            }
        }
        // remove from this string
        else
        {
            $object = str_replace( " ", "", $object );
        }

        return $object;
    }

    /**
     * @method	Temove_quotes
     * @access	public
     * @desc	this method removes the single and double quotes from the specified string
     * @author	Zoltan Jozsa
     *
     * @param 	string 				        $string				        - the string where we have to remove
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Remove_quotes( $string = '' )
    {
        return str_replace( array( '"', "'" ), '', $string );
    }

    /**
     * @method	quotes_to_entities
     * @access	public
     * @desc	  this method converts the single and double quotes to html entites in the specified string
     * @author	Zoltan Jozsa
     *
     * @param 	string 					    $string				        - the string where we have to remove
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Quotes_to_entities( $string )
    {
        return str_replace( array( "\'", "\"", "'", '"' ), array( "&#39;", "&quot;", "&#39;", "&quot;" ), $string );
    }

    /**
     * @method	Encode_url
     * @access	public
     * @desc	this method converts the specified text to an url readable format
     * @author	Zoltan Jozsa
     *
     * @param 	string 					    $string				        - the string where we have to remove
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Encode_url( $string )
    {
        $replace= array( "\'", "\"", "'", '"', ' ' );
        $string = str_replace( $replace, "-", $string );
        $string = str_replace( array( "-&-", "&" ), "-and-", $string );
        //$string = iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE', $string );


        return $string;
    }

    /**
     * @method	Encode_email
     * @access	public
     * @desc	this method converts an email address to an encoded formats
     * @author	Zoltan Jozsa
     *
     * @param 	string 					    $email				        - the string where we have to remove
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Encode_email( $email )
    {
        $replace    = array( "@", ".", );
        $replace_to = array( "[@]", "[.]" );
        $email      = str_replace( $replace, $replace_to, $email );

        return $email;
    }

    /**
     * @method	Encode_name
     * @access	public
     * @desc	this method converts the specified name text to format without whitespace, commas, etc.
     * @author	Zoltan Jozsa
     *
     * @param 	string 					    $string				        - the string where we have to remove
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Encode_name( $string )
    {
        $string = preg_replace( "/[\s,;'\"]{1,}/", "-", $string );
        $string = iconv( 'UTF-8', 'ASCII//TRANSLIT', $string );

        return $string;
    }

    /**
     * @method	Random_string
     * @access	public
     * @desc	this method generates a random string by parameters
     * @author	Zoltan Jozsa
     *
     * @param 	string 					    $type				        - the type of the randomisation
     * @param	int						    $length				        - length of the random string
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Random_string( $type = 'alnum', $length = 8 )
    {
        switch ( $type )
        {
            case 'alnum'	:	$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'numeric'	:	$pool = '0123456789';
                break;
            case 'nozero'	:	$pool = '123456789';
                break;
            case 'unique' 	: 	return md5( uniqid( mt_rand() ) );
        }

        // create the string from pool
        $string 	= '';
        for ($i=0; $i < $length; $i++)
        {
            $string .= substr( $pool, mt_rand( 0, strlen( $pool ) -1 ), 1 );
        }

        // return the generated string
        return $string;
    }

    /**
     * @method	Is_link
     * @access	public
     * @desc	check if sting is link
     * @author	Zoltan Jozsa
     *
     * @param	string      			    $string				        - string to check
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Is_link( $string = "" )
    {
        if( preg_match( "/^(http\:\/\/|https\:\/\/|www\.).*/i", $string ) )
        {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * @method	Get_helper_name
     * @access	public
     * @desc	return helper name
     * @author	Zoltan Jozsa
     *
     * @param	string      			    $table_name 		        - name of table
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Get_helper_name( $table_name = "" )
    {
        if( strpos( $table_name, "_categories" ) !== FALSE )
        {
            $helper_name = str_replace( "_categories" , "_category", $table_name );
        }
        else
        {
            $helper_name = self::Get_name( $table_name );
        }

        $helper_name = ucfirst( $helper_name."_helper" );

        return $helper_name;
    }

    private static function Get_name( $table_name = "" )
    {
        $name = ucfirst( substr( $table_name, 0, -1 ) );

        if( App()->Has_module( $table_name ) )
        {
            $module = App()->Get_module( $table_name );

            if( ! empty( $module->helper ) )
            {
                $name = ucfirst( $module->helper );
            }
        }
        
        return $name;
    }

    /**
     * @method	Get_categories_name
     * @access	public
     * @desc	return category name
     * @author	Zoltan Jozsa
     *
     * @param	string      			    $table_name 		        - name of table
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Get_categories_name( $table_name = "" )
    {
        $category_name = $table_name;

        if( preg_match( "/(.*)\_categories$/", $category_name ) )
        {
            $category_name = preg_replace( "/(.*)\_categories$/", "$1", $category_name );
        }
        else
        {
            $category_name = self::Get_name( $category_name );
        }

        $category_name = ucfirst( $category_name."_categories" );

        return $category_name;
    }

    /**
     * @method	Get_validation_name
     * @access	public
     * @desc	return validation name
     * @author	Zoltan Jozsa
     *
     * @param	string      			    $table_name 		        - name of table
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Get_validation_name( $table_name = "" )
    {
        $validation_name = $table_name;

        if( preg_match( "/(.*)\_categories$/", $validation_name ) )
        {
            $validation_name = preg_replace( "/(.*)\_categories$/", "$1_category", $validation_name );
        }
        elseif( ! preg_match( "/(.*)_details/", $validation_name ) )
        {
            $validation_name = self::Get_name( $validation_name );
        }

        $validation_name = ucfirst( $validation_name."_form_validation" );

        return $validation_name;
    }

    /**
     * @method	Get_model_name
     * @access	public
     * @desc	return helper name
     * @author	Zoltan Jozsa
     *
     * @param	string      			    $table_name 		        - name of table
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Get_model_name( $table_name = "" )
    {
        $model_name = ucfirst( strtolower( $table_name ) );

        return $model_name .= "_model";
    }

    /**
     * @method	Get_item_name
     * @access	public
     * @desc	This method returns the name of the item
     * @author	Zoltan Jozsa
     *
     * @param	string      			    $item_name                  - the name of the item
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Get_item_name( $item_name = "" )
    {
        if( strpos( $item_name, "_categories" ) !== FALSE )
        {
            $item_name = ucfirst( str_replace( "_categories" , " categories", $item_name ) );
        }
        else
        {
            $item_name = ucfirst( $item_name );
        }

        return $item_name;
    }

    /**
     * @method	Get_item_id_column_name
     * @access	public
     * @desc	This method returns the name of the item
     * @author	Zoltan Jozsa
     *
     * @param	string      			    $item_name                  - the name of the item
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Get_item_id_column_name( $item_name = "" )
    {
        $column_name= strtolower( $item_name );
        $column_name= str_replace( array( "_details", "_properties", "_combinations" ), "", $column_name );
        $column_name.= "_id";

        return $column_name;
    }

    /**
     * @method	Convert_name_to_code
     * @access	public
     * @desc	This method replaces all spaces with underscore, and returns the text with lowercase characters
     * @author	Csenteri Attila
     *
     * @param	string      			    $name 		                - the name to convert
     *
     * @version	1.0.0
     * @return	string
     */
    public static function Convert_name_to_code( $name )
    {
        $name = strtolower( $name );
        // Make alphanumeric ( replaces with underscore all unwanted characters )
        $name = iconv( 'UTF-8', 'ASCII//TRANSLIT', $name );
        $name = preg_replace( "/[;'\"\']{1,}/", "", $name );
        $name = preg_replace( "/[\s\.\-\,\:\%\@\?\!\+]{1,}/", "_", $name );
        $name = preg_replace( "/[?]/", "", $name );
        $name = str_replace( array( "_&_", "&", ), "_and_", $name );
        $name = str_replace( array( "^", ), "", $name );

        return $name;
    }
}

/* End of file String_helper.php */
/* Location: ./Core/Helpers/ */