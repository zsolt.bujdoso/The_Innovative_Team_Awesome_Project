<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Logger_helper
 *
 * This class help us to set messages into session or session flash
 *
 * @version 1.0.0
 */
class Logger_helper
{
    private static function Add_user_to_message( & $message )
    {
        if( User_helper::Get_user_id() )
        {
            $message = "User (ID: ".User_helper::Get_user_id().") ".$message;
            return;
        }

        // Get active URL
        //$link       = App()->lang->Get_language_code()."/".MY_Url_helper::Get_active_url_without_language();
        $message    = "User (Unknown: ".Server_helper::Get( "remote_addr" ).") ".$message;
    }

    /**
     * @method	Info
     * @access	public
     * @desc    Add an info message to logs
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Info( $message )
    {
        self::Add_user_to_message( $message );

        App()->logger->Info( $message );
    }

    /**
     * @method	Info_insert
     * @access	public
     * @desc    Add an info message to logs
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - type of the item
     * @param   int                         $new_id                     - primary key of the recently inserted item
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Info_insert( $item_name, $new_id )
    {
        $message = 'inserted a new "' . $item_name . '" item (ID: ' . $new_id . ')';
        
        App()->logger->Info( $message );
    }

    /**
     * @method	Info_update
     * @access	public
     * @desc    Add an info message to logs
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - type of the item
     * @param   int                         $item_id                    - primary key of the recently inserted item
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Info_update( $item_name, $item_id )
    {
        $message = 'saved modification(s) for "' . $item_name . '" item (ID: ' . $item_id . ')';
        
        App()->logger->Info( $message );
    }

    /**
     * @method	Debug
     * @access	public
     * @desc    Add an debug message to logs
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Debug( $message )
    {
        self::Add_user_to_message( $message );

        App()->logger->Debug( $message );
    }

    /**
     * @method	Error
     * @access	public
     * @desc    Add an error message to logs
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Error( $message )
    {
        self::Add_user_to_message( $message );

        App()->logger->Error( $message );
    }

    /**
     * @method	Warning
     * @access	public
     * @desc    Add an warning message to logs
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Warning( $message )
    {
        self::Add_user_to_message( $message );

        App()->logger->Warning( $message );
    }
}

/* End of file Logger_helper.php */
/* Location: ./Core/Helpers/ */