<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Message_helper
 *
 * This class help us to set messages into session or session flash
 *
 * @version 1.0.0
 */
class Message_helper
{
    /**
     * @method	Add_success_message
     * @access	public
     * @desc    Add a success message into data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_success_message( $message )
    {
        self::Add_message( $message, "success" );
    }

    /**
     * @method	Add_error_message
     * @access	public
     * @desc    Add an error message into data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_error_message( $message )
    {
        self::Add_message( $message );
    }

    /**
     * @method	Add_info_message
     * @access	public
     * @desc    Add an error message into data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_info_message( $message )
    {
        self::Add_message( $message, "info" );
    }

    /**
     * @method	Add_alert_message
     * @access	public
     * @desc    Add an error message into data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_alert_message( $message )
    {
        self::Add_message( $message, "alert" );
    }

    /**
     * @method	Add_admin_message
     * @access	public
     * @desc    Add an error message into data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_fancybox_message( $message )
    {
        self::Add_message( $message, "fancybox" );
    }

    /**
     * @method	Add_admin_success_message
     * @access	public
     * @desc    Add a success message into data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_admin_success_message( $message, $id )
    {
        self::Add_admin_message( $message, $id, "success" );
    }

    /**
     * @method	Add_admin_error_message
     * @access	public
     * @desc    Add an error message into data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_admin_error_message( $message, $id )
    {
        self::Add_admin_message( $message, $id );
    }

    /**
     * @method	Add_admin_info_message
     * @access	public
     * @desc    Add an error message into data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_admin_info_message( $message, $id )
    {
        self::Add_admin_message( $message, $id, "info" );
    }

    /**
     * @method	Add_admin_alert_message
     * @access	public
     * @desc    Add an error message into data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_admin_alert_message( $message, $id )
    {
        self::Add_admin_message( $message, $id, "alert" );
    }

    /**
     * @method	Add_admin_fancybox_message
     * @access	public
     * @desc    Add an error message into data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_admin_fancybox_message( $message, $id )
    {
        self::Add_admin_message( $message, $id, "fancybox" );
    }

    /**
     * @method	Add_success_flash_message
     * @access	public
     * @desc    Add a success message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_success_flash_message( $message )
    {
        self::Add_flash_message( $message, "success" );
    }

    /**
     * @method	Add_error_flash_message
     * @access	public
     * @desc    Add an error message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_error_flash_message( $message )
    {
        self::Add_flash_message( $message );
    }

    /**
     * @method	Add_info_flash_message
     * @access	public
     * @desc    Add an error message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_info_flash_message( $message )
    {
        self::Add_flash_message( $message, "info" );
    }

    /**
     * @method	Add_alert_flash_message
     * @access	public
     * @desc    Add an error message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_alert_flash_message( $message )
    {
        self::Add_flash_message( $message, "alert" );
    }

    /**
     * @method	Add_fancybox_flash_message
     * @access	public
     * @desc    Add an error message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_fancybox_flash_message( $message )
    {
        self::Add_flash_message( $message, "fancybox" );
    }

    /**
     * @method	Add_admin_success_flash_message
     * @access	public
     * @desc    Add a success message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_admin_success_flash_message( $message, $id )
    {
        self::Add_admin_flash_message( $message, $id, "success" );
    }

    /**
     * @method	Add_admin_error_flash_message
     * @access	public
     * @desc    Add an error message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_admin_error_flash_message( $message, $id )
    {
        self::Add_admin_flash_message( $message, $id );
    }

    /**
     * @method	Add_admin_info_flash_message
     * @access	public
     * @desc    Add an error message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_admin_info_flash_message( $message, $id )
    {
        self::Add_admin_flash_message( $message, $id, "info" );
    }

    /**
     * @method	Add_admin_alert_flash_message
     * @access	public
     * @desc    Add an error message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_admin_alert_flash_message( $message, $id )
    {
        self::Add_admin_flash_message( $message, $id, "alert" );
    }

    /**
     * @method	Add_admin_fancybox_flash_message
     * @access	public
     * @desc    Add an error message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_admin_fancybox_flash_message( $message, $id )
    {
        self::Add_admin_flash_message( $message, $id, "fancybox" );
    }

    /**
     * @method	Add_flash_message
     * @access	public
     * @desc    Add a message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   string                      $message_type               - type of the message
     *
     * @version	1.0.0
     * @return  string
     */
    private static function Add_flash_message( $message, $message_type = "error" )
    {
        $messages = array();

        if( App()->session->Has_flash_without_checking_level( "messages" ) )
        {
            $messages = App()->session->Get_flash_without_checking_level( "messages" );
        }

        $messages   []= array(
                        "type"  => $message_type,
                        "text"  => $message
                    );

        App()->session->Set_flash( "messages", $messages );
    }

    /**
     * @method	Add_admin_flash_message
     * @access	public
     * @desc    Add a message into flash data in session
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     * @param   string                      $message_type               - type of the message
     *
     * @version	1.0.0
     * @return  string
     */
    private static function Add_admin_flash_message( $message, $id, $message_type = "error" )
    {
        $messages = array();

        if( App()->session->Has_flash( "admin_flash_messages" ) )
        {
            $messages = App()->session->Get_flash( "admin_flash_messages" );
        }

        $messages   []= array(
                        "type"  => $message_type,
                        "text"  => $message,
                        "id"    => $id
                    );

        App()->session->Set_flash( "admin_flash_messages", $messages );
    }

    /**
     * @method	Add_message
     * @access	public
     * @desc    Add a message into controller data
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   string                      $message_type               - type of the message
     *
     * @version	1.0.0
     * @return  string
     */
    private static function Add_message( $message, $message_type = "error" )
    {
        $messages = array();

        if( ! empty( App()->controller->data["system_messages"] ) )
        {
            $messages = App()->controller->data["system_messages"];
        }

        $messages   []= array(
                        "type"  => $message_type,
                        "text"  => $message
                    );

        App()->controller->data["system_messages"] = $messages;
    }

    /**
     * @method	Add_admin_message
     * @access	public
     * @desc    Add a message into controller data
     * @author	Cousin Béla
     *
     * @param   string                      $message                    - the message to set
     * @param   int                         $id                         - id of the message
     * @param   string                      $message_type               - type of the message
     *
     * @version	1.0.0
     * @return  string
     */
    private static function Add_admin_message( $message, $id, $message_type = "error" )
    {
        $messages = array();

        if( ! empty( App()->controller->data["admin_messages"] ) )
        {
            $messages = App()->controller->data["admin_messages"];
        }

        $messages   []= array(
                        "type"  => $message_type,
                        "text"  => $message,
                        "id"    => $id,
                    );

        App()->controller->data["admin_messages"] = $messages;
    }

    public static function Show_message( $message, $message_type = "info" )
    {
        $data = array(
            "message"       => $message,
            "message_type"  => $message_type
        );

        echo App()->controller->Load_partial_view( "//partials/message", $data );
    }

    public static function Show_error_message( $message )
    {
        self::Show_message( $message, "error" );
    }

    public static function Show_success_message( $message )
    {
        self::Show_message( $message, "success" );
    }

    public static function Show_info_message( $message )
    {
        self::Show_message( $message );
    }

    public static function Show_alert_message( $message )
    {
        self::Show_message( $message, "alert" );
    }
}

/* End of file Message_helper.php */
/* Location: ./Core/Helpers/ */