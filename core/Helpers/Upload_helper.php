<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Upload_helper
 *
 * This class help us to set messages into session or session flash
 *
 * @version 1.0.0
 */
class Upload_helper
{
    /**
     * @method  Upload_files
     * @access  public
     * @desc    Upload files for gallery
     * @author  Cousin Bela
     *
     * @param   string                      $file_path                  - the temp file name to move
     * @param   string                      $path_to                    - the path where to move it
     * @param   string                      $old_file_name              - the old file name
     *
     * @version 1.0
     * @return  string
     */
    public static function Move_uploaded_file_with_auto_name( $file_path, $path_to, $old_file_name )
    {
        if( ! file_exists( $file_path ) )
        {
            return FALSE;
        }

        // Create the directory if not exists
        if( ! file_exists( $path_to ) || ! is_dir( $path_to ) )
        {
            $chmod = Config_helper::Get( "chmod_for_created_folders" );
            if( empty( $chmod ) )
            {
                $chmod = 0777;
            }

            Directory_helper::Create_directories_recursively( $path_to, intval( $chmod ) );
        }

        $file_name_array    = explode( ".", $old_file_name );
        $extension          = end( $file_name_array );
        $file_name          = String_helper::Random_string().".".$extension;

        // Move uploaded file
        if( ! move_uploaded_file( $file_path, $path_to.$file_name ) )
        {
            return FALSE;
        }

        $chmod = Config_helper::Get( "chmod_for_created_files" );
        if( empty( $chmod ) )
        {
            $chmod = 0777;
        }

        if( preg_match( "/^\/(.*)$/", $path_to ) )
        {
            //chmod( $path_to . $file_name, $chmod );
        }

        return $file_name;
    }
}

/* End of file Upload_helper.php */
/* Location: ./Core/Helpers/ */