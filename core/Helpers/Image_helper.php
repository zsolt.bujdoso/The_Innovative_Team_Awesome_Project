<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Image_helper
 *
 * This class helps us to get html elements printed in templates, exemple: javascript, css
 *
 * @version 1.0.0
 */
class Image_helper
{
    public static $available_images = array(
        "png"   => 1,
        "jpg"   => 1,
        "jpeg"  => 1,
        "gif"   => 1,
        "tif"   => 1,
    );

    /**
     * @method	Create_thumbnail_from_template
     * @access	public
     * @desc    Get a javascript html element with name specified from themes or module
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - the name of the file with or without path to get from javascripts
     * @param   string                      $template_name              - the name of the template contains settings
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Create_thumbnail_from_template( $file_name, $template_name )
    {
        if( empty( $file_name ) )
        {
            $file_name = self::Get_default_image();
        }

        $file_name  = urldecode( $file_name );
        $image      = new MImage();

        return $image->Create_thumbnail_from_template( $file_name, $template_name );
    }

    /**
     * @method	Get_default_image
     * @access	public
     * @desc    This method checks if a file name is an image name or not
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_default_image()
    {
        $default_image = Config_helper::Get( "default_image" );

        if( empty( $default_image ) )
        {
            $default_image = "no-image.png";
        }

        return $default_image;
    }

    /**
     * @method	Create_thumbnail_from_template_by_file_path
     * @access	public
     * @desc    Get a javascript html element with name specified from themes or module
     * @author	Cousin Béla
     *
     * @param   string                      $file_path                  - the full path of the file with or without path to get from javascripts
     * @param   string                      $template_name              - the name of the template contains settings
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Create_thumbnail_from_template_by_file_path( $file_path, $template_name )
    {
        $image = new MImage();

        return $image->Create_thumbnail_from_template_by_path( $file_path, $template_name );
    }

    /**
     * @method	Get_image_path
     * @access	public
     * @desc    Search in possible locations for image by given name, and return the path when founded
     * @author	Cousin Béla
     *
     * @param   string                      $image_name                 - the name of the image to find
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_image_path( $image_name )
    {
        $image_name         = trim( $image_name, "/" );
        $image_directory    = "assets".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."static".DIRECTORY_SEPARATOR;

        if( empty( $image_name ) || ! is_string( $image_name ) )
        {
            return FALSE;
        }

        $path = MODULEPATH.App()->router->Get_module()."/".$image_directory.$image_name;
        if( file_exists( $path ) )
        {
            return $path;
        }

        $path = App()->theme->Get_path().$image_directory.$image_name;
        if( file_exists( $path ) )
        {
            return $path;
        }

        $path = BASEPATH."upload".DIRECTORY_SEPARATOR.$image_name;
        if( file_exists( $path ) )
        {
            return $path;
        }
        
        $path = BASEPATH.$image_directory.$image_name;
        if( file_exists( $path ) )
        {
            return $path;
        }

        $path = BASEPATH.$image_name;
        if( strpos( $image_name, "upload" ) === 0 && file_exists( $path ) )
        {
            return $path;
        }

        return FALSE;
    }

    /**
     * @method	Get_image_url
     * @access	public
     * @desc    Search in possible locations for image by given name, and return the url when founded
     * @author	Cousin Béla
     *
     * @param   string                      $image_name                 - the name of the image to find
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_image_url( $image_name )
    {
        $image_path = self::Get_image_path( $image_name );

        if( empty( $image_name ) || empty( $image_path ) )
        {
            $image_name = self::Get_default_image();
            $image_path = self::Get_image_path( $image_name );
        }

        if( ! empty( $image_path ) )
        {
            $image_path = str_replace( BASEPATH, Url_helper::Base_url(), $image_path );
            $image_path = str_replace( "\\", "/", $image_path );

            return $image_path;
        }

        return "";
    }

    /**
     * @method	Is_image
     * @access	public
     * @desc    This method checks if a file name is an image name or not
     * @author	Cousin Béla
     *
     * @param   string                      $image_name                 - the name of the image to check
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Is_image( $image_name )
    {
        $extension = strtolower( pathinfo( $image_name, PATHINFO_EXTENSION ) );

        if( ! empty( self::$available_images[$extension] ) )
        {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * @method	Get_default_image_alt
     * @access	public
     * @desc    This method checks if a file name is an image name or not
     * @author	Cousin Béla
     *
     * @param   string                      $image                      - the name of the image
     * @param   string                      $item_name                  - the name of the selected item
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_default_image_alt( $image, $item_name = "" )
    {
        $default_image = Config_helper::Get( "default_image" );

        if( empty( $image ) )
        {
            $image = ( ! empty( $default_image ) ? $default_image : "no-image.png" );
        }
        elseif( ! empty( $item_name ) )
        {
            $image = $item_name;
        }

        return basename( $image );
    }
}

/* End of file Image_helper.php */
/* Location: ./Core/Helpers/ */