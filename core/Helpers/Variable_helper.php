<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Variable_helper
 *
 * @version     1.0.0
 */
class Variable_helper
{
    public static function Has_value( & $variable, $index = null, $default_value = FALSE )
    {
        // if variable is not set than return FALSE or default value
        if( ! isset( $variable ) )
        {
            return $default_value;
        }

        // if index is not set check if variable is TRUE
        if( ! isset( $index ) )
        {
            return ( $variable ? $variable : $default_value );
        }

        // if variable is object and property exists, return it
        if( is_object( $variable ) && property_exists( $variable, $index ) )
        {
            return $variable->$index;
        }

        // if variable is array and index is set, return the value
        if( is_array( $variable ) && isset( $variable[$index] ) )
        {
            return $variable[$index];
        }

        return $default_value;
    }
}

/* End of file Variable_helper.php */
/* Location: ./core/Helpers/ */