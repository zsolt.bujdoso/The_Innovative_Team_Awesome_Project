<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Post_helper
 *
 * This class helps us to get back data after posted,
 * if it was not posted tries to get from object
 *
 * @version 1.0.0
 */
class Post_helper
{
    /**
     * @method	Is_set
     * @access	public
     * @desc    This method check an item in POST if is set, if not than check in object, if it is set
     * @author	Cousin Béla
     *
     * @param   string                      $name                       - the name of the item to check in POST or object
     * @param   mixed                       $object                     - the object where to get from
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Is_set( $name, & $object = null )
    {
        $get_data =& $_GET;

        if( isset( $get_data[$name] ) )
        {
            return TRUE;
        }

        if( is_array( $object ) && isset( $object[$name] ) )
        {
            return TRUE;
        }

        if( is_object( $object ) && isset( $object->$name ) )
        {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * @method	Get
     * @access	public static
     * @desc    This method check an item in POST if were set, if not returns it from object, if it is set
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - the name of the item to check in POST or object
     * @param   mixed                       $object                     - the object where to get from
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get( $item_name, & $object = null, $default_value = "" )
    {
        $post_data = $_POST;

        if( empty( $post_data ) && App()->session->Has_flash( "post_data" ) )
        {
            $post_data = App()->session->Get_flash( "post_data" );
        }

        if( isset( $post_data[$item_name] ) )
        {
            /*
             * todo: ellenőrizni a htmlspecialchars-t
             */
            //return ( is_string( $post_data[$item_name] ) ? htmlspecialchars( $post_data[$item_name] ) : $post_data[$item_name] );
            return ( is_string( $post_data[$item_name] ) ? ( $post_data[$item_name] ) : $post_data[$item_name] );
        }

        if( is_array( $object ) && isset( $object[$item_name] ) )
        {
            return ( is_string( $object[$item_name] ) ? htmlspecialchars( $object[$item_name] ) : $object[$item_name] );
        }

        if( is_object( $object ) && isset( $object->$item_name ) )
        {
            return ( is_string( $object->$item_name ) ? htmlspecialchars( $object->$item_name ) : $object->$item_name );
        }

        return $default_value;
    }

    /**
     * @method	Get_without_quot
     * @access	public static
     * @desc    This method check an item in POST if were set, if not returns it from object, if it is set
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - the name of the item to check in POST or object
     * @param   mixed                       $object                     - the object where to get from
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_without_quot( $item_name, & $object = null, $default_value = "" )
    {
        return str_replace( '"', "&quot;", self::Get( $item_name, $object, $default_value ) );
    }

    /**
     * @method	Get_integer
     * @access	public
     * @desc    This method check an item in POST if were set, if not returns it from object, if it is set
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - the name of the item to check in POST or object
     * @param   mixed                       $object                     - the object where to get from
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_integer( $item_name, & $object = null, $default_value = "0" )
    {
        $value = self::Get( $item_name, $object, $default_value );

        return ( int ) $value;
    }

    /**
     * @method	Get_float
     * @access	public
     * @desc    This method check an item in POST if were set, if not returns it from object, if it is set
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - the name of the item to check in POST or object
     * @param   mixed                       $object                     - the object where to get from
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_float( $item_name, & $object = null, $default_value = "0.00" )
    {
        $value = self::Get( $item_name, $object, $default_value );

        if( ! is_numeric( $value ) )
        {
            $value = 0.00;
        }

        return $value;
    }

    /**
     * @method	Get_from_sublevel
     * @access	public static
     * @desc    This method check an item in POST if were set with sublevel, if not set returns the item from object,
     *          named as $item_name, if it is set in object
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - the sublevel name of the item to check in POST or
     *                                                                      the name of the item to check in object
     * @param   string                      $subitem_name               - the name of the item to check in POST
     * @param   mixed                       $object                     - the object where to get from
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_from_sublevel( $item_name, $subitem_name, $object = null, $default_value = "" )
    {
        $post_data = $_POST;

        if( empty( $post_data ) && App()->session->Has_flash( "post_data" ) )
        {
            $post_data = App()->session->Get_flash( "post_data" );
        }

        if( isset( $post_data[$item_name][$subitem_name] ) )
        {
            return $post_data[$item_name][$subitem_name];
        }

        // is sublevel item is an array
        if( is_array( $object ) && isset( $object[$item_name] ) )
        {
            // if sublevel array has item
            if( is_array( $object[$item_name] ) && isset( $object[$item_name][$subitem_name] ) )
            {
                return $object[$item_name][$subitem_name];
            }

            // if sublevel item is object and has item
            if( is_object( $object[$item_name] ) && isset( $object[$item_name]->$subitem_name ) )
            {
                return $object[$item_name]->$subitem_name;
            }
        }

        if( is_object( $object ) && isset( $object->$item_name ) )
        {
            // if sublevel array has item
            if( is_array( $object->$item_name ) && isset( $object->$item_name[$subitem_name] ) )
            {
                return $object->$item_name[$subitem_name];
            }

            // if sublevel item is object and has item
            if( is_object( $object->$item_name ) && isset( $object->$item_name->$subitem_name ) )
            {
                return $object->$item_name->$subitem_name;
            }
        }

        if( is_array( $object ) && isset( $object[$item_name] ) )
        {
            return $object[$item_name];
        }

        if( is_object( $object ) && isset( $object->$item_name ) )
        {
            return $object->$item_name;
        }

        return $default_value;
    }

    /**
     * @method	Get_from_sublevel_without_quot
     * @access	public static
     * @desc    This method check an item in POST if were set with sublevel, if not set returns the item from object,
     *          named as $item_name, if it is set in object
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - the sublevel name of the item to check in POST or
     *                                                                      the name of the item to check in object
     * @param   string                      $subitem_name               - the name of the item to check in POST
     * @param   mixed                       $object                     - the object where to get from
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_from_sublevel_without_quot( $item_name, $subitem_name, $object = null, $default_value = "" )
    {
        return str_replace( '"', "&quot;", self::Get_from_sublevel( $item_name, $subitem_name, $object, $default_value ) );
    }

    /**
     * @method	Get_integer_from_sublevel
     * @access	public static
     * @desc    This method check an item in POST if were set with sublevel, if not set returns the item from object,
     *          named as $item_name, if it is set in object
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - the sublevel name of the item to check in POST or
     *                                                                      the name of the item to check in object
     * @param   string                      $subitem_name               - the name of the item to check in POST
     * @param   mixed                       $object                     - the object where to get from
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  int
     */
    public static function Get_integer_from_sublevel( $item_name, $subitem_name, $object = null, $default_value = "0" )
    {
        $value = self::Get_from_sublevel( $item_name, $subitem_name, $object, $default_value );

        return ( int ) $value;
    }
}

/* End of file Post_helper.php */
/* Location: ./Core/Helpers/ */