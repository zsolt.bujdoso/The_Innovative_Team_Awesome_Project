<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Directory_helper
 *
 * This class helps us to create or modify directories
 *
 * @version 1.0.0
 */
class Directory_helper
{   
    /**
     * @method	Create_directories_recursively
     * @access	public
     * @desc    Create directories for a path recursively
     * @author	Cousin Béla
     *
     * @param   string                      $path                       - the path to check and create
     * @param   int                         $permissions                - permissions for created folders
     *
     * @version	1.0.0
     * @return  bool
     */
    public static function Create_directories_recursively( $path, $permissions = 0755 )
    {
        $path           = str_replace( BASEPATH, "", $path );
        $path           = str_replace( "\\", "/", $path );
        $path_array     = explode( "/", $path );
        $file_path      = BASEPATH;

        foreach( $path_array as $path )
        {
            $file_path .= $path."/";
            if( file_exists( $file_path ) && is_dir( $file_path ) )
            {
                continue;
            }

            mkdir( $file_path );
        }

        return self::Change_folder_permission_recursively( $file_path, $permissions );
    }

    /**
     * @method	Create_directories_recursively_for_file
     * @access	public
     * @desc    Create directories for a file recursively
     * @author	Cousin Béla
     *
     * @param   string                      $file_path                  - the path to check and create
     * @param   int                         $permissions                - permissions for created folders
     *
     * @version	1.0.0
     * @return  bool
     */
    public static function Create_directories_recursively_for_file( $file_path, $permissions = 0777 )
    {
        $file_path = str_replace( BASEPATH, "", dirname( $file_path ) );

        return self::Create_directories_recursively( $file_path, $permissions );
    }

    /**
     * @method	Change_folder_permission_recursively
     * @access	public
     * @desc    Create directories for a file recursively
     * @author	Cousin Béla
     *
     * @param   string                      $file_path                  - the path to check and create
     * @param   string                      $permission                 - permission for folders recursively
     *
     * @version	1.0.0
     * @return  bool
     */
    public static function Change_folder_permission_recursively( $file_path, $permission )
    {
        $file_path  = str_replace( BASEPATH, "", $file_path );
        $file_path  = str_replace( "\\", "/", $file_path );
        $path_array = explode( "/", trim( $file_path, "/" ) );
        $path       = BASEPATH;

        foreach( $path_array as $folder )
        {
            $path .= $folder."/";

            if( in_array( $folder, array( "upload", "users" ) ) )
            {
                continue;
            }

            if( Permission_helper::Get_file_permissions( $path ) == $permission )
            {
                continue;
            }

            try
            {
                if( preg_match( "/^\/(.*)$/", $path ) )
                {
                    //chmod( $path, $permission );
                }
            }
            catch( MException $e ){}
        }

        return TRUE;
    }
}

/* End of file Directory_helper.php */
/* Location: ./Core/Helpers/ */