<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Database_helper
 *
 * @version     1.0.0
 */
class Database_helper
{
    public static function Load_database_config( $database_config_name )
    {
        if( ! empty( $database_config_name ) && ! isset( App()->config->database["list"][$database_config_name] ) )
        {
            throw new MPHP_exception( E_ERROR, "Database settings not found for : ".$database_config_name );
        }

        // Load config for this database
        $database_config = App()->config->database["list"][$database_config_name];

        // If config is empty or has errors
        if( self::Check_if_config_is_empty( $database_config, $database_config_name ) )
        {
            return FALSE;
        }

        return $database_config;
    }

    private static function Check_if_config_is_empty( & $database_config, $database_config_name )
    {
        if( empty( $database_config ) )
        {
            return TRUE;
        }

        if( empty( $database_config["hostname"] ) )
        {
            return TRUE;
        }

        if( empty( $database_config["username"] ) )
        {
            throw new MPHP_exception( E_ERROR, "Username not set for database config: ".$database_config_name );
        }

        if( empty( $database_config["driver"] ) )
        {
            throw new MPHP_exception( E_ERROR, "Database driver not set for database config: ".$database_config_name );
        }
    }
}

/* End of file Database_helper.php */
/* Location: ./core/Helpers/ */