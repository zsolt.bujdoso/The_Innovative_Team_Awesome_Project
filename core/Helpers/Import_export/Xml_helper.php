<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Xml_helper
 *
 * @version  1.0.0
 */
class Xml_helper
{
    public static $extension    = ".xml";
    public static $path         = BASEPATH."Runtime".DIRECTORY_SEPARATOR."exports/xmls/".DIRECTORY_SEPARATOR;
    public static $upload_path  = BASEPATH."upload".DIRECTORY_SEPARATOR."import_export/import/xmls".DIRECTORY_SEPARATOR;
    private static $data        = array();
    private static $headers     = array();
    private static $has_started = FALSE;

    /**
     * @method	Get
     * @access	public
     * @desc    Create a new CSV file with name
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Create_file( $file_name )
    {
        if( ! file_exists( self::$path ) || ! is_dir( self::$path ) )
        {
            mkdir( self::$path, 0755, TRUE );
            //chmod( self::$path, 0777 );
        }

        $content = '<?xml version="1.0" encoding="UTF-8" ?>'."\r\n<catalog>\r\n";

        file_put_contents( self::$path.$file_name.self::$extension, $content );

        self::$has_started = TRUE;
    }

    /**
     * @method	Add_data_to_file
     * @access	public
     * @desc    Create a new Add_data_to_file file with name
     * @author	Cousin Béla
     *
     * @param   array                       $data                       - array with data to insert into file
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_data_to_file( $data, $file_name = "" )
    {
        if( empty( self::$headers ) )
        {
            self::$headers = array_flip( $data );

            return;
        }

        $file       = fopen( self::$path.$file_name.self::$extension, "a" );
        $content    = "\t<item>\r\n";
        $content    .= self::Get_item_content( $data );
        $content    .= "\t</item>\r\n";

        fputs( $file, $content, strlen( $content ) );
        fclose( $file );
    }

    /**
     * @method	Get_item_content
     * @access	private
     * @desc    Getting item content add to file
     * @author	Cousin Béla
     *
     * @param   array                       $data                       - array with data to insert into file
     *
     * @version	1.0.0
     * @return  string
     */
    private static function Get_item_content( $data )
    {
        if( empty( $data ) )
        {
            return "";
        }

        $content = "";

        // Get all data from item by header names
        foreach( self::$headers as $header => $index )
        {
            $content .= "\t\t<".$header.">".$data[$index]."</".$header.">\r\n";
        }
        
        return $content;
    }

    /**
     * @method	Export_data_into_file_or_close_content
     * @access	public
     * @desc    Closing the catalog param in file
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Export_data_into_file_or_close_content( $file_name )
    {
        $file_path = self::$path.$file_name.self::$extension;

        if( ! self::$has_started || ! file_exists( $file_path ) )
        {
            return;
        }

        $file   = fopen( $file_path, "a" );
        $content= "</catalog>";

        fputs( $file, $content, strlen( $content ) );
        fclose( $file );
    }

    /**
     * @method	Get_headers_from_file
     * @access	public
     * @desc    Return headers as array
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to get
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Get_headers_from_file( $file_name )
    {
        if( ! self::Open_for_import( $file_name ) )
        {
            return array();
        }

        $headers_from_file  = ( ! empty( self::$data->item[0] ) ? self::$data->item[0] : array() );
        $headers            = array();

        foreach( $headers_from_file as $header => $value )
        {
            $headers[]= $header;
        }

        return $headers;
    }

    /**
     * @method	Open_for_import
     * @access	private
     * @desc    Create a new CSV file with name
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    private static function Open_for_import( $file_name )
    {
        if( ! empty( self::$data ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^(.*)".self::$extension."/", $file_name ) )
        {
            $file_name .= self::$extension;
        }

        if( ! file_exists( self::$upload_path.$file_name ) )
        {
            return FALSE;
        }

        $file_content   = self::Get_file_content( self::$upload_path.$file_name );
        self::$data     = simplexml_load_string( $file_content );

        return TRUE;
    }

    /**
     * @method	Get_file_content_for_import
     * @access	public
     * @desc    Return items as array
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to get
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Get_file_content_for_import( $file_name )
    {
        if( ! self::Open_for_import( $file_name ) )
        {
            return array();
        }

        if( empty( self::$data->item ) )
        {
            return;
        }

        $items = array();

        foreach( self::$data->item as $item_from_file )
        {
            $item = array();

            foreach( $item_from_file as $header => $value )
            {
                $item[$header] = current( $value );
            }

            $items []= $item;
        }

        return $items;
    }

    /**
     * @method	Get_file_content
     * @access	public
     * @desc    Get csv file content
     * @author	Cousin Béla
     *
     * @param   string                      $file_path                  - path and name of the file to get
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_file_content( $file_path )
    {
        if( ! file_exists( $file_path ) )
        {
            return "";
        }

        return file_get_contents( $file_path );
    }
}

/* End of file Xml_helper.php */
/* Location: ./Core/Helpers/Import_export/ */