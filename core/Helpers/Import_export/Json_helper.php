<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Json_helper
 *
 * @version  1.0.0
 */
class Json_helper
{
    public static $extension    = ".json";
    public static $path         = BASEPATH."Runtime".DIRECTORY_SEPARATOR."exports/jsons/".DIRECTORY_SEPARATOR;
    public static $upload_path  = BASEPATH."upload".DIRECTORY_SEPARATOR."import_export/import/jsons".DIRECTORY_SEPARATOR;
    private static $data        = array();
    private static $headers     = array();
    private static $has_started = FALSE;

    /**
     * @method	Get
     * @access	public
     * @desc    Create a new CSV file with name
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Create_file( $file_name )
    {
        if( ! file_exists( self::$path ) || ! is_dir( self::$path ) )
        {
            mkdir( self::$path, 0755, TRUE );
            //chmod( self::$path, 0777 );
        }

        $file = fopen( self::$path.$file_name.self::$extension, "w" );
        fclose( $file );

        self::$has_started = TRUE;
    }

    /**
     * @method	Add_data_to_file
     * @access	public
     * @desc    Create a new Add_data_to_file file with name
     * @author	Cousin Béla
     *
     * @param   array                       $data                       - array with data to insert into file
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_data_to_file( $data, $file_name = "" )
    {
        if( empty( self::$headers ) )
        {
            self::$headers = array_flip( $data );

            return;
        }

        $new_data = array();

        // Get all data from item by header names
        foreach( self::$headers as $header => $index )
        {
            $new_data[$header] = $data[$index];
        }

        self::$data []= $new_data;
    }

    /**
     * @method	Export_data_into_file_or_close_content
     * @access	public
     * @desc    Create a new Add_data_to_file file with name
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Export_data_into_file_or_close_content( $file_name )
    {
        $file_path = self::$path.$file_name.self::$extension;

        if( ! self::$has_started || ! file_exists( $file_path ) )
        {
            return;
        }

        file_put_contents( $file_path, json_encode( self::$data ) );
    }

    /**
     * @method	Get_headers_from_file
     * @access	public
     * @desc    Return headers as array
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to get
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Get_headers_from_file( $file_name )
    {
        if( ! self::Open_for_import( $file_name ) )
        {
            return array();
        }

        $headers_from_file  = ( ! empty( self::$data[0] ) ? self::$data[0] : array() );
        $headers            = array();

        foreach( $headers_from_file as $header => $value )
        {
            $headers[]= $header;
        }

        return $headers;
    }

    /**
     * @method	Open_for_import
     * @access	private
     * @desc    Create a new CSV file with name
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    private static function Open_for_import( $file_name )
    {
        if( ! empty( self::$data ) )
        {
            return TRUE;
        }

        if( ! preg_match( "/^(.*)".self::$extension."/", $file_name ) )
        {
            $file_name .= self::$extension;
        }

        if( ! file_exists( self::$upload_path.$file_name ) )
        {
            return FALSE;
        }

        $file_content   = self::Get_file_content( self::$upload_path.$file_name );
        self::$data     = json_decode( $file_content );

        return TRUE;
    }

    /**
     * @method	Get_file_content_for_import
     * @access	public
     * @desc    Return items as array
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to get
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Get_file_content_for_import( $file_name )
    {
        if( ! self::Open_for_import( $file_name ) )
        {
            return array();
        }

        if( empty( self::$data ) )
        {
            return array();
        }

        /*$items = array();

        foreach( self::$data as $item_from_file )
        {
            $item = array();

            foreach( $item_from_file as $header => $value )
            {
                $item []= current( $value );
            }

            $items []= $item;
        }*/

        return self::$data;
    }

    /**
     * @method	Get_file_content
     * @access	public
     * @desc    Get csv file content
     * @author	Cousin Béla
     *
     * @param   string                      $file_path                  - path and name of the file to get
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_file_content( $file_path )
    {
        if( ! file_exists( $file_path ) )
        {
            return "";
        }

        return file_get_contents( $file_path );
    }
}

/* End of file Json_helper.php */
/* Location: ./Core/Helpers/Import_export/ */