<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Csv_helper
 *
 * @version  1.0.0
 */
class Csv_helper
{
    public static $delimiter    = ";";
    public static $enclosure    = '"';
    public static $extension    = ".csv";
    private static $file        = null;
    public static $path         = BASEPATH."Runtime".DIRECTORY_SEPARATOR."exports/csvs/".DIRECTORY_SEPARATOR;
    public static $upload_path  = BASEPATH."upload".DIRECTORY_SEPARATOR."import_export/import/csvs".DIRECTORY_SEPARATOR;

    /**
     * @method	Get
     * @access	public
     * @desc    Create a new CSV file with name
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Create_file( $file_name )
    {
        if( ! file_exists( self::$path ) || ! is_dir( self::$path ) )
        {
            mkdir( self::$path, 0755, TRUE );
            //chmod( self::$path, 0777 );
        }

        self::Open_for_export( $file_name, "w" );
        fclose( self::$file );
    }

    /**
     * @method	Open
     * @access	private
     * @desc    Create a new CSV file with name
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to create
     * @param   string                      $mode                       - opening mode, w, a, b, etc.
     *
     * @version	1.0.0
     * @return  string
     */
    private static function Open_for_export( $file_name, $mode = "w" )
    {
        if( ! file_exists( self::$path ) || ! is_dir( self::$path ) )
        {
            mkdir( self::$path, 0755, TRUE );
            //chmod( self::$path, 0777 );
        }

        self::$file = fopen( self::$path.$file_name.self::$extension, $mode );
    }

    /**
     * @method	Add_data_to_file
     * @access	public
     * @desc    Create a new Add_data_to_file file with name
     * @author	Cousin Béla
     *
     * @param   array                       $data                       - array with data to insert into file
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Add_data_to_file( $data, $file_name = "" )
    {
        self::Open_for_export( $file_name, "a" );

        fputcsv( self::$file, $data, self::$delimiter, self::$enclosure );

        fclose( self::$file );
    }

    /**
     * @method	Export_data_into_file_or_close_content
     * @access	public
     * @desc    Closing the catalog param in file
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     */
    public static function Export_data_into_file_or_close_content( $file_name )
    {
        return;
    }

    /**
     * @method	Get_headers_from_file
     * @access	public
     * @desc    Return headers as array
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to get
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Get_headers_from_file( $file_name )
    {
        if( ! self::Open_for_import( $file_name ) )
        {
            return array();
        }

        $headers= fgetcsv( self::$file, 10000, self::$delimiter, self::$enclosure );

        fclose( self::$file );

        return $headers;
    }

    /**
     * @method	Open_for_import
     * @access	private
     * @desc    Create a new CSV file with name
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to create
     *
     * @version	1.0.0
     * @return  string
     */
    private static function Open_for_import( $file_name )
    {
        if( ! preg_match( "/^(.*)".self::$extension."/", $file_name ) )
        {
            $file_name .= self::$extension;
        }

        if( ! file_exists( self::$upload_path.$file_name ) )
        {
            return FALSE;
        }

        self::$file = fopen( self::$upload_path.$file_name, "r" );

        return TRUE;
    }

    /**
     * @method	Get_file_content_for_import
     * @access	public
     * @desc    Return items as array
     * @author	Cousin Béla
     *
     * @param   string                      $file_name                  - name of the file to get
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Get_file_content_for_import( $file_name )
    {
        if( ! self::Open_for_import( $file_name ) )
        {
            return array();
        }

        $items_from_file= array();
        $items          = array();
        $headers        = array();
        $i              = 0;

        while( ! feof( self::$file ) )
        {
            $items_from_file[$i] = fgetcsv( self::$file, 10000, self::$delimiter, self::$enclosure );

            if( ! $i )
            {
                $headers = $items_from_file[$i];
            }

            if( ! $i || empty( $items_from_file[$i] ) )
            {
                unset( $items_from_file[$i] );
            }

            ++$i;
        }

        fclose( self::$file );

        foreach( $items_from_file as $item_from_file )
        {
            $item = array();

            foreach( $headers as $index => $header )
            {
                $item[$header] = $item_from_file[$index];
            }

            $items []= $item;
        }

        return $items;
    }

    /**
     * @method	Get_file_content
     * @access	public
     * @desc    Get csv file content
     * @author	Cousin Béla
     *
     * @param   string                      $file_path                  - path and name of the file to get
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_file_content( $file_path )
    {
        if( ! file_exists( $file_path ) )
        {
            return "";
        }

        return file_get_contents( $file_path );
    }
}

/* End of file Csv_helper.php */
/* Location: ./Core/Helpers/Import_export/ */