<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Get_helper
 *
 * This class helps us to get back data after posted,
 * if it was not posted tries to get from object
 *
 * @version 1.0.0
 */
class Get_helper
{
    /**
     * @method	Is_set
     * @access	public
     * @desc    This method check an item in GET if is set, if not than check in object, if it is set
     * @author	Cousin Béla
     *
     * @param   string                      $name                       - the name of the item to check in POST or object
     * @param   mixed                       $object                     - the object where to get from
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Is_set( $name, & $object = null )
    {
        $get_data =& $_GET;

        if( isset( $get_data[$name] ) )
        {
            return TRUE;
        }

        if( is_array( $object ) && isset( $object[$name] ) )
        {
            return TRUE;
        }

        if( is_object( $object ) && isset( $object->$name ) )
        {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * @method	Get
     * @access	public
     * @desc    This method check an item in GET if were set, if not returns it from object, if it is set
     * @author	Cousin Béla
     *
     * @param   string                      $name                       - the name of the item to check in POST or object
     * @param   mixed                       $object                     - the object where to get from
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get( $name, & $object = null, $default_value = "" )
    {
        $get_data =& $_GET;

        if( isset( $get_data[$name] ) )
        {
            return $get_data[$name];
        }

        if( is_array( $object ) && isset( $object[$name] ) )
        {
            return $object[$name];
        }

        if( is_object( $object ) && isset( $object->$name ) )
        {
            return $object->$name;
        }

        return $default_value;
    }

    /**
     * @method	Get_without_quot
     * @access	public
     * @desc    This method check an item in GET if were set, if not returns it from object, if it is set
     * @author	Cousin Béla
     *
     * @param   string                      $name                       - the name of the item to check in POST or object
     * @param   mixed                       $object                     - the object where to get from
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_without_quot( $name, & $object = null, $default_value = "" )
    {
        return str_replace( '"', "&quot;", self::Get( $name, $object, $default_value ) );
    }

    /**
     * @method	Get_integer
     * @access	public
     * @desc    This method check an item in GET if were set, if not returns it from object, if it is set
     * @author	Cousin Béla
     *
     * @param   string                      $name                       - the name of the item to check in POST or object
     * @param   mixed                       $object                     - the object where to get from
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_integer( $name, & $object = null, $default_value = 0 )
    {
        $get_data =& $_GET;

        if( isset( $get_data[$name] ) )
        {
            return ( int ) $get_data[$name];
        }

        if( is_array( $object ) && isset( $object[$name] ) )
        {
            return ( int ) $object[$name];
        }

        if( is_object( $object ) && isset( $object->$name ) )
        {
            return ( int ) $object->$name;
        }

        return $default_value;
    }
}

/* End of file Get_helper.php */
/* Location: ./Core/Helpers/ */