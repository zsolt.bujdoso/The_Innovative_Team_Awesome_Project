<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Validation_helper
 *
 *
 * @version     1.0.0
 */
class Validation_helper
{
    /**
     * @method	Validate_ids
     * @access	public
     * @desc    This method validates the array of ids
     * @author	Csenteri Attila
     *
     * @param   array                       $ids_array                  - array with ids to validate
     * @param   boolean                     $is_required                - set the rules to "required" or not
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Validate_ids( $ids_array, $is_required = TRUE )
    {
        if( empty( $ids_array ) || ! is_array( $ids_array ) )
        {
            return TRUE;
        }

        foreach( $ids_array as $id )
        {
            $result = self::Validate_id( $id, $is_required );

            if( $result !== TRUE )
            {
                return $result;
            }
        }

        return TRUE;
    }

    /**
     * @method	Validate_id
     * @access	public
     * @desc    This method validates an id
     * @author	Cousin Bela
     *
     * @param   int                         $id                         - value of the primary key
     * @param   boolean                     $is_required                - set the rules to "required" or not
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Validate_id( $id, $is_required = TRUE )
    {
        $required   = ( ! empty( $is_required ) ? "required|" : "" );
        $validation = new MVariable_validation();
        
        $validation->Set_value( $id );
        $validation->Set_name( "ID" );
        $validation->Set_rules( $required . "numeric|max_length[20]" );

        if( ! $validation->Validate() )
        {
            return $validation->Get_error_messages();
        }

        return TRUE;
    }

    /**
     * @method	Validate_name
     * @access	public
     * @desc    This method validates a name
     * @author	Cousin Bela
     *
     * @param   string                      $name                       - the name to validate
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Validate_name( $name )
    {
        $validation = new MVariable_validation();

        $validation->Set_value( $name );
        $validation->Set_name( App()->lang->Get( "Name" ) );
        $validation->Set_rules( "required|xss_clean|title|max_length[150]" );
        
        if( ! $validation->Validate() )
        {
            return $validation->Get_error_messages();
        }

        return TRUE;
    }

    /**
     * @method	Validate_code
     * @access	public
     * @desc    This method validates a code
     * @author	Cousin Bela
     *
     * @param   string                      $code                       - the code to validate
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Validate_code( $code )
    {
        $validation = new MVariable_validation();

        $validation->Set_value( $code );
        $validation->Set_name( App()->lang->Get( "Code" ) );
        $validation->Set_rules( "required|xss_clean|alpha_dash|max_length[150]" );

        if( ! $validation->Validate() )
        {
            return $validation->Get_error_messages();
        }

        return TRUE;
    }

    /**
     * @method	Validate_email
     * @access	public
     * @desc    This method validates an email address
     * @author	Cousin Bela
     *
     * @param   string                      $email                      - the email address to validate
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Validate_email( $email )
    {
        $rules = "required|email|max_length[150]";

        return self::Validate_variable( $email, App()->lang->Get( "Email_address" ), $rules );
    }

    /**
     * @method	Validate_variable
     * @access	public
     * @desc    This method validates a variable
     * @author	Cousin Bela
     *
     * @param   string                      $value                      - the value to validate
     * @param   string                      $name                       - name of the variable
     * @param   string                      $validation_string          - the validation string
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Validate_variable( $value, $name, $validation_string )
    {
        $validation = new MVariable_validation();

        $validation->Set_value( $value );
        $validation->Set_name( $name );
        $validation->Set_rules( $validation_string );

        if( ! $validation->Validate() )
        {
            return $validation->Get_error_messages();
        }

        return TRUE;
    }
}

/* End of file Validation_helper.php */
/* Location: ./Core/Helpers/ */