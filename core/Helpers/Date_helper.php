<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Date_helper
 *
 * @version  1.0.0
 */
class Date_helper
{
    public static $date_format       = "Y-m-d";
    public static $iso_date_format   = "Y-m-d\TH:i:s";
    public static $time_format       = "H:i:s";
    public static $date_time_format  = "Y-m-d H:i:s";

    /**
     * @method	Show_date
     * @access	public static
     * @desc    This method shows a date in a format by language
     * @author	Cousin Béla
     *
     * @param   string                      $date                       - the date to reformat
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Show_date( & $date )
    {
        if( empty( $date ) || ! strtotime( $date ) )
        {
            return "";
        }

        $language_details   = App()->lang->Get_language_details();
        $format             = ( ! empty( $language_details["date_format"] ) ? $language_details["date_format"] : "Y-m-d" );

        return date( $format, strtotime( $date ) );
    }

    /**
     * @method	Show_date_time
     * @access	public static
     * @desc    This method shows a date and time in a format by language
     * @author	Cousin Béla
     *
     * @param   string                      $date                       - the date to reformat
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Show_date_time( & $date )
    {
        if( empty( $date ) || ! strtotime( $date ) )
        {
            return "";
        }

        $language_details   = App()->lang->Get_language_details();
        $format             = ( ! empty( $language_details["date_format"] ) ? $language_details["date_format"] : self::$date_format );
        $format             .= " " . self::$time_format;

        return date( $format, strtotime( $date ) );
    }

    /**
     * @method	Get_date_time_by_timestamp
     * @access	public static
     * @desc    This method shows a date and time in a format by language
     * @author	Cousin Béla
     *
     * @param   string                      $timestamp                  - the date to reformat
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_date_time_by_timestamp( & $timestamp )
    {
        if( empty( $timestamp ) )
        {
            return "";
        }

        return date( self::$date_time_format, $timestamp );
    }

    /**
     * @method	Get_empty_date_time
     * @access	public static
     * @desc    This method shows an empty date and time in a format
     * @author	Csenteri Attila
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_empty_date_time()
    {
        return "0000-00-00 00:00:00";
    }

    /**
     * @method	Show_date_by_item_language
     * @access	public static
     * @desc    This method shows a date in a format by item language
     * @author	Cousin Béla
     *
     * @param   string                      $date                       - the date to reformat
     * @param   object                      $object                     - item with language code
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Show_date_by_item_language( & $date, & $object )
    {
        if( empty( $date ) || ! strtotime( $date ) )
        {
            return "";
        }

        $language_code      = ( ! empty( $object->language_code ) ? $object->language_code : "" );
        $language_details   = App()->lang->Get_language_details_by_code( $language_code );
        $format             = ( ! empty( $language_details["date_format"] ) ? $language_details["date_format"] : self::$date_format );

        return date( $format, strtotime( $date ) );
    }

    /**
     * @method	Show_date_time_by_item_language
     * @access	public static
     * @desc    This method shows a date and time in a format by item language
     * @author	Cousin Béla
     *
     * @param   string                      $date                       - the date to reformat
     * @param   object                      $object                     - item with language code
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Show_date_time_by_item_language( & $date, & $object )
    {
        if( empty( $date ) || ! strtotime( $date ) )
        {
            return "";
        }

        $language_code      = ( ! empty( $object->language_code ) ? $object->language_code : "" );
        $language_details   = App()->lang->Get_language_details_by_code( $language_code );
        $format             = ( ! empty( $language_details["date_format"] ) ? $language_details["date_format"] : self::$date_format );
        $format             .= " " . self::$time_format;

        return date( $format, strtotime( $date ) );
    }

    /**
     * @method	Show_date_time_by_full_text_format
     * @access	public static
     * @desc    This method shows a date and time in a format by item language
     * @author	Cousin Béla
     *
     * @param   string                      $date                       - the date to reformat
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Show_date_by_full_text_format( & $date )
    {
        if( empty( $date ) || ! strtotime( $date ) )
        {
            return "";
        }

        $date_format        = "F d, Y";
        $language_details   = App()->lang->Get_language_details();

        if( ! empty( $language_details["date_format"] ) )
        {
            $language_details["date_format"] = "F d, Y";
        }

        $format = ( ! empty( $language_details["date_format"] ) ? $language_details["date_format"] : $date_format );

        return date( $format, strtotime( $date ) );
    }

    /**
     * @method	Show_date_time_by_full_text_format
     * @access	public static
     * @desc    This method shows a date and time in a format by item language
     * @author	Cousin Béla
     *
     * @param   string                      $date                       - the date to reformat
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Show_date_time_by_full_text_format( & $date )
    {
        if( empty( $date ) || ! strtotime( $date ) )
        {
            return "";
        }

        $date_format        = "F d, Y";
        $language_details   = App()->lang->Get_language_details();

        if( ! empty( $language_details["date_format"] ) )
        {
            $language_details["date_format"] = "F d, Y";
        }

        $format             = ( ! empty( $language_details["date_format"] ) ? $language_details["date_format"] : $date_format );
        $time_format        = self::$time_format;
        $date_time_format   = "<span>".date( $format, strtotime( $date ) )."</span>".date( $time_format, strtotime( $date ) );

        return $date_time_format;
    }

    /**
     * @method	Show_date_by_month_and_day
     * @access	public static
     * @desc    This method shows a date by month and day
     * @author	Cousin Béla
     *
     * @param   string                      $date                       - the date to reformat
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Show_date_by_month_and_day( & $date )
    {
        if( empty( $date ) || ! strtotime( $date ) )
        {
            return "";
        }

        $date_format        = "d F";
        $language_details   = App()->lang->Get_language_details();

        if( ! empty( $language_details["date_format"] ) )
        {
            $language_details["date_format"] = "d F";
        }

        $format = ( ! empty( $language_details["date_format"] ) ? $language_details["date_format"] : $date_format );

        return date( $format, strtotime( $date ) );
    }

    /**
     * @method	Get_date_by_language
     * @access	public static
     * @desc    This method returns the actual date in format depends on language
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_date_by_language()
    {
        $language_details   = App()->lang->Get_language_details();
        $format             = ( ! empty( $language_details["date_format"] ) ? $language_details["date_format"] : self::$date_format );

        return date( $format );
    }

    /**
     * @method	Get_date_time_by_language
     * @access	public static
     * @desc    This method returns the actual date time in format depends on language
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_date_time_by_language()
    {
        $language_details   = App()->lang->Get_language_details();
        $format             = ( ! empty( $language_details["date_format"] ) ? $language_details["date_format"] : self::$date_format );

        return date( $format . " " . self::$time_format );
    }

    /**
     * @method	Get_date
     * @access	public static
     * @desc    This method returns the actual date in CET format
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_date()
    {
        return date( self::$date_format );
    }

    /**
     * @method	Get_date
     * @access	public static
     * @desc    This method sets the modify date if the id of the item exists, otherwise sets to empty date
     * @author	Csenteri Attila
     *
     * @param   int                         $item_id                    - id of the item to set the modify date
     *                                                                  
     * @version	1.0.0
     * @return  string
     */
    public static function Get_modify_date( $item_id = 0 )
    {
        return ( ! empty( $item_id ) ? self::Get_date_time() : self::Get_empty_date_time() );
    }

    /**
     * @method	Get_date_time
     * @access	public static
     * @desc    This method returns the actual date in CET format
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_date_time()
    {
        return date( self::$date_time_format );
    }

    /**
     * @method	Get_year
     * @access	public static
     * @desc    This method returns the actual year
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_year()
    {
        return date( "Y" );
    }

    /**
     * @method	Get_month
     * @access	public static
     * @desc    This method returns the actual month
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_month()
    {
        return date( "m" );
    }

    /**
     * @method	Get_month_without_zeros
     * @access	public static
     * @desc    This method returns the actual month without zeros, 1, 2, 3, ... not 01, 02, 03 ...
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_month_without_zeros()
    {
        return date( "n" );
    }

    /**
     * @method	Get_day
     * @access	public static
     * @desc    This method returns the actual day
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_day()
    {
        return date( "d" );
    }

    /**
     * @method	Get_day_number
     * @access	public static
     * @desc    This method returns the actual day number
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_day_number()
    {
        return date( "N" );
    }

    /**
     * @method	Get_hour
     * @access	public static
     * @desc    This method returns the actual Hour
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_hour()
    {
        return date( "H" );
    }

    /**
     * @method	Get_hour_without_zeros
     * @access	public static
     * @desc    This method returns the actual Hour without zeros
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_hour_without_zeros()
    {
        return date( "G" );
    }

    /**
     * @method	Get_minute
     * @access	public static
     * @desc    This method returns the actual Minute
     * @author	Cousin Béla
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_minute()
    {
        return date( "i" );
    }

    /**
     * @method	Is_between_two_dates
     * @access	public static
     * @desc    This method returns true if the current date is between given two dates
     * @author	Csenteri Attila
     *
     * @param   string                      $date_start                 - the start date
     * @param   string                      $date_finish                - the finish date
     *                                                                  
     * @version	1.0.0
     * @return  string
     */
    public static function Is_between_two_dates( $date_start, $date_finish )
    {
        $date_start     = strtotime( $date_start );
        $date_finish    = strtotime( $date_finish );
       
        if( empty( $date_start ) && ( $date_finish && time() > $date_finish ) )
        {
            return FALSE;
        }
        elseif( ( $date_start && time() < $date_start ) && empty( $date_finish ) )
        {
            return FALSE;
        }
        elseif( ( $date_start && time() < $date_start ) || ( $date_finish && time() > $date_finish ) )
        {
            return FALSE;
        }
        
        return TRUE;
    }

    /**
     * @method	Get_previuos_month
     * @access	public static
     * @desc    This method returns the previous month's number
     * @author	Csenteri Attila
     *
     * @version	1.0.0
     * @return  int
     */
    public static function Get_previuos_month()
    {
        $this_month = self::Get_month();
        
        return ( $this_month == 1 ? 12 : $this_month - 1 );
    }

    /**
     * @method	Get_date_time_in_format
     * @access	public static
     * @desc    This method creates a date from a string
     * @author	Csenteri Attila
     *
     * @version	1.0.0
     * @return  datetime
     */
    public static function Get_date_time_in_format( $date )
    {
        $date   = date_create( $date );
        $format = self::$date_time_format;
        
        return $date->format( $format );
    }
    
    /**
     * @method	Get_date_in_format
     * @access	public static
     * @desc    This method creates a date from a string
     * @author	Csenteri Attila
     *
     * @version	1.0.0
     * @return  datetime
     */
    public static function Get_date_in_format( $date )
    {
        $date   = date_create( $date );
        $format = self::$date_format;
        
        return $date->format( $format );
    }

    /**
     * @method	Get_date_in_iso_format
     * @access	public static
     * @desc    This method creates a date from a string
     * @author	Csenteri Attila
     *
     * @version	1.0.0
     * @return  datetime
     */
    public static function Get_date_in_iso_format( $date )
    {
        $date   = date_create( $date );
        $format = self::$iso_date_format;

        return $date->format( $format );
    }
}

/* End of file Date_helper.php */
/* Location: ./Core/Helpers/ */