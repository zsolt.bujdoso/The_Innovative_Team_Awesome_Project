<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Server_helper
 *
 * This class helps us to get data from SERVER
 *
 * @version 1.0.0
 */
class Server_helper
{
    /**
     * @method	Get
     * @access	public
     * @desc    This method check an item in SERVER if were set, if not returns it from object, if it is set
     * @author	Cousin Béla
     *
     * @param   string                      $name                       - the name of the item to check in SERVER or object
     * @param   mixed                       $default_value              - default value if not exists
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get( $name, $default_value = "" )
    {
        $name = strtoupper( $name );

        if( empty( $_SERVER[$name] ) )
        {
            return $default_value;
        }

        $value =& $_SERVER[$name];

        return $value;
    }

    /**
     * @method	Get_visitor_ip
     * @access	public
     * @desc    This method returns the ip address of visitor (client)
     * @author	CopyPaste (Attila)
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_visitor_ip()
    {
        if( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) )
        {
            return $_SERVER['HTTP_CLIENT_IP'];
        }

        if( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) )
        {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        if( ! empty( $_SERVER['HTTP_X_FORWARDED'] ) )
        {
            return $_SERVER['HTTP_X_FORWARDED'];
        }

        if( ! empty( $_SERVER['HTTP_FORWARDED_FOR'] ) )
        {
            return $_SERVER['HTTP_FORWARDED_FOR'];
        }

        if( ! empty( $_SERVER['HTTP_FORWARDED'] ) )
        {
            return $_SERVER['HTTP_FORWARDED'];
        }

        if( ! empty( $_SERVER['REMOTE_ADDR'] ) )
        {
            return $_SERVER['REMOTE_ADDR'];
        }

        return "Unknown";
    }

    /**
     * @method	Get_ip
     * @access	public
     * @desc    This method returns the ip address if actual machine
     * @author	Cousin Bela
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_ip()
    {
        if( ! empty( $_SERVER['SERVER_ADDR'] ) )
        {
            return $_SERVER['SERVER_ADDR'];
        }

        return "Unknown";
    }

    /**
     * @method	Get_visitor_agent
     * @access	public
     * @desc    This method returns the browser agent you use
     * @author	Csenteri Attila
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_visitor_agent()
    {
        return self::Get( "HTTP_USER_AGENT" );
    }

    /**
     * @method	Get_last_visited_url
     * @access	public
     * @desc    This method returns the last visited url, where we come from
     * @author	Csenteri Attila
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_last_visited_url()
    {
        return self::Get( "HTTP_REFERER" );
    }
}

/* End of file Server_helper.php */
/* Location: ./Core/Helpers/ */