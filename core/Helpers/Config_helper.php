<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Config_helper
 *
 * @version  1.0.0
 */
class Config_helper
{
    /**
     * @method	Get
     * @access	public
     * @desc    Get config by name
     * @author	Cousin Béla
     *
     * @param   string                      $date                       - the date to reformat
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get( $name )
    {
        if( ! App()->config->Has( $name ) )
        {
            return "";
        }

        return App()->config->$name;
    }
}

/* End of file Config_helper.php */
/* Location: ./Core/Helpers/ */