<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Array_helper
 *
 * @version     1.0.0
 */
class Array_helper
{
    public static function Get_status_by_code( $code )
    {
        $status_codes = array(
            "200"   => "HTTP/1.1 200 OK",
            "301"   => "HTTP/1.1 301 Moved Permanently",
            "302"   => "HTTP/1.1 302 Moved Permanently",
            "404"   => "HTTP/1.1 404 Not Found",
            "403"   => "HTTP/1.1 403 Forbidden",
            "500"   => "HTTP/1.1 500 Internal Server Error",
        );

        if( ! array_key_exists( $code, $status_codes ) )
        {
            return $status_codes[200];
        }

        return $status_codes[$code];
    }

    public static function Get_meta_robots()
    {
        $robots = array(
            "index, follow"     => "index, follow",
            "noindex, follow"   => "noindex, follow",
            "index, nofollow"   => "index, nofollow",
            "noindex, nofollow" => "noindex, nofollow"
        );

        return $robots;
    }

    public static function Get_messages_from_errors_array( $errors )
    {
        $result = "";

        if( ! is_array( $errors ) )
        {
            return $result;
        }

        return implode( "<br />", $errors );
    }

    public static function Get_logical_array( $add_empty_at_first_position = true )
    {
        $result = array();

        if( $add_empty_at_first_position )
        {
            $result[""] = "";
        }

        $result += array(
            "1" => App()->lang->Get( "Yes" ),
            "0" => App()->lang->Get( "No" ),
        );

        return $result;
    }

    public static function Get_has_or_not_array( $add_empty_at_first_position = true )
    {
        $result = array();

        if( $add_empty_at_first_position )
        {
            $result[""] = "";
        }

        $result += array(
            "1" => App()->lang->Get( "Has" ),
            "0" => App()->lang->Get( "Has_not" ),
        );

        return $result;
    }

    public static function Get_months()
    {
        $result = array(
            "1" => App()->lang->Get( "January" ),
            "2" => App()->lang->Get( "February" ),
            "3" => App()->lang->Get( "March" ),
            "4" => App()->lang->Get( "April" ),
            "5" => App()->lang->Get( "May" ),
            "6" => App()->lang->Get( "June" ),
            "7" => App()->lang->Get( "July" ),
            "8" => App()->lang->Get( "August" ),
            "9" => App()->lang->Get( "September" ),
            "10" => App()->lang->Get( "October" ),
            "11" => App()->lang->Get( "November" ),
            "12" => App()->lang->Get( "December" ),
        );

        return $result;
    }

    public static function Get_days()
    {
        $result = array(
            "1" => App()->lang->Get( "Monday" ),
            "2" => App()->lang->Get( "Tuesday" ),
            "3" => App()->lang->Get( "Wednesday" ),
            "4" => App()->lang->Get( "Thursday" ),
            "5" => App()->lang->Get( "Friday" ),
            "6" => App()->lang->Get( "Saturday" ),
            "0" => App()->lang->Get( "Sunday" ),
        );

        return $result;
    }

    public static function Get_hours()
    {
        $result = array();

        for( $i=0; $i<24; ++$i )
        {
            $result[$i] = $i;
        }

        return $result;
    }

    public static function Get_minutes()
    {
        $result = array();

        for( $i=0; $i<60; ++$i )
        {
            $result[$i] = $i;
        }

        return $result;
    }

    /**
     * @method	Get_visible_for_array
     * @access	public static
     * @desc    This method returns an array with visible for options, listed on admin side
     * @author	Csenteri Attila
     *
     * @version	1.0.0
     * @return  array
     */
    public static function Get_visible_for_array()
    {
        return array(
            ""          => App()->lang->Get( "All_visitor" ),
            "logged_in" => App()->lang->Get( "Who_is_logged_in" ),
            "logged_out"=> App()->lang->Get( "Who_is_logged_out" ),
        );
    }
    /**
     * @method	Get_browser_platforms_array
     * @access	public static
     * @desc    this method create an array with browser platforms and return it
     * @author	Zoltan Jozsa
     *
     * @version	1.0
     * @return	array
     */
    public static function Get_browser_platforms_array()
    {
        return array(
            "Mobile Windows"    => array( "IEMobile", "Windows Phone" ),
            "Mobile Android"    => "android",
            "Mobile Iphone"     => "iphone",
            "Mobile Symbian"    => array(
                "symbian", "symbos", "mobil", "mobi", "phone"
                                ),
            "Firefox"           => "Firefox",
            "Chrome"            => "Chrome",
            "Opera"             => "opera",
            "IExplorer"         => "MSIE",
            "Safari"            => "Safari",
            "Mac"               => "Apple",
        );
    }
}

/* End of file Array_helper.php */
/* Location: ./core/Helpers/ */