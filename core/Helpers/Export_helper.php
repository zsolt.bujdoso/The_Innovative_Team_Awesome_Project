<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2015 EasyBoard GMBH
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Export_helper
 *
 * This class helps us to get Exports from DB
 *
 * @version 1.0.0
 */
class Export_helper
{
    /**
     * @method	Create_new_file
     * @access	public static
     * @desc    Get Export details for selected Export by type and id where language is equal to actual language
     * @author	Cousin Béla
     *
     * @param   string                      $file                           - the name of file to Export
     *
     * @version	1.0.0
     */
    public static function Create_new_file( $file )
    {
        if( ! file_exists( dirname( $file ) ) )
        {
            Directory_helper::Create_directories_recursively_for_file( $file, 0777 );
        }

        $file_handle = fopen( $file, "w" );
        fclose( $file_handle );

        if( file_exists( $file ) )
        {
            //chmod( $file, 0777 );
        }

    }

    /**
     * @method	Add_row_to_csv
     * @access	public static
     * @desc    Get Export details for selected Export by type and id where language is equal to actual language
     * @author	Cousin Béla
     *
     * @param   string                      $file                           - the name of file to Export
     * @param   array                       $data                           - data to insert into file
     * @param   string                      $delimiter                      - delimiter to separate columns
     *
     * @version	1.0.0
     * @return  boolean
     */
    public static function Add_row_to_csv( $file, $data, $delimiter = ";" )
    {
        if( ! file_exists( $file ) )
        {
            return FALSE;
        }

        $file_csv       = fopen( $file, "a" );

        if( empty( $delimiter ) )
        {
            $delimiter = Config_helper::Get( "csv_columns_delimiter" );
        }

        fputcsv( $file_csv, $data, $delimiter );
        fclose( $file_csv );

        return TRUE;
    }
}

/* End of file Export_helper.php */
/* Location: ./Core/Helpers/ */