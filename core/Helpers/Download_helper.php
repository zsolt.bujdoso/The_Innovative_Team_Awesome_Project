<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Download_helper
 *
 * This class help us to set messages into session or session flash
 *
 * @version 1.0.0
 */
class Download_helper
{
    /**
     * @method	Download_file
     * @access	public
     * @desc    This method tries to download a file forcely
     * @author	Cousin Béla
     *
     * @param   string                      $path                       - the path to selected file
     * @param   string                      $file_name                  - the selected file name
     * @param   string                      $original_file_name         - the filename to download on
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Download_file( $path, $file_name, $original_file_name = "" )
    {
        $path       = rtrim( $path, "\\" );
        $path       = rtrim( $path, "/" );
        $fullpath   = $path.DIRECTORY_SEPARATOR.$file_name;

        if( ! empty( $original_file_name ) )
        {
            $file_name = $original_file_name;
        }

        if( ! file_exists( $fullpath ) )
        {
            Message_helper::Add_error_flash_message( App()->lang->Get( "File_not_found" ) );
            App()->controller->redirect->To_url( Server_helper::Get( "HTTP_REFERER" ) );
        }

        header( 'Content-Type: application/octet-stream' );
        header( 'Content-Transfer-Encoding: Binary' );
        header( 'Content-disposition: attachment; filename="'.$file_name.'"');
        readfile( $fullpath );

        App()->Finish();
    }
}

/* End of file Download_helper.php */
/* Location: ./Core/Helpers/ */