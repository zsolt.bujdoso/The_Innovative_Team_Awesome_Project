<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Url_helper
 *
 * This class has functions to get the active url, or base url, or other url helper functions
 *
 * @author  Cousin Bela
 * @version 1.0
 */
class Url_helper
{
    /**
     * @method  Base_url
     * @access  public static
     * @desc    This method return the base url for the site where we are, ex: http://tizbani.no-ip.org/framework/project/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Base_url()
    {
        $path       = dirname( $_SERVER["SCRIPT_NAME"] );
        $base_url   = self::Get_protocol();
        $base_url   .= $_SERVER["HTTP_HOST"];
        $base_url   .= "/".( strlen( $path ) > 1 ? trim( $path, "/" )."/" : "" );
        $base_url   = str_replace( DIRECTORY_SEPARATOR, "/", $base_url );

        return $base_url;
    }

    /**
     * @method  Get_protocol
     * @access  public static
     * @desc    This method returns the protocol used for system
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Get_protocol()
    {
        $port   = $_SERVER["SERVER_PORT"];
        $url    = "http";
        $url    .= ( self::Is_https() ? "s" : "" );
        //$url .= ( $port != 80 ? ":".$port : "" );
        $url    .= "://";

        return $url;
    }

    /**
     * @method  Is_https
     * @access  public static
     * @desc    This method checks if the system use https or not
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Is_https()
    {
        return ( ! empty( $_SERVER["HTTPS"] ) && $_SERVER["HTTPS"] == "on" ? TRUE : FALSE );
    }

    /**
     * @method  Site_url
     * @access  public static
     * @desc    This method return the active url for the site where we are,
     *          ex: http://tizbani.no-ip.org/framework/project/index.php/page/contact
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Site_url()
    {
        $path       = dirname( $_SERVER["SCRIPT_NAME"] );
        $site_url   = self::Get_protocol();
        $site_url   .= $_SERVER["HTTP_HOST"];
        $site_url   .= "/".( strlen( $path ) > 1 ? trim( $path, "/" )."/" : "" );
        $site_url   = str_replace( DIRECTORY_SEPARATOR, "/", $site_url );
        $site_url   .= trim( $site_url, "/" )."/";
        $site_url   .= App()->lang->Get_language_code()."/";

        return $site_url;
    }

    /**
     * @method  Theme_url
     * @access  public static
     * @desc    This method return the active theme url for the site where we are,
     *          ex: http://tizbani.no-ip.org/framework/project/themes/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Theme_url()
    {
        $site_url   = self::Get_protocol();
        $site_url   .= $_SERVER["HTTP_HOST"];

        $site_url   .= dirname( $_SERVER["SCRIPT_NAME"] ).DIRECTORY_SEPARATOR;
        $site_url   .= App()->theme->Get_path_for_url();
        $site_url   = str_replace( DIRECTORY_SEPARATOR, "/", $site_url );

        return $site_url;
    }

    /**
     * @method  Theme_image_url
     * @access  public static
     * @desc    This method return the image url from active theme url for the site where we are,
     *          ex: http://tizbani.no-ip.org/framework/project/themes/test/assets/images/static/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Theme_image_url()
    {
        return self::Theme_url()."assets/images/static/";
    }

    /**
     * @method  Active_module_url
     * @access  public static
     * @desc    This method return the active module url
     *          ex: http://tizbani.no-ip.org/framework/project/admin/products/
     * @author  Cousin Bela
     *
     * @version 1.0
     * @return  string
     */
    public static function Active_module_url()
    {
        return self::Base_url().App()->lang->Get_language_code()."/".App()->router->Get_module()."/";
    }
}

/* End of file Url_helper.php */
/* Location: ./core/Helpers/ */