<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Number_helper
 *
 * This class helps us to get back data after posted,
 * if it was not posted tries to get from object
 *
 * @version 1.0.0
 */
class Number_helper
{
    /**
     * @method	Show_price
     * @access	public
     * @desc    This method returns price in a specified format
     * @author	Cousin Béla
     *
     * @param   string                      $price                      - the price to reformat
     * @param   mixed                       $decimals                   - how many decimals will has
     * @param   mixed                       $thousands_separator        - which character will be for thousand separator
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Show_price( $price, $decimals = null, $thousands_separator = "," )
    {
        if( is_null( $decimals ) )
        {
            $decimals = App()->config->decimals_for_prices;
        }

        return "<b>".number_format( $price, $decimals, ".", $thousands_separator )."</b>";
    }

    /**
     * @method	Show_price_with_currency
     * @access	public
     * @desc    This method returns price in a specified format
     * @author	Cousin Béla
     *
     * @param   string                      $price                      - the price to reformat
     * @param   object                      $currency                   - the currency object
     * @param   mixed                       $decimals                   - how many decimals will has
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Show_price_with_currency( $price, & $currency, $decimals = null )
    {
        if( empty( $decimals ) )
        {
            $decimals = Variable_helper::Has_value( $currency, "nr_decimals" );
        }

        $price_value    = number_format( $price, $decimals );
        $currency_sign  = Variable_helper::Has_value( $currency, "sign" );

        if( Variable_helper::Has_value( $currency, "show_before_price", FALSE ) )
        {
            return $currency_sign."<b>".$price_value."</b>";
        }

        return "<b>".$price_value."</b> ".$currency_sign;
    }

    /**
     * @method	Get_price_in_format
     * @access	public
     * @desc    This method returns price in a specified format
     * @author	Cousin Béla
     *
     * @param   string                      $price                      - the price to reformat
     * @param   mixed                       $decimals                   - how many decimals will has
     * @param   mixed                       $thousands_separator        - which character will be for thousand separator
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_price_in_format( $price, $decimals = null, $thousands_separator = "," )
    {
        if( is_null( $decimals ) )
        {
            $decimals = App()->config->decimals_for_prices;
        }
        
        if( empty( $thousands_separator ) )
        {
            return floatval( number_format( $price, $decimals, ".", $thousands_separator ) );
        }

        return number_format( $price, $decimals, ".", $thousands_separator );
    }

    /**
     * @method	Get_letters_from_number
     * @access	public
     * @desc    This method returns the letter by given number (Ex. A-Z, AA, AB, ....)
     * @author	Csenteri Attila
     *
     * @param   int                         $number                     - the number to be converted
     *
     * @version	1.0.0
     * @return  string
     */
    public static function Get_letters_from_number( $number )
    {
        $numeric    = ( $number - 1 ) % 26;
        $letter     = chr( 65 + $numeric );
        $num2       = intval( ( $number - 1 ) / 26 );
        
        if( $num2 > 0 )
        {
            return self::Get_letter_from_number( $num2 ) . $letter;
        } 
        
        return $letter;
    }
}

/* End of file Number_helper.php */
/* Location: ./Core/Helpers/ */