<?php if( ! defined( "BASEPATH" ) ) die( "Direct call not allowed" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MYSE - Make your site easy
//  Copyright 2014 SakerSoft
//  All Rights Reserved.
//
//	This software is a property of SakerSoft. Any redistribution or
//	reproduction of part or all of the contents in any form is prohibited.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Class Post_helper
 *
 * This class helps us to get back data after posted,
 * if it was not posted tries to get from object
 *
 * @version 1.0.0
 */
class Input_helper
{
    /**
     * @method	Is_selected_from_post
     * @access	public static
     * @desc    This method check if an item was selected in a select or if not exists check if we have to select or not
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - the name of the item to check in POST or object
     * @param   string                      $value                      - the value to check
     * @param   string                      $default_selected           - default value to select
     *
     * @version	1.0.0
     * @return  bool
     */
    public static function Is_selected_from_POST( $item_name, & $value = null, $default_selected = "" )
    {
        if( Post_helper::Is_set( $item_name ) )
        {
            $field_value = Post_helper::Get( $item_name );

            if( $field_value == $value )
            {
                return TRUE;
            }
        }
        // needed when value = 0 or default value = 0
        elseif( ! empty( $value ) && ! empty( $default_selected ) && $value == $default_selected )
        {
            return TRUE;
        }
        elseif( $value === $default_selected )
        {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * @method	Is_selected_from_get
     * @access	public static
     * @desc    This method check if an item was selected in a select or if not exists check if we have to select or not
     * @author	Cousin Béla
     *
     * @param   string                      $item_name                  - the name of the item to check in POST or object
     * @param   string                      $value                      - the value to check
     * @param   string                      $default_selected           - default value to select
     *
     * @version	1.0.0
     * @return  bool
     */
    public static function Is_selected_from_GET( $item_name, & $value = null, $default_selected = "" )
    {
        if( Get_helper::Is_set( $item_name ) )
        {
            $field_value = Get_helper::Get( $item_name );

            if( $field_value == $value )
            {
                return TRUE;
            }
        }
        // needed when value = 0 or default value = 0
        elseif( ! empty( $value ) && ! empty( $default_selected ) && $value == $default_selected )
        {
            return TRUE;
        }
        elseif( $value === $default_selected )
        {
            return TRUE;
        }

        return FALSE;
    }
}

/* End of file Post_helper.php */
/* Location: ./Core/Helpers/ */